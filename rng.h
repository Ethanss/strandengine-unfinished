﻿/*Copyright 2018 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _RNG_H_
#define _RNG_H_

#include "endianness.h"
#include <stdbool.h>
#include <stdlib.h>

typedef struct {
	void *buffer;
	uint32_t length;
} rng;

#define new_rng(s) (rng){malloc(s),s}
#define delete_rng(r) free((r)->buffer)

//random unsigned integer from 0-uint_max
static inline uint32_t rng_uint32(rng *seed) {
	uint8_t *sp = seed->buffer, *sv = sp;
	uint32_t sl = seed->length, v = 0x6A7F8FAA;
	for (uint32_t i = 0; i < sl; i++) {
		v = (v^(uint32_t)*sv)*0x01000193;
		*sv++ += (sp[sl-i-1]>>(i%6))+i*27;
	}
	return v;
}

//fill dst with random bytes
void rng_bytes(rng *seed, uint8_t *dst, const uint32_t count) {
	bool swe = ENDIANNESS != LITTLEENDIAN;
	uint32_t rv, rl = 4;
	uint8_t *rvb = &rv;
	for (uint32_t i = 0; i < count; i++) {
		rl++;
		if (rl > 3) {
			rv = rng_uint32(seed);
			if (swe) reverse(rvb, 4);
			rl = 0;
		}
		dst[i] = rvb[rl];
	}
}

//xor dst with random bytes
void rng_xor(rng *seed, uint8_t *dst, const uint32_t count) {
	bool swe = ENDIANNESS != LITTLEENDIAN;
	uint32_t rv, rl = 4;
	uint8_t *rvb = &rv;
	for (uint32_t i = 0; i < count; i++) {
		rl++;
		if (rl > 3) {
			rv = rng_uint32(seed);
			if (swe) reverse(rvb, 4);
			rl = 0;
		}
		dst[i] ^= rvb[rl];
	}
}


//random 32bit signed integer within range
static inline int32_t rng_int32(rng *seed, const int32_t min, const int32_t max) {
	return min+(int32_t)(rng_uint32(seed)%(uint32_t)(max-min));
}
static inline void rng_int32s(rng *seed, const int32_t min, const int32_t max, int32_t *a, const uint32_t count) {
	for (uint32_t i = 0; i < count; i++) *a++ = rng_int32(seed,min,max);
}

//random 64bit unsigned integer
uint64_t rng_uint64(rng *seed) {
	uint64_t v;
	uint32_t *p = (void*)&v;
	p[0] = rng_uint32(seed);
	p[1] = rng_uint32(seed);
	return v;
}
//random 64bit signed integer within range
static inline int32_t rng_int64(rng *seed, const int64_t min, const int64_t max) {
	return min+(int64_t)(rng_uint64(seed)%(uint64_t)(max-min));
}

//random 32bit real within range
static inline float rng_float(rng *seed, const float min, const float max) {
	return min+rng_uint32(seed)*((max-min)/4294967296.0f);
}
static inline void rng_floats(rng *seed, const float min, const float max, float *a, const uint32_t count) {
	const float range = (max-min)/4294967296.0f;
	for (uint32_t i = 0; i < count; i++) *a++ = min+rng_uint32(seed)*range;
}

//random 64bit real within range
static inline double rng_double(rng *seed, const double min, const double max) {
	return min+rng_uint64(seed)*((max-min)/18446744073709551615.0);
}

//random 32-bit unsigned integer between 0-'l'(exclusive) using real probabilties 'p', set sum to 0 or negative to automatically calculate
uint32_t rng_id(rng *seed, float *p, float sum, const uint32_t l) {
	if (sum <= 0.0f) {
		sum = 0.0f;
		float *rp = p;
		for (uint32_t k = 0; k < l; k++) sum += *rp++;
	}

	const float r = rng_float(seed,0.0f,sum);
	sum = 0.0f;
	for (uint32_t k = 0; k < l; k++) {
		sum += *p++;
		if (sum >= r) return k;
	}
	return rng_uint32(seed)%l;
}

#endif
