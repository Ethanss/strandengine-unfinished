﻿/*Copyright 2018 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _BUILDASSETS_H_
#define _BUILDASSETS_H_

#include "list.h"
#include "hashmap.h"
#include "stream.h"


const char *ASSET_OPTIONS_EXT = ".assetoptions";
#define ASSET_OPTIONS_EXT_LEN 14

uint64_t assets_buildMeta(uint32_t bdirLen, const char *dir, hashmap *fmap, list *flist, uint64_t dataPos) {
	list files = listFiles(dir),ndir;
	uint32_t dstrLen = strlen(dir);

	new_list(&ndir,1);
	list_addm(&ndir, dir, dstrLen);
	char sepc = '/';
	list_add(&ndir, &sepc);

	file_info *finfo = files.buffer;
	for (uint32_t i = 0; i < files.length; i++) {
		uint32_t fnlen = strlen(finfo->name);
		list_addm(&ndir, finfo->name, fnlen);
		char *ndirCStr = list_as_cstring(&ndir);

		if (finfo->type != FILE_TYPE_DIRECTORY) {
			if (!memEndsWith(finfo->name,fnlen,(void*)ASSET_OPTIONS_EXT,ASSET_OPTIONS_EXT_LEN)) { 
				char *nfCStr = ndirCStr+bdirLen+1;
				uint32_t nfLen = ndir.length-bdirLen-2;
				if (nfLen > 256) printf("Assets Build: Asset path cannot be longer then 256 characters, '%s' is too long.\n",nfCStr);

				uint32_t hv = hash(nfCStr,nfLen);
				if (hashmap_get(fmap,hv) != NULL) printf("Assets Build: Hash collision at file '%s'.\n", ndirCStr);
				else printf("Included asset '%s' as hash %d.\n",nfCStr,hv);

				finfo->type = nfLen;
				memcpy(finfo->name,nfCStr,nfLen);

				uint64_t meta[3];
				meta[0] = dataPos;
				meta[1] = file_size(ndirCStr);

				//json asset options
				ndir.length--;
				list_addm(&ndir, ASSET_OPTIONS_EXT, ASSET_OPTIONS_EXT_LEN);
				ndirCStr = list_as_cstring(&ndir);
				if (file_exists(ndirCStr)) meta[2] = file_size(ndirCStr);
				else meta[2] = 0;

				ndir.length -= ASSET_OPTIONS_EXT_LEN;
				hashmap_add(fmap,hv,meta);
				list_add(flist,finfo);
				dataPos += meta[1]+meta[2]+nfLen+4;
			}
		} else {
			//recursive directory search
			dataPos = assets_buildMeta(bdirLen,ndirCStr,fmap,flist,dataPos);
		}

		ndir.length = dstrLen+1;
		finfo++;
	}

	delete_list(&files);
	delete_list(&ndir);

	return dataPos;
}

//build all files in 'dir' into assets file at 'dstPath'
void assets_build(const char *dir, const char *dstPath) {
	//build hashmap and list of assets
	hashmap fmap;
	new_hashmap(&fmap,24,2);
	list flist;
	new_list(&flist,sizeof(file_info));

	uint32_t dlen = strlen(dir);
	assets_buildMeta(dlen,dir,&fmap,&flist,0);

	file_stream fs,lfs;
	if (!new_file_stream(&fs, dstPath, FILE_MODE_WRITE)) {
		delete_hashmap(&fmap);
		delete_list(&flist);
		return;
	}
	
	//write hashmap size
	stream_writeUniversal(&fs,&fmap.bufferSize,4,4);
	//write hashes
	stream_writeUniversal(&fs,fmap.buffer,4,fmap.bufferSize*4);
	//write metadata
	stream_writeUniversal(&fs,hashmap_values(&fmap),8,fmap.bufferSize*fmap.valueSize);

	//write files
	list nbuf;
	new_list(&nbuf,1);
	nbuf.length = 512;
	list_expand(&nbuf);
	nbuf.length = 0;
	list_addm(&nbuf,dir,dlen);
	if (dlen != 0 && dir[dlen-1] != '/') {
		char sepc = '/';
		list_add(&nbuf, &sepc);
	}

	file_info *files = flist.buffer;
	for (uint32_t i = 0; i < flist.length; i++) {
		uint32_t fnlen = files->type, hv = hash(files->name,fnlen);
		stream_writeUniversal(&fs,&fnlen,4,4);
		file_stream_write(&fs,files->name,fnlen);

		list_addm(&nbuf,files->name,fnlen);
		uint64_t *mdata = hashmap_get(&fmap,hv);
		file_stream ifs;
		if (mdata[2] != 0) {
			list_addm(&nbuf,ASSET_OPTIONS_EXT,ASSET_OPTIONS_EXT_LEN);
			char *nbCStr = list_as_cstring(&nbuf);
			new_file_stream(&ifs,nbCStr,FILE_MODE_READ);
			stream_copy(&fs,&ifs,mdata[2]);
			file_stream_close(&ifs);
			nbuf.length -= ASSET_OPTIONS_EXT_LEN+1;
		}
		char *nbCStr = list_as_cstring(&nbuf);
		if (!new_file_stream(&ifs,nbCStr,FILE_MODE_READ)) break;
		stream_copy(&fs,&ifs,mdata[1]);
		file_stream_close(&ifs);
		nbuf.length -= fnlen+1;
		files++;
	}

	file_stream_close(&fs);

	delete_list(&nbuf);
	delete_hashmap(&fmap);
	delete_list(&flist);
}

#endif
