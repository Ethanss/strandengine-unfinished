﻿/*Copyright 2019 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _TIMER_H_
#define _TIMER_H_

#include "slist.h"
#include "misc.h"


//timer definition
typedef struct {
	ifunction delegate;
	uint32_t delay;
	uint64_t lastTime;
} timer;

//timers execution handling
typedef struct {
	slist absTimers1, absTimers, tsTimers1, tsTimers, sTimers1, sTimers;
	uint64_t lastTSPoll, lastSPoll;
} timers_handling;


void new_timers(timers_handling *t) {
	new_slist(&t->absTimers, sizeof(timer), false);
	new_slist(&t->absTimers1, sizeof(timer), false);
	new_slist(&t->tsTimers, sizeof(timer), false);
	new_slist(&t->tsTimers1, sizeof(timer), false);
	new_slist(&t->sTimers, sizeof(timer), false);
	new_slist(&t->sTimers1, sizeof(timer), false);

	t->lastTSPoll = 0;
	t->lastSPoll = 0;
}
void delete_timers(timers_handling *t) {
	delete_slist(&t->absTimers);
	delete_slist(&t->absTimers1);
	delete_slist(&t->tsTimers);
	delete_slist(&t->tsTimers1);
	delete_slist(&t->sTimers);
	delete_slist(&t->sTimers1);
}

//create/remove timers, if singleCall is true the timer will only be called once and it cannot be removed.
void timers_addTimer(timers_handling* t, one_arg_function f, void *d, const uint32_t time, const uint64_t lastTime, bool singleCall, timer *nt) {
	timer tm = {(ifunction){f,d}, time, lastTime};
	slist *sd;
	if (singleCall) sd = time<=1000?&t->absTimers1:(time<=10000?&t->tsTimers1:&t->sTimers1);
	else sd = time<=1000?&t->absTimers:(time<=10000?&t->tsTimers:&t->sTimers);
	slist_add(sd, &tm);
	if (nt != NULL) *nt = tm;
}
void timers_removeTimer(timers_handling *t, timer td) {
	slist *sd = td.delay<1000?&t->absTimers:(td.delay<10000?&t->tsTimers:&t->sTimers);
	slist_removeCustomSize(sd, &td, sizeof(ifunction)+4);
}


//poll list of timers
void timers_poll(const uint64_t currTime, slist *tl, const bool single) {
	slist_lock(tl);
	timer *tms = (timer*)tl->list.buffer;
	const uint32_t llen = tl->list.length;
	for (uint32_t i = 0; i < llen; i++) {
		uint32_t offset = currTime-tms->lastTime;
		if (offset >= tms->delay) {
			tms->delegate.delegate(tms->delegate.owner);
			if (single) slist_removeCustomSize(tl,tms,sizeof(ifunction)+4);
			else tms->lastTime += tms->delay;
		}
		tms++;
	}
	slist_unlock(tl);
}
void timers_execute(timers_handling *t, const uint64_t currTime) {
	//absolute precision timers(<1000ms delay)
	timers_poll(currTime, &t->absTimers,false);
	timers_poll(currTime, &t->absTimers1,true);

	//100ms precision timers(<10000ms delay)
	if (currTime-t->lastTSPoll > 100) {
		timers_poll(currTime, &t->tsTimers,false);
		timers_poll(currTime, &t->tsTimers1,true);
		t->lastTSPoll = currTime;
	}

	//1000ms precision timers(>10000ms delay)
	if (currTime-t->lastSPoll > 1000) {
		timers_poll(currTime, &t->sTimers,false);
		timers_poll(currTime, &t->sTimers1,true);
		t->lastSPoll = currTime;
	}
}


//execute in hertz, hi = 1000/hertz
#define START_SYNC_HERTZ() uint64_t hsyncTime = timeNow();int32_t hdeltaTime;
#define SYNC_HERTZ(hi) hsyncTime += hi;\
hdeltaTime = (int64_t)hsyncTime-(int64_t)timeNow();\
threadSleep(hdeltaTime>0?hdeltaTime:0);


#endif
