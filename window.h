﻿/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _WINDOW_H_
#define _WINDOW_H_

#ifndef __linux__
#include <GL/glew.h>
#endif
#include <stdbool.h>
#include <stdio.h>
#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL.h>
#include "endianness.h"

typedef struct {
	SDL_Window* window;
	SDL_GLContext glContext;
	uint32_t width, height, refreshRate, refreshInterval;
	float widthf, heightf, oneDivWidth, oneDivHeight, aspect;
	bool fullscreen, active, border, vsync;
} window;

void window_setSize(window *w, uint32_t width, uint32_t height, bool border, bool fullscreen) {
	SDL_SetWindowBordered(w->window, border);
	if (width == 0) {
		SDL_DisplayMode cdm;
		if (SDL_GetCurrentDisplayMode(0, &cdm) != 0) {
			width = 800; height = 600;
		} else {
			width = cdm.w; height = cdm.h;
			if (border && !fullscreen) {
				width -= 100;
				height -= 100;
			}
		}
	}

	SDL_SetWindowSize(w->window, width, height);
	if (fullscreen) {
		SDL_SetWindowFullscreen(w->window, SDL_WINDOW_FULLSCREEN);
	} else {
		if (w->fullscreen) SDL_SetWindowFullscreen(w->window, 0);
		SDL_SetWindowPosition(w->window, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
	}

	glViewport(0, 0, width, height);
	glScissor(0, 0, width, height);

	w->width = width;
	w->height = height;
	w->aspect = height/(float)width;
	w->widthf = (float)width;
	w->heightf = (float)height;
	w->oneDivWidth = 1.0f/(float)width;
	w->oneDivHeight = 1.0f/(float)height;
	w->fullscreen = fullscreen;
	w->border = border;
}

void window_enableVSync(window *w, bool vsync) {
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, vsync?1:0);
	if (vsync) {
		if (SDL_GL_SetSwapInterval(-1) == -1) SDL_GL_SetSwapInterval(1);
	}
	else SDL_GL_SetSwapInterval(0);
	w->vsync = vsync;
}

bool new_window(window *w, const char* title, uint32_t width, uint32_t height, bool border, bool fullscreen, bool vsync) {
	//disable stretching
	SDL_SetHint(SDL_HINT_VIDEO_HIGHDPI_DISABLED, "1");

	//set to opengl version 3.2
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);

	//get display refresh rate and calc refresh interval for vsync timing
	SDL_DisplayMode cdm;
	if (SDL_GetCurrentDisplayMode(0, &cdm) != 0 && cdm.refresh_rate != 0) w->refreshRate = cdm.refresh_rate;
	else w->refreshRate = 30;
	w->refreshInterval = 1000 / w->refreshRate;
	window_enableVSync(w, vsync);
	
	w->window = NULL;
	w->border = border;

	//create SDL window, trying to get the highest depth buffer size available
	int dat = 2;
	w->window = 0;
	while (dat > 0 && !w->window) {
		int dsz = dat==2?24:16;
		SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, dsz);
		w->window = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
		if (w->window == NULL) printf("%d-bit depth buffer is unavailable.\n", dsz);
		dat--;
	}
	if (w->window == NULL) {
		printf("SDL error at SDL_CreateWindow: '%s'.\n",SDL_GetError());
		return false;
	}

	//create opengl context
	w->glContext = SDL_GL_CreateContext(w->window);
	if (w->glContext == NULL) {
		printf("SDL error at SDL_GL_CreateContext: '%s'.\n",SDL_GetError());
		return false;
	}

	window_setSize(w, width, height, border, fullscreen);
	w->active = true;

	return true;
}

static inline void delete_window(window *w) {
	//destroy SDL window and opengl context
	SDL_GL_DeleteContext(w->glContext);
	SDL_DestroyWindow(w->window);
}

static inline void window_setIcon(window *w, void *px, uint32_t width, uint32_t height) {
	uint32_t maskLE[4] = {0x000000FF,0x0000FF00,0x00FF0000,0xFF000000},
	 		maskBE[4] = {0xFF000000,0x00FF0000,0x0000FF00,0x000000FF};
	uint32_t *mask = (ENDIANNESS==LITTLEENDIAN)?maskLE:maskBE;
	SDL_Surface *isurf = SDL_CreateRGBSurfaceFrom(px,width,height,32,width*4,mask[0],mask[1],mask[2],mask[3]);
	SDL_SetWindowIcon(w->window, isurf);
	SDL_FreeSurface(isurf);
}

static inline void window_swapBuffers(window *w) {
	if (w->vsync) SDL_GL_SwapWindow(w->window);
	else glFlush();
}

#define window_setResizeable(w,r) SDL_SetWindowResizeable((w)->window,r?SDL_TRUE:SDL_FALSE)
#define window_bindOpenGLContext(w) SDL_GL_MakeCurrent((w)->window,(w)->glContext)
#define window_init_graphics(w) init_graphics(&(w).width, &(w).height, &(w).aspect, &(w).widthf, &(w).heightf, &(w).oneDivWidth, &(w).oneDivHeight);

#endif
