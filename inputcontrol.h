/*Copyright 2018 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files(the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and / or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions :
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _INPUTCONTROL_H_
#define _INPUTCONTROL_H_

#include <stdint.h>
#include <stdbool.h>
#include "system.h"
#include "misc.h"
#include "stream.h"

typedef void(*inputcontrolGlobalKeyboardCallback)(uint16_t,uint8_t);//arg1 = keycode, arg2 = type(0=release, 1=press, 2=repeat)
typedef void(*inputcontrolGlobalMouseCallback)(uint8_t,int8_t);//arg1 = mouse button, arg2 = state(0=release,1=press)

inputcontrolGlobalKeyboardCallback globalKeyboardEventCallback = NULL, globalMouseEventCallback = NULL;
bool globalKeyboardListenerStopped = true, globalMouseListenerStopped = true;

void stopGlobalKeyboardListener() {
	globalKeyboardEventCallback = NULL;
	while (!globalKeyboardListenerStopped) threadSleep(1);
}
void stopGlobalMouseListener() {
	globalMouseEventCallback = NULL;
	while (!globalMouseListenerStopped) threadSleep(1);
}

#ifdef __linux__
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/extensions/XTest.h>
#include <linux/input.h>
#include <fcntl.h>
#include <stdio.h>

typedef struct {
	Display *display;
	Window window;
} inputcontrol;

static inline void new_inputcontrol(inputcontrol *i, const char *displayName) {
	i->display = XOpenDisplay(displayName);
	i->window = RootWindow(i->display,DefaultScreen(i->display));
}
static inline void delete_inputcontrol(inputcontrol *i) {
	XCloseDisplay(i->display);
}

static inline bool inputcontrol_getMousePosition(inputcontrol *i, uint32_t *p) {
	Window buf;
	return XQueryPointer(i->display,i->window,&buf,&buf,(void*)p,(void*)(p+1),&buf,&buf,&buf);
}
#define inputcontrol_mousePosition(i,x,y) XTestFakeMotionEvent((i)->display,-1,x,y,0)
static inline void mousePositionRelative(inputcontrol *i, int32_t mx, int32_t my) {
	uint32_t pos[2];
	Window buf;
	XQueryPointer(i->display, i->window, &buf, &buf, (void*)p, (void*)(p + 1), &buf, &buf, &buf);
	XTestFakeMotionEvent(i->display, -1, pos[0] + mx, pos[1] + my, 0);
}
#define inputcontrol_mouse(i,x,y,button,pressed) XTestFakeButtonEvent((i)->display,button+1,pressed,0)
#define inputcontrol_key(i,key,pressed) XTestFakeKeyEvent((i)->display,key,pressed,0)

static inline uint32_t inputcontrol_stringToKeycode(inputcontrol *i, const char *str) {
	return XKeysymToKeycode(i->display,XStringToKeysym(str));
}
static inline char *inputcontrol_keycodeToString(inputcontrol *i, const uint32_t keycode) {
	return XKeysymToString(XKeycodeToKeysym(i->display, keycode, 0);
}

//listen for global key events, probably requires root/sudo
void linuxGlobalKeyboardListening(void UNUSED *unu) {
	//find keyboard device
	FILE *fp = fopen("/proc/bus/input/devices",FILE_MODE_READ);
	if (fp == NULL) {
		globalKeyboardEventCallback = NULL;
		return;
	}
	bool pbuf = false;
	char fb[512],pb[256];
	uint32_t pbl;
	while (fgets(fb,512,fp) != NULL) {
		const char sc = fb[0];
		if (sc == 'H') {
			const uint32_t slen = strlen(fb);
			if (slen > 10) {
				pbl = slen-9;
				if (pbl > 255) pbl = 255;
				memcpy(pb,fb+9,pbl+1);
				pbuf = true;
			} else pbuf = false; 
		} else if (pbuf && sc == 'B' && fb[3] == 'E' && fb[4] == 'V') {
			uint32_t ev = strtoul(fb+6,NULL,16);
			//if EV_KEY && EV_REP && !EV_REL && !EV_ABS then keyboard
			if (((ev>>1)&1 && (ev>>20)&1) && !((ev>>2)&1 || (ev>>3)&1)) {
				//parse event id
				const char *eventStr = "event";
				int32_t index = memIndexOf(pb,1,pbl,eventStr,strlen(eventStr));
				if (index != -1) {
					const char space = ' ';
					int32_t eind = memIndexOf(pb+index,1,pbl-index,&space,1);
					if (eind != -1) pb[index+eind] = 0;
					snprintf(pb,256,"/dev/input/event%d",strtoul(pb+index+5,NULL,10));
					break;
				}
			}
			pbuf = false;
		}
	}
	fclose(fp);
	
	if (!pbuf) {
		globalKeyboardEventCallback = NULL;
		return;
	}
	
	//open keyboard device
	int fd = open(pb,O_RDONLY);
	if (fd == -1) {
		globalKeyboardEventCallback = NULL;
		return;
	}
	
	struct input_event evt;
	
	globalKeyboardListenerStopped = false;
	while (globalKeyboardEventCallback != NULL) {
		int rsz = read(fd,&evt,sizeof(struct input_event));
		if (rsz == 0 || evt.type != EV_KEY || globalKeyboardEventCallback == NULL) continue;
		globalKeyboardEventCallback(evt.code,evt.value);
	}
	close(fd);
	globalKeyboardListenerStopped = true;
}
void startGlobalKeyboardListener(inputcontrolGlobalKeyboardCallback cb) {
	if (globalKeyboardEventCallback != NULL) return;
	globalKeyboardEventCallback = cb;
	createThread(linuxGlobalKeyboardListening,NULL,64*1024);
}

//listen for global mouse click events, probably requires root/sudo
void linuxGlobalMouseListening(void UNUSED *unu) {
	//find mouse device
	FILE *fp = fopen("/proc/bus/input/devices",FILE_MODE_READ);
	if (fp == NULL) {
		linuxGlobalMouseEventCallback = NULL;
		return;
	}
	bool pbuf = false;
	char fb[512],pb[256];
	uint32_t pbl;
	while (fgets(fb,512,fp) != NULL) {
		const char sc = fb[0];
		if (sc == 'H') {
			const uint32_t slen = strlen(fb);
			if (slen > 10) {
				pbl = slen-9;
				if (pbl > 255) pbl = 255;
				memcpy(pb,fb+9,pbl+1);
				pbuf = true;
			} else pbuf = false; 
		} else if (pbuf && sc == 'B' && fb[3] == 'E' && fb[4] == 'V') {
			uint32_t ev = strtoul(fb+6,NULL,16);
			//if EV_KEY && EV_REL && EV_ABS then mouse
			if ((ev>>1)&1 && (ev>>2)&1 && (ev>>3)&1) {
				//parse event id
				const char *eventStr = "event";
				int32_t index = memIndexOf(pb,1,pbl,eventStr,strlen(eventStr));
				if (index != -1) {
					const char space = ' ';
					int32_t eind = memIndexOf(pb+index,1,pbl-index,&space,1);
					if (eind != -1) pb[index+eind] = 0;
					snprintf(pb,256,"/dev/input/event%d",strtoul(pb+index+5,NULL,10));
					break;
				}
			}
			pbuf = false;
		}
	}
	fclose(fp);
	
	if (!pbuf) {
		globalMouseEventCallback = NULL;
		return;
	}
	
	//open mouse device
	int fd = open(pb,O_RDONLY);
	if (fd == -1) {
		globalMouseEventCallback = NULL;
		return;
	}
	
	uint8_t data[6], *ndata = data, *ldata = data + 3;
	ldata[0] = ldata[1] = ldata[2];
	globalMouseListenerStopped = false;
	while (globalMouseEventCallback != NULL) {
		int rsz = read(fd,ndata,3);
		if (rsz == 0) continue;

		const uint8_t mv = ndata[0];
		for (uint32_t i = 0; i < 3; i++) {
			ndata[i] = mv&(1 << i);
			if (ndata[i] != ldata[i]) globalMouseEventCallback(i, ndata[i]);
		}

		uint8_t *tmp = ndata;
		ndata = ldata;
		ldata = tmp;
	}
	close(fd);
	globalMouseListenerStopped = true;
}
void startGlobalMouseListener(inputcontrolGlobalMouseCallback cb) {
	if (globalMouseEventCallback != NULL) return;
	globalMouseEventCallback = cb;
	createThread(linuxGlobalMouseListening,NULL,64*1024);
}

#else
#include <windows.h>

typedef struct {
	uint32_t screenWidth, screenHeight;
} inputcontrol;

static inline void new_inputcontrol(inputcontrol *i, char UNUSED *displayName) {
	i->screenWidth = GetSystemMetrics(SM_CXSCREEN);
	i->screenHeight = GetSystemMetrics(SM_CYSCREEN);
}
static inline void delete_inputcontrol(inputcontrol *i) {
	i->screenWidth = 0;
}

static inline bool inputcontrol_getMousePosition(inputcontrol *i, uint32_t *p) {
	POINT pt;
	bool res = GetCursorPos(&pt)==TRUE;
	p[0] = pt.x;
	p[1] = pt.y;
	return res;
}
void inputcontrol_mousePosition(inputcontrol *i, const uint32_t x, const uint32_t y) {
	SetCursorPos(x, y);
	INPUT ip;
	ip.type = INPUT_MOUSE;
	ip.mi.dx = (x*65535)/i->screenWidth;
	ip.mi.dy = (y*65535)/i->screenHeight;
	ip.mi.dwFlags = MOUSEEVENTF_MOVE|MOUSEEVENTF_ABSOLUTE;
	ip.mi.mouseData = 0;
	ip.mi.dwExtraInfo = GetMessageExtraInfo();
	ip.mi.time = 0;
	return SendInput(1, &ip, sizeof(INPUT)) == 1;
}
void inputcontrol_mousePositionRelative(inputcontrol *i, const int16_t x, const int16_t y) {
	INPUT ip;
	ip.type = INPUT_MOUSE;
	ip.mi.dx = x;
	ip.mi.dy = y;
	ip.mi.dwFlags = MOUSEEVENTF_MOVE;
	ip.mi.mouseData = 0;
	ip.mi.dwExtraInfo = GetMessageExtraInfo();
	ip.mi.time = 0;
	return SendInput(1, &ip, sizeof(INPUT)) == 1;
}
bool inputcontrol_mouse(inputcontrol *i, const uint32_t x, const uint32_t y, const uint8_t button, const bool pressed) {
	INPUT ip;
	ip.type = INPUT_MOUSE;
	ip.mi.dx = (x*65535)/i->screenWidth;
	ip.mi.dy = (y*65535)/i->screenHeight;
	DWORD dwf;
	if (button == 0) dwf = pressed ? MOUSEEVENTF_LEFTDOWN : MOUSEEVENTF_LEFTUP;
	else if (button == 1) dwf = pressed ? MOUSEEVENTF_MIDDLEDOWN : MOUSEEVENTF_MIDDLEUP;
	else dwf = pressed ? MOUSEEVENTF_RIGHTDOWN : MOUSEEVENTF_RIGHTUP;
	ip.mi.dwFlags = dwf|MOUSEEVENTF_ABSOLUTE;
	ip.mi.mouseData = 0;
	ip.mi.dwExtraInfo = GetMessageExtraInfo();
	ip.mi.time = 0;
	return SendInput(1, &ip, sizeof(INPUT))==1;
}
bool inputcontrol_mousewheel(inputcontrol *i, const int32_t amount) {
	INPUT ip;
	ip.type = INPUT_MOUSE;
	ip.mi.dx = 0;
	ip.mi.dy = 0;
	ip.mi.dwFlags = MOUSEEVENTF_WHEEL;
	ip.mi.mouseData = amount;
	ip.mi.dwExtraInfo = GetMessageExtraInfo();
	ip.mi.time = 0;
	return SendInput(1, &ip, sizeof(INPUT)) == 1;
}
bool inputcontrol_key(inputcontrol *i, const uint32_t key, const bool pressed) {
	INPUT ip;
	ip.type = INPUT_KEYBOARD;
	ip.ki.wScan = MapVirtualKey(key, MAPVK_VK_TO_VSC);
	ip.ki.dwFlags = pressed ? (KEYEVENTF_SCANCODE) : (KEYEVENTF_KEYUP|KEYEVENTF_SCANCODE);
	ip.mi.dwExtraInfo = GetMessageExtraInfo();
	ip.mi.time = 0;
	return SendInput(1, &ip, sizeof(INPUT)) == 1;
}

typedef struct {
	char *string;
	uint8_t keycode;
} winkeycodestring;

#define winkeycodestring_count 84
winkeycodestring winkeycodestrings[winkeycodestring_count] = {
	{"BackSpace",VK_BACK},
	{"Tab", VK_TAB},
	{"Return", VK_RETURN},
	{"Pause",VK_PAUSE},
	{"Scroll_Lock",VK_SCROLL},
	{"Escape",VK_ESCAPE},
	{"Delete",VK_DELETE},
	{"Home",VK_HOME},
	{"Left",VK_LEFT},
	{ "Up",VK_UP },
	{ "Right", VK_RIGHT },
	{ "Down", VK_DOWN },
	{ "Prior", VK_PRIOR },
	{ "Next", VK_NEXT },
	{ "End", VK_END },
	{ "Insert", VK_INSERT },
	{ "Num_Lock", VK_NUMLOCK },
	{ "F1", VK_F1 },
	{ "F2", VK_F2 },
	{ "F3", VK_F3 },
	{ "F4", VK_F4 },
	{ "F5", VK_F5 },
	{ "F6", VK_F6 },
	{ "F7", VK_F7 },
	{ "F8", VK_F8 },
	{ "F9", VK_F9 },
	{ "F10", VK_F10 },
	{ "F11", VK_F11 },
	{ "F12", VK_F12 },
	{ "Shift_Left", VK_LSHIFT },
	{ "Shift_Right", VK_RSHIFT },
	{ "Control_Left", VK_LCONTROL },
	{ "Control_Right", VK_RCONTROL },
	{ "Caps_Lock", VK_CAPITAL },
	{ "Alt_Left", VK_LMENU},
	{ "Alt_Right", VK_RMENU},
	{ "Space", VK_SPACE },
	{ "Comma", VK_OEM_COMMA },
	{ "Period", VK_OEM_PERIOD},
	{ "Slash", VK_OEM_2 },
	{ "Backslash", VK_OEM_5 },
	{ "Semicolon", VK_OEM_1 },
	{ "Apostrophe", VK_OEM_7 },
	{ "BracketLeft", VK_OEM_4 },
	{ "BacketRight", VK_OEM_6 },
	{ "Minus", VK_OEM_MINUS },
	{ "Equal", VK_OEM_PLUS },
	{ "Grave", VK_OEM_3 },
	{ "0", '0' },
	{ "1", '1' },
	{ "2", '2' },
	{ "3", '3' },
	{ "4", '4' },
	{ "5", '5' },
	{ "6", '6' },
	{ "7", '7' },
	{ "8", '8' },
	{ "9", '9' },
	{ "A", 'A' },
	{ "B", 'B' },
	{ "C", 'C' },
	{ "D", 'D' },
	{ "E", 'E' },
	{ "F", 'F' },
	{ "G", 'G' },
	{ "H", 'H' },
	{ "I", 'I' },
	{ "J", 'J' },
	{ "K", 'K' },
	{ "L", 'L' },
	{ "M", 'M' },
	{ "N", 'N' },
	{ "O", 'O' },
	{ "P", 'P' },
	{ "Q", 'Q' },
	{ "R", 'R' },
	{ "S", 'S' },
	{ "T", 'T' },
	{ "U", 'U' },
	{ "V", 'V' },
	{ "W", 'W' },
	{ "X", 'X' },
	{ "Y", 'Y' },
	{ "Z", 'Z' }
};
uint32_t inputcontrol_stringToKeycode(inputcontrol *i, const char *str) {
	uint32_t max = 0, mkc = 0;
	winkeycodestring *ks = winkeycodestrings;
	for (uint32_t q = 0; q < winkeycodestring_count; q++) {
		char *sb = str, *kb = ks->string;
		uint32_t sum = 0;
		while (true) {
			char sc = *sb++, lc = *kb++;
			if (sc == 0) {
				if (lc == 0) return ks->keycode;
				break;
			}
			else if (lc == 0) break;
			if (tolower(sc) == tolower(lc)) sum++;
			else break;
		}
		if (sum > max) {
			max = sum;
			mkc = ks->keycode;
		}
		ks++;
	}
	return mkc;
}
char *inputcontrol_keycodeToString(inputcontrol *i, const uint32_t keycode) {
	winkeycodestring *ks = winkeycodestrings;
	for (uint32_t q = 0; q < winkeycodestring_count; q++) {
		if (ks->keycode == keycode) return ks->string;
		ks++;
	}
	return NULL;
}


LRESULT CALLBACK windowsGlobalKeyboardCallback(int code, WPARAM wParam, LPARAM lParam) {
	if (code >= 0 && globalKeyboardEventCallback != NULL) {
		KBDLLHOOKSTRUCT *kbs = (KBDLLHOOKSTRUCT*)lParam;
		globalKeyboardEventCallback(kbs->vkCode, wParam == WM_KEYDOWN ? 1 : 0);
	}
	return CallNextHookEx(NULL, code, wParam, lParam);
}
void windowsGlobalKeyboardListening(void UNUSED *unu) {
	HHOOK hook = SetWindowsHookEx(WH_KEYBOARD_LL, windowsGlobalKeyboardCallback, NULL, 0);
	globalKeyboardListenerStopped = false;
	UINT_PTR tid = SetTimer(NULL, NULL, 1000, NULL);
	MSG msg;
	while (globalKeyboardEventCallback != NULL) GetMessage(&msg, NULL, 0, 0);
	KillTimer(NULL,tid);
	UnhookWindowsHookEx(hook);
	globalKeyboardListenerStopped = true;
}
void startGlobalKeyboardListener(inputcontrolGlobalKeyboardCallback cb) {
	if (globalKeyboardEventCallback != NULL) return;
	globalKeyboardEventCallback = cb;
	createThread(windowsGlobalKeyboardListening, NULL, 64*1024);
}


LRESULT CALLBACK windowsGlobalMouseCallback(int code, WPARAM wParam, LPARAM lParam) {
	if (code >= 0) {
		uint8_t c, s = 0xFF;
		switch (wParam) {
		case WM_LBUTTONDOWN:
			c = 0;
			s = 1;
			break;
		case WM_LBUTTONUP:
			c = 0;
			s = 0;
			break;

		case WM_MBUTTONDOWN:
			c = 1;
			s = 1;
			break;
		case WM_MBUTTONUP:
			c = 1;
			s = 0;
			break;

		case WM_RBUTTONDOWN:
			c = 2;
			s = 1;
			break;
		case WM_RBUTTONUP:
			c = 2;
			s = 0;
			break;
		}
		if (s != 0xFF) globalMouseEventCallback(c, s);
	}
	return CallNextHookEx(NULL, code, wParam, lParam);
}
void windowsGlobalMouseListening(void UNUSED *unu) {
	HHOOK hook = SetWindowsHookEx(WH_MOUSE_LL, windowsGlobalMouseCallback, NULL, 0);
	globalMouseListenerStopped = false;
	UINT_PTR tid = SetTimer(NULL, NULL, 1000, NULL);
	MSG msg;
	while (globalMouseEventCallback != NULL) GetMessage(&msg, NULL, 0, 0);
	KillTimer(NULL, tid);
	UnhookWindowsHookEx(hook);
	globalMouseListenerStopped = true;
}
void startGlobalMouseListener(inputcontrolGlobalMouseCallback cb) {
	if (globalMouseEventCallback != NULL) return;
	globalMouseEventCallback = cb;
	createThread(windowsGlobalMouseListening, NULL, 64 * 1024);
}
#endif

#endif
