﻿/*Copyright 2019 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _TEMPLATE_H_
#define _TEMPLATE_H_

#include "../engine.h"


typedef struct {
	component base;
	bool weGood;
} template;

//component functions
void template_update(template *o) {
	
}

void template_enable(template *o) {

}

void init_template(template *o) {
	//init component
	o->weGood = true;

	//assign event callbacks
	o->base.enable = template_enable;
	o->base.update = template_update;
}

//component reflection
#define REFLECT_TARGET template
#define template_fieldCount 1
const reflection_data template_fields[template_fieldCount] = {RCF(uint8_t,weGood)};
#undef REFLECT_TARGET

//register component in engines linked list of components
define_component(template,template_fields,template_fieldCount);
#undef LAST_COMPONENT_DATA
#define LAST_COMPONENT_DATA &component_data_template

#endif
