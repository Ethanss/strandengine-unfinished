/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _GUI_TEXT_H_
#define _GUI_TEXT_H_

#include "gui.h"

typedef bool(*gui_text_parsingFunction)(char, void*, vec4*);

typedef struct {
	gui_component base;
	list text, blitIds, lineLengths;
	font *font, *activeFont;
	asset *fontAsset;
	void *parsingFunctionArg;
	uint32_t fontSize,cursorIndex,cursorBlitID,fontAssetHash,cursorSelection;
	float normalizedFontSize, lastLineY;
	vec2 padding, cursorPos, cursorSize;
	vec3 offset;
	vec4 color, textContainer;
	gui_text_parsingFunction parsingFunction;
	uint8_t anchor;
	bool multiline,renderCursor,updatedText;
} gui_text;


//returns height of text block and outputs individual line widths to l, if l is NULL and then the last line width is returned. If 'cursor' is not NULL then cursor index of 'cursorPos' is stored in 'cursor'
float gui_text_size(uint32_t fontSize, font *font, vec2 padding, list *text, float containerWidth, gui_text_parsingFunction parsingFunction, void *parsingFunctionArg, bool multiline, list *l, vec2 cursorPos, uint32_t *cursor) {
	if (l != NULL) l->length = 0;
	
	glyph *offsets = font->offsets;
	const float fscale = fontSize*oneDivScreenHeight[0], aspect = screenAspect[0],
		fdiff = (fontSize / (float)font->maxSize)*aspect*oneDivScreenHeight[0] * padding.x,
		fspace = offsets[32].advance*oneDivScreenHeight[0] * aspect * padding.x,
		fpad = fscale*padding.x;
	containerWidth = (containerWidth*2.0f)*aspect;

	vec2 tcpos = (vec2){0.0f, 0.0f};
	vec4 bbuf;
	bool parsing = parsingFunction != NULL,
	findCursor = cursor != NULL;
	const float cursorScale = multiline?fscale*2.0f:1e7f;
	
	char *tb = text->buffer;
	const uint32_t tl = text->length;
	for (uint32_t i = 0; i < tl; i++) {
		uint8_t c = *tb++;
		if (parsing && !parsingFunction(c, parsingFunctionArg, &bbuf)) continue;
		bool cnl = c=='\n';

		if (multiline && (tcpos.x+fpad > containerWidth || cnl)) {
			tcpos.x += fscale;
			if (l != NULL) list_add(l, &tcpos.x);
			tcpos.x = 0.0f;
			tcpos.y -= fscale*padding.y;
			if (findCursor && cursorPos.y > tcpos.y) {
				*cursor = i;
				findCursor = false;
			}
		}

		if (!cnl) {
			if (offsets[c].exists) tcpos.x += offsets[c].advance*fdiff;
			else tcpos.x += fspace;
		}
		
		if (findCursor && tcpos.x > cursorPos.x && fabs(tcpos.y-cursorPos.y) < cursorScale) {
			*cursor = i;
			findCursor = false;
		}
	}
	
	if (findCursor) *cursor = tl;

	if (l != NULL) {
		tcpos.x += fscale;
		list_add(l, &tcpos.x);
		return tcpos.y;
	} else {
		return tcpos.x;
	}
}

void gui_text_freeBlitIds(gui_text *p) {
	const uint32_t nbid = p->blitIds.length;
	uint32_t *bid = p->blitIds.buffer;
	for (uint32_t i = 0; i < nbid; i++) {
		gui_freeBlitID(p->base.targetGUI,&p->activeFont->glyphs,*bid++);
	}
	p->blitIds.length = 0;
}

void gui_text_mainUpdateText(gui_text *b) {
	if (b->font != b->activeFont) {
		if (b->activeFont != NULL && b->blitIds.length > 0) gui_text_freeBlitIds(b);
		b->activeFont = b->font;
	}

	if (b->renderCursor) {
		if (b->cursorBlitID == UINT_MAX) gui_allocateBlitID(b->base.targetGUI, &whiteTexture, &b->cursorBlitID);
	} else if (b->cursorBlitID != UINT_MAX) {
		gui_freeBlitID(b->base.targetGUI, &whiteTexture, b->cursorBlitID);
		b->cursorBlitID = UINT_MAX;
	}

	if (b->normalizedFontSize > 0.0f) b->fontSize = (uint32_t)(b->normalizedFontSize*screenHeight[0]);

	//buffer instanced glyphs
	const int32_t anchorX = ANCHOR_X(b->anchor),
			 anchorY = ANCHOR_Y(b->anchor);
	const float fscale = b->fontSize*oneDivScreenHeight[0],
		  fdiff = b->fontSize/(float)b->font->maxSize,
		  aspect = *screenAspect,
		  fmov = fdiff*oneDivScreenHeight[0]*aspect,
		  fpad = fscale*b->padding.x;
	float fspace;
	object *obj = b->base.base.object;

	gui_blit *gblt = gui_getBlit(b->base.targetGUI, &b->activeFont->glyphs);
	vec2 basePos = (vec2){obj->absoluteScale.x*anchorX*aspect, obj->absoluteScale.y*anchorY};

	blit_material bm;
	bm.clipPosition = ((vec2*)&b->textContainer)[0];
	bm.clipPosition.x *= aspect;
	bm.clipScale = ((vec2*)&b->textContainer)[1];
	bm.clipScale.x *= aspect;
	bm.aspect = 1.0f;
	bm.rotationSin = 0.0f;
	bm.rotationCos = 1.0f;
	bm.depth = obj->absolutePosition.z+b->offset.z;
	bm.tint = b->color;

	bool parsing = b->parsingFunction!=NULL;
	glyph *offsets = b->activeFont->offsets;
	fspace = offsets[32].advance*fmov;

	//calculate text line lengths for anchoring
	if (b->anchor != ANCHOR_TOP_LEFT) {
		float tcy = gui_text_size(b->fontSize, b->activeFont, b->padding, &b->text, obj->absoluteScale.x, b->parsingFunction, b, b->multiline, &b->lineLengths, (vec2){0.0f,0.0f}, NULL);
		if (anchorY == 0) {
			basePos.y -= tcy*0.5f;
		} else if (anchorY < 0) {
			basePos.y -= tcy;
		}
		b->lastLineY = tcy;
	}

	float *lineLens = b->lineLengths.buffer;
	uint32_t ln = 0;
	vec2 pos = basePos,offset = *(vec2*)&obj->absolutePosition;
 	offset.x += fpad*0.5f;
 	offset.x *= aspect;
	offset.y -= fscale*b->padding.y*anchorY*0.5f;
	vec2_add(&offset,(vec2*)&b->offset);
	if (b->anchor != ANCHOR_TOP_LEFT) {
		if (anchorX == 0) {
			pos.x -= lineLens[0]*0.5f+fpad*0.25f*aspect;
		} else if (anchorX > 0) {
			pos.x -= lineLens[0]+fpad*0.25f*aspect;
		}
	}

	float padX = b->padding.x,
	containerWidth = obj->absoluteScale.x*aspect;
	bool multiline = b->multiline, renderCursor = b->renderCursor, selection = b->cursorSelection!=0;

	vec2 cursorPos;
	if (b->renderCursor && b->cursorIndex == 0) cursorPos = pos;

	//set glyph uniform buffer data
	uint32_t bbId = 0, cursorIndex = b->cursorIndex, cursorEnd = cursorIndex+b->cursorSelection;
	uint8_t *cptr = b->text.buffer;
	const uint32_t tlen = b->text.length;
	for (uint32_t i = 0; i < tlen; i++) {
		if (renderCursor && i == cursorIndex) cursorPos = pos;
	
		uint8_t c = *cptr++;
		if (parsing && !b->parsingFunction(c, b, &bm.tint)) continue;
		
		bool cnl = c=='\n';
		if (multiline && (pos.x+fpad > containerWidth || cnl)) {
			ln++;

			pos.x = basePos.x;
			if (b->anchor != ANCHOR_TOP_LEFT) {
				if (ln >= b->lineLengths.length) break;
				
				if (anchorX == 0) {
					pos.x -= lineLens[ln]*0.5f+fpad*0.5f*aspect;
				} else if (anchorX > 0) {
					pos.x -= lineLens[ln]+fpad*0.5f*aspect;
				}
			}

			pos.y -= fscale*b->padding.y;
		}

		if (!cnl) {
			if (offsets[c].exists) {
				bm.position = pos;
				float avp = offsets[c].advance*fmov;
				bm.position.x += avp;
				vec2_add(&bm.position, &offset);

				bm.scale = (vec2){offsets[c].aspect*aspect, 1.0f};
				vec2_mulSingle(&bm.scale, fscale);

				//advance position
				pos.x += avp*padX;

				//culling
				if (fmax(fabs(bm.position.x-bm.clipPosition.x)-bm.scale.x-bm.clipScale.x, fabs(bm.position.y-bm.clipPosition.y)-bm.scale.y-bm.clipScale.y) > 0.0f) continue;

				if (selection && i >= cursorIndex && i < cursorEnd) {
					bm.sourcePosition = (vec2){-1.0f,-1.0f};
					bm.sourceScale = (vec2){0.1f,0.1f};
					bm.scale.x *= padX*0.75f;
				} else {
					bm.sourcePosition = offsets[c].position;
					bm.sourceScale = offsets[c].size;
				}

				//blit memory
				if (bbId >= b->blitIds.length) {
					void *lastBuf = b->blitIds.buffer;
					list_addEmpty(&b->blitIds);
					if (lastBuf != b->blitIds.buffer && gblt != NULL) {
						uint32_t *bid = b->blitIds.buffer;
						for (uint32_t i = 0; i < bbId; i++) {
							uint32_t bi = *bid, l = bi/GUI_BLIT_BATCH_COUNT;
							((uint32_t**)((gui_blit_batch*)gblt->batches.buffer+l)->ids.buffer)[bi-l*GUI_BLIT_BATCH_COUNT] = bid++;
						}
					}
					gui_allocateBlitID(b->base.targetGUI, &b->activeFont->glyphs, (uint32_t*)b->blitIds.buffer+bbId);
					if (gblt == NULL) gblt = gui_getBlit(b->base.targetGUI, &b->activeFont->glyphs);
				}
				uint32_t blitId = ((uint32_t*)b->blitIds.buffer)[bbId],
				bl = blitId/GUI_BLIT_BATCH_COUNT, bi = blitId-bl*GUI_BLIT_BATCH_COUNT;
				ubp_memory_setArray(((gui_blit_batch*)gblt->batches.buffer)[bl].ubm,bi*sizeof(blit_material),&bm,sizeof(blit_material));
				bbId++;
			} else {
				pos.x += fspace*padX;
			}
		}
	}
	if (bbId < b->blitIds.length) {
		//free extra
		uint32_t nAbove = b->blitIds.length-bbId, *bids = b->blitIds.buffer;
		bids += bbId;
		for (uint32_t i = 0; i < nAbove; i++) gui_freeBlitID(b->base.targetGUI, &b->activeFont->glyphs, *bids++);
		b->blitIds.length = bbId;
	}

	if (b->renderCursor) {
		if (b->cursorIndex >= b->text.length) cursorPos = pos;
		
		bm.position = cursorPos;
		vec2_add(&bm.position, &offset);
		b->cursorPos = bm.position;

		bm.scale = b->cursorSize;
		bm.scale.x *= aspect;
		vec2_mulSingle(&bm.scale, fscale);

		uint32_t blitID = b->cursorBlitID, bl = blitID/GUI_BLIT_BATCH_COUNT, bi = blitID-bl*GUI_BLIT_BATCH_COUNT;
		ubp_memory_setArray(((gui_blit_batch*)gui_getBlit(b->base.targetGUI, &whiteTexture)->batches.buffer)[bl].ubm,bi*sizeof(blit_material),&bm,sizeof(blit_material));
	}

	b->updatedText = true;
}

void gui_text_updateText(gui_text *o) {
	gui *tg = o->base.targetGUI;
	if (tg == NULL || o->font == NULL || !o->updatedText) return;
	o->updatedText = false;
	singleRenderCall(gui_text_mainUpdateText,o);
}

void gui_text_updateContainer(gui_text *t) {
	object *obj = t->base.base.object;
	vec4 cr;
	*(vec2*)&cr = *(vec2*)&obj->absolutePosition;
	((vec2*)&cr)[1] = *(vec2*)&obj->absoluteScale;
	t->textContainer = vec4_rectangleCombine(cr,t->base.container);
}

void gui_text_transformUpdate(void *p) {
	gui_component_updateContainer(p);
	gui_text_updateContainer(p);
	gui_text_updateText(p);
}

void gui_text_enable(void *p) {
	gui_component_findTargetGUI(p);
	gui_text *t = p;
	asset_database_load(&assets,t->fontAssetHash, &t->fontAsset, &t->font);
	gui_text_transformUpdate(p);
}

void gui_text_freeGUIBlits(gui_text *p) {
	if (p->base.targetGUI != NULL) {
		gui_text_freeBlitIds(p);
		if (p->cursorBlitID != UINT_MAX) {
			gui_freeBlitID(p->base.targetGUI, &whiteTexture, p->cursorBlitID);
			p->cursorBlitID = UINT_MAX;
			//slist_remove(&renderCalls,&p);
		}
	}
	p->activeFont = NULL;
}

void gui_text_disable(void *p) {
	gui_component_disable(p);
	gui_text *t = p;
	singleRenderCall(gui_text_freeGUIBlits,p);
}

void free_gui_text(void *p) {
	gui_component_free(p);
	gui_text *o = p;
	delete_list(&o->lineLengths);
	delete_list(&o->text);
	gui_text_freeGUIBlits(o);
	delete_list(&o->blitIds);
	if (o->fontAsset != NULL) asset_database_return(&assets, &o->fontAsset);
}

void init_gui_text(gui_text *o) {
	init_gui_component(&o->base);

	o->base.base.transformUpdate = (void*)&gui_text_transformUpdate;
	o->base.base.free = &free_gui_text;
	o->base.base.enable = &gui_text_enable;
	o->base.base.disable = &gui_text_disable;

	new_list(&o->lineLengths,4);
	new_list(&o->blitIds,4);
	new_list(&o->text,1);
	o->font = NULL;
	o->activeFont = NULL;
	o->fontAsset = NULL;
	o->fontAssetHash = NULL_HASH;
	o->normalizedFontSize = -1.0f;
	o->color = (vec4){1.0f,1.0f,1.0f,1.0f};
	o->padding = (vec2){2.0f,2.0f};
	o->offset = (vec3){0.0f,0.0f,-1e-4f};
	o->parsingFunction = NULL;
	o->anchor = ANCHOR_TOP_LEFT;
	o->multiline = false;
	o->updatedText = true;

	o->renderCursor = false;
	o->cursorBlitID = UINT_MAX;
	o->cursorSize = (vec2){0.05f,0.8f};
	o->cursorSelection = 0;
}
void duplicate_gui_text(gui_text *d, gui_text *s) {
	duplicate_gui_component(&d->base,&s->base);
	d->parsingFunction = s->parsingFunction;
}

#define REFLECT_TARGET gui_text
#define gui_text_fieldCount 12
#define gui_text_fieldsdef RCF(list,text),RCF(uint32_t,fontAssetHash),RCF(uint32_t,fontSize),RCF(float,normalizedFontSize),RCF(float,color.x),RCF(float,color.y),RCF(float,color.z),RCF(float,color.w),RCF(float,padding.x),RCF(float,padding.y),RCF(uint8_t,anchor),RCF(uint8_t,multiline)
const reflection_data gui_text_fields[gui_text_fieldCount] = {gui_text_fieldsdef};
#undef REFLECT_TARGET
define_componentd(gui_text,gui_text_fields,gui_text_fieldCount,duplicate_gui_text);
#undef LAST_COMPONENT_DATA
#define LAST_COMPONENT_DATA &component_data_gui_text

#endif
