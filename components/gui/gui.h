/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _GUI_H_
#define _GUI_H_

#include "../../engine.h"
#include "../../assets_base.h"

typedef struct {
	texture *texture;
	list batches;
} gui_blit;
typedef struct {
	ubp_memory ubm;
	list ids;
} gui_blit_batch;
typedef struct {
	component base;
	list blits;
	void *hovered, *selected;
	rendertexture *renderTarget;
	float hoveredDepth, viewX, viewY, viewWidth, viewHeight, viewAspect;
	bool dragging, hoveredTested;
} gui;

typedef struct {
	component base;
	one_arg_function hoverEnter, hoverLeave, drag, click;
	texture *texture, *activeTexture;
	vec2 textureOffset, textureScale;
	vec4 container,color;
	uint32_t blitID, textureAssetHash;
	gui *targetGUI;
	void *parentContainer, *attachment;
	asset *textureAsset;
	bool updateBlitUBM, interactable, culled;
} gui_component;

void gui_component_updateBlitMain(gui_component *c);
void gui_component_updateContainer(gui_component *c);
void gui_component_transformUpdate(gui_component *c) {
	gui_component_updateContainer(c);
	if (c->targetGUI == NULL || c->updateBlitUBM) return;
	c->updateBlitUBM = true;
	singleRenderCall(gui_component_updateBlitMain, c);
}

bool gui_component_intersectsPoint(gui_component *o, vec2 p) {
	vec3 *ppos = &o->base.object->absolutePosition,*psz = &o->base.object->absoluteScale;
	return fmax(fabs(ppos->x-p.x)-psz->x,fabs(ppos->y-p.y)-psz->y)<=0.0f;
}

void gui_component_update(void *p) {
	gui_component *o = p;

	if (o->interactable) {
		gui* targetGUI = o->targetGUI;
		gui_component *hovd = targetGUI->hovered;

		if (hovd == NULL || hovd == o || (!targetGUI->dragging && targetGUI->hoveredDepth >= o->base.object->absolutePosition.z)) {
			vec3 *ppos = &o->base.object->absolutePosition,
			 	*psz = &o->base.object->absoluteScale;

			if (hovd == o && targetGUI->dragging) {
				//drag
				if (mouse[0]) {
					if (o->drag != NULL && (mouseDeltaX != 0 || mouseDeltaY != 0)) o->drag(o);
				} else {
					targetGUI->dragging = false;
				}
			} else {
				//check hovered
				const float sas = targetGUI->viewAspect;
				bool over = fmax(fabs(ppos->x*sas-nMouseX)-psz->x*sas,fabs(ppos->y-nMouseY)-psz->y)<=0.0f &&
				fmax(fabs(o->container.x*sas-nMouseX)-o->container.z*sas,fabs(o->container.y-nMouseY)-o->container.w)<=0.0f && !o->culled;
				if (hovd == o) {
					if (over) {
						if (!targetGUI->hoveredTested) {
							targetGUI->hoveredTested = true;
							if (o->hoverEnter != NULL) o->hoverEnter(o);
						}
					} else {
						targetGUI->hovered = NULL;
						if (targetGUI->hoveredTested && o->hoverLeave != NULL) o->hoverLeave(o);
					}
				} else {
					if (over) {
						if (hovd != NULL && targetGUI->hoveredTested && hovd->hoverLeave != NULL) hovd->hoverLeave(hovd);
						targetGUI->hovered = o;
						targetGUI->hoveredDepth = ppos->z;
						targetGUI->hoveredTested = false;
					}
				}

				//check click
				if (targetGUI->hovered == o && targetGUI->hoveredTested && mouse[0]) {
					targetGUI->dragging = true;
					targetGUI->selected = o;
					if (o->click != NULL) o->click(o);
				}
			}
		}
	}
}

void gui_component_disable(void *o) {
	gui_component *c = o;
	if (c->targetGUI != NULL) {
		if (c->targetGUI->hovered == c) {
			c->targetGUI->hovered = NULL;
			c->targetGUI->hoveredTested = false;
		}
		if (c->targetGUI->selected == c) {
			c->targetGUI->selected = NULL;
			c->targetGUI->dragging = false;
		}
		gui_component_transformUpdate(c);
	}
}
void gui_component_free(void *o) {
	gui_component *c = o;
	if (c->textureAsset != NULL) asset_database_return(&assets, c->textureAsset);
}
void gui_component_enable(void *o);

void init_gui_component(gui_component *o) {
	o->texture = &whiteTexture;
	o->activeTexture = NULL;
	o->textureAssetHash = NULL_HASH;
	o->textureAsset = NULL;
	o->textureOffset = (vec2){0.5f,0.5f};
	o->textureScale = (vec2){1.0f,1.0f};
	o->color = (vec4){1.0f,1.0f,1.0f,1.0f};
	o->updateBlitUBM = false;
	o->hoverEnter = NULL;
	o->hoverLeave = NULL;
	o->drag = NULL;
	o->click = NULL;
	o->interactable = false;
	o->blitID = UINT_MAX;

	o->base.enable = &gui_component_enable;
	o->base.disable = &gui_component_disable;
	o->base.free = &gui_component_free;
	o->base.transformUpdate = (void*)&gui_component_transformUpdate;
}

void duplicate_gui_component(gui_component *d, gui_component *s) {
	memcpy(&d->hoverEnter,&s->hoverEnter,sizeof(void*)*6);
	d->attachment = s->attachment;	
}

#define REFLECT_TARGET gui_component
#define gui_component_fieldCount 9
#define gui_component_fieldsdef RCF(float,color.x),RCF(float,color.y),RCF(float,color.z),RCF(float,color.w),RCF(float,textureOffset.x),RCF(float,textureOffset.y),RCF(float,textureScale.x),RCF(float,textureScale.y),RCF(uint32_t,textureAssetHash)
const reflection_data gui_component_fields[gui_component_fieldCount] = {gui_component_fieldsdef};
#undef REFLECT_TARGET
define_componentd(gui_component,gui_component_fields,gui_component_fieldCount,duplicate_gui_component);
#undef LAST_COMPONENT_DATA
#define LAST_COMPONENT_DATA &component_data_gui_component


#define GUI_BLIT_BATCH_COUNT 204

gui_blit *gui_getBlit(gui *g, texture *t) {
	const uint32_t nbt = g->blits.length;
	gui_blit *b = g->blits.buffer;
	for (uint32_t i = 0; i < nbt; i++) {
		if (b->texture == t) return b;
		b++;
	}
	return NULL;
}
void gui_allocateBlitID(gui *g, texture *t, uint32_t *id) {
	gui_blit *gblt = gui_getBlit(g,t);
	if (gblt == NULL) {
		gui_blit ngb;
		ngb.texture = t;
		new_list(&ngb.batches, sizeof(gui_blit_batch));
		list_add(&g->blits,&ngb);
		gblt = (gui_blit*)g->blits.buffer+g->blits.length-1;
	}

	uint32_t i = gblt->batches.length;
	if (i == 0 || ((gui_blit_batch*)gblt->batches.buffer)[i-1].ids.length == GUI_BLIT_BATCH_COUNT) {
		gui_blit_batch nb;
		nb.ubm = ubp_alloc(sizeof(blit_material)*8);
		new_list(&nb.ids,sizeof(void*));
		list_add(&gblt->batches, &nb);
	} else {
		i--;
	}

	gui_blit_batch *gb = (gui_blit_batch*)gblt->batches.buffer+i;
	*id = i*GUI_BLIT_BATCH_COUNT+gb->ids.length;

	list_add(&gb->ids,&id);
	uint32_t ubsz = gb->ubm.size/sizeof(blit_material);
	if (gb->ids.length > ubsz) {
		ubp_memory nm = ubp_alloc(min(GUI_BLIT_BATCH_COUNT,ubsz*8)*sizeof(blit_material));
		ubp_memory_copyArray(gb->ubm, 0, nm, 0, ubsz, sizeof(blit_material));
		ubp_free(gb->ubm);
		gb->ubm = nm;
	}
}
void gui_freeBlitID(gui *g, texture *t, uint32_t bid) {
	gui_blit *gblt = gui_getBlit(g, t);

	uint32_t l = bid / GUI_BLIT_BATCH_COUNT, i = bid - l*GUI_BLIT_BATCH_COUNT,
		tl = gblt->batches.length - 1;
	gui_blit_batch *tb = (gui_blit_batch*)gblt->batches.buffer + tl;
	uint32_t ti = tb->ids.length - 1;

	if (!(tl == l && ti == i)) {
		gui_blit_batch *cb = (gui_blit_batch*)gblt->batches.buffer + l;
		ubp_memory_copyArray(tb->ubm, ti, cb->ubm, i, 1, sizeof(blit_material));
		uint32_t **cbi = (uint32_t**)cb->ids.buffer;
		uint32_t *cbp = cbi[i] = ((uint32_t**)tb->ids.buffer)[ti];
		*cbp = bid;
	}
	
	tb->ids.length--;
	if (tb->ids.length == 0) {
		ubp_free(tb->ubm);
		delete_list(&tb->ids);
		gblt->batches.length--;
		if (gblt->batches.length == 0) list_removeAt(&g->blits, ((uintptr_t)gblt - (uintptr_t)g->blits.buffer) / sizeof(gui_blit));
	}
}

void gui_component_updateBlitMain(gui_component *o) {
	o->updateBlitUBM = false;
	if (o->base.enabled && !o->culled && o->texture != NULL) {
		if (o->texture != o->activeTexture) {
			if (o->activeTexture != NULL && o->blitID != UINT_MAX) {
				gui_freeBlitID(o->targetGUI,o->activeTexture,o->blitID);
				o->blitID = UINT_MAX;
			}
			o->activeTexture = o->texture;
		}
		if (o->blitID == UINT_MAX) gui_allocateBlitID(o->targetGUI, o->activeTexture, &o->blitID);
		gui_blit *gblt = gui_getBlit(o->targetGUI, o->activeTexture);

		//set blit uniform data
		object *obj = o->base.object;
		float rotv = obj->absoluteRotation.z;
		const float sas = o->targetGUI->viewAspect;
		blit_material bm = (blit_material){{obj->absolutePosition.x*sas,obj->absolutePosition.y},{obj->absoluteScale.x,obj->absoluteScale.y}, o->textureOffset,o->textureScale, {o->container.x*sas,o->container.y},{o->container.z*sas,o->container.w},o->color,sas,obj->absolutePosition.z,sinf(rotv),cosf(rotv)};
		uint32_t bid = o->blitID,b = bid/GUI_BLIT_BATCH_COUNT;
		ubp_memory_setArray(((gui_blit_batch*)gblt->batches.buffer)[b].ubm,(bid-b*GUI_BLIT_BATCH_COUNT)*sizeof(blit_material),&bm,sizeof(blit_material));
	} else {
		if (o->blitID != UINT_MAX) {
			gui_freeBlitID(o->targetGUI, o->activeTexture, o->blitID);
			o->activeTexture = NULL;
			o->blitID = UINT_MAX;
		}
	}
}


void gui_render(void *p) {
	gui *l = p;

	//render
	setCullFace(CULLFACE_BACK);
	setDepthTest(DEPTHTEST_LESS);
	setBlendMode(BLENDMODE_TRANSPARENT);
	enableDepthWrite(true);

	rendertexture_bind(l->renderTarget, l->viewX, l->viewY, l->viewWidth, l->viewHeight);
	glClear(GL_DEPTH_BUFFER_BIT);

	shader_bind(&blitShader);
	mesh_bind(&quadMesh);

	//batched blit
	const uint32_t tbc = l->blits.length;
	gui_blit *bs = l->blits.buffer;
	for (uint32_t i = 0; i < tbc; i++) {
		texture_bind(bs->texture,blitShaderTextureIndex);
		const uint32_t bbc = bs->batches.length;
		gui_blit_batch *bc = bs->batches.buffer;
		for (uint32_t k = 0; k < bbc; k++) {
			ubp_memory_bind(bc->ubm,blitShaderMaterialIndex);
			glDrawArraysInstanced(GL_TRIANGLE_FAN, 0, 4, bc->ids.length);
			ubp_memory_unbind();
			bc++;
		}
		texture_unbind();
		bs++;
	}
}

void free_gui(gui *p) {
	const uint32_t nb = p->blits.length;
	gui_blit *bs = p->blits.buffer;
	for (uint32_t i = 0; i < nb; i++) {
		const uint32_t nt = bs->batches.length;
		gui_blit_batch *bt = bs->batches.buffer;
		for (uint32_t k = 0; k < nt; k++) {
			ubp_free(bt->ubm);
			delete_list(&bt->ids);
			bt++;
		}
		delete_list(&bs->batches);
		bs++;
	}
	delete_list(&p->blits);
}
void init_gui(gui *o) {
	new_list(&o->blits, sizeof(gui_blit));
	o->hovered = NULL;
	o->selected = NULL;
	o->dragging = false;
	o->hoveredDepth = 0.0f;
	o->viewX = 0.0f;
	o->viewY = 0.0f;
	o->viewWidth = 1.0f;
	o->viewHeight = 1.0f;
	o->viewAspect = screenAspect[0];
	o->renderTarget = &screenRenderTexture;
	o->base.render = &gui_render;
	o->base.free = &free_gui;
}

#define REFLECT_TARGET gui
#define gui_fieldCount 4
const reflection_data gui_fields[gui_fieldCount] = {RCF(float,viewX),RCF(float,viewY),RCF(float,viewWidth),RCF(float,viewHeight)};
#undef REFLECT_TARGET
define_component(gui,gui_fields,gui_fieldCount);
#undef LAST_COMPONENT_DATA
#define LAST_COMPONENT_DATA &component_data_gui


void gui_component_updateContainer(gui_component *c) {
	object *obj = c->base.object, *pob = obj->parent;
	vec4 cr;
	*(vec2*)&cr = *(vec2*)&pob->absolutePosition;
	((vec2*)&cr)[1] = *(vec2*)&pob->absoluteScale;
	if (c->parentContainer != NULL) cr = vec4_rectangleCombine(cr,((gui_component*)c->parentContainer)->container);
	c->container = cr;
	
	c->culled = fabs(obj->absolutePosition.x-cr.x)-obj->absoluteScale.x-cr.z > 0.0f || 
	fabs(obj->absolutePosition.y-cr.y)-obj->absoluteScale.y-cr.w > 0.0f;
}
void gui_component_findTargetGUI(gui_component *c) {
	object *obj = c->base.object;
	list gcstr = static_list(component_data_gui_component.name, 3, 1);
	c->targetGUI = (gui*)object_get(obj,&gcstr);
	gcstr.length = 4;
	c->parentContainer = object_getClosest(obj->parent,&gcstr);

	if (c->targetGUI != NULL) {
		if (c->interactable) c->base.update = &gui_component_update;
	}
}
void gui_component_enable(void *o) {
	gui_component *c = o;
	gui_component_findTargetGUI(c);
	asset_database_load(&assets, c->textureAssetHash, &c->textureAsset, &c->texture);
	if (c->targetGUI != NULL) gui_component_transformUpdate(c);
}

#endif
