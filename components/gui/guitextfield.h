﻿/*Copyright 2019 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _GUI_TEXTFIELD_H_
#define _GUI_TEXTFIELD_H_

#include "guitext.h"

typedef struct {
	gui_text base;
	uint32_t limit,selectionBegin;
	uint64_t lastClickTime;
	one_arg_function onChange;
	bool selected,multiline;
} gui_textfield;

void gui_textfield_update(void *p) {
	gui_textfield *t = p;
	gui_component_update(p);
	
	if (!t->base.base.interactable) return;

	gui *targetGUI = t->base.base.targetGUI;
	if (targetGUI->selected == p) {
		if (t->selected) {
			if (!modifyingTextInput) modifyingTextInput = true;

			if (textInputModified) {
				textInputModified = false;
				while (ATOMIC_SWAP(&textInputLock,0,1) == 1);
				if (t->limit != 0 && textInput.length > t->limit) {
					textInput.length = t->limit;
					textInputCursor = t->limit;
				}
				list_copy(&t->base.text, &textInput);
				t->base.cursorIndex = textInputCursor;
				t->base.cursorSelection = textInputSelection;
				textInputLock = 0;

				if (!t->base.multiline) {
					float as = screenAspect[0], ox = t->base.cursorPos.x-t->base.textContainer.x*as, ws = (t->base.textContainer.z-t->base.padding.x*oneDivScreenWidth[0]*t->base.fontSize)*as;
					if (ox > ws) ox -= ws;
					else if (ox < -ws) ox += ws;
					else ox = 0.0f;
					t->base.offset.x -= ox;
				}
				if (t->onChange != NULL) t->onChange(t);
				gui_text_updateText(&t->base);
			}
		} else {
			while (ATOMIC_SWAP(&textInputLock,0,1) == 1);
			list_copy(&textInput, &t->base.text);
			textInputLock = 0;

			textInputMultiline = t->multiline;
			textInputNavigation = true;
			t->selected = true;
			t->base.renderCursor = true;

			gui_text_updateText(&t->base);
		}
	} else {
		if (t->selected) {
			t->base.renderCursor = false;
			t->base.offset.x = 0.0f;
			gui_text_updateText(&t->base);

			modifyingTextInput = false;
			t->selected = false;
		}
	}
}

void gui_textfield_screenToTextPosition(gui_textfield *t, vec2 screenp, vec2 *textp) {
	object *o = t->base.base.base.object;
	const float aspect = screenAspect[0], asx = o->absoluteScale.x, asy = o->absoluteScale.y;
	const uint8_t anchor = t->base.anchor;
	if (anchor == ANCHOR_TOP_LEFT) {
		screenp.x -= t->base.fontSize*oneDivScreenHeight[0]*t->base.padding.x*0.5f;
	} else {
		const int32_t anchorX = ANCHOR_X(anchor),
				anchorY = ANCHOR_Y(anchor);
		const float fscale = t->base.fontSize*oneDivScreenHeight[0]*t->base.padding.y;
		const uint32_t llen = t->base.lineLengths.length;
		screenp.x -= asx*(float)(1+anchorX);
		screenp.y += (asy-fscale*0.5f)*(float)(1-anchorY);
		if (anchorY == 0) {
			screenp.y += t->base.lastLineY*0.5f;
		} else if (anchorY < 0) {
			screenp.y += t->base.lastLineY;
		}
		if (llen != 0) {
			float *lineLens = t->base.lineLengths.buffer;
			uint32_t tline = (uint32_t)fmaxf(0.0f,screenp.y/-fscale-1.0f);
			if (tline >= llen) tline = llen-1;
			if (anchorX == 0) {
				screenp.x += lineLens[tline]*0.5f;
			} else if (anchorX > 0) {
				screenp.x += lineLens[tline];
			}
		}
	}
	textp->x = screenp.x-t->base.offset.x+(asx-o->absolutePosition.x)*aspect;
	textp->y = screenp.y-t->base.offset.y-asy-o->absolutePosition.y;
}

void gui_textfield_click(void *p) {
	gui_textfield *t = p;
	if (!t->base.base.interactable) return;
	if (currentTime < t->lastClickTime+200) {
		t->base.cursorIndex = t->selectionBegin = textInputCursor = 0;
		t->base.cursorSelection = textInputSelection = t->base.text.length;
	} else {
		vec2 mp;
		gui_textfield_screenToTextPosition(t,(vec2){nMouseX,nMouseY},&mp);
		gui_text_size(t->base.fontSize, t->base.font, t->base.padding, &t->base.text, t->base.base.base.object->absoluteScale.x, NULL, NULL, t->base.multiline, NULL, mp, &textInputCursor);
		t->base.cursorIndex = t->selectionBegin = textInputCursor;
		t->base.cursorSelection = textInputSelection = 0;
	}
	t->lastClickTime = currentTime;
	t->base.renderCursor = true;
	gui_text_updateText(&t->base);
}
void gui_textfield_drag(void *p) {
	gui_textfield *t = p;
	if (!t->base.base.interactable || currentTime < t->lastClickTime+200) return;
	uint32_t cursor;
	vec2 mp;
	gui_textfield_screenToTextPosition(t,(vec2){nMouseX,nMouseY},&mp);
	gui_text_size(t->base.fontSize, t->base.font, t->base.padding, &t->base.text, t->base.base.base.object->absoluteScale.x, NULL, NULL, t->base.multiline, NULL, mp, &cursor);
	uint32_t start = t->selectionBegin;
	if (cursor < start) {
		uint32_t tmp = cursor;
		cursor = start;
		start = tmp;
	}
	textInputCursor = t->base.cursorIndex = start;
	textInputSelection = t->base.cursorSelection = cursor-start;
	gui_text_updateText(&t->base);
}

void gui_textfield_select(gui_textfield *t, const uint32_t cursor, const uint32_t selection) {
	t->base.cursorIndex = cursor;
	t->base.cursorSelection = selection;
	gui_text_updateText(&t->base);
	if (t->selected) {
		textInputCursor = cursor;
		textInputSelection = selection;
	}
}

void gui_textfield_transformUpdate(void *t) {
	gui_component_transformUpdate(t);
	gui_text_updateContainer(t);
	gui_text_updateText(t);
}

void gui_textfield_enable(void *p) {
	gui_component_enable(p);
	gui_text_enable(p);

	gui_textfield *t = p;
	if (t->base.base.targetGUI != NULL) t->base.base.base.update = &gui_textfield_update;
}

void init_gui_textfield(gui_textfield *o) {
	init_gui_text(&o->base);

	o->base.base.base.enable = &gui_textfield_enable;
	o->base.base.base.transformUpdate = (void*)&gui_textfield_transformUpdate;
	o->base.base.interactable = true;
	o->base.base.click = &gui_textfield_click;
	o->base.base.drag = &gui_textfield_drag;

	o->limit = 0;
	o->selected = false;
	o->multiline = false;
	o->onChange = NULL;
	o->lastClickTime = 0;
}

#define REFLECT_TARGET gui_textfield
#define gui_textfield_fieldCount gui_component_fieldCount+gui_text_fieldCount+1
const reflection_data gui_textfield_fields[gui_textfield_fieldCount] = {RCF(uint32_t,limit),
#undef REFLECT_TARGET
#define REFLECT_TARGET gui_text
gui_text_fieldsdef,
#undef REFLECT_TARGET
#define REFLECT_TARGET gui_component
gui_component_fieldsdef
};
#undef REFLECT_TARGET
define_componentd(gui_textfield,gui_textfield_fields,gui_textfield_fieldCount,duplicate_gui_text);
#undef LAST_COMPONENT_DATA
#define LAST_COMPONENT_DATA &component_data_gui_textfield

#endif
