﻿/*Copyright 2018 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _GUI_DRAGGABLE_H_
#define _GUI_DRAGGABLE_H_

#include "gui.h"


typedef struct {
	gui_component base;
	bool freezeX, freezeY, clampX, clampY;
} gui_draggable;

void gui_draggable_drag(void *p) {
	gui_draggable *o = p;

	object *obj = o->base.base.object,
		   *pob = obj->parent;

	vec3 npos;
	if (o->freezeX) {
		npos.x = obj->position.x;
	} else {
		npos.x = obj->position.x+mouseDeltaX/(float)engineWindow.width*2.0f/screenAspect[0]/pob->absoluteScale.x;
		if (o->clampX) {
			float tx = (pob->absoluteScale.x-obj->absoluteScale.x)*screenAspect[0];
			if (npos.x > tx) npos.x = tx; 
			else if (npos.x < -tx) npos.x = -tx;
		}
	}
	if (o->freezeY) {
		npos.y = obj->position.y;
	} else {
		npos.y = obj->position.y-mouseDeltaY/(float)engineWindow.height*2.0f/pob->absoluteScale.y;
		if (o->clampY) {
			float ty = pob->absoluteScale.y-obj->absoluteScale.y;
			if (npos.y > ty) npos.y = ty;
			else if (npos.y < -ty) npos.y = -ty;
		}
	}
	npos.z = obj->position.z;

	obj->position = npos;
	object_updateTransform(obj);
}

void init_gui_draggable(gui_draggable *o) {
	init_gui_component(&o->base);

	o->freezeX = false;
	o->freezeY = false;
	o->clampX = false;
	o->clampY = false;

	o->base.interactable = true;
	o->base.drag = &gui_draggable_drag;
}

#define REFLECT_TARGET gui_draggable
#define gui_draggable_fieldCount 4+gui_component_fieldCount
const reflection_data gui_draggable_fields[gui_draggable_fieldCount] = {RCF(uint8_t,freezeX),RCF(uint8_t,freezeY),RCF(uint8_t,clampX),RCF(uint8_t,clampY),
#undef REFLECT_TARGET
#define REFLECT_TARGET gui_component
gui_component_fieldsdef
};
#undef REFLECT_TARGET
define_component(gui_draggable,gui_draggable_fields,gui_draggable_fieldCount);
#undef LAST_COMPONENT_DATA
#define LAST_COMPONENT_DATA &component_data_gui_draggable

#endif