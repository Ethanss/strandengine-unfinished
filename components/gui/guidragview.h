﻿/*Copyright 2018 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _GUI_DRAGVIEW_H_
#define _GUI_DRAGVIEW_H_

#include "gui.h"


typedef struct {
	gui_component base;
	float minX,maxX,minY,maxY;
} gui_dragview;

void gui_dragview_move(gui_dragview *c, vec2 op) {
	object *o = c->base.base.object;

	float minX = c->minX, maxX = c->maxX,
		minY = c->minY, maxY = c->maxY;
	bool clampedX = false,
		clampedY = false;
	uint32_t clampix = 0, clampiy = 0;

	//apply drag position and check for limits
	uint32_t nl = o->children.length;
	object **child = o->children.buffer;
	for (uint32_t i = 0; i < nl && (!clampedX || !clampedY); i++) {
		object *cc = *child;
		if (!clampedX) {
			float xv = cc->position.x + op.x;
			cc->position.x = xv;
			if (xv < minX || xv > maxX) {
				clampedX = true;
				clampix = i + 1;
			}
		}
		if (!clampedY) {
			float yv = cc->position.y + op.y;
			cc->position.y = yv;
			if (yv < minY || yv > maxY) {
				clampedY = true;
				clampiy = i + 1;
			}
		}
		child++;
	}

	//unapply drag when clamped
	if (clampedX) {
		child = o->children.buffer;
		for (uint32_t i = 0; i < clampix; i++) (*child++)->position.x -= op.x;
	}
	if (clampedY) {
		child = o->children.buffer;
		for (uint32_t i = 0; i < clampiy; i++) (*child++)->position.y -= op.y;
	}

	//update transforms
	if (!clampedX || !clampedY) {
		child = o->children.buffer;
		for (uint32_t i = 0; i < nl; i++) object_updateTransform(*child++);
	}
}
void gui_dragview_drag(void *p) {
	gui_dragview *c = p;
	object *o = c->base.base.object;

	//drag vars
	vec2 op;
	op.x = mouseDeltaX*2.0f/engineWindow.aspect*engineWindow.oneDivWidth/o->absoluteScale.x;
	op.y = mouseDeltaY*-2.0f*engineWindow.oneDivHeight/o->absoluteScale.y;

	gui_dragview_move(p,op);
}

void init_gui_dragview(gui_dragview *o) {
	init_gui_component(&o->base);

	o->minX = -1e6f;
	o->maxX = 1e6f;
	o->minY = -1e6f;
	o->maxY = 1e6f;

	o->base.interactable = true;
	o->base.drag = &gui_dragview_drag;
}

#define REFLECT_TARGET gui_dragview
#define gui_dragview_fieldCount 4+gui_component_fieldCount
const reflection_data gui_dragview_fields[gui_dragview_fieldCount] = {RCF(float,minX),RCF(float,maxX),RCF(float,minY),RCF(float,maxY),
#undef REFLECT_TARGET
#define REFLECT_TARGET gui_component
gui_component_fieldsdef
};
#undef REFLECT_TARGET
define_component(gui_dragview,gui_dragview_fields,gui_dragview_fieldCount);
#undef LAST_COMPONENT_DATA
#define LAST_COMPONENT_DATA &component_data_gui_dragview

#endif