/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _RENDERING_H_
#define _RENDERING_H_

#include "../../engine.h"
#include "../../material.h"

#ifndef RENDERER_QUEUE_COUNT
#define RENDERER_QUEUE_COUNT 8
#endif
#define RENDERER_BATCH_COUNT 340

typedef struct {
	uint32_t *id[RENDERER_BATCH_COUNT];
	ubp_memory ubm;
	uint16_t count;
} renderer_batch;

typedef struct {
	list children;
	void *object, *parent;
} renderer_qnode;

typedef struct {
	vec3 position, scale;
	vec2 rotationX;
	vec4 rotationYZ;
} graphic_data;

typedef struct {
	component base;
	material *material, *activeMaterial;
	mesh *mesh, *activeMesh;
	asset *materialAsset, *meshAsset;
	renderer_qnode *qnode;
	uint32_t materialAssetHash, meshAssetHash, batchID;
	float cullingRadius;
	uint8_t queue;
	bool updated;
} graphic;

typedef struct {
	vec4 rotationXY;
	vec2 rotationZ;
	vec3 position;
	float viewX,viewY,viewWidth,viewHeight,viewNear,viewFar,projection,fieldOfView,screenAspect,screenHeight;
	uint32_t time;
} renderer_data;

typedef struct {
	component base;
	ifunction onRender[RENDERER_QUEUE_COUNT+1];
	rendertexture *target;
	float viewX,viewY,viewWidth,viewHeight,fieldOfView,viewNear,viewFar;
	uint32_t queueMask;
	bool projection;
} renderer;

renderer *screenRenderer = NULL;
list renderer_queue[RENDERER_QUEUE_COUNT];
ubp_memory renderer_dataUBM;
bool renderer_queue_initialized = false;
#ifdef ENGINE_EDITMODE
vec3 editorRendererViewPosition,editorRendererViewRotation;
bool editorRendererViewEnabled = false;
#endif

static inline int32_t new_renderer_qnode(void *o, uint32_t csz, void *po, list *p) {
	renderer_qnode *n = malloc(sizeof(renderer_qnode));
	new_list(&n->children, csz);
	n->object = o;
	n->parent = po;
	return list_add(p, &n);
}
int32_t find_renderer_qnode(list *l, void *o) {
	uint32_t nl = l->length;
	renderer_qnode **e = l->buffer;
	for (uint32_t i = 0; i < nl; i++) {
		if (e[0]->object == o) return i;
		e++;
	}
	return -1;
}

void setup_renderer(renderer *r, float viewX, float viewY, float viewWidth, float viewHeight, float viewNear, float viewFar, float fieldOfView, uint32_t queueMask, bool projection) {
	r->viewX = viewX;
	r->viewY = viewY;
	r->viewWidth = viewWidth;
	r->viewHeight = viewHeight;
	r->viewNear = viewNear;
	r->viewFar = viewFar;
	r->fieldOfView = fieldOfView;
	r->queueMask = queueMask;
	r->projection = projection;
	for (uint32_t i = 0; i < RENDERER_QUEUE_COUNT+1; i++) r->onRender[i].delegate = NULL;
}

void renderer_render(void *p) {
	renderer *o = p;
	if (!renderer_queue_initialized) return;
	
	rendertexture_bind(o->target, o->viewX, o->viewY, o->viewWidth, o->viewHeight);
	{
		object *obj= o->base.object;
		vec3 rot = obj->absoluteRotation;

		renderer_data rdata = (renderer_data){{sinf(rot.x),cosf(rot.x),sinf(rot.y),cosf(rot.y)},{sinf(rot.z),cosf(rot.z)},obj->absolutePosition,o->viewX,o->viewY,o->viewWidth,o->viewHeight,o->viewNear,o->viewFar,o->projection?1.0f:0.0f,o->fieldOfView,*screenAspect,*screenHeight,(uint32_t)currentTime};
		#ifdef ENGINE_EDITMODE
		if (editorRendererViewEnabled && o == screenRenderer) {
			rdata.rotationXY = (vec4){sinf(editorRendererViewRotation.x),cosf(editorRendererViewRotation.x),sinf(editorRendererViewRotation.y),cosf(editorRendererViewRotation.y)};
			rdata.rotationZ = (vec2){sinf(editorRendererViewRotation.z),cosf(editorRendererViewRotation.z)};
			rdata.position = editorRendererViewPosition;
			rdata.viewX = 0.0f;
			rdata.viewY = 0.0f;
			rdata.viewWidth = 1.0f;
			rdata.viewHeight = 1.0f;
		}
		#endif
		ubp_memory_set(renderer_dataUBM,&rdata,sizeof(renderer_data));
	}

	const uint32_t qmask = o->queueMask;
	for (uint32_t q = 0; q < RENDERER_QUEUE_COUNT; q++) {
		if (((qmask>>q)&1) == 0) continue;
		
		if (o->onRender[q].delegate != NULL) o->onRender[q].delegate(o->onRender[q].owner);
		
		list *matqn = renderer_queue+q;
		renderer_qnode **matb = matqn->buffer;
		for (uint32_t mi = 0; mi < matqn->length; mi++) {
			material *mat = matb[0]->object;
			list *mshqn = &matb[0]->children;
			renderer_qnode **meshb = mshqn->buffer;
			const GLenum matGeo = RENDERER_GEOMETRY_OPENGL[mat->geometryType];
			const GLuint tUBI = mat->transformUBI;
			material_bind(mat);
			for (uint32_t k = 0; k < mshqn->length; k++) {
				mesh *msh = meshb[0]->object;
				list *trqn = &meshb[0]->children;
				renderer_batch *trb = trqn->buffer;
				const bool mshIndexed = (msh->flags&VERTEX_INDEXED) != 0;
				const uint32_t pcount = mshIndexed ? msh->numIndices : msh->numVertices;
				mesh_bind(msh);
				for (uint32_t t = 0; t < trqn->length; t++) {
					if (tUBI != GL_INVALID_INDEX) ubp_memory_bind(trb->ubm, tUBI);
					if (mshIndexed) glDrawElementsInstanced(matGeo,pcount,GL_UNSIGNED_INT,NULL,trb->count);
					else glDrawArraysInstanced(matGeo, 0, pcount, trb->count);
					if (tUBI != GL_INVALID_INDEX) ubp_memory_unbind();
					trb++;
				}
				meshb++;
			}
			material_unbind(mat);
			matb++;
		}
	}
	if (o->onRender[RENDERER_QUEUE_COUNT].delegate != NULL) o->onRender[RENDERER_QUEUE_COUNT].delegate(o->onRender[RENDERER_QUEUE_COUNT].owner);
	
	if (screenRenderer == NULL) {
		if (activeRenderTexture == &screenRenderTexture) screenRenderer = o;
	} else if (o == screenRenderer && activeRenderTexture != &screenRenderTexture) {
		screenRenderer = NULL;
	}
}

void renderer_renderToTexture(renderer *r, rendertexture *target, const vec3 cameraPos, const vec3 cameraRotation) {
	object sobj;
	sobj.absolutePosition = cameraPos;
	sobj.absoluteRotation = cameraRotation;
	r->base.object = &sobj;
	r->target = target;
	renderer_render(r);
}

void renderer_free(void *p) {
	if (screenRenderer == p) screenRenderer = NULL;
}
void init_renderer(renderer *o) {
	o->projection = true;
	o->target = &screenRenderTexture;
	o->queueMask = UINT_MAX;
	o->viewX = 0.0f;
	o->viewY = 0.0f;
	o->viewWidth = 1.0f;
	o->viewHeight = 1.0f;
	o->viewNear = 1e-4f;
	o->viewFar = 1000.0f;
	o->fieldOfView = 1.0f;
	for (uint32_t i = 0; i < RENDERER_QUEUE_COUNT+1; i++) o->onRender[i].delegate = NULL;
	o->base.render = &renderer_render;
	o->base.free = &renderer_free;
}

#define REFLECT_TARGET renderer
#define renderer_fieldCount 9
const reflection_data renderer_fields[renderer_fieldCount] = {RCF(float,viewX),RCF(float,viewY),RCF(float,viewWidth),RCF(float,viewHeight),RCF(float,viewNear),RCF(float,viewFar),RCF(float,fieldOfView),RCF(uint32_t,queueMask),RCF(uint8_t,projection)};
#undef REFLECT_TARGET

define_component(renderer,renderer_fields,renderer_fieldCount);
#undef LAST_COMPONENT_DATA
#define LAST_COMPONENT_DATA &component_data_renderer


void graphic_freeBatchID(graphic *o) {
	uint32_t bid = o->batchID, id, lid, bs, lbs;
	if (bid != UINT_MAX) {
		renderer_qnode *qnd = o->qnode;
		bs = bid / RENDERER_BATCH_COUNT;
		id = bid - bs*RENDERER_BATCH_COUNT;
		lbs = qnd->children.length - 1;
		
		renderer_batch *rb = (renderer_batch*)qnd->children.buffer + bs, 
			*lrb = (renderer_batch*)qnd->children.buffer + lbs;
		lid = lrb->count-1;

		if (!(bs == lbs && id == lid)) {
			uint32_t *bptr = lrb->id[lid];
			rb->id[id] = bptr;
			*bptr = bid;
			ubp_memory_copyArray(lrb->ubm, lid * sizeof(graphic_data), rb->ubm, id * sizeof(graphic_data), 1, sizeof(graphic_data));
		}
		
		if (lid == 0) {
			ubp_free(lrb->ubm);
			qnd->children.length--;
			if (qnd->children.length == 0) {
				renderer_qnode *pn = qnd->parent;
				list_remove(&pn->children, &qnd);
				delete_list(&qnd->children);				
				free(qnd);
				if (pn->children.length == 0) {
					list_remove(pn->parent, &pn);
					delete_list(&pn->children);
					free(pn);
				}
			}
		}
		else lrb->count = lid;

		o->batchID = UINT_MAX;
	}
}
void graphic_updateRendererMain(void *p) {
	graphic *g = p;
	g->updated = false;
	
	if (!renderer_queue_initialized) {
		for (uint32_t i = 0; i < RENDERER_QUEUE_COUNT; i++) new_list(renderer_queue+i,sizeof(void*));
		renderer_dataUBM = ubp_alloc(sizeof(renderer_data));
		shader_setGlobalData("RendererBlock",renderer_dataUBM);
		renderer_queue_initialized = true;
	}
	
	if (g->base.enabled && g->material != NULL && g->mesh != NULL) {
		if (g->material != g->activeMaterial || g->mesh != g->activeMesh) {
			graphic_freeBatchID(g);
			g->activeMaterial = g->material;
			g->activeMesh = g->mesh;
		}
		
		uint32_t bid = g->batchID, lid;
		renderer_batch *rb;
		if (bid == UINT_MAX) {
			list *rq = renderer_queue+g->queue;

			int32_t ri = find_renderer_qnode(rq, g->activeMaterial);
			if (ri == -1) ri = new_renderer_qnode(g->activeMaterial, sizeof(void*), rq, rq);

			renderer_qnode *tempq = ((renderer_qnode**)rq->buffer)[ri];
			rq = &tempq->children;
			ri = find_renderer_qnode(rq, g->activeMesh);
			if (ri == -1) ri = new_renderer_qnode(g->activeMesh, sizeof(renderer_batch), tempq, rq);

			g->qnode = ((renderer_qnode**)rq->buffer)[ri];
			rq = &g->qnode->children;
			rb = rq->buffer;
			uint32_t rql = rq->length, rqi = rql-1;
			if (rql == 0 || rb[rqi].count == RENDERER_BATCH_COUNT) {
				list_addEmpty(rq);
				rqi = rql;
				rb = rq->buffer;
				rb += rql;
				rb->ubm = ubp_alloc(sizeof(graphic_data) * RENDERER_BATCH_COUNT);
				rb->id[0] = &g->batchID;
				rb->count = 1;
				lid = 0;
			}
			else {
				rb += rqi;
				lid = rb->count++;
				rb->id[lid] = &g->batchID;
			}
			g->batchID = lid + rqi*RENDERER_BATCH_COUNT;
		}
		else {
			uint32_t bs = bid / RENDERER_BATCH_COUNT;
			rb = (renderer_batch*)g->qnode->children.buffer + bs;
			lid = bid - bs*RENDERER_BATCH_COUNT;
		}

		object *go = g->base.object;
		graphic_data tdb;
		tdb.position = go->absolutePosition;
		tdb.scale = go->absoluteScale;
		vec3 ar = go->absoluteRotation;
		tdb.rotationX = (vec2){ sinf(ar.x), cosf(ar.x) };
		tdb.rotationYZ = (vec4){ sinf(ar.y), cosf(ar.y), sinf(ar.z), cosf(ar.z) };
		ubp_memory_setArray(rb->ubm, lid * sizeof(graphic_data), &tdb, sizeof(graphic_data));
	} else {
		if (g->batchID != UINT_MAX) {
			graphic_freeBatchID(g);
			g->activeMaterial = NULL;
			g->activeMesh = NULL;
		}
	}
}
void graphic_transformUpdate(void *p) {
	graphic *o = p;
	if (o->mesh != NULL && o->material != NULL && !o->updated) {
		o->updated = true;
		singleRenderCall(graphic_updateRendererMain,p);
	}
}
void graphic_enable(void *p) {
	graphic *o = p;
	asset_database_load(&assets, o->materialAssetHash, &o->materialAsset, &o->material);
	asset_database_load(&assets, o->meshAssetHash, &o->meshAsset, &o->mesh);
	if (o->updated || o->material == NULL || o->mesh == NULL) return;
	o->updated = true;
	singleRenderCall(graphic_updateRendererMain,p);
}
void init_graphic(graphic *o) {
	o->materialAsset = NULL;
	o->materialAssetHash = NULL_HASH;
	o->material = NULL;
	o->activeMaterial = NULL;
	o->meshAsset = NULL;
	o->meshAssetHash = NULL_HASH;
	o->mesh = &quadMesh;
	o->activeMesh = NULL;
	o->queue = 0;
	o->batchID = UINT_MAX;
	o->cullingRadius = 0.0f;
	o->updated = false;
	o->base.enable = &graphic_enable;
	o->base.disable = &graphic_transformUpdate;
	o->base.transformUpdate = &graphic_transformUpdate;
}

#define REFLECT_TARGET graphic
#define graphic_fieldCount 3
const reflection_data graphic_fields[graphic_fieldCount] = {RCF(uint32_t,materialAssetHash),RCF(uint32_t,meshAssetHash),RCF(uint8_t,queue)};
#undef REFLECT_TARGET

define_component(graphic, graphic_fields, graphic_fieldCount);
#undef LAST_COMPONENT_DATA
#define LAST_COMPONENT_DATA &component_data_graphic

#endif
