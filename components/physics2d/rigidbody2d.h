﻿/*Copyright 2019 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _RIGIDBODY2D_H_
#define _RIGIDBODY2D_H_

#include "../../engine.h"
#include <float.h>

#ifndef PHYSICS2D_GRIDSIZE
#define PHYSICS2D_GRIDSIZE 20
#endif
#ifndef PHYSICS2D_GRIDGAP
#define PHYSICS2D_GRIDGAP 100.0f
#endif
#define PHYSICS2D_GRIDTSZ (PHYSICS2D_GRIDSIZE*PHYSICS2D_GRIDSIZE)
#define PHYSICS2D_GRIDLEFT (PHYSICS2D_GRIDSIZE*PHYSICS2D_GRIDGAP*-0.5f)

typedef struct {
	component base;
	bool isCollider;
	vec2 velocity;
	float radius,epsilon,mass,friction,bounce;
	uint32_t collisionMask,gridPosition;
	void *jointSystem;
	three_arg_function onCollision;//(*rigidbody2d this,*collider2d hit,*vec2 normal)
} rigidbody2d;

typedef struct {
	rigidbody2d *body1, *body2;
	float radius,strength;
} joint2d;
typedef struct {
	joint2d *joints;
	uint32_t count, activeCount, steps;
} jointsystem2d;

typedef struct {
	void *collider;
	float distance;
} dsample2d;


#ifdef PHYSICS2D_CUSTOMGRAVITY
two_arg_function physics2DGravity;//(*vec3 position, *vec2 velocity)
#elif PHYSICS2D_NOGRAVITY
#else
vec2 physics2DGravity = {0.0f,0.0f};
#endif
list physics2DBodies = construct_list(sizeof(void*)),
physics2DColliders = construct_list(sizeof(void*)),
physics2DGrid[PHYSICS2D_GRIDTSZ],
physics2DJoints = construct_list(sizeof(void*));

void physics2D_init();


#include "collider2d.h"


void new_jointsystem2d(jointsystem2d *js, const uint32_t count) {
	js->joints = malloc(sizeof(joint2d)*count);
	js->count = count;
	js->activeCount = 0;
	js->steps = 10;
}
void delete_jointsystem2d(jointsystem2d *js) {
	free(js->joints);
}
void jointsystem2d_enable(jointsystem2d *js) {
	list_add(&physics2DJoints,&js);
}
void jointsystem2d_disable(jointsystem2d *js) {
	list_remove(&physics2DJoints,&js);
}


void updatePhysics2D(void *na) {
	rigidbody2d **bods = physics2DBodies.buffer;
	const uint32_t nbod = physics2DBodies.length;
	for (uint32_t b = 0; b < nbod; b++) {
		rigidbody2d *rb = *bods++;
		object *ob = rb->base.object;
	
		vec2 vel = rb->velocity,mov;
		mov.x = vel.x*deltaTime;
		mov.y = vel.y*deltaTime;
		float msq = mov.x*mov.x+mov.y*mov.y;
		if (msq > 0.0f) {
			const float rad = rb->radius;
			const uint32_t cmask = rb->collisionMask;
			vec2 ap = *(vec2*)&ob->position;
			msq = sqrtf(msq);
			const float mscal = 1.0f/msq;
			mov.x *= mscal;
			mov.y *= mscal;
			while (msq > 0.0f) {
				rigidbody2d *collider;
				float dist = physics2D_distance(ap,cmask,rb,&collider)-rad;
				if (dist <= 0.0f) {
					vec2 nrm;
					if (collider->isCollider) {
						collider2d *c2d = collider;
						nrm = collider2d_normal(c2d,ap);
						vec2 rv;
						const float vdp = mov.x*nrm.x+mov.y*nrm.y,
						bounce = rb->bounce*c2d->bounce,
						friction = 1.0f-fmin(1.0f,rb->friction*c2d->friction)*(1.0f+vdp),
						vl = vec2_length(&vel)*-vdp*(1.0f+bounce);
						rv.x = nrm.x*vl;
						rv.y = nrm.y*vl;
						vel.x = (vel.x+rv.x)*friction;
						vel.y = (vel.y+rv.y)*friction;
						if (c2d->onCollision) c2d->onCollision(c2d,rb,&nrm);
					} else {
						rigidbody2d *r2d = collider;
						object *r2o = r2d->base.object;
						vec2 vel2 = r2d->velocity,mov2 = vel2,veldiff;
						nrm.x = ap.x-r2o->position.x;
						nrm.y = ap.y-r2o->position.y;
						vec2_normalize(&nrm);
						const float vl1 = vec2_length(&vel), vl2 = vec2_length(&vel2);
						vec2_mulSingle(&mov2,1.0f/vl2);
						
						const float mdiff = r2d->mass/rb->mass,
						mscale1 = fmin(1.0f,mdiff),
						mscale2 = fmin(1.0f,1.0f/mdiff),
						bounce = rb->bounce*r2d->bounce,
						bounce1 = 1.0f+bounce*(1.0f-1.0f/fmax(1.0f,mdiff)),
						bounce2 = 1.0f+bounce*(1.0f-1.0f/fmax(1.0f,1.0f/mdiff)),
						vdp = vec2_dot(&mov,&nrm),
						vdp2 = vec2_dot(&mov2,&nrm),
						gfrict = fmin(1.0f,rb->friction*r2d->friction),
						friction = 1.0f-gfrict*(1.0f+vdp),
						friction2 = 1.0f-gfrict*(1.0f-vdp2),
						v1 = vl1*-vdp,
						v2 = vl2*-vdp2,
						dx = nrm.x*v1,
						dy = nrm.y*v1,
						dx2 = nrm.x*v2,
						dy2 = nrm.y*v2;
						
						vel.x = vel.x*friction+(dx*bounce1-dx2)*mscale1;
						vel.y = vel.y*friction+(dy*bounce1-dy2)*mscale1;
						r2d->velocity.x = vel2.x*friction2+(dx2*bounce2-dx)*mscale2;
						r2d->velocity.y = vel2.y*friction2+(dy2*bounce2-dy)*mscale2;
						if (r2d->onCollision) {
							vec2 fn = (vec2){-nrm.x,-nrm.y};
							r2d->onCollision(r2d,rb,&fn);
						}
					}
					if (rb->onCollision) rb->onCollision(rb,collider,&nrm);
					const float eps = 2.0f*rb->epsilon;
					ap.x += nrm.x*eps;
					ap.y += nrm.y*eps;
					msq = 0.0f;
				} else {
					const float mm = fmin(dist+rb->epsilon,msq);
					ap.x += mov.x*mm;
					ap.y += mov.y*mm;
					msq -= mm;
				}
			}
			ob->position.x = ap.x;
			ob->position.y = ap.y;
			int32_t gx = (ap.x-PHYSICS2D_GRIDLEFT)*(1.0f/PHYSICS2D_GRIDGAP),
					gy = (ap.y-PHYSICS2D_GRIDLEFT)*(1.0f/PHYSICS2D_GRIDGAP);
			uint32_t gid;
			if (gx < 0 || gx >= PHYSICS2D_GRIDSIZE || gy < 0 || gy >= PHYSICS2D_GRIDSIZE) gid = UINT_MAX;
			else gid = gx+gy*PHYSICS2D_GRIDSIZE;
			const uint32_t ogid = rb->gridPosition;
			if (gid != ogid) {
				if (ogid != UINT_MAX) list_remove(physics2DGrid+ogid,&rb);
				if (gid != UINT_MAX) list_add(physics2DGrid+gid,&rb);
				rb->gridPosition = gid;
			}
			object_updateTransform(ob);
		}
		
		#ifdef PHYSICS2D_NOGRAVITY
		#elif PHYSICS2D_CUSTOMGRAVITY
		physics2DGravity(&ob->position,&vel);
		#else
		vel.x += physics2DGravity.x;
		vel.y += physics2DGravity.y; 
		#endif
		rb->velocity = vel;
	}
	
	jointsystem2d **joints = physics2DJoints.buffer;
	const uint32_t njoint = physics2DJoints.length;
	for (uint32_t b = 0; b < njoint; b++) {
		jointsystem2d *js = *joints++;
		const uint32_t njb = js->activeCount, njs = js->steps;
		for (uint32_t step = 0; step < njs; step++) {
			joint2d *joints = js->joints;
			for (uint32_t k = 0; k < njb; k++) {
				rigidbody2d *b1 = joints->body1, *b2 = joints->body2;
				object *ob1 = b1->base.object, *ob2 = b2->base.object;
				vec2 v1 = b1->velocity, v2 = b2->velocity;
				float dx = (ob1->position.x+v1.x*deltaTime)-(ob2->position.x+v2.x*deltaTime),
					dy = (ob1->position.y+v2.y*deltaTime)-(ob2->position.y+v2.y*deltaTime),
					len = dx*dx+dy*dy;
				if (len != 0.0f) {
					len = sqrtf(len);
					len = (len-joints->radius)*joints->strength/len;
					dx *= len;
					dy *= len;
					b1->velocity.x = v1.x-dx;
					b1->velocity.y = v1.y-dy;
					b2->velocity.x = v2.x+dx;
					b2->velocity.y = v2.y+dy;
				}
				joints++;
			}
		}
	}
}


void rigidbody2d_enable(rigidbody2d *o) {
	list_add(&physics2DBodies,&o);
	object *ob = o->base.object;
	int32_t gx = (ob->absolutePosition.x-PHYSICS2D_GRIDLEFT)*(1.0f/PHYSICS2D_GRIDGAP),
			gy = (ob->absolutePosition.y-PHYSICS2D_GRIDLEFT)*(1.0f/PHYSICS2D_GRIDGAP);
	uint32_t gid;
	if (gx < 0 || gx >= PHYSICS2D_GRIDSIZE || gy < 0 || gy >= PHYSICS2D_GRIDSIZE) gid = UINT_MAX;
	else {
		gid = gx+gy*PHYSICS2D_GRIDSIZE;
		list_add(physics2DGrid+gid,&o);
	}
	o->gridPosition = gid;
}
void rigidbody2d_disable(rigidbody2d *o) {
	list_remove(&physics2DBodies,&o);
	const uint32_t gid = o->gridPosition;
	if (gid != UINT_MAX) list_remove(physics2DGrid+gid,&o);
}

void physics2D_init() {
	for (uint32_t i = 0; i < PHYSICS2D_GRIDTSZ; i++) new_list(physics2DGrid+i,sizeof(void*));
	cpifunction(updatePhysics2D,NULL,slist_add(&updateCalls,&ifunc));
}

void init_rigidbody2d(rigidbody2d *o) {
	o->velocity = (vec2){0.0f,0.0f};
	o->radius = 1.0f;
	o->epsilon = 1e-3f;
	o->mass = 1.0f;
	o->friction = 0.5f;
	o->bounce = 0.0f;
	o->collisionMask = UINT_MAX;
	o->gridPosition = UINT_MAX;
	o->isCollider = false;
	o->jointSystem = NULL;
	
	o->base.enable = rigidbody2d_enable;
	o->base.disable = rigidbody2d_disable;
	o->onCollision = NULL;
}

//component reflection
#define REFLECT_TARGET rigidbody2d
#define rigidbody2d_fieldCount 5
const reflection_data rigidbody2d_fields[rigidbody2d_fieldCount] = {RCF(float,radius),RCF(float,epsilon),RCF(float,mass),RCF(float,friction),RCF(float,bounce)};
#undef REFLECT_TARGET

//register component in engines linked list of components
define_component(rigidbody2d,rigidbody2d_fields,rigidbody2d_fieldCount);
#undef LAST_COMPONENT_DATA
#define LAST_COMPONENT_DATA &component_data_rigidbody2d

#endif
