﻿/*Copyright 2019 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _COLLIDER2D_H_
#define _COLLIDER2D_H_

enum {
	COLLIDER2D_CIRCLE = 0,
	COLLIDER2D_BOX = 1,
	COLLIDER2D_CUSTOM = 2
};

typedef struct {
	component base;
	bool isCollider;
	uint32_t gridPosition;
	three_arg_function onCollision,//(*collider2d this,*rigidbody2d hit,*vec2 normal)
	customDistance;//(*collider2d this, *vec2 position,*float distance)
	vec2 size;
	float friction,bounce;
	uint8_t type;
} collider2d;

vec2 collider2d_normal(collider2d *o, vec2 p) {
	const uint8_t ctype = o->type;
	if (ctype != COLLIDER2D_CUSTOM) {
		object *ob = o->base.object;
		p.x -= ob->absolutePosition.x;
		p.y -= ob->absolutePosition.y;
		if (ctype == COLLIDER2D_BOX) {
			const float rang = ob->absoluteRotation.z;
			vec2_rotInv(&p,rang);
			float xa = fabs(p.x)-o->size.x, ya = fabs(p.y)-o->size.y;
			if (xa < 0.0f) {
				if (ya < 0.0f) {
					if (xa < ya) ya = 0.0f;
					else xa = 0.0f;
				} else xa = 0.0f;
			} else if (ya < 0.0f) ya = 0.0f;
			p.x = copysign(xa,p.x);
			p.y = copysign(ya,p.y);
			vec2_rot(&p,rang);
		}
	} else {
		float bdst,dstx,dsty;
		three_arg_function dstf = o->customDistance;
		dstf(o,&p,&bdst);
		p.x += 1e-6f;
		dstf(o,&p,&dstx);
		p.x -= 1e-6f;
		p.y += 1e-6f;
		dstf(o,&p,&dsty);
		p.x = dstx-bdst;
		p.y = dsty-bdst;
	}
	vec2_normalize(&p);
	return p;
}

//returns nearest distance to collider/body from point p
float physics2D_distance(const vec2 p, const uint32_t cmask, rigidbody2d *exclude, void **nearest) {
	const bool excluding = exclude!=NULL;
	float mindst = PHYSICS2D_GRIDGAP;
	void *nearp;
	int32_t gx = (p.x-PHYSICS2D_GRIDLEFT)*(1.0f/PHYSICS2D_GRIDGAP),
			gy = (p.y-PHYSICS2D_GRIDLEFT)*(1.0f/PHYSICS2D_GRIDGAP);
	gy--;
	for (uint32_t y = 0; y < 3; y++) {
		if (gy < 0 || gy >= PHYSICS2D_GRIDSIZE) {
			gy++;
			continue;
		}
		int32_t ex = gx-1;
		for (uint32_t x = 0; x < 3; x++) {
			if (ex < 0 || ex >= PHYSICS2D_GRIDSIZE) {
				ex++;
				continue;
			}
			list *gl = physics2DGrid+(ex+gy*PHYSICS2D_GRIDSIZE);
			const uint32_t len = gl->length;
			rigidbody2d **lobj = gl->buffer;
			for (uint32_t i = 0; i < len; i++) {
				rigidbody2d *rb = *lobj++;
				object *aob = rb->base.object;
				if ((1<<aob->layer)&cmask) {
					if (rb->isCollider) {
						collider2d *tb = rb;
						const uint8_t ctype = tb->type;
						if (ctype == COLLIDER2D_CIRCLE) {
							vec2 pos = *(vec2*)&aob->absolutePosition;
							pos.x -= p.x;
							pos.y -= p.y;
							const float dst = sqrtf(pos.x*pos.x+pos.y*pos.y)-tb->size.x;
							if (dst < mindst) {
								mindst = dst;
								nearp = tb;
							}
						} else if (ctype == COLLIDER2D_BOX) {
							vec2 pos = *(vec2*)&aob->absolutePosition;
							pos.x -= p.x;
							pos.y -= p.y;
							vec2_rotInv(&pos,aob->absoluteRotation.z);
							pos.x = fmax(0.0f,fabs(pos.x)-tb->size.x);
							pos.y = fmax(0.0f,fabs(pos.y)-tb->size.y);
							const float dst = sqrtf(pos.x*pos.x+pos.y*pos.y);
							if (dst < mindst) {
								mindst = dst;
								nearp = tb;
							}
						} else {
							float dst;
							tb->customDistance(tb,&p,&dst);
							if (dst < mindst) {
								mindst = dst;
								nearp = tb;
							}
						}
					} else {
						vec2 pos = *(vec2*)&aob->position;
						pos.x -= p.x;
						pos.y -= p.y;
						const float dst = sqrtf(pos.x*pos.x+pos.y*pos.y)-rb->radius;
						if (dst < mindst) {
							if (excluding) {
								if (rb != exclude && rb->jointSystem != exclude->jointSystem) {
									mindst = dst;
									nearp = rb;
								}
							} else {
								mindst = dst;
								nearp = rb;
							}
						}
					}
				}
			}
			ex++;
		}
		gy++;
	}
	const uint32_t ncol = physics2DColliders.length;
	collider2d **cols = physics2DColliders.buffer;
	for (uint32_t i = 0; i < ncol; i++) {
		collider2d *tb = *cols++;
		object *aob = tb->base.object;
		if ((1<<aob->layer)&cmask) {
			const uint8_t ctype = tb->type;
			if (ctype == COLLIDER2D_CIRCLE) {
				vec2 pos = *(vec2*)&aob->absolutePosition;
				pos.x -= p.x;
				pos.y -= p.y;
				const float dst = sqrtf(pos.x*pos.x+pos.y*pos.y)-tb->size.x;
				if (dst < mindst) {
					mindst = dst;
					nearp = tb;
				}
			} else if (ctype == COLLIDER2D_BOX) {
				vec2 pos = *(vec2*)&aob->absolutePosition;
				pos.x -= p.x;
				pos.y -= p.y;
				vec2_rotInv(&pos,aob->absoluteRotation.z);
				pos.x = fmax(0.0f,fabs(pos.x)-tb->size.x);
				pos.y = fmax(0.0f,fabs(pos.y)-tb->size.y);
				const float dst = sqrtf(pos.x*pos.x+pos.y*pos.y);
				if (dst < mindst) {
					mindst = dst;
					nearp = tb;
				}
			} else {
				float dst;
				tb->customDistance(tb,&p,&dst);
				if (dst < mindst) {
					mindst = dst;
					nearp = tb;
				}
			}
		}
	}
	if (nearest != NULL) *nearest = nearp;
	return mindst;
}

void collider2d_transformUpdate(collider2d *o) {
	uint32_t gid = 0;
	if (o->type == COLLIDER2D_BOX) {
		if (o->size.x > PHYSICS2D_GRIDGAP*0.5f || o->size.y > PHYSICS2D_GRIDGAP*0.5f) gid = UINT_MAX;
	} else {
		if (o->size.x > PHYSICS2D_GRIDGAP*0.5f) gid = UINT_MAX;
	}
	if (gid == 0) {
		object *ob = o->base.object;
		int32_t gx = (ob->absolutePosition.x-PHYSICS2D_GRIDLEFT)*(1.0f/PHYSICS2D_GRIDGAP),
				gy = (ob->absolutePosition.y-PHYSICS2D_GRIDLEFT)*(1.0f/PHYSICS2D_GRIDGAP);
		if (gx < 0 || gx >= PHYSICS2D_GRIDSIZE || gy < 0 || gy >= PHYSICS2D_GRIDSIZE) gid = UINT_MAX;
		else gid = gx+gy*PHYSICS2D_GRIDSIZE;
	}
	const uint32_t ogid = o->gridPosition;
	if (gid != ogid) {
		if (ogid < UINT_MAX-1) list_remove(physics2DGrid+ogid,&o);
		else if (ogid == UINT_MAX) list_remove(&physics2DColliders,&o);
		if (gid < UINT_MAX-1) list_add(physics2DGrid+gid,&o);
		else if (gid == UINT_MAX) list_add(&physics2DColliders,&o);
		o->gridPosition = gid;
	}
}

void collider2d_enable(collider2d *o) {
	collider2d_transformUpdate(o);
}
void collider2d_disable(collider2d *o) {
	const uint32_t ogid = o->gridPosition;
	if (ogid < UINT_MAX-1) list_remove(physics2DGrid+ogid,&o);
	else if (ogid == UINT_MAX) list_remove(&physics2DColliders,&o);
	o->gridPosition = UINT_MAX-1;
}

void init_collider2d(collider2d *o) {
	o->type = COLLIDER2D_CIRCLE;
	o->isCollider = true;
	o->size = (vec2){1.0f,1.0f};
	o->friction = 0.5f;
	o->bounce = 0.5f;
	o->onCollision = NULL;
	o->customDistance = NULL;
	o->gridPosition = UINT_MAX-1;
	o->base.enable = collider2d_enable;
	o->base.disable = collider2d_disable;
	o->base.transformUpdate = collider2d_transformUpdate;
}

//component reflection
#define REFLECT_TARGET collider2d
#define collider2d_fieldCount 5
const reflection_data collider2d_fields[collider2d_fieldCount] = {RCF(uint8_t,type),RCF(float,size.x),RCF(float,size.y),RCF(float,friction),RCF(float,bounce)};
#undef REFLECT_TARGET

//register component in engines linked list of components
define_component(collider2d,collider2d_fields,collider2d_fieldCount);
#undef LAST_COMPONENT_DATA
#define LAST_COMPONENT_DATA &component_data_collider2d

#endif
