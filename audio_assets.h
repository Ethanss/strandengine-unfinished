/*Copyright 2019 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _AUDIO_ASSETS_H_
#define _AUDIO_ASSETS_H_

#include "assets_base.h"
#include "audio.h"

//audio_piece .wav asset type
void asset_type_audio_wav_load(asset *a) {
	if (audioChannels == 0) return;
	audio_piece *ap = malloc(sizeof(audio_piece));
	void *wdat = malloc(a->fileSize);
	fread(wdat, 1, a->fileSize, a->fileHandle);
	SDL_RWops* srw = SDL_RWFromMem(wdat, a->fileSize);
	SDL_AudioSpec wspec;
	uint32_t wlen;
	uint16_t *wbuf;
	if (SDL_LoadWAV_RW(srw, 0, &wspec, &wbuf, &wlen) == NULL) new_audio_piece(ap, NULL, 0, true);
	else {
		wlen = audio_convert(wbuf,wlen,wspec.format,wspec.freq,wspec.channels);
		new_audio_piece(ap,wbuf,wlen,true);
	}
	free(wdat);
	a->asset = ap;
}

void asset_type_audio_unload(asset *a) {
	delete_audio_piece(a->asset);
	asset_type_unload(a);
}

const fixed_array asset_type_audio_wav_exts[1] = {{".wav",4,1}};
const asset_type ASSET_TYPE_AUDIO_WAV = {(void*)&asset_type_audio_wav_load, (void*)&asset_type_audio_unload, {(void*)asset_type_audio_wav_exts,1,0}, (void*)LAST_ASSET_TYPE };
#undef LAST_ASSET_TYPE
#define LAST_ASSET_TYPE &ASSET_TYPE_AUDIO_WAV

#endif
