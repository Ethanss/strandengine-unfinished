/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _ASSETS_BASE_H_
#define _ASSETS_BASE_H_

#include "json.h"
#include "stream.h"
#include "hashmap.h"

typedef struct {
	file_stream stream;
	hashmap loaded;
	uint32_t mapSize;
	char *path;
} asset_database;

asset_database assets;

typedef struct {
	one_arg_function load,unload;
	fixed_array extensions;
	struct asset_type *next;
} asset_type;

typedef struct {
	fixed_array name;
	union {
		void *asset, *fileHandle;
	};
	asset_type *type;
	json_object *options;
	void **includes;
	asset_database *database;
	uint32_t instances,hash,fileSize;
} asset;

asset *asset_database_get(asset_database *a, uint32_t hashv);
void asset_database_return(asset_database *a, asset *r);

#define assets_get(hv) asset_database_get(&assets,hv)
#define assets_return(as) asset_database_return(&assets,as)

//raw text/bytes asset type
void asset_type_raw_load(asset *a) {
	uint32_t fsz = a->fileSize;
	void *d = malloc(fsz), *f = a->fileHandle;
	fsz = fread(d,1,fsz,f);
	a->asset = d;
}

void asset_type_unload(asset *a) {
	asset_database *db = a->database;
	if (db != NULL) {
		asset **sinc = a->includes;
		if (sinc != NULL) {
			for (asset **sp = sinc; *sp != NULL; sp++) asset_database_return(db, *sp);
			free(sinc);
		}
	}
	if (a->options != NULL) {
		delete_json_object(a->options);
		free(a->options);
	}
	free(a->asset);
	free(a);
}

const fixed_array asset_type_raw_exts[2] = {{".txt",4,1},{".bytes",6,1}};
const asset_type ASSET_TYPE_RAW = {(void*)&asset_type_raw_load, (void*)&asset_type_unload, {(void*)asset_type_raw_exts,2,0}, NULL};
#define LAST_ASSET_TYPE &ASSET_TYPE_RAW

#endif
