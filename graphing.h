﻿/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _GRAPHING_H_
#define _GRAPHING_H_

#include "misc.h"
#include "vecmath.h"

typedef struct {
	uint8_t *data;
	uint32_t width,height;
} graphimage;

void new_graphimage(graphimage *gi, const uint32_t w, const uint32_t h) {
	gi->width = w;
	gi->height = h;
	gi->data = malloc(w*h*3);
}
#define delete_graphimage(gi) free((gi)->data)

void graphimage_clear(graphimage *gi, vec4 color) {
	uint8_t *d = gi->data, r = color.x*255.99f, g = color.y*255.99f, b = color.z*255.99f;
	const uint32_t width = gi->width, height = gi->height;
	for (uint32_t y = 0; y < height; y++) {
		for (uint32_t x = 0; x < width; x++) {
			*d++ = r;
			*d++ = g;
			*d++ = b;
		}
	}
}

float distanceToLine(vec2 uv, vec2 a, vec2 b) {
	vec2_sub(&uv,&a);
	vec2_sub(&b,&a);
	float dd = vec2_dot(&uv,&b)/vec2_dot(&b,&b);
	vec2_mulSingle(&b,clamp(dd,0.0f,1.0f));
	vec2_sub(&uv,&b);
	return vec2_length(&uv);
}

float distanceToBox(vec2 uv, vec2 pos, vec2 ext) {
	vec2_sub(&uv,&pos);
	uv.x = fmax(0.0f,fabs(uv.x)-ext.x);
	uv.y = fmax(0.0f,fabs(uv.y)-ext.y);
	return vec2_length(&uv);
}

void graphimage_box(graphimage *gi, vec2 pos, vec2 ext, const float rad, vec4 color) {
	const float width = gi->width, height = gi->height;
	pos.y = height-1.0f-pos.y;
	float minx = floorf(pos.x-ext.x-0.5f-rad), maxx = ceilf(pos.x+ext.x+0.5f+rad),
		miny = floorf(pos.y-ext.y-0.5f-rad), maxy = ceilf(pos.y+ext.y+0.5f+rad);
	minx = fmax(0.0f,minx);
	maxx = fmin(width,maxx);
	miny = fmax(0.0f,miny);
	maxy = fmin(height,maxy);
	
	color.x *= 255.0f;color.y *= 255.0f;color.z *= 255.0f;
		
	const uint32_t wstride = gi->width*3;
	uint8_t *dat = gi->data+((uint32_t)minx)*3;
	dat += ((uint32_t)miny)*wstride;
	for (float y = miny; y < maxy; y++) {
		uint8_t *gd = dat;
		for (float x = minx; x < maxx; x++) {
			const float lv = (1.0f-fmax(0.0f,fmin(1.0f,distanceToBox((vec2){x,y},pos,ext)-rad)))*color.w;
			*gd = (uint8_t)lerp(gd[0],color.x,lv);gd++;
			*gd = (uint8_t)lerp(gd[0],color.y,lv);gd++;
			*gd = (uint8_t)lerp(gd[0],color.z,lv);gd++;
		}
		dat += wstride;
	}
}

void graphimage_line(graphimage *gi, vec2 p1, vec2 p2, const float rad, vec4 color) {
	const float rad1 = rad+2.0f;
	const uint32_t width = gi->width, w3 = width*3, height = gi->height;
	p1.y = height-1.0f-p1.y;
	p2.y = height-1.0f-p2.y;
	int32_t ox,oy,ow,oh;
	if (p2.x > p1.x) {
		ox = p1.x-rad1;
		ow = p2.x+rad1;
	} else {
		ox = p2.x-rad1;
		ow = p1.x+rad1;
	}
	if (p2.y > p1.y) {
		oy = p1.y-rad1;
		oh = p2.y+rad1;
	} else {
		oy = p2.y-rad1;
		oh = p1.y+rad1;
	}
	
	ox = clamp(ox,0,width);
	ow = clamp(ow,0,width);
	oy = clamp(oy,0,height);
	oh = clamp(oh,0,height);
	
	color.x *= 255.0f;color.y *= 255.0f;color.z *= 255.0f;
	
	uint8_t *dat = gi->data+(ox+oy*width)*3;
	for (uint32_t y = oy; y < oh; y++) {
		uint8_t *d = dat;
		const float yv = y;
		for (uint32_t x = ox; x < ow; x++) {
			const float lv = (1.0f-fmax(0.0f,fmin(1.0f,distanceToLine((vec2){x,yv},p1,p2)-rad)))*color.w;
			*d = (uint8_t)lerp(d[0],color.x,lv);d++;
			*d = (uint8_t)lerp(d[0],color.y,lv);d++;
			*d = (uint8_t)lerp(d[0],color.z,lv);d++;
		}
		dat += w3;
	}
}

void graphimage_grid(graphimage *gi, const vec2 gmin, const vec2 gmax, const float rad, vec4 color, const uint32_t xlines, const uint32_t ylines) {
	vec2 p1 = gmin, p2;
	p2.x = gmin.x;
	p2.y = gmax.y;
	float step = (gmax.x-gmin.x)/(float)(xlines==1?1:xlines-1);
	for (uint32_t x = 0; x < xlines; x++) {
		graphimage_line(gi,p1,p2,rad,color);
		p1.x += step;
		p2.x += step;
	}
	p1 = gmin;
	p2.x = gmax.x;
	p2.y = gmin.y;
	step = (gmax.y-gmin.y)/(float)(ylines==1?1:ylines-1);
	for (uint32_t y = 0; y < ylines; y++) {
		graphimage_line(gi,p1,p2,rad,color);
		p1.y += step;
		p2.y += step;
	}
}

//simple graph font consisting of 5 lines for each character
vec4 GRAPH_FONT_LINES[180] = {
{0.019,0.009,0.447,0.981},{0.994,0.006,-0.528,0.984},{0.213,0.428,0.556,0},{0.213,0.428,0.556,0},{0.213,0.428,0.556,0},//A
{0.066,0.956,-0.009,-0.9},{0.072,0.953,0.663,-0.012},{0.059,0.506,0.663,-0.009},{0.731,0.934,-0.019,-0.884},{0.056,0.053,0.659,-0.003},//B
{0.075,0.931,-0.003,-0.853},{0.072,0.078,0.819,-0.006},{0.078,0.928,0.825,-0.009},{0.078,0.928,0.825,-0.009},{0.078,0.928,0.825,-0.009},//C
{0.066,0.944,0,-0.884},{0.066,0.059,0.747,0.059},{0.069,0.938,0.728,-0.066},{0.822,0.122,-0.025,0.759},{0.822,0.122,-0.025,0.759},//D
{0.066,0.944,0.006,-0.856},{0.072,0.081,0.734,-0.006},{0.072,0.481,0.653,0.013},{0.072,0.938,0.737,-0.019},{0.072,0.938,0.737,-0.019},//E
{0.103,0.056,-0.006,0.884},{0.1,0.934,0.809,-0.012},{0.103,0.544,0.666,0},{0.103,0.544,0.666,0},{0.103,0.544,0.666,0},//F
{0.856,0.888,-0.781,-0.003},{0.081,0.881,0.003,-0.8},{0.081,0.084,0.759,0.009},{0.841,0.094,-0.019,0.409},{0.822,0.503,-0.519,-0.047},//G
{0.044,0.037,-0.006,0.928},{0.053,0.466,0.859,0.003},{0.903,0.044,0.006,0.928},{0.903,0.044,0.006,0.928},{0.903,0.044,0.006,0.928},//H
{0.031,0.037,0.922,-0.012},{0.488,0.031,0.003,0.938},{0.031,0.972,0.928,0},{0.031,0.972,0.928,0},{0.031,0.972,0.928,0},//I
{0.028,0.969,0.931,-0.009},{0.556,0.959,-0.003,-0.884},{0.553,0.075,-0.506,0.003},{0.553,0.075,-0.506,0.003},{0.553,0.075,-0.506,0.003},//J
{0.066,0.969,-0.009,-0.922},{0.066,0.522,0.834,-0.484},{0.241,0.419,0.625,0.525},{0.241,0.419,0.625,0.525},{0.241,0.419,0.625,0.525},//K
{0.072,0.956,-0.009,-0.9},{0.066,0.056,0.881,0},{0.066,0.056,0.881,0},{0.066,0.056,0.881,0},{0.066,0.056,0.881,0},//L
{0.031,0.041,0.175,0.931},{0.206,0.969,0.238,-0.328},{0.444,0.641,0.378,0.334},{0.819,0.972,0.113,-0.925},{0.819,0.972,0.113,-0.925},//M
{0.05,0.047,0.022,0.916},{0.072,0.959,0.856,-0.906},{0.928,0.053,-0.012,0.916},{0.928,0.053,-0.012,0.916},{0.928,0.053,-0.012,0.916},//N
{0.063,0.056,0,0.909},{0.066,0.959,0.878,0.003},{0.944,0.963,-0.009,-0.903},{0.934,0.059,-0.875,-0.003},{0.934,0.059,-0.875,-0.003},//O
{0.066,0.066,0.009,0.884},{0.075,0.95,0.806,-0.009},{0.881,0.947,-0.009,-0.372},{0.872,0.578,-0.797,0.006},{0.872,0.578,-0.797,0.006},//P
{0.05,0.059,0,0.894},{0.05,0.941,0.897,-0.003},{0.941,0.941,-0.003,-0.884},{0.938,0.056,-0.887,0.006},{0.572,0.225,0.1,-0.194},//Q
{0.075,0.063,-0.003,0.897},{0.078,0.959,0.778,-0.025},{0.856,0.928,-0.012,-0.381},{0.844,0.538,-0.769,0.013},{0.088,0.547,0.794,-0.466},//R
{0.881,0.916,-0.816,0.006},{0.072,0.916,0.031,-0.5},{0.103,0.419,0.722,0.028},{0.822,0.434,-0.019,-0.403},{0.803,0.037,-0.719,0.041},//S
{0.472,0.059,0.006,0.866},{0.059,0.941,0.903,-0.022},{0.059,0.941,0.903,-0.022},{0.059,0.941,0.903,-0.022},{0.059,0.941,0.903,-0.022},//T
{0.069,0.925,0.031,-0.822},{0.1,0.106,0.766,0},{0.872,0.034,-0.056,0.913},{0.872,0.034,-0.056,0.913},{0.872,0.034,-0.056,0.913},//U
{0.047,0.975,0.381,-0.903},{0.428,0.078,0.488,0.894},{0.428,0.078,0.488,0.894},{0.428,0.078,0.488,0.894},{0.428,0.078,0.488,0.894},//V
{0.025,0.969,0.269,-0.903},{0.294,0.047,0.178,0.459},{0.472,0.506,0.15,-0.444},{0.625,0.066,0.322,0.916},{0.625,0.066,0.322,0.916},//W
{0.028,0.025,0.919,0.931},{0.963,0.041,-0.928,0.931},{0.963,0.041,-0.928,0.931},{0.963,0.041,-0.928,0.931},{0.963,0.041,-0.928,0.931},//X
{0.409,0.034,0.013,0.622},{0.422,0.656,-0.381,0.319},{0.425,0.653,0.506,0.322},{0.425,0.653,0.506,0.322},{0.425,0.653,0.506,0.322},//Y
{0.034,0.947,0.888,0.003},{0.922,0.95,-0.872,-0.9},{0.05,0.059,0.872,0},{0.05,0.059,0.872,0},{0.05,0.059,0.872,0},//Z
{0.063,0.056,0,0.909},{0.066,0.959,0.878,0.003},{0.944,0.963,-0.009,-0.903},{0.934,0.059,-0.875,-0.003},{0.934,0.059,-0.875,-0.003},//0
{0.519,0.047,0.006,0.916},{0.525,0.963,-0.134,-0.138},{0.525,0.963,-0.134,-0.138},{0.525,0.963,-0.134,-0.138},{0.525,0.963,-0.134,-0.138},//1
{0.072,0.691,0.472,0.269},{0.541,0.956,0.366,-0.288},{0.906,0.672,-0.841,-0.625},{0.072,0.047,0.813,0.009},{0.072,0.047,0.813,0.009},//2
{0.056,0.941,0.847,-0.009},{0.903,0.931,-0.012,-0.856},{0.891,0.066,-0.822,0},{0.897,0.475,-0.706,0.003},{0.897,0.475,-0.706,0.003},//3
{0.847,0.059,0.009,0.906},{0.853,0.959,-0.759,-0.562},{0.094,0.397,0.853,0.016},{0.094,0.397,0.853,0.016},{0.094,0.397,0.853,0.016},//4
{0.897,0.906,-0.822,-0.009},{0.088,0.903,0.013,-0.487},{0.094,0.422,0.753,0.012},{0.847,0.441,-0.038,-0.375},{0.809,0.066,-0.741,0.025},//5
{0.806,0.969,-0.659,-0.325},{0.153,0.644,-0.1,-0.572},{0.053,0.072,0.853,-0.019},{0.903,0.059,-0.109,0.403},{0.794,0.469,-0.684,-0.066},//6
{0.444,0.053,0.441,0.878},{0.881,0.928,-0.756,0},{0.881,0.928,-0.756,0},{0.881,0.928,-0.756,0},{0.881,0.928,-0.756,0},//7
{0.066,0.956,-0.009,-0.9},{0.072,0.953,0.663,-0.012},{0.059,0.506,0.663,-0.009},{0.731,0.934,-0.019,-0.884},{0.056,0.053,0.659,-0.003},//8
{0.919,0.959,-0.006,-0.906},{0.919,0.956,-0.809,-0.066},{0.113,0.894,0.062,-0.341},{0.175,0.547,0.738,0.019},{0.175,0.547,0.738,0.019}//9
};
void graphimage_text(graphimage *gi, list *str, vec2 pos, const float size, const float thick, const float pad, vec4 color) {
	const uint32_t slen = str->length;
	float tx = pos.x, spad = size*pad, thicksz = thick*size;
	uint8_t *sch = str->buffer;
	for (uint32_t i = 0; i < slen; i++) {
		uint8_t ch = tolower(*sch++), did = 255;
		if (ch == '.') {
			graphimage_box(gi,(vec2){tx+size*0.5f,pos.y},(vec2){size*0.05f,size*0.05f},size*0.05f,color);
		} else if (ch == '-') {
			graphimage_box(gi,(vec2){tx+size*0.5f,pos.y+size*0.5f},(vec2){size*0.4f,size*0.06f},0.0f,color);
		} else {
			if (ch > 47 && ch < 58) did = 26+(ch-48);
			else if (ch > 96 && ch < 123) did = ch-97;
		
			if (did != 255) {
				vec4 *ld = GRAPH_FONT_LINES+did*5;
				for (uint32_t l = 0; l < 5; l++) {
					const float bx = tx+ld->x*size, by = pos.y+ld->y*size;
					graphimage_line(gi,(vec2){bx,by},(vec2){bx+ld->z*size,by+ld->w*size},thicksz,color);
					ld++;
				}
			}
		}
		
		tx += spad;
	}
}

#endif
