﻿/*Copyright 2019 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _INPUTMANAGER_H_
#define _INPUTMANAGER_H_

#include "json.h"


bool input_manager_hasInitializedGamepad = false;

enum {
	INPUT_TYPE_MOUSEMOVE = 0,
	INPUT_TYPE_MOUSEBUTTON = 1,
	INPUT_TYPE_KEY = 2,
	INPUT_TYPE_GAMEPAD_BUTTON = 3,
	INPUT_TYPE_GAMEPAD_AXIS = 4
};

typedef struct {
	char *name;
	uint32_t id;
	uint8_t type, nameLength;
} input_binding;

#define NUM_INPUT_BINDINGS 263
input_binding input_bindings[NUM_INPUT_BINDINGS];

typedef struct {
	list name, positiveBindings, negativeBindings;
	float sensitivity, gravity, absoluteAxis, axis, deadzone;
	bool positive,negative;
} input;

#define MAX_NUM_INPUTS 32
input inputs[MAX_NUM_INPUTS];
list activeInputAxes;


typedef struct {
	input_binding *binding;
	input *object;
	uint32_t id;
	bool positive;
} input_bind;
list mouseMoveBinds, mouseButtonBinds, keyBinds, gamepadButtonBinds, gamepadAxisBinds;

SDL_GameController *gamepad = NULL;

void inputmanager_onEvent(SDL_Event *e) {
	input_bind *ibind;
	uint32_t bid;

	switch (e->type) {
	//mouse button state
	case SDL_MOUSEBUTTONDOWN:
		bid = e->button.button;
		ibind = mouseButtonBinds.buffer;
		for (uint32_t i = 0; i < mouseButtonBinds.length; i++) {
			if (bid == ibind->id) {
				if (ibind->positive) ibind->object->positive = true;
				else ibind->object->negative = true;
			}

			ibind++;
		}
		break;
	case SDL_MOUSEBUTTONUP:
		bid = e->button.button;
		ibind = mouseButtonBinds.buffer;
		for (uint32_t i = 0; i < mouseButtonBinds.length; i++) {
			if (bid == ibind->id) {
				if (ibind->positive) ibind->object->positive = false;
				else ibind->object->negative = false;
			}

			ibind++;
		}
		break;

	case SDL_KEYDOWN:
		bid = e->key.keysym.sym;
		ibind = keyBinds.buffer;
		for (uint32_t i = 0; i < keyBinds.length; i++) {
			if (bid == ibind->id) {
				if (ibind->positive) ibind->object->positive = true;
				else ibind->object->negative = true;
			}

			ibind++;
		}
		break;

	case SDL_KEYUP:
		bid = e->key.keysym.sym;
		ibind = keyBinds.buffer;
		for (uint32_t i = 0; i < keyBinds.length; i++) {
			if (bid == ibind->id) {
				if (ibind->positive) ibind->object->positive = false;
				else ibind->object->negative = false;
			}

			ibind++;
		}
		break;

	case SDL_CONTROLLERBUTTONDOWN:
		bid = e->cbutton.button;
		ibind = gamepadButtonBinds.buffer;
		for (uint32_t i = 0; i < gamepadButtonBinds.length; i++) {
			if (bid == ibind->id) {
				if (ibind->positive) ibind->object->positive = true;
				else ibind->object->negative = true;
			}

			ibind++;
		}
		break;

	case SDL_CONTROLLERBUTTONUP:
		bid = e->cbutton.button;
		ibind = gamepadButtonBinds.buffer;
		for (uint32_t i = 0; i < gamepadButtonBinds.length; i++) {
			if (bid == ibind->id) {
				if (ibind->positive) ibind->object->positive = false;
				else ibind->object->negative = false;
			}

			ibind++;
		}
		break;
	}
}

void inputmanager_update(void UNUSED *unsd) {
	float fmdx = mouseDeltaX*0.1f, fmdy = mouseDeltaY*0.1f;
	input_bind *ibind = mouseMoveBinds.buffer;
	for (uint32_t i = 0; i < mouseMoveBinds.length; i++) {
		ibind->object->absoluteAxis = ibind->id==0?fmdx:fmdy;
		ibind++;
	}

	ibind = gamepadAxisBinds.buffer;
	for (uint32_t i = 0; i < gamepadAxisBinds.length; i++) {
		float aax = SDL_GameControllerGetAxis(gamepad,ibind->id)/(32767.0f);
		if (aax < -1.0f) aax = -1.0f;
		ibind->object->absoluteAxis = aax;
		ibind++;
	}

	input **inp = activeInputAxes.buffer;
	for (uint32_t i = 0; i < activeInputAxes.length; i++) {
		input* ip = *inp;

		float av = ip->axis;
		if (ip->positive) {
			av += deltaTime*ip->sensitivity;
			if (av > 1.0f) av = 1.0f+deltaTime*ip->gravity;
		}
		if (ip->negative) {
			av -= deltaTime*ip->sensitivity;
			if (av < -1.0f) av = -1.0f-deltaTime*ip->gravity;
		}
		float dz = ip->deadzone;
		if (av > 0.0f) {
			av -= deltaTime*ip->gravity;
			if (av < dz) av = 0.0f;
		} else if (av < 0.0f) {
			av += deltaTime*ip->gravity;
			if (av > -dz) av = 0.0f;
		}
		float aax = ip->absoluteAxis*ip->sensitivity;
		if (aax < dz && aax > -dz) aax = 0.0f;
		if (fabs(aax) > fabs(av)) av = aax;
		ip->axis = av;

		inp++;
	}
}


void init_inputmanager() {
	//setup input_bindings
	//mouse
	input_bindings[0] = (input_binding){"mouse x",0,INPUT_TYPE_MOUSEMOVE,0};
	input_bindings[1] = (input_binding){"mouse y",1,INPUT_TYPE_MOUSEMOVE,0};
	input_bindings[2] = (input_binding){"mousebutton left",SDL_BUTTON_LEFT,INPUT_TYPE_MOUSEBUTTON,0};
	input_bindings[3] = (input_binding){"mousebutton middle",SDL_BUTTON_MIDDLE,INPUT_TYPE_MOUSEBUTTON,0};
	input_bindings[4] = (input_binding){"mousebutton right",SDL_BUTTON_RIGHT,INPUT_TYPE_MOUSEBUTTON,0};

	//keys
	list keyNameBuf;
	new_list(&keyNameBuf, 1);
	SDL_Keycode bindedKeys[236] = {SDLK_0,SDLK_1,SDLK_2,SDLK_3,SDLK_4,SDLK_5,SDLK_6,SDLK_7,SDLK_8,SDLK_9,SDLK_a,SDLK_AC_BACK,SDLK_AC_BOOKMARKS,SDLK_AC_FORWARD,SDLK_AC_HOME,SDLK_AC_REFRESH,SDLK_AC_SEARCH,SDLK_AC_STOP,SDLK_AGAIN,SDLK_ALTERASE,SDLK_QUOTE,SDLK_APPLICATION,SDLK_AUDIOMUTE,SDLK_AUDIONEXT,SDLK_AUDIOPLAY,SDLK_AUDIOPREV,SDLK_AUDIOSTOP,SDLK_b,SDLK_BACKSLASH,SDLK_BACKSPACE,SDLK_BRIGHTNESSDOWN,SDLK_BRIGHTNESSUP,SDLK_c,SDLK_CALCULATOR,SDLK_CANCEL,SDLK_CAPSLOCK,SDLK_CLEAR,SDLK_CLEARAGAIN,SDLK_COMMA,SDLK_COMPUTER,SDLK_COPY,SDLK_CRSEL,SDLK_CURRENCYSUBUNIT,SDLK_CURRENCYUNIT,SDLK_CUT,SDLK_d,SDLK_DECIMALSEPARATOR,SDLK_DELETE,SDLK_DISPLAYSWITCH,SDLK_DOWN,SDLK_e,SDLK_EJECT,SDLK_END,SDLK_EQUALS,SDLK_ESCAPE,SDLK_EXECUTE,SDLK_EXSEL,SDLK_f,SDLK_F1,SDLK_F10,SDLK_F11,SDLK_F12,SDLK_F13,SDLK_F14,SDLK_F15,SDLK_F16,SDLK_F17,SDLK_F18,SDLK_F19,SDLK_F2,SDLK_F20,SDLK_F21,SDLK_F22,SDLK_F23,SDLK_F24,SDLK_F3,SDLK_F4,SDLK_F5,SDLK_F6,SDLK_F7,SDLK_F8,SDLK_F9,SDLK_FIND,SDLK_g,SDLK_BACKQUOTE,SDLK_h,SDLK_HELP,SDLK_HOME,SDLK_i,SDLK_INSERT,SDLK_j,SDLK_k,SDLK_KBDILLUMDOWN,SDLK_KBDILLUMTOGGLE,SDLK_KBDILLUMUP,SDLK_KP_0,SDLK_KP_00,SDLK_KP_000,SDLK_KP_1,SDLK_KP_2,SDLK_KP_3,SDLK_KP_4,SDLK_KP_5,SDLK_KP_6,SDLK_KP_7,SDLK_KP_8,SDLK_KP_9,SDLK_KP_A,SDLK_KP_AMPERSAND,SDLK_KP_AT,SDLK_KP_B,SDLK_KP_BACKSPACE,SDLK_KP_BINARY,SDLK_KP_C,SDLK_KP_CLEAR,SDLK_KP_CLEARENTRY,SDLK_KP_COLON,SDLK_KP_COMMA,SDLK_KP_D,SDLK_KP_DBLAMPERSAND,SDLK_KP_DBLVERTICALBAR,SDLK_KP_DECIMAL,SDLK_KP_DIVIDE,SDLK_KP_E,SDLK_KP_ENTER,SDLK_KP_EQUALS,SDLK_KP_EQUALSAS400,SDLK_KP_EXCLAM,SDLK_KP_F,SDLK_KP_GREATER,SDLK_KP_HASH,SDLK_KP_HEXADECIMAL,SDLK_KP_LEFTBRACE,SDLK_KP_LEFTPAREN,SDLK_KP_LESS,SDLK_KP_MEMADD,SDLK_KP_MEMCLEAR,SDLK_KP_MEMDIVIDE,SDLK_KP_MEMMULTIPLY,SDLK_KP_MEMRECALL,SDLK_KP_MEMSTORE,SDLK_KP_MEMSUBTRACT,SDLK_KP_MINUS,SDLK_KP_MULTIPLY,SDLK_KP_OCTAL,SDLK_KP_PERCENT,SDLK_KP_PERIOD,SDLK_KP_PLUS,SDLK_KP_PLUSMINUS,SDLK_KP_POWER,SDLK_KP_RIGHTBRACE,SDLK_KP_RIGHTPAREN,SDLK_KP_SPACE,SDLK_KP_TAB,SDLK_KP_VERTICALBAR,SDLK_KP_XOR,SDLK_l,SDLK_LALT,SDLK_LCTRL,SDLK_LEFT,SDLK_LEFTBRACKET,SDLK_LGUI,SDLK_LSHIFT,SDLK_m,SDLK_MAIL,SDLK_MEDIASELECT,SDLK_MENU,SDLK_MINUS,SDLK_MODE,SDLK_MUTE,SDLK_n,SDLK_NUMLOCKCLEAR,SDLK_o,SDLK_OPER,SDLK_OUT,SDLK_p,SDLK_PAGEDOWN,SDLK_PAGEUP,SDLK_PASTE,SDLK_PAUSE,SDLK_PERIOD,SDLK_POWER,SDLK_PRINTSCREEN,SDLK_PRIOR,SDLK_q,SDLK_r,SDLK_RALT,SDLK_RCTRL,SDLK_RETURN,SDLK_RETURN2,SDLK_RGUI,SDLK_RIGHT,SDLK_RIGHTBRACKET,SDLK_RSHIFT,SDLK_s,SDLK_SCROLLLOCK,SDLK_SELECT,SDLK_SEMICOLON,SDLK_SEPARATOR,SDLK_SLASH,SDLK_SLEEP,SDLK_SPACE,SDLK_STOP,SDLK_SYSREQ,SDLK_t,SDLK_TAB,SDLK_THOUSANDSSEPARATOR,SDLK_u,SDLK_UNDO,SDLK_UNKNOWN,SDLK_UP,SDLK_v,SDLK_VOLUMEDOWN,SDLK_VOLUMEUP,SDLK_w,SDLK_WWW,SDLK_x,SDLK_y,SDLK_z,SDLK_AMPERSAND,SDLK_ASTERISK,SDLK_AT,SDLK_CARET,SDLK_COLON,SDLK_DOLLAR,SDLK_EXCLAIM,SDLK_GREATER,SDLK_HASH,SDLK_LEFTPAREN,SDLK_LESS,SDLK_PERCENT,SDLK_PLUS,SDLK_QUESTION,SDLK_QUOTEDBL,SDLK_RIGHTPAREN,SDLK_UNDERSCORE};
	for (uint32_t i = 0; i < 236; i++) {
		char *ks = (char*)SDL_GetKeyName(bindedKeys[i]);
		uint32_t len = strlen(ks), ind;
		ind = list_addm(&keyNameBuf, ks, len + 1);
		string_applyCharacterModifier((char*)keyNameBuf.buffer + ind, len, tolower);
		input_bindings[i+5] = (input_binding){*(void**)&ind,bindedKeys[i],INPUT_TYPE_KEY,0};
	}
	for (uint32_t i = 0; i < 236; i++) {
		input_bindings[i+5].name = (char*)keyNameBuf.buffer+*(uint32_t*)&input_bindings[5+i].name;
	}
	string_applyCharacterModifier(keyNameBuf.buffer, keyNameBuf.length, tolower);

	//gamepad
	//buttons
	const uint32_t numGamepadButtons = 15;
	void* gamepadButtons[30] = {"a button", (void*)SDL_CONTROLLER_BUTTON_A, "b button", (void*)SDL_CONTROLLER_BUTTON_B,
"x button", (void*)SDL_CONTROLLER_BUTTON_X, "y button", (void*)SDL_CONTROLLER_BUTTON_Y, "back button", (void*)SDL_CONTROLLER_BUTTON_BACK,
"guide button", (void*)SDL_CONTROLLER_BUTTON_GUIDE, "start button", (void*)SDL_CONTROLLER_BUTTON_START, "left stick button", (void*)SDL_CONTROLLER_BUTTON_LEFTSTICK,
"right stick button", (void*)SDL_CONTROLLER_BUTTON_RIGHTSTICK, "left shoulder button", (void*)SDL_CONTROLLER_BUTTON_LEFTSHOULDER,
"right shoulder button", (void*)SDL_CONTROLLER_BUTTON_RIGHTSHOULDER, "dpad up", (void*)SDL_CONTROLLER_BUTTON_DPAD_UP,
"dpad down", (void*)SDL_CONTROLLER_BUTTON_DPAD_DOWN, "dpad left", (void*)SDL_CONTROLLER_BUTTON_DPAD_LEFT,
"dpad right", (void*)SDL_CONTROLLER_BUTTON_DPAD_RIGHT};
	for (uint32_t i = 0; i < numGamepadButtons; i++) {
		uint32_t ai = i*2;
		input_bindings[241+i] = (input_binding){gamepadButtons[ai],*(uint32_t*)&gamepadButtons[ai+1],INPUT_TYPE_GAMEPAD_BUTTON,0};
	}

	//joysticks axes
	const uint32_t numGamepadSticks = 6;
	void* gamepadSticks[12] = {"left stick x", (void*)SDL_CONTROLLER_AXIS_LEFTX, "left stick y", (void*)SDL_CONTROLLER_AXIS_LEFTY,
"right stick x", (void*)SDL_CONTROLLER_AXIS_RIGHTX, "right stick y", (void*)SDL_CONTROLLER_AXIS_RIGHTY,
"left trigger", (void*)SDL_CONTROLLER_AXIS_TRIGGERLEFT, "right trigger", (void*)SDL_CONTROLLER_AXIS_TRIGGERRIGHT};
	for (uint32_t i = 0; i < numGamepadSticks; i++) {
		uint32_t ai = i*2;
		input_bindings[256+i] = (input_binding){gamepadSticks[ai],*(uint32_t*)&gamepadSticks[ai+1],INPUT_TYPE_GAMEPAD_AXIS,0};
	}

	//allocate inputs
	new_list(&activeInputAxes, sizeof(void*));
	new_list(&mouseMoveBinds, sizeof(input_bind));
	new_list(&mouseButtonBinds, sizeof(input_bind));
	new_list(&keyBinds, sizeof(input_bind));
	new_list(&gamepadButtonBinds, sizeof(input_bind));
	new_list(&gamepadAxisBinds, sizeof(input_bind));

	for (uint32_t i = 0; i < MAX_NUM_INPUTS; i++) {
		input *inp = inputs+i;
		new_list(&inp->name, 1);
		inp->axis = 0.0f;
		inp->absoluteAxis = 0.0f;
		inp->sensitivity = 0.0f;
		inp->gravity = 0.0f;
		inp->deadzone = 0.5f;
		inp->positive = false;
		inp->negative = false;
		new_list(&inp->positiveBindings, sizeof(void*));
		new_list(&inp->negativeBindings, sizeof(void*));
	}

#ifdef _ENGINE_H_
	ifunction ifunc = (ifunction){ &inputmanager_update, NULL };
	#ifdef ENGINE_EDITMODE
	slist_add(&engineEditorUpdateCalls,&ifunc);
	#else
	slist_add(&updateCalls,&ifunc);
	#endif
	one_arg_function oe = (void*)&inputmanager_onEvent;
	slist_add(&eventCalls, &oe);
#endif
}
void free_inputmanager() {
	free(input_bindings[5].name);

	delete_list(&activeInputAxes);
	delete_list(&mouseMoveBinds);
	delete_list(&mouseButtonBinds);
	delete_list(&keyBinds);
	delete_list(&gamepadButtonBinds);
	delete_list(&gamepadAxisBinds);

	for (uint32_t i = 0; i < MAX_NUM_INPUTS; i++) {
		input *inp = inputs+i;
		delete_list(&inp->name);
		void **bbuf = inp->positiveBindings.buffer;
		uint8_t *maxPtr = input_bindings+NUM_INPUT_BINDINGS;
		uint32_t blen = inp->positiveBindings.length;
		for (uint32_t k = 0; k < blen; k++) {
			uint8_t *pv = *bbuf++;
			if (pv < (uint8_t*)input_bindings || pv >= maxPtr) free(pv);
		}
		bbuf = inp->negativeBindings.buffer;
		blen = inp->negativeBindings.length;
		for (uint32_t k = 0; k < blen; k++) {
			uint8_t *pv = *bbuf++;
			if (pv < (uint8_t*)input_bindings || pv >= maxPtr) free(pv);
		}
		delete_list(&inp->positiveBindings);
		delete_list(&inp->negativeBindings);
	}

#ifdef _ENGINE_H_
	ifunction ifunc = (ifunction){ &inputmanager_update, NULL };
#ifdef ENGINE_EDITMODE
	slist_remove(&engineEditorUpdateCalls, &ifunc);
#else
	slist_remove(&updateCalls, &ifunc);
#endif
	one_arg_function oe = (void*)&inputmanager_onEvent;
	slist_remove(&eventCalls, &oe);
#endif
}


void inputmanager_clearInputs() {
	//reset all inputs
	mouseMoveBinds.length = 0;
	mouseButtonBinds.length = 0;
	activeInputAxes.length = 0;

	for (uint32_t i = 0; i < MAX_NUM_INPUTS; i++) {
		input *inp = inputs+i;
		inp->name.length = 0;
		inp->positiveBindings.length = 0;
		inp->negativeBindings.length = 0;
		inp->axis = 0.0f;
		inp->deadzone = 0.5f;
		inp->absoluteAxis = 0.0f;
		inp->sensitivity = 0.0f;
		inp->gravity = 0.0f;
		inp->positive = false;
		inp->negative = false;
	}
}

void inputmanager_configureInputs() {
	//configure all input settings
	bool hasGamepadInput = false;

	for (uint32_t i = 0; i < MAX_NUM_INPUTS; i++) {
		input *inp = inputs+i;

		bool hasBinding = false;
		list *bl = &inp->positiveBindings;
		if (bl->length > 0) hasBinding = true;
		input_binding **ibf = bl->buffer;
		for (uint32_t q = 0; q < bl->length; q++) {
			input_binding *ib = *ibf;
			input_bind bind = (input_bind){ib,inp,ib->id,true};
			switch (ib->type) {
				case INPUT_TYPE_MOUSEMOVE: list_add(&mouseMoveBinds, &bind); break;
				case INPUT_TYPE_MOUSEBUTTON: list_add(&mouseButtonBinds, &bind); break;
				case INPUT_TYPE_KEY: list_add(&keyBinds, &bind); break;
				case INPUT_TYPE_GAMEPAD_BUTTON: list_add(&gamepadButtonBinds, &bind); hasGamepadInput = true; break;
				case INPUT_TYPE_GAMEPAD_AXIS: list_add(&gamepadAxisBinds, &bind); hasGamepadInput = true; break;
			}
			ibf++;
		}

		bl = &inp->negativeBindings;
		if (bl->length > 0) hasBinding = true;
		ibf = bl->buffer;
		for (uint32_t q = 0; q < bl->length; q++) {
			input_binding *ib = *ibf;
			input_bind bind = (input_bind){ib,inp,ib->id,false};
			switch (ib->type) {
				case INPUT_TYPE_MOUSEMOVE: list_add(&mouseMoveBinds, &bind); break;
				case INPUT_TYPE_MOUSEBUTTON: list_add(&mouseButtonBinds, &bind); break;
				case INPUT_TYPE_KEY: list_add(&keyBinds, &bind); break;
				case INPUT_TYPE_GAMEPAD_BUTTON: list_add(&gamepadButtonBinds, &bind); hasGamepadInput = true; break;
				case INPUT_TYPE_GAMEPAD_AXIS: list_add(&gamepadAxisBinds, &bind); hasGamepadInput = true; break;
			}
			ibf++;
		}

		if (hasBinding && inp->sensitivity != 0.0f) list_add(&activeInputAxes, &inp);
	}

	//init gamepads if needed
	if (hasGamepadInput && !input_manager_hasInitializedGamepad) {
		input_manager_hasInitializedGamepad = true;

		SDL_InitSubSystem(SDL_INIT_JOYSTICK | SDL_INIT_GAMECONTROLLER | SDL_INIT_HAPTIC);
		int32_t nsticks = SDL_NumJoysticks();
		if (nsticks > 0) {
			for (int32_t i = 0; i < nsticks; i++) {
			    SDL_JoystickOpen(i);
	    		gamepad = SDL_GameControllerOpen(i);
			}
		}
	}
}


//get input from name
input *input_manager_get(list *s) {
	for (uint32_t i = 0; i < MAX_NUM_INPUTS; i++) {
		if (list_equals(&inputs[i].name,s)) return inputs+i;
	}
	return NULL;
}

//get input event binding from name
input_binding *input_binding_get(list *s) {
	string_applyCharacterModifier(s->buffer, s->length, tolower);
	for (uint32_t i = 0; i < NUM_INPUT_BINDINGS; i++) {
		uint32_t slen = input_bindings[i].nameLength;
		if (slen == 0) input_bindings[i].nameLength = slen = strlen(input_bindings[i].name);
		if (slen == s->length && memcmp(input_bindings[i].name,s->buffer,slen) == 0) return input_bindings+i;
	}
	input_binding *ibind = malloc(sizeof(input_binding)+s->length+1);
	ibind->name = ibind + 1;
	ibind->nameLength = s->length;
	char *buf = list_as_cstring(s);
	memcpy(ibind->name, buf, s->length + 1);
	const char *customMouseBtnStr = "custom mousebutton ",
				*customKeyStr = "custom key ",
				*customStickStr = "custom stick ",
				*customButtonStr = "custom button ";
	uint32_t slen = strlen(customMouseBtnStr);
	if (memStartsWith(buf, s->length, customMouseBtnStr, slen)) {
		ibind->type = INPUT_TYPE_MOUSEBUTTON;
		ibind->id = atoi(buf + slen);
		buf = NULL;
	}
	if (buf != NULL) {
		slen = strlen(customKeyStr);
		if (memStartsWith(buf, s->length, customKeyStr, slen)) {
			ibind->type = INPUT_TYPE_KEY;
			ibind->id = atoi(buf + slen);
			buf = NULL;
		}
	}
	if (buf != NULL) {
		slen = strlen(customStickStr);
		if (memStartsWith(buf, s->length, customStickStr, slen)) {
			ibind->type = INPUT_TYPE_GAMEPAD_AXIS;
			ibind->id = atoi(buf + slen);
			buf = NULL;
		}
	}
	if (buf != NULL) {
		slen = strlen(customButtonStr);
		if (memStartsWith(buf, s->length, customButtonStr, slen)) {
			ibind->type = INPUT_TYPE_GAMEPAD_BUTTON;
			ibind->id = atoi(buf + slen);
			buf = NULL;
		}
	}
	list_clear_cstring(s);
	if (buf == NULL) return ibind;
	else free(ibind);
	return NULL;
}

//load inputs from JSON string
void inputmanager_loadInputs(list *s) {
	inputmanager_clearInputs();

	json_object *ijs = json_parse(s).objectv;
	list positiveN = cstring_as_list("positive"),
		negativeN = cstring_as_list("negative"),
		sensN = cstring_as_list("sensitivity"),
		gravityN = cstring_as_list("gravity"),
		deadzoneN = cstring_as_list("deadzone");

	json *jv = ijs->values.buffer;
	list *jk = ijs->keys.buffer;
	input *inp = inputs;
	uint32_t len = ijs->values.length;
	if (len > MAX_NUM_INPUTS) len = MAX_NUM_INPUTS;
	for (uint32_t i = 0; i < len; i++) {
		if (jv->type == JSON_TYPE_OBJECT) {
			json_object *jso = jv->data;
			inp->name.length = 0;
			list_addm(&inp->name, jk->buffer, jk->length);

			json *positiveBinds = json_object_get(jso, &positiveN);
			if (positiveBinds != NULL && positiveBinds->type == JSON_TYPE_ARRAY) {
				list *bl = positiveBinds->data;
				json *bljs = bl->buffer;
				for (uint32_t i = 0; i < bl->length; i++) {
					if (bljs->type == JSON_TYPE_STRING) {
						input_binding *ib = input_binding_get(bljs->data);
						if (ib != NULL) list_add(&inp->positiveBindings, &ib);
					}
					bljs++;
				}
			}

			json *negativeBinds = json_object_get(jso, &negativeN);
			if (negativeBinds != NULL && negativeBinds->type == JSON_TYPE_ARRAY) {
				list *bl = negativeBinds->data;
				json *bljs = bl->buffer;
				for (uint32_t i = 0; i < bl->length; i++) {
					if (bljs->type == JSON_TYPE_STRING) {
						input_binding *ib = input_binding_get(bljs->data);
						if (ib != NULL) list_add(&inp->negativeBindings, &ib);
					}
					bljs++;
				}
			}

			json *jsensitivity = json_object_get(jso, &sensN);
			if (jsensitivity != NULL && jsensitivity->type == JSON_TYPE_FLOAT) inp->sensitivity = jsensitivity->floatv;

			json *jgravity = json_object_get(jso, &gravityN);
			if (jgravity != NULL && jgravity->type == JSON_TYPE_FLOAT) inp->gravity = jgravity->floatv;

			json *jdead = json_object_get(jso, &deadzoneN);
			if (jdead != NULL && jdead->type == JSON_TYPE_FLOAT) inp->deadzone = jdead->floatv;
		}

		jv++;
		jk++;
		inp++;
	}

	delete_json_object(ijs);
	free(ijs);
	inputmanager_configureInputs();
}
void inputmanager_loadInputsFromFile(const char *fp) {
	uint64_t sz;
	char *fd = file_readall(fp, &sz);
	if (fd == NULL) return;
	list ls;
	ls.stride = 1;
	list_setBuffer(&ls, fd, (uint32_t)sz);
	inputmanager_loadInputs(&ls);
	free(fd);
}

#undef MAX_NUM_INPUTS
#undef NUM_INPUT_BINDINGS

#endif
