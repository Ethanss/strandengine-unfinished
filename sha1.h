//Public domain
#ifndef _SHA1_H_
#define _SHA1_H_

#include <stdio.h>
#include <string.h>
#include <stdint.h>

typedef struct {
    uint32_t state[5],count[2];
    unsigned char buffer[64];
} SHA1_CTX;

#define sha1_rol(value, bits) (((value) << (bits)) | ((value) >> (32 - (bits))))
#define sha1_blk0(i) (block->l[i] = (sha1_rol(block->l[i],24)&0xFF00FF00)|(sha1_rol(block->l[i],8)&0x00FF00FF))
#define sha1_blk(i) (block->l[i&15] = sha1_rol(block->l[(i+13)&15]^block->l[(i+8)&15]^block->l[(i+2)&15]^block->l[i&15],1))
#define sha1_R0(v,w,x,y,z,i) z+=((w&(x^y))^y)+sha1_blk0(i)+0x5A827999+sha1_rol(v,5);w=sha1_rol(w,30);
#define sha1_R1(v,w,x,y,z,i) z+=((w&(x^y))^y)+sha1_blk(i)+0x5A827999+sha1_rol(v,5);w=sha1_rol(w,30);
#define sha1_R2(v,w,x,y,z,i) z+=(w^x^y)+sha1_blk(i)+0x6ED9EBA1+sha1_rol(v,5);w=sha1_rol(w,30);
#define sha1_R3(v,w,x,y,z,i) z+=(((w|x)&y)|(w&x))+sha1_blk(i)+0x8F1BBCDC+sha1_rol(v,5);w=sha1_rol(w,30);
#define sha1_R4(v,w,x,y,z,i) z+=(w^x^y)+sha1_blk(i)+0xCA62C1D6+sha1_rol(v,5);w=sha1_rol(w,30);

void SHA1Transform(uint32_t state[5], unsigned char buffer[64]) {
    uint32_t a, b, c, d, e;

    typedef union {
        unsigned char c[64];
        uint32_t l[16];
    } CHAR64LONG16;
    CHAR64LONG16 *block = buffer;

    a = state[0];
    b = state[1];
    c = state[2];
    d = state[3];
    e = state[4];
    sha1_R0(a, b, c, d, e, 0);
    sha1_R0(e, a, b, c, d, 1);
    sha1_R0(d, e, a, b, c, 2);
    sha1_R0(c, d, e, a, b, 3);
    sha1_R0(b, c, d, e, a, 4);
    sha1_R0(a, b, c, d, e, 5);
    sha1_R0(e, a, b, c, d, 6);
    sha1_R0(d, e, a, b, c, 7);
    sha1_R0(c, d, e, a, b, 8);
    sha1_R0(b, c, d, e, a, 9);
    sha1_R0(a, b, c, d, e, 10);
    sha1_R0(e, a, b, c, d, 11);
    sha1_R0(d, e, a, b, c, 12);
    sha1_R0(c, d, e, a, b, 13);
    sha1_R0(b, c, d, e, a, 14);
    sha1_R0(a, b, c, d, e, 15);
    sha1_R1(e, a, b, c, d, 16);
    sha1_R1(d, e, a, b, c, 17);
    sha1_R1(c, d, e, a, b, 18);
    sha1_R1(b, c, d, e, a, 19);
    sha1_R2(a, b, c, d, e, 20);
    sha1_R2(e, a, b, c, d, 21);
    sha1_R2(d, e, a, b, c, 22);
    sha1_R2(c, d, e, a, b, 23);
    sha1_R2(b, c, d, e, a, 24);
    sha1_R2(a, b, c, d, e, 25);
    sha1_R2(e, a, b, c, d, 26);
    sha1_R2(d, e, a, b, c, 27);
    sha1_R2(c, d, e, a, b, 28);
    sha1_R2(b, c, d, e, a, 29);
    sha1_R2(a, b, c, d, e, 30);
    sha1_R2(e, a, b, c, d, 31);
    sha1_R2(d, e, a, b, c, 32);
    sha1_R2(c, d, e, a, b, 33);
    sha1_R2(b, c, d, e, a, 34);
    sha1_R2(a, b, c, d, e, 35);
    sha1_R2(e, a, b, c, d, 36);
    sha1_R2(d, e, a, b, c, 37);
    sha1_R2(c, d, e, a, b, 38);
    sha1_R2(b, c, d, e, a, 39);
    sha1_R3(a, b, c, d, e, 40);
    sha1_R3(e, a, b, c, d, 41);
    sha1_R3(d, e, a, b, c, 42);
    sha1_R3(c, d, e, a, b, 43);
    sha1_R3(b, c, d, e, a, 44);
    sha1_R3(a, b, c, d, e, 45);
    sha1_R3(e, a, b, c, d, 46);
    sha1_R3(d, e, a, b, c, 47);
    sha1_R3(c, d, e, a, b, 48);
    sha1_R3(b, c, d, e, a, 49);
    sha1_R3(a, b, c, d, e, 50);
    sha1_R3(e, a, b, c, d, 51);
    sha1_R3(d, e, a, b, c, 52);
    sha1_R3(c, d, e, a, b, 53);
    sha1_R3(b, c, d, e, a, 54);
    sha1_R3(a, b, c, d, e, 55);
    sha1_R3(e, a, b, c, d, 56);
    sha1_R3(d, e, a, b, c, 57);
    sha1_R3(c, d, e, a, b, 58);
    sha1_R3(b, c, d, e, a, 59);
    sha1_R4(a, b, c, d, e, 60);
    sha1_R4(e, a, b, c, d, 61);
    sha1_R4(d, e, a, b, c, 62);
    sha1_R4(c, d, e, a, b, 63);
    sha1_R4(b, c, d, e, a, 64);
    sha1_R4(a, b, c, d, e, 65);
    sha1_R4(e, a, b, c, d, 66);
    sha1_R4(d, e, a, b, c, 67);
    sha1_R4(c, d, e, a, b, 68);
    sha1_R4(b, c, d, e, a, 69);
    sha1_R4(a, b, c, d, e, 70);
    sha1_R4(e, a, b, c, d, 71);
    sha1_R4(d, e, a, b, c, 72);
    sha1_R4(c, d, e, a, b, 73);
    sha1_R4(b, c, d, e, a, 74);
    sha1_R4(a, b, c, d, e, 75);
    sha1_R4(e, a, b, c, d, 76);
    sha1_R4(d, e, a, b, c, 77);
    sha1_R4(c, d, e, a, b, 78);
    sha1_R4(b, c, d, e, a, 79);
    state[0] += a;
    state[1] += b;
    state[2] += c;
    state[3] += d;
    state[4] += e;
}

void SHA1Init(SHA1_CTX * context) {
    context->state[0] = 0x67452301;
    context->state[1] = 0xEFCDAB89;
    context->state[2] = 0x98BADCFE;
    context->state[3] = 0x10325476;
    context->state[4] = 0xC3D2E1F0;
    context->count[0] = context->count[1] = 0;
}

void SHA1Update(SHA1_CTX * context, const unsigned char *data, uint32_t len) {
    uint32_t i,j = context->count[0];
    if ((context->count[0] += len << 3) < j) context->count[1]++;
    context->count[1] += (len >> 29);
    j = (j >> 3) & 63;
    if ((j + len) > 63) {
        memcpy(&context->buffer[j], data, (i = 64 - j));
        SHA1Transform(context->state, context->buffer);
        for (; i + 63 < len; i += 64) SHA1Transform(context->state, &data[i]);
        j = 0;
    }
    else i = 0;
    memcpy(&context->buffer[j], &data[i], len - i);
}

void SHA1Final(unsigned char digest[20], SHA1_CTX * context) {
    unsigned i;
    unsigned char finalcount[8], c;

    for (i = 0; i < 8; i++) finalcount[i] = (unsigned char) ((context->count[(i >= 4 ? 0 : 1)] >> ((3 - (i & 3)) * 8)) & 255);

    c = 0200;
    SHA1Update(context, &c, 1);
    while ((context->count[0] & 504) != 448) {
        c = 0000;
        SHA1Update(context, &c, 1);
    }
    SHA1Update(context, finalcount, 8);
    for (i = 0; i < 20; i++) digest[i] = (unsigned char)((context->state[i >> 2] >> ((3 - (i & 3)) * 8)) & 255);
}

void SHA1(char *hash_out, const char *str, int len) {
    SHA1_CTX ctx;
    unsigned int ii;

    SHA1Init(&ctx);
    for (ii=0; ii<len; ii+=1) SHA1Update(&ctx, (const unsigned char*)str + ii, 1);
    SHA1Final((unsigned char *)hash_out, &ctx);
}
#endif


