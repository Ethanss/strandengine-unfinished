﻿/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _AUDIO_H_
#define _AUDIO_H_

#include "slist.h"
#include "misc.h"
#include <SDL2/SDL.h>
#include <xmmintrin.h>
#include <smmintrin.h>

enum {
	AUDIO_44KHZ = 44100,
	AUDIO_22KHZ = 22050,
	AUDIO_11KHZ = 11025
};
#define AUDIO_SAMPLES 2048
#define AUDIO_MAX_CHANNELS 2

bool audioUseSSE,audioUseSSE4_1;
uint32_t audioFrequency, audioChannels = 0, audioStreamRequestLength, audioTimeMultiplier, audioTimeStep;
int32_t MEM_ALIGN(16) audioStreamBuffer[AUDIO_SAMPLES*AUDIO_MAX_CHANNELS];
SDL_AudioDeviceID audioPlaybackDevice;
slist audioCalls;

#define AUDIO_TIME_TO_MS(t) (t*1000/AUDIO_44KHZ)
#define AUDIO_TIME_FROM_MS(t) (t*AUDIO_44KHZ/1000)

void processAudio(void *userdata, int16_t *stream, int len) {
	if (audioCalls.list.length == 0) {
		memset(stream,audioStreamRequestLength>0?audioStreamBuffer[audioStreamRequestLength-1]:0,len);	
		return;
	}
	
	len /= 2;
	audioStreamRequestLength = len;
	audioTimeStep = len*audioTimeMultiplier/audioChannels;
	
	memset(audioStreamBuffer,0,len*4);

	slist_lock(&audioCalls);
	ifunction *ifp = audioCalls.list.buffer;
	for (uint32_t i = 0; i < audioCalls.list.length; i++) {
		ifp->delegate(ifp->owner);
		ifp++;
	}
	slist_unlock(&audioCalls);
	
	int32_t *s = audioStreamBuffer;
	if (audioUseSSE4_1 && len%4 == 0) {
		uint32_t MEM_ALIGN(16) tmp[4];
		uint32_t l4 = len/4;
		__m128i maxv = _mm_set_epi32(SHRT_MAX,SHRT_MAX,SHRT_MAX,SHRT_MAX),
		minv = _mm_set_epi32(SHRT_MIN,SHRT_MIN,SHRT_MIN,SHRT_MIN);
		for (uint32_t i = 0; i < l4; i++) {
			_mm_store_si128(tmp,_mm_max_epi32(minv,_mm_min_epi32(maxv,_mm_load_si128(s))));
			s += 4;
			*stream++ = tmp[0];
			*stream++ = tmp[1];
			*stream++ = tmp[2];
			*stream++ = tmp[3];
		}
	} else {
		uint32_t l4 = len/4;
		for (uint32_t i = 0; i < l4; i++) {
			int32_t sv = *s++;
			if (sv < SHRT_MIN) sv = SHRT_MIN;
			*stream++ = sv>SHRT_MAX?SHRT_MAX:sv;
			sv = *s++;
			if (sv < SHRT_MIN) sv = SHRT_MIN;
			*stream++ = sv>SHRT_MAX?SHRT_MAX:sv;
			sv = *s++;
			if (sv < SHRT_MIN) sv = SHRT_MIN;
			*stream++ = sv>SHRT_MAX?SHRT_MAX:sv;
			sv = *s++;
			if (sv < SHRT_MIN) sv = SHRT_MIN;
			*stream++ = sv>SHRT_MAX?SHRT_MAX:sv;
		}
		len -= l4*4;
		for (uint32_t i = 0; i < len; i++) {
			int32_t sv = *s++;
			if (sv < SHRT_MIN) sv = SHRT_MIN;
			*stream++ = sv>SHRT_MAX?SHRT_MAX:sv;
		}
	}
}

bool init_audio(char *devName, uint32_t freq, uint32_t channels) {
	if (SDL_InitSubSystem(SDL_INIT_AUDIO) < 0) {
		printf("SDL error at SDL_InitSubSystem(SDL_INIT_AUDIO): '%s'.\n",SDL_GetError());
		return false;
	}
	
	audioUseSSE = CPU_SUPPORTS_SSE2();
	audioUseSSE4_1 = CPU_SUPPORTS_SSE4_1();
	new_slist(&audioCalls,sizeof(ifunction),true);
	
	SDL_AudioSpec spec, respec;
	memset(&spec,0,sizeof(SDL_AudioSpec));
	spec.freq = freq;
	spec.channels = channels;
	spec.format = AUDIO_S16SYS;
	spec.samples = AUDIO_SAMPLES;
	spec.callback = processAudio;
	
	audioPlaybackDevice = SDL_OpenAudioDevice(devName, 0, &spec, &respec, 0);
	if (audioPlaybackDevice == 0) {
		printf("SDL error at SDL_OpenAudioDevice(playback): '%s'.\n", SDL_GetError());
		return false;
	}
	audioFrequency = respec.freq;
	audioChannels = respec.channels;
	audioTimeMultiplier = AUDIO_44KHZ/audioFrequency;
	SDL_PauseAudioDevice(audioPlaybackDevice,0);
	return true;
}

SDL_AudioDeviceID init_audio_recording(char *devName, uint32_t *frequency, uint32_t *channels, SDL_AudioFormat *format) {
	SDL_AudioSpec spec;
	memset(&spec,0,sizeof(SDL_AudioSpec));
	SDL_AudioDeviceID dev = SDL_OpenAudioDevice(devName, 1, &spec, &spec, SDL_AUDIO_ALLOW_FORMAT_CHANGE);
	if (dev == 0) {
		printf("SDL error at SDL_OpenAudioDevice(recording): '%s'.\n", SDL_GetError());
		return 0;
	}
	*frequency = spec.freq;
	*channels = spec.channels;
	*format = spec.format;
	return dev;
}

#define audio_record(dev,buf,len) SDL_DequeueAudio(dev,buf,len)

void free_audio() {
	SDL_CloseAudioDevice(audioPlaybackDevice);
	SDL_QuitSubSystem(SDL_INIT_AUDIO);
	delete_list(&audioCalls);
}

#define audio_play(p) SDL_PauseAudioDevice(audioPlaybackDevice,p?0:1);

//get buffer size required for audio_convert
uint32_t audio_csize(const uint32_t dataLen, const SDL_AudioFormat format, const uint32_t freq, const uint32_t channels) {
	const uint32_t bs = (format&0xFF)/8*freq*channels,
	cs = 2*audioFrequency*audioChannels;
	if (cs <= bs) return dataLen;
	return ((uint64_t)dataLen*(uint64_t)cs)/(uint64_t)bs;
}

//convert audio data to u16 format, returns new dataLength
uint32_t audio_convert(int16_t *data, const uint32_t dataLength, const SDL_AudioFormat format, const uint32_t frequency, const uint32_t channels) {
	SDL_AudioCVT cvt;
	SDL_BuildAudioCVT(&cvt, format, channels, frequency, AUDIO_S16SYS, audioChannels, audioFrequency);
	if (!cvt.needed) return dataLength;
	cvt.len = dataLength;
	cvt.buf = data;
	SDL_ConvertAudio(&cvt);
	return cvt.len_cvt;
}

/*add audio data to stream applying per-channel volume and playback rate
volume, 100 = 0.01% volume, 50000 = 50% volume, 100000 = 100% volume
rate, 1 = 0.01x rate, 100 = 1x rate, 10000 = 100x rate*/
int16_t *audio_stream(int16_t *data, uint32_t *length, uint32_t rate, int32_t leftVolume, int32_t rightVolume) {
	uint32_t llen = *length;
	if (llen > audioStreamRequestLength) llen = audioStreamRequestLength;

	leftVolume = (leftVolume*328)/1001;
	rightVolume = (rightVolume*328)/1001;
	int32_t *s = audioStreamBuffer;
	if (rate == 100) {
		if (audioUseSSE && llen%4 == 0) {
			const uint32_t alen = llen/4;
			int32_t MEM_ALIGN(16) miab[4];
			if (audioUseSSE4_1) {
				miab[0] = leftVolume;miab[1] = rightVolume;miab[2] = leftVolume;miab[3] = rightVolume;
				__m128i vv = _mm_load_si128(miab);
				for (uint32_t i = 0; i < alen; i++) {
					miab[0] = *data++;miab[1] = *data++;miab[2] = *data++;miab[3] = *data++;
					_mm_store_si128(s,_mm_add_epi32(_mm_load_si128(s),_mm_srai_epi32(_mm_mullo_epi32(_mm_load_si128(miab),vv),16)));
					s += 4;
				}
			} else {
				for (uint32_t i = 0; i < alen; i++) {
					miab[0] = (((int32_t)*data++)*leftVolume)>>16;
					miab[1] = (((int32_t)*data++)*rightVolume)>>16;
					miab[2] = (((int32_t)*data++)*leftVolume)>>16;
					miab[3] = (((int32_t)*data++)*rightVolume)>>16;
					_mm_store_si128(s,_mm_add_epi32(_mm_load_si128(s),_mm_load_si128(miab)));
					s += 4;
				}
			}
		} else {
			if (audioChannels != 1 && llen%2 == 0) {
				const uint32_t alen = llen/2;
				for (uint32_t i = 0; i < alen; i++) {
					*s++ += (((int32_t)*data++)*leftVolume)>>16;
					*s++ += (((int32_t)*data++)*rightVolume)>>16;
				}
			} else {
				int32_t mv = leftVolume>rightVolume?leftVolume:rightVolume;
				for (uint32_t i = 0; i < llen; i++) {
					*s++ += (((int32_t)*data++)*mv)>>16;
				}
			}
		}
		uint32_t plen = *length;
		if (llen > *length) *length = 0;
		else *length = plen-llen;
	} else {
		const uint32_t nauc = audioChannels;
		bool mchannels = nauc!=1;
	
		int32_t *vola = alloca(nauc*4);
		if (nauc == 1) vola[0] = max(leftVolume,rightVolume);
		else {
			for (uint32_t i = 0; i < nauc; i++) vola[i] = i%2==0?leftVolume:rightVolume;
		}
		
		uint32_t sc = 0, rl = 0, li = 0;
		rate /= nauc;
		for (uint32_t i = 0; i < llen; i++) {
			*s++ += (((int32_t)data[li])*vola[li])>>16;
			if (mchannels) li = (li+1)%nauc;
			sc += rate;
			if (sc >= 100) {
				uint32_t ni = sc/100;
				sc -= ni*100;
				ni *= nauc;
				rl += ni;
				data += ni;
			}
		}
		uint32_t plen = *length;
		if (rl > *length) *length = 0;
		else *length = plen-rl;
	}
	
	if (llen < audioStreamRequestLength) {
		if (audioChannels == 1) {
			int16_t rv = s[-1];
			for (uint32_t i = llen; i < audioStreamRequestLength; i++) *s++ = rv;
		} else {
			int16_t *av = alloca(audioChannels*2);
			for (uint32_t i = 0; i < audioChannels; i++) av[i] = s[(int32_t)i-(int32_t)audioChannels];
			
			int16_t nauc = audioChannels;
			for (uint32_t i = llen; i < audioStreamRequestLength; i++) *s++ = av[i%nauc];
		}
	}
	
	return data;
}


//audio piece used for storing music, sound and other audio
typedef struct {
	int16_t *data;
	uint32_t length,//length in samples
		lengthInMilliseconds;
} audio_piece;

//load .WAV file and create new audio_piece filled with the data
bool load_audio_piece(audio_piece *a, const char *file) {
	SDL_AudioSpec spec;
	uint8_t *wdat;
	uint32_t wlen;
	if (SDL_LoadWAV(file, &spec, &wdat, &wlen) == NULL) return false;
	const uint32_t csz = audio_csize(wlen,spec.format,spec.freq,spec.channels);
	a->data = malloc(csz);
	memcpy(a->data,wdat,wlen);
	SDL_FreeWAV(wdat);
	a->length = audio_convert(a->data,wlen,spec.format,spec.freq,spec.channels);
	if (a->length < csz) a->data = realloc(a->data,a->length);
	a->length /= 2;
	a->lengthInMilliseconds = a->length*1000/(audioFrequency*audioChannels);
	return true;
}
//create new audio_piece from raw data, dataLen is in bytes
void new_audio_piece(audio_piece *a, int16_t *d, const uint32_t dataLen) {
	a->data = d;
	a->length = dataLen/2;
	a->lengthInMilliseconds = a->length*1000/(audioFrequency*audioChannels);
}


//audio channel for easily playing and controlling audio piece
typedef struct {
	audio_piece *playing;
	int16_t *data;
	uint32_t dataLength, rate, leftVolume, rightVolume;
	bool loop, pause;
} audio_channel;

void new_audio_channel(audio_channel *c) {
	c->playing = NULL;
	c->rate = 100;
	c->leftVolume = 100000;
	c->rightVolume = 100000;
	c->loop = false;
	c->pause = false;
}

void audio_channel_play(audio_channel *c, audio_piece *p) {
	c->data = p->data;
	c->dataLength = p->length;
	c->playing = p;
}
#define audio_channel_stop(c) (c)->playing = NULL

void audio_channel_seek(audio_channel *c, const uint32_t ms) {
	audio_piece *p = c->playing;
	uint32_t di = ms*(audioFrequency*audioChannels)/1000;
	c->data = p->data+di;
	c->dataLength = p->length-di;
}

void audio_channel_stream(audio_channel *c) {
	if (c->pause || c->playing == NULL) return;
	c->data = audio_stream(c->data,&c->dataLength,c->rate,c->leftVolume,c->rightVolume);
	if (c->dataLength == 0) {
		if (c->loop) {
			audio_piece *p = c->playing;
			c->data = p->data;
			c->dataLength = p->length;
		} else c->playing = NULL;
	}
}

void audio_channel_enable(audio_channel *c) {
	cpifunction(audio_channel_stream,c,slist_add(&audioCalls,&ifunc));
}
void audio_channel_disable(audio_channel *c) {
	cpifunction(audio_channel_stream,c,slist_remove(&audioCalls,&ifunc));
}

#endif
