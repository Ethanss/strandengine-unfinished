﻿/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _ASSETS_H_
#define _ASSETS_H_

#include "assets_base.h"

//new asset database from file 'f'
void new_asset_database(asset_database *a, char *f) {
	new_hashmap(&a->loaded,sizeof(void*),2);
	if (!new_file_stream(&a->stream, f, FILE_MODE_READ)) {
		a->path = NULL;
		return;
	}
	stream_readUniversal(&a->stream, &a->mapSize, 4, 4);
	a->path = f;
}
//unload all assets from database
void asset_database_unloadAll(asset_database *a) {
	uint32_t hv,i = 0;
	while (true) {
		void **c;
		hashmap_enumerateEntries(&a->loaded,&i,&hv,(void*)&c);
		if (hv == NULL_HASH) break;
		asset *a = *c;
		a->instances = 0;
		a->type->unload(a);
	}
}
//delete asset database and unload assets
void delete_asset_database(asset_database *a) {
	asset_database_unloadAll(a);
	delete_hashmap(&a->loaded);
	if (a->path == NULL) return;
	file_stream_close(&a->stream);
}

//get asset from database via name hash, returns NULL if no asset exists
asset *asset_database_get(asset_database *a, const uint32_t hashv) {
	//check if asset already loaded
	asset **easset = hashmap_get(&a->loaded,hashv);
	if (easset != NULL) {
		asset *la = *easset;
		la->instances++;
		return la;
	}
	
	if (a->path == NULL) return NULL;
	
	//load fresh asset instance, find hash index
	file_stream *fs = &a->stream;
	uint32_t fmapSz = a->mapSize,hi,hv,i;
	hi = hashv%fmapSz;
	file_stream_setPosition(fs,4+hi*4);
	for (i = 0; i < fmapSz; i++) {
		stream_readUniversal(fs,&hv,4,4);
		if (hv == hashv) break;
		hi++;
		if (hi == fmapSz) {
			hi = 0;
			file_stream_setPosition(fs,4);
		}
	}
	if (i >= fmapSz) return NULL;

	//load asset
	uint64_t meta[3];
	file_stream_setPosition(fs,4+fmapSz*4+hi*24);
	stream_readUniversal(fs,&meta[0],8,24);
	file_stream_setPosition(fs,4+fmapSz*28+meta[0]);

	fixed_array name;
	name.stride = 1;
	stream_readUniversal(fs,&name.length,4,4);
	asset *na = malloc(sizeof(asset)+name.length+1);
	name.buffer = na+1;
	file_stream_read(fs,name.buffer,name.length);
	((char*)name.buffer)[name.length] = '\0';

	//find asset type from extension
	asset_type *atype = NULL;
	for (atype = (void*)LAST_ASSET_TYPE; atype != NULL; atype = (void*)atype->next) {
		uint32_t next = atype->extensions.length;
		fixed_array *fa = atype->extensions.buffer;
		uint32_t k;
		for (k = 0; k < next; k++) {
			if (list_endsWith((void*)&name,(void*)fa)) break;
			fa++;
		}
		if (k < next) break;
	}
	if (atype == NULL) {
		free(na);
		return NULL;
	}

	na->name = name;
	na->type = atype;
	na->includes = NULL;
	na->database = a;
	na->hash = hashv;
	na->instances = 1;

	if (meta[2] > 0) {
		fixed_array jlist;
		jlist.stride = 1;
		jlist.length = meta[2];
		jlist.buffer = malloc(meta[2]);
		file_stream_read(fs,jlist.buffer,meta[2]);
		na->options = json_parse((void*)&jlist).objectv;
		free(jlist.buffer);
	} else {
		na->options = NULL;
	}

	na->fileHandle = fs->handle;
	na->fileSize = (uint32_t)meta[1];
	atype->load(na);

	hashmap_add(&a->loaded,hashv,&na);
	return na;
}

//return asset so the databse knows memory can be freed
void asset_database_return(asset_database *a, asset *r) {
	if (r->instances == 0) return;
	r->instances--;
	if (r->instances == 0) {
		hashmap_remove(&a->loaded,r->hash);
		r->type->unload(r);
	}
}

//helper for loading new assets and unloading old
void asset_database_load(asset_database *db, const uint32_t hv, asset **a, void **dst) {
	if (hv != NULL_HASH) {
		asset *ta = *a;
		bool ns = true;
		if (ta != NULL) {
			ns = ta->hash != hv;
			if (ns) asset_database_return(db, ta);
		}
		if (ns) {
			ta = asset_database_get(db, hv);
			if (ta != NULL) *a = ta;
		}
		*dst = ta==NULL?NULL:ta->asset;
	}
}

bool asset_loadManaged(asset *a, const char *filePath, json_object *jsobj) {
	//find asset type from extension
	list name = cstring_as_list(filePath);
	asset_type *atype = NULL;
	for (atype = (void*)LAST_ASSET_TYPE; atype != NULL; atype = (void*)atype->next) {
		uint32_t next = atype->extensions.length;
		fixed_array *fa = atype->extensions.buffer;
		uint32_t k;
		for (k = 0; k < next; k++) {
			if (list_endsWith((void*)&name,(void*)fa)) break;
			fa++;
		}
		if (k < next) break;
	}
	if (atype == NULL) return false;
	
	a->database = NULL;
	a->includes = NULL;
	a->options = jsobj;
	a->type = atype;
	a->instances = 1;
	a->name = *(fixed_array*)&name;
	a->fileSize = file_size(filePath);
	FILE *fh = fopen(filePath, FILE_MODE_READ);
	a->fileHandle = fh;
	if (a->fileHandle == NULL) return false;
	atype->load(a);
	fclose(fh);
	return true;
}


//get file_stream with position at asset of 'hashv', returns length of asset in bytes otherwise returns 0 if no asset exists
uint64_t asset_database_seek(asset_database *a, const uint32_t hashv) {
	file_stream *fs = &a->stream;

	//find hash index
	uint32_t fmapSz,hi,hv,i;
	stream_readUniversal(fs,&fmapSz,4,4);
	hi = hashv%fmapSz;
	file_stream_setPosition(fs,4+hi*4);
	for (i = 0; i < fmapSz; i++) {
		stream_readUniversal(fs,&hv,4,4);
		if (hv == hashv) break;
		hi++;
		if (hi == fmapSz) {
			hi = 0;
			file_stream_setPosition(fs,4);
		}
	}
	if (i >= fmapSz) {
		file_stream_close(fs);
		return 0;
	}

	//load asset
	uint64_t meta[3];
	file_stream_setPosition(fs,4+fmapSz*4+hi*24);
	stream_readUniversal(fs,&meta[0],8,24);
	file_stream_setPosition(fs,4+fmapSz*28+meta[0]);

	uint32_t nlen;
	stream_readUniversal(fs,&nlen,4,4);
	file_stream_setPosition(fs,fs->position+nlen+meta[2]);

	return meta[1];
}

#ifdef _ENGINE_SERIALIZATION_H_
bool asset_database_loadObject(asset_database *db, const uint32_t hashv, object *o) {
	char nbuf[256];
	uint64_t slen = asset_database_seek(db,hashv);
	if (slen == 0) return false;
	object_deserialize(o, &db->stream);
	return true;
}
bool asset_database_loadObjectFixed(asset_database *db, const uint32_t hashv, object *o) {
	uint64_t slen = asset_database_seek(db,hashv);
	if (slen == 0) return false;
	object_deserializeFixed(o,&db->stream);
}
#endif

#endif
