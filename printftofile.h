
bool initializedPrintfToFile = false;
FILE *printfToFile;
char printfFileBuffer[4096];
uint32_t printfBufferLength;
#define printf(...) if (!initializedPrintfToFile) {\
	initializedPrintfToFile = true;\
	printfToFile = fopen("log.txt","wb");\
}\
printfBufferLength = snprintf(printfFileBuffer,4096,__VA_ARGS__);\
fwrite(printfFileBuffer,1,printfBufferLength,printfToFile);\
fflush(printfToFile);
