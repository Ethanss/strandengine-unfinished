﻿/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _REFLECTION_H_
#define _REFLECTION_H_

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#define REFLECTION_TYPE_uint8_t 0
#define REFLECTION_TYPE_int8_t 0
#define REFLECTION_TYPE_uint16_t 1
#define REFLECTION_TYPE_int16_t 1
#define REFLECTION_TYPE_uint32_t 2
#define REFLECTION_TYPE_int32_t 2
#define REFLECTION_TYPE_uint64_t 3
#define REFLECTION_TYPE_int64_t 3
#define REFLECTION_TYPE_float 4
#define REFLECTION_TYPE_double 5
#define REFLECTION_TYPE_list 6
#define REFLECTION_TYPE_fixed_array 7


const char* REFLECTION_TYPE_NAME[8] = {"int8","int16","int32","int64","real32","real64","list","fixed_array"};
const uint8_t REFLECTION_TYPE_SIZE[8] = {1,2,4,8,4,8,12+sizeof(void*),8+sizeof(void*)};


typedef struct {
	char *name;
	uint16_t offset;
	uint8_t type,nameLength;
} reflection_data;

#define REFLECT(t,n) {#n,offsetof(REFLECT_TARGET,n),REFLECTION_TYPE_##t,sizeof(#n)-1}
#define REFLECTF(t,n) {NULL,offsetof(REFLECT_TARGET,n),REFLECTION_TYPE_##t,0}

reflection_data *reflection_findField(reflection_data *fs, const uint16_t fc, char *n, uint8_t nlen, uint8_t type) {
	for (uint16_t i = 0; i < fc; i++) {
		if (fs->type == type && fs->nameLength == nlen && memcmp(n,fs->name,nlen) == 0) return fs;
		fs++;
	}
	return NULL;
}
void reflection_copy(reflection_data *fs, uint16_t fc, void *src, void *dst, bool newArrays) {
	for (uint32_t i = 0; i < fc; i++) {
		uint16_t offset = fs->offset;
		memcpy((uint8_t*)dst + offset, (uint8_t*)src + offset, REFLECTION_TYPE_SIZE[fs->type]);
		if (newArrays && fs->type > 5) {
			void **da = (void*)((uint8_t*)dst + offset), **sa = (void*)((uint8_t*)src + offset);
			uint32_t *dp = (uint32_t*)(da + 1),
				sz = (fs->type == 6 ? dp[2] : dp[0])*dp[1];
			void *nd;
			*sa = nd = malloc(sz);
			memcpy(nd, *da, sz);
		}
		fs++;
	}
}
bool reflection_compare(reflection_data *fs, uint16_t fc, void *src, void *dst) {
	for (uint32_t i = 0; i < fc; i++) {
		uint16_t offset = fs->offset;
		if (fs->type > 5) {
			void **da = (void*)((uint8_t*)dst + offset),
				**sa = (void*)((uint8_t*)src+offset);
			uint32_t dl = *(uint32_t*)(da + 1),
				sl = *(uint32_t*)(sa+1);
			if (memcmp(*da, *sa, sl<dl?sl:dl) != 0) return false;
		}
		else {
			if (memcmp((uint8_t*)dst + offset, (uint8_t*)src + offset, REFLECTION_TYPE_SIZE[fs->type]) != 0) return false;
		}
		fs++;
	}
	return true;
}
void reflection_free(reflection_data *fs, uint16_t fc, uint8_t *p) {
	for (uint32_t i = 0; i < fc; i++) {
		if (fs->type > 5) free(*(void**)(p + fs->offset));
		fs++;
	}
}
#endif
