﻿/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _SYSTEM_H_
#define _SYSTEM_H_

#ifdef __linux__
#include <time.h>
#include <pthread.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/sysinfo.h>
#else
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#define popen _popen
#define pclose _pclose
#endif

#include <stdio.h>
#include <stdbool.h>
#include "list.h"

//command line launch arguments
int argumentCount;
char **arguments;
#if __linux__
void parseCommandLineArguments(int argc, char **argv) {
	argumentCount = argc-1;
	arguments = argv+1;
}
#else
void parseCommandLineArguments() {
	int argc = 0;
	char **argv;

	char *cmlb = GetCommandLine(), *p = cmlb, cc, lc = 0;
	bool pname = true, inq = false;
	while ((cc = *p++) != 0) {
		if (cc == 34) inq = !inq;
		if (!inq) {
			if (pname) {
				if (cc == 32 && lc == 32) {
					pname = false;
					argc++;
				}
				lc = cc;
			}
			else {
				if (cc == 32) argc++;
			}
		}
	}
	if (argc != 0) {
		char **argb = malloc(sizeof(void*)*argc);
		argv = argb;
		p = cmlb;
		pname = true;
		inq = false;
		lc = 0;
		uint32_t pc = 0;
		while ((cc = *p++) != 0) {
			if (cc == 34) inq = !inq;
			if (!inq) {
				if (pname) {
					if (cc == 32 && lc == 32) {
						pname = false;
					}
					lc = cc;
				}
				else {
					if (cc == 32) {
						argb[0] = malloc(pc + 1);
						memcpy(argb[0], p - pc, pc);
						argb[0][pc] = 0;
						argb++;
						*argb = p;
						pc = 0;
					}
				}
			}
			if (!pname) pc++;
		}
		argb[0] = malloc(pc + 1);
		memcpy(argb[0], p - pc, pc);
		argb[0][pc] = 0;
	}

	argumentCount = argc;
	arguments = argv;
}
void setupWindowsConsole(const bool allocNew) {
	if (allocNew) AllocConsole();
	else AttachConsole(-1);
	freopen("CONIN$", "r", stdin);
	freopen("CONOUT$", "w", stdout);
	freopen("CONOUT$", "w", stderr);
}
#endif

//run system command
bool runSystemCommand(char *scmd, list *out) {
	FILE *fp = popen(scmd,"r");
	if (fp == NULL) return false;
	if (out != NULL) {
		char b[LIST_MAX_STACK_ALLOC];
		while (fgets(b,LIST_MAX_STACK_ALLOC,fp) != NULL) {
			const uint32_t bl = strlen(b)+1;
			list_addm(out,b,bl);
		}
	}
	pclose(fp);
	return true;
}

//current working directory
list getCurrentDirectory() {
	list str;
	new_list(&str, 1);
	str.length = 4096;
	list_expand(&str);

	#ifdef __linux__
	getcwd(str.buffer, 4096);
	str.length = strlen(str.buffer);
	#else
	str.length = GetCurrentDirectory(4096,str.buffer);
	#endif

	return str;
}

enum {
	FILE_TYPE_FILE = 0,
	FILE_TYPE_DIRECTORY = 1,
	FILE_TYPE_LINK = 2
};

typedef struct {
	char name[256];
	uint8_t type;
} file_info;

//list all files/directories/symlinks in directory
list listFiles(const char *path) {
	list files;
	new_list(&files, sizeof(file_info));

	#ifdef __linux__
	DIR *d = opendir(path);
	if (d == NULL) return files;
	struct dirent *entry;
	while ((entry = readdir(d)) != NULL) {
		uint8_t ft = entry->d_type;
		
		file_info fi;
		if (ft == DT_REG) fi.type = FILE_TYPE_FILE;
		else if (ft == DT_DIR) fi.type = FILE_TYPE_DIRECTORY;
		else if (ft == DT_LNK) fi.type = FILE_TYPE_LINK;
		else continue;

		memcpy(fi.name,entry->d_name,256);
		if (strcmp(fi.name,".") == 0 || strcmp(fi.name,"..") == 0) continue;
		list_add(&files, &fi);
	}
	closedir(d);
	#else
	WIN32_FIND_DATA search_data;
	memset(&search_data, 0, sizeof(WIN32_FIND_DATA));

	uint32_t plen = strlen(path);
	char *pbuf = malloc(plen+3);
	memcpy(pbuf, path, plen);
	pbuf[plen] = '\\';
	pbuf[plen + 1] = '*';
	pbuf[plen + 2] = '\0';
	file_info fi;
	HANDLE handle = FindFirstFile(pbuf, &search_data);
	if (handle != INVALID_HANDLE_VALUE) {
		while (true)
		{
			DWORD fa = search_data.dwFileAttributes;
			if ((fa&FILE_ATTRIBUTE_DIRECTORY) != 0) fi.type = FILE_TYPE_DIRECTORY;
			else if ((fa&FILE_ATTRIBUTE_REPARSE_POINT) != 0) fi.type = FILE_TYPE_LINK;
			else fi.type = FILE_TYPE_FILE;

			if (strcmp(search_data.cFileName, ".") != 0 && strcmp(search_data.cFileName, "..") != 0) {
				memcpy(fi.name, search_data.cFileName, strlen(search_data.cFileName)+1);
				list_add(&files, &fi);
			}

			if (FindNextFile(handle, &search_data) == FALSE) break;
		}
	}
	free(pbuf);
	FindClose(handle);
	#endif

	return files;
}

//create thread
#define THREAD_DEFAULT_STACK_SIZE 0
#ifdef __linux__
#define thread_id pid_t
#else
#define thread_id HANDLE
#endif
void createThread(const void *startPtr, void *arg, const uint32_t stackSize) {
	#ifdef __linux__
	pthread_t pt;
	if (stackSize != THREAD_DEFAULT_STACK_SIZE) {
		pthread_attr_t attr;
		pthread_attr_init(&attr);
		pthread_attr_setstacksize(&attr,stackSize);
		pthread_create(&pt,&attr,startPtr,arg);
		pthread_attr_destroy(&attr);
	} else pthread_create(&pt, NULL, startPtr, arg);
	#else
	CreateThread(NULL, stackSize, startPtr, arg, 0, NULL);
	#endif
}

//sleep, wait for 'ms' milliseconds before continuing execution
#ifdef __linux__
#define threadSleep(ms) usleep(((useconds_t)ms)*1000)
#else
#define threadSleep(ms) Sleep(ms)
#endif

//allocate using operating system functions, allows setting memory permissions and alignment
uint32_t cachedMemoryPageSize = 0;
#ifdef __linux__
static inline uint32_t memoryPageSize() {
	if (cachedMemoryPageSize == 0) cachedMemoryPageSize = sysconf(_SC_PAGESIZE);
	return cachedMemoryPageSize;
}
#define osm_noaccess PROT_NONE
#define osm_read PROT_READ
#define osm_readwrite PROT_READ|PROT_WRITE
#define osm_execute PROT_EXEC|PROT_READ
#define osm_executewrite PROT_EXEC|PROT_READ|PROT_WRITE
#define osmalloc(sz) mmap(NULL, sz, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0)
#define osmprotect(p,sz,ex) mprotect((void*)((uintptr_t)p&(uintptr_t)(~(memoryPageSize()-1))),sz,ex)
#define osmexec(p,sz,ex) mprotect(p,sz,ex?(PROT_EXEC|PROT_READ):(PROT_READ|PROT_WRITE))
#define osmfree(p,sz) munmap(p,sz)
#else
static inline uint32_t memoryPageSize() {
	if (cachedMemoryPageSize == 0) {
		SYSTEM_INFO si;
		GetSystemInfo(&si);
		cachedMemoryPageSize = si.dwPageSize;
	}
	return cachedMemoryPageSize;
}
#define osm_noaccess PAGE_NOACCESS
#define osm_read PAGE_READONLY
#define osm_readwrite PAGE_READWRITE
#define osm_execute PAGE_EXECUTE_READ
#define osm_executewrite PAGE_EXECUTE_READWRITE
#define osmalloc(sz) VirtualAlloc(NULL, sz, MEM_COMMIT, PAGE_READWRITE)
#define osmexec(p,sz,ex) VirtualAlloc(p, sz, MEM_COMMIT, ex ? PAGE_EXECUTE : PAGE_READWRITE)
#define osmprotect(p,sz,ex) VirtualAlloc(p, sz, MEM_COMMIT, ex)
#define osmfree(p,sz) VirtualFree(p, 0, MEM_RELEASE)
#endif


//system time in milliseconds
#ifndef __linux__
bool measuredTimeNowPerformanceFrequency = false;
LARGE_INTEGER timeNowPerformanceFrequency;
#endif
static inline uint64_t timeNow() {
	#ifdef __linux__
	struct timespec t;
	clock_gettime(CLOCK_MONOTONIC, &t);
	return ((uint64_t)t.tv_sec)*1000 + (uint64_t)(t.tv_nsec/1000000);
	#else
	if (!measuredTimeNowPerformanceFrequency) {
		QueryPerformanceFrequency(&timeNowPerformanceFrequency);
		timeNowPerformanceFrequency.QuadPart /= 1000;
		measuredTimeNowPerformanceFrequency = true;
	}
	LARGE_INTEGER t;
	QueryPerformanceCounter(&t);
	return t.QuadPart / timeNowPerformanceFrequency.QuadPart;
	#endif

	return 0;
}

//number of CPU cores
static inline int32_t logicalProcessorCount() {
	#ifdef __linux__
	return sysconf(_SC_NPROCESSORS_ONLN);
	#else
	SYSTEM_INFO sysInfo;
	GetSystemInfo(&sysInfo);
	return sysInfo.dwNumberOfProcessors;
	#endif
}

//cryptographic pseudo-random bytes
#ifndef __linux__
#include <wincrypt.h>
bool initializedWindowsRandomCryptContext = false;
HCRYPTPROV windowsRandomCryptContext;
#endif
static inline void random_bytes(void *d, uint32_t sz) {
	#ifdef __linux__
	FILE *fp = fopen("/dev/urandom", "r");
	fread(d,1,sz,fp);
	fclose(fp);
	#else
	if (!initializedWindowsRandomCryptContext) {
		CryptAcquireContext(&windowsRandomCryptContext, NULL, NULL, 0, 0);
		initializedWindowsRandomCryptContext = true;
	}
	CryptGenRandom(windowsRandomCryptContext, sz, d);
	#endif
}

//mutex/atomic compare synchronization
#ifdef __linux__
#define DECLARE_LOCK(n) pthread_mutex_t n
#define INIT_LOCK(n) pthread_mutex_init(&n,NULL)
#define FREE_LOCK(n) pthread_mutex_destroy(&n)
#define SYNCHRONIZE_BEGIN(n) pthread_mutex_lock(&n)
#define SYNCHRONIZE_END(n) pthread_mutex_unlock(&n)
#define ATOMIC_SWAP(src,test,next) __sync_val_compare_and_swap(src,test,next)
#else
#define DECLARE_LOCK(n) HANDLE n
#define INIT_LOCK(n) n = CreateMutex(NULL,FALSE,NULL)
#define FREE_LOCK(n) CloseHandle(n)
#define SYNCHRONIZE_BEGIN(n) WaitForSingleObject(n,INFINITE)
#define SYNCHRONIZE_END(n) ReleaseMutex(n)
#define ATOMIC_SWAP(src,test,next) InterlockedCompareExchange(src,next,test)
#endif

//audio beep for message/notification
#ifdef __linux__
#define messagebeep() {printf("%c\n",7);}
#else
#define messagebeep() MessageBeep(MB_ICONWARNING)
#endif
#endif
