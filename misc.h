﻿/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _MISC_H_
#define _MISC_H_

#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include "endianness.h"

#define clamp(v,min,max) ((v)<(min)?(min):((v)>(max)?(max):(v)))

static inline uint32_t nalign(const uint32_t v, const uint32_t align) {
	return v==0?0:((v-1)/align+1)*align;
}
static inline uintptr_t ptralign(const uintptr_t ptr, const uint32_t align) {
	uint32_t off = ptr%align;
	if (off == 0) return ptr;
	return ptr+(align-off);
}

void *malloc_aligned(const uintptr_t sz, const uint32_t align) {
	uint8_t *m = malloc(sz+align+1),
	al = ((uintptr_t)m+1)%align;
	if (al == 0) al = 1;
	else al = 1+align-al;
	m[al-1] = al;
	return m+al;
}
void free_aligned(uint8_t *m) {
	free(m-m[-1]);
}

typedef struct {
	void *buffer;
	uint32_t length, stride;
} fixed_array;

typedef void(*zero_arg_function)();
typedef void(*one_arg_function)(void*);
typedef void(*two_arg_function)(void*, void*);
typedef void(*three_arg_function)(void*, void*, void*);
typedef void(*four_arg_function)(void*, void*, void*, void*);
typedef void(*five_arg_function)(void*, void*, void*, void*, void*);
typedef void(*six_arg_function)(void*, void*, void*, void*, void*, void*);
typedef void(*seven_arg_function)(void*, void*, void*, void*, void*, void*, void*);
typedef void(*eight_arg_function)(void*, void*, void*, void*, void*, void*, void*, void*);

//class function delegate
typedef struct {
	one_arg_function delegate;
	void *owner;
} ifunction;

#define cpifunction(f,o,c) {ifunction ifunc = (ifunction){f,o};c;}

//array of bitfields/bools
#define flags_nbytes(n) (((int)(n)-1)/8+1)
#define declare_flags(f,n) uint8_t f[flags_nbytes(n)]
#define malloc_flags(n) malloc(flags_nbytes(n))
#define alloca_flags(n) alloca(flags_nbytes(n))
static inline bool flags_get(uint8_t *f, const uint32_t i) {
	uint32_t d = i / 8;
	return (f[d] >> (i - d*8)) & 1;
}
static inline void flags_set(uint8_t *f, const uint32_t i, const bool v) {
	uint32_t d = i / 8;
	uint8_t m = 1<<(i-d*8);
	if (v) f[d] |= m;
	else f[d] &= ~m;
}
#define flags_setall(f,n,v) memset(f,v?0xFF:0,flags_nbytes(n))
#define flags_equal(f,b,n) (memcmp(f,b,flags_nbytes(n))==0)
#define flags_beginLoopGet(f,vn) uint8_t *p##vn = f, v##vn, c##vn = 8;
#define flags_loopGet(vn,res) if (c##vn == 8) {\
	v##vn = *p##vn++;\
	c##vn = 0;\
}\
res = ((v##vn)>>(c##vn++))&1;
#define flags_beginLoopSet(f,vn) uint8_t *p##vn = f, v##vn = 0, c##vn = 0
#define flags_loopSet(vn,sv) v##vn |= (sv)<<(c##vn++);\
if (c##vn == 8) {\
	*p##vn++ = v##vn;\
	c##vn = 0;\
	v##vn = 0;\
}
#define flags_endLoopSet(vn) *p##vn = v##vn

//cpu id/feature support detection
typedef struct {
	union {
		uint32_t data[4];
		struct {
			uint32_t eax,ebx,ecx,edx;
		};
	};
} cpu_id_bits;
#ifdef _MSC_VER
#include <intrin.h>
#else
#include <cpuid.h>
uint64_t _xgetbv(uint32_t index)
{
	uint32_t eax, edx;
	__asm__ __volatile__("xgetbv;" : "=a" (eax), "=d"(edx) : "c" (index));
	return ((uint64_t)edx << 32) | eax;
}
#endif
static inline cpu_id_bits CPU_ID(uint32_t level) {
	cpu_id_bits b = {0,0,0,0};
	#ifdef _MSC_VER
	__cpuid(&b.data[0],level);
	#else
	__get_cpuid(level,&b.data[0],&b.data[1],&b.data[2],&b.data[3]);
	#endif
	return b;
}

bool HAS_CPU_SUPPORTS_BEEN_CALLED = false;
declare_flags(CPU_SUPPORTS_FLAGS,32);
void CPU_SUPPORTS() {
	HAS_CPU_SUPPORTS_BEEN_CALLED = true;

	uint32_t bcount = 0,v,q;
	#define fsf(reg,b) q = bcount++;v = (bid.##reg>>b)&1;flags_set(CPU_SUPPORTS_FLAGS,q,v)
	
	//processor features eax=1
	cpu_id_bits bid = CPU_ID(1);
	fsf(edx,23);//0 - MMX
	fsf(edx,25);//SSE
	fsf(edx,26);//SSE2
	fsf(ecx,0);//SSE3
	fsf(ecx,9);//SSSE3
	fsf(ecx,12);//FMA
	fsf(ecx,18);//DCA
	fsf(ecx,19);//SSE4_1
	fsf(ecx,20);//SSE4_2
	fsf(ecx,22);//MOVBE
	fsf(ecx,25);//AES
	fsf(ecx,27);//OSXSAVE
	fsf(ecx,28);//AVX
	fsf(ecx,30);//13 - RDRAND
	
	//extended features eax=7
	bid = CPU_ID(7);
	fsf(ebx,5);//14 - AVX2
	fsf(ebx,16);//AVX512_f
	fsf(ebx,17);//AVX512_dq
	fsf(ebx,18);//RDSEED
	fsf(ebx,21);//AVX512_ifma
	fsf(ebx,26);//AVX512_pf
	fsf(ebx,27);//AVX512_er
	fsf(ebx,28);//AVX512_cd
	fsf(ebx,29);//SHA
	fsf(ebx,30);//AVX512_bw
	fsf(ebx,31);//AVX512_vl
	fsf(ecx,1);//AVX512_vbmi
	fsf(ecx,6);//AVX512_vbmi2
	fsf(ecx,11);//AVX512_vnni
	fsf(ecx,12);//AVX512_bitalg
	fsf(ecx,14);//AVX512a_vpopcntdq
	fsf(edx,2);//avx512_4vnniw
	fsf(edx,3);//avx512_4fmaps
	#undef fsf

	//ensure OSXSAVE is actually enabled
	if (flags_get(CPU_SUPPORTS_FLAGS,9)) flags_set(CPU_SUPPORTS_FLAGS,9,(_xgetbv(0)&6)==6);
}
#define dcs(n,i) static inline bool CPU_SUPPORTS_##n() {if (!HAS_CPU_SUPPORTS_BEEN_CALLED) {CPU_SUPPORTS();}return flags_get(CPU_SUPPORTS_FLAGS,i);}
dcs(MMX,0);
dcs(SSE,1);
dcs(SSE2,2);
dcs(SSE3,3);
dcs(SSSE3,4);
dcs(FMA,5);
dcs(DCA,6);
dcs(SSE4_1,7);
dcs(SSE4_2,8);
dcs(MOVBE,9);
dcs(AES,10);
dcs(OSXSAVE,11);
dcs(AVX,12);
dcs(RDRAND,13);
dcs(AVX2,14);
dcs(AVX512_F,15);
dcs(AVX512_DQ,16);
dcs(RDSEED,17);
dcs(AVX512_IFMA,18);
dcs(AVX512_PF,19);
dcs(AVX512_ER,20);
dcs(AVX512_CD,21);
dcs(SHA,22);
dcs(AVX512_BW,23);
dcs(AVX512_VL,24);
dcs(AVX512_VBMI,25);
dcs(AVX512_VBMI2,26);
dcs(AVX512_VNNI,27);
dcs(AVX512_BITALG,28);
dcs(AVX512_VPOPCNTDQ,29);
dcs(AVX512_4VNNIW,30);
dcs(AVX512_4FMAPS,31);
#undef dcs


//count number of set bits and get last set bit. nbits must be smaller then 256
typedef struct {
	uint8_t numSet, lastSet;
} bits_info;
bits_info countbits(const void *p, const uint32_t nbits) {
	bool nle = ENDIANNESS!=LITTLEENDIAN;
	uint32_t n = 0, ls = 0;
	const uint32_t sz = (nbits-1)/8+1;
	uint8_t *bp = (void*)p;
	if (nle) reverse(bp,sz);
	for (uint32_t i = 0; i < sz; i++) {
		uint8_t v = *bp++, bsz = nbits-i*8;
		if (bsz > 8) bsz = 8;
		for (uint16_t b = 0; b < bsz; b++) {
			if ((v>>b)&1) {
				n++;
				ls = i*8+b;
			}
		}
	}
	if (nle) reverse(bp,sz);
	return (bits_info){(uint8_t)n, (uint8_t)ls};
}

//returns nearest power of 2 for integer 'i' that is larger or equal to 'i', does not work with integers nearing 1billion
static inline uint32_t toPowerOf2(uint32_t i) {
	bits_info bi = countbits(&i, 32);
	if (bi.numSet < 2) return i;
	return 1<<(bi.lastSet+1);
}

//calculate 32-bit integer square root
uint32_t isqrt(uint32_t n) {
    uint32_t v = 0, b = 1<<30;
    while (b > n) b >>= 2;
	
	while (b != 0) {
		if (n >= v+b) {
			n -= v+b;
			v = (v>>1)+b;
		} else v >>= 1;
		b >>= 2;
	}
	return v;
}

//uint32 hash, uses FNV-1a hash
#define NULL_HASH 0
static inline uint32_t hash(void *d, const uint32_t sz) {
	uint8_t *p = d;
	uint32_t v = 0x6A7F8FAA;
	for (uint32_t i = 0; i < sz; i++) v = (v^(uint32_t)*p++)*0x01000193;
	if (v == NULL_HASH) v = 1;
	return v;
}
#define cstring_hash(s) hash(s,strlen(s))
//uint32 array hash, extended FNV-1a hash
uint32_t hashn(void *src, const uint32_t sz, uint32_t *hash, const uint32_t hashSz, uint32_t iter) {
	uint32_t *hval = hash, hlen = hashSz, hiter = iter%hlen;
	uint8_t *p = src;
	if (iter == 0) {
		for (uint32_t i = 0; i < hlen; i++) *hval++ = 0x6A7F8FAA;
		hval = hash;
	}
	for (uint32_t i = 0; i < sz; i++) {
		hval[hiter] = (hval[hiter] ^ (uint32_t)*p++) * 0x01000193;
		hiter = (hiter + 1) % hlen;
	}
	return iter + sz;
}

//32/64 bit check
#define M64 _M_X64 || __LP64__ || _LP64 || __x86_64__

//#preprocessorthings
#define CAT(a,...) PP_CAT(a,__VA_ARGS__)
#define PP_CAT(a,...) a##__VA_ARGS__

//compiler
#ifdef _MSC_VER
#define NOINLINE __declspec(noinline)
#define CALLINGCONVENTION(c) __##c
#define UNUSED
#define MEM_ALIGN(b) __declspec(align(b))
#define RETURN_ADDRESS _ReturnAddress()
#else
#define UNUSED __attribute__((unused))
#define NOINLINE __attribute__((noinline))
#define CALLINGCONVENTION(c) __attribute__((c))
#define MEM_ALIGN(b) __attribute__((aligned (b)))
#define RETURN_ADDRESS __builtin_return_address(0)
#endif

//get pointer to next byte of code
NOINLINE void* nextOpcodeAddress() {
	return RETURN_ADDRESS;
}

//count macro arguments, NARGS returns a integer representing the number of arguments. MAX 64 arguments
#ifdef _MSC_VER // Microsoft compilers
#define EXPAND(x) x
#define __NARGS(_0,_1,_2,_3,_4,_5,_6,_7,_8,_9,_10,_11,_12,_13,_14,_15,_16,_17,_18,_19,_20,_21,_22,_23,_24,_25,_26,_27,_28,_29,_30,_31,_32,_33,_34,_35,_36,_37,_38,_39,_40,_41,_42,_43,_44,_45,_46,_47,_48,_49,_50,_51,_52,_53,_54,_55,_56,_57,_58,_59,_60,_61,_62,_63,_64,N,...) N
#define NARGS_1(...) EXPAND(__NARGS(__VA_ARGS__,64,63,62,61,60,59,58,57,56,55,54,53,52,51,50,49,48,47,46,45,44,43,42,41,40,39,38,37,36,35,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0))
#define AUGMENTER(...) unused, __VA_ARGS__
#define NARGS(...) NARGS_1(AUGMENTER(__VA_ARGS__))
#else
#define NARGS(...) __NARGS(0, ##__VA_ARGS__,64,63,62,61,60,59,58,57,56,55,54,53,52,51,50,49,48,47,46,45,44,43,42,41,40,39,38,37,36,35,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0)
#define __NARGS(_0,_1,_2,_3,_4,_5,_6,_7,_8,_9,_10,_11,_12,_13,_14,_15,_16,_17,_18,_19,_20,_21,_22,_23,_24,_25,_26,_27,_28,_29,_30,_31,_32,_33,_34,_35,_36,_37,_38,_39,_40,_41,_42,_43,_44,_45,_46,_47,_48,_49,_50,_51,_52,_53,_54,_55,_56,_57,_58,_59,_60,_61,_62,_63,_64,N,...) N
#endif

#endif
