/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _COSINETRANSFORM_H_
#define _COSINETRANSFORM_H_

#ifndef __linux__
#define _USE_MATH_DEFINES
#endif
#include <math.h>
#include <stdlib.h>
#include <stdint.h>

//DCT type 2/3, using optimized algorithm by Byeong Gi Lee http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.118.3056&rep=rep1&type=pdf#page=34

float *cosineTransformTempBuffer;
uint32_t cosineTransformTempBufferSize = 0;

void cosinetransformFwd(float *bufA, float *bufB, const uint32_t size) {
	const uint32_t size2 = size/2;
	const float sizef = M_PI/(float)size;
	uint32_t szc = size;
	for (uint32_t i = 0; i < size2; i++) {
		const float a = bufA[i], b = bufA[--szc];
		bufB[i] = a+b;
		bufB[i+size2] = (a-b)*0.5f/cosf((i+0.5f)*sizef);
	}
	if (size2 != 1) {
		cosinetransformFwd(bufB,bufA,size2);
		cosinetransformFwd(bufB+size2,bufA+size2,size2);
	}
	szc = size2-1;
	uint32_t q = 0, k = size2;
	for (uint32_t i = 0; i < szc; i++) {
		bufA[q++] = bufB[i];
		bufA[q++] = bufB[k]+bufB[(k++)+1];
	}
	bufA[size-2] = bufB[szc];
	szc = size-1;
	bufA[szc] = bufB[szc];
}

void cosinetransformInv(float *bufA, float *bufB, const uint32_t size) {
	const uint32_t size2 = size/2;
	bufB[0] = bufA[0];
	bufB[size2] = bufA[1];
	uint32_t q = 2, k = size2+1;
	for (uint32_t i = 1; i < size2; i++) {
		bufB[i] = bufA[q];
		bufB[k++] = bufA[q-1]+bufA[q+1];
		q += 2;
	}
	if (size2 != 1) {
		cosinetransformInv(bufB,bufA,size2);
		cosinetransformInv(bufB+size2,bufA,size2);
	}
	const float sizef = M_PI/(float)size;
	q = size;
	for (uint32_t i = 0; i < size2; i++) {
		const float a = bufB[i], b = bufB[i+size2]*0.5f/cosf((i+0.5f)*sizef);
		bufA[i] = a+b;
		bufA[--q] = a-b;
	}
}

//scaled cosine transform, size must be power of 2
void cosinetransform(float *src, float *dst, const uint32_t size, const bool inv) {
	if (cosineTransformTempBufferSize < size) {
		if (cosineTransformTempBufferSize == 0) cosineTransformTempBuffer = malloc(size*4);
		else cosineTransformTempBuffer = realloc(cosineTransformTempBuffer,size*4);
		cosineTransformTempBufferSize = size;
	}
	memcpy(cosineTransformTempBuffer,src,size*4);
	if (inv) {
		cosineTransformTempBuffer[0] *= 0.5f;
		cosinetransformInv(cosineTransformTempBuffer,dst,size);
		memcpy(dst,cosineTransformTempBuffer,size*4);
	} else {
		cosinetransformFwd(cosineTransformTempBuffer,dst,size);
		float dv = 2.0f/(float)size, *sp = cosineTransformTempBuffer;
		for (uint32_t i = 0; i < size; i++) *dst++ = (*sp++)*dv;
	}
}

#endif

