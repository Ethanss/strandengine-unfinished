/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files(the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and / or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions :
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _COMPRESSION_H_
#define _COMPRESSION_H_

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

/*Lossless binary compression using run length encoding.
Dst buffer should be larger then src to make room for potential inflation.
Base must be between 1-32(exclusive), higher is better for more random data. 
Returns compressed size in bytes.*/
uint32_t compressbinary(uint8_t *src, uint8_t *dst, const uint32_t len, const uint32_t base) {
	uint32_t last = 2, reps = 1, repbit, srci = 0, srcv = *src, bi = 0, outlen = 0, obv = 0, obi = 0;
	const uint32_t basev = 1<<base;
	#define pb(s) obv |= s<<(obi++);\
	if (obi >= 8) {\
		dst[outlen++] = obv;\
		obi = obv = 0;\
	}
	while (true) {
		while (bi < 8) {
			uint32_t bit = (srcv>>bi)&1;
			if (bit == last) {
				if (++reps < basev) {
					pb(bit);
				}
				if ((reps>>repbit)&1) {
					pb(bit);
					repbit++;
				}
			} else {
				if (reps >= basev) {
					pb(bit);
					uint32_t nrep = 1<<(repbit-1);
					if (nrep != reps) {
						uint32_t tbp = srci*8+bi-(reps-nrep);
						srci = tbp/8;
						bi = tbp-srci*8;
						srcv = src[srci];
						last = 2;
						reps = 1;
						continue;
					}
				}
				last = bit;
				reps = 1;
				repbit = base;
				pb(bit);
			}
			bi++;
		}
		
		bi = 0;
		srci++;
		srcv = src[srci];
		if (srci >= len) {
			if (reps >= basev) {
				last ^= 1;
				pb(last);
				uint32_t nrep = 1<<(repbit-1);
				if (nrep != reps) {
					uint32_t tbp = srci*8+bi-(reps-nrep);
					srci = tbp/8;
					bi = tbp-srci*8;
					srcv = src[srci];
					last = 2;
					reps = 1;
					continue;
				}
			}
			break;
		}
	}
	#undef pb
	if (obi > 0) dst[outlen++] = obv;
	return outlen;
}


//Reads compressed data from src and decompresses it into dst, len is the size in bytes of the uncompressed dst data block
void decompressbinary(uint8_t *src, uint8_t *dst, const uint32_t len, const uint32_t base) {
	uint32_t last = 2, reps = 0, outlen = 0, obv = 0, obi = 0, srci = 0;
	const uint32_t basev = 1<<base, baseb = basev-base;
	#define pb(s) obv |= s<<(obi++);\
	if (obi >= 8) {\
		dst[outlen++] = obv;\
		obi = obv = 0;\
		if (outlen >= len) {bi = 8;break;}\
	}
	while (outlen < len) {
		uint32_t v = *src++;
		for (uint32_t bi = 0; bi < 8; bi++) {
			uint32_t bit = (v>>bi)&1;
			if (bit == last) {
				reps++;
			} else {
				if (reps >= basev) {
					reps = 1<<(reps-baseb);
					for (uint32_t i = 0; i < reps; i++) {
						pb(last);
					}
					reps = 0;
				} else {
					for (uint32_t i = 0; i < reps; i++) {
						pb(last);
					}
					reps = 1;
				}
				last = bit;
			}
		}
	}
	#undef pb
	if (obi > 0) dst[outlen++] = obv;
}

#endif
