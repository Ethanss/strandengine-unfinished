/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _GRAPHICS_H_
#define _GRAPHICS_H_

#ifndef __linux__
#include <GL/glew.h>
#include <stdio.h>
#endif
#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL.h>
#ifndef EXCLUDE_LIB_SDL_TTF
#include <SDL2/SDL_ttf.h>
#endif
#include "stream.h"
#include "vecmath.h"


uint32_t *screenWidth, *screenHeight;
float *screenAspect, *screenWidthf, *screenHeightf, *oneDivScreenWidth, *oneDivScreenHeight;

//general opengl state
enum {
	BLENDMODE_NONE = 0,
	BLENDMODE_SUBTRACT = 1,
	BLENDMODE_REVERSE_SUBTRACT = 2,
	BLENDMODE_ADDITIVE = 3,
	BLENDMODE_TRANSPARENT = 4,
	BLENDMODE_PREMULTIPLIED_TRANSPARENT = 5,
	BLENDMODE_MULTIPLY = 6
};
enum {
	CULLFACE_NONE = 0,
	CULLFACE_FRONT = 1,
	CULLFACE_BACK = 2
};
enum {
	DEPTHTEST_NONE = 0,
	DEPTHTEST_LESS = 1,
	DEPTHTEST_GREATER = 2
};

uint8_t activeBlendMode = BLENDMODE_NONE, activeCullFace = CULLFACE_NONE, activeDepthTest = DEPTHTEST_NONE;
bool activeDepthWrite = true;

void setBlendMode(const uint32_t bm) {
	if (activeBlendMode == bm) return;

	if (bm == BLENDMODE_NONE) {
		glDisable(GL_BLEND);
	} else {
		if (activeBlendMode == BLENDMODE_NONE) glEnable(GL_BLEND);

		if (bm > BLENDMODE_REVERSE_SUBTRACT) {
			if (activeBlendMode <= BLENDMODE_REVERSE_SUBTRACT) glBlendEquation(GL_FUNC_ADD);
			
			if (bm == BLENDMODE_ADDITIVE) glBlendFunc(GL_ONE,GL_ONE);
			else if (bm == BLENDMODE_TRANSPARENT) glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			else if (bm == BLENDMODE_PREMULTIPLIED_TRANSPARENT) glBlendFunc(GL_ONE,GL_ONE_MINUS_SRC_ALPHA);
			else if (bm == BLENDMODE_MULTIPLY) glBlendFunc(GL_DST_COLOR, GL_ZERO);
			else glBlendFunc(GL_DST_COLOR, GL_SRC_COLOR);
		} else {
			if (activeBlendMode > BLENDMODE_REVERSE_SUBTRACT && activeBlendMode != BLENDMODE_ADDITIVE) glBlendFunc(GL_ONE,GL_ONE);

			if (bm == BLENDMODE_REVERSE_SUBTRACT) glBlendEquation(GL_FUNC_REVERSE_SUBTRACT);
			else glBlendEquation(GL_FUNC_SUBTRACT);
		}
	}

	activeBlendMode = bm;
}
static inline void setCullFace(const uint32_t cf) {
	if (activeCullFace == cf) return;

	if (cf != 0) {
		if (activeCullFace == 0) glEnable(GL_CULL_FACE);

		glCullFace(cf==CULLFACE_FRONT?GL_FRONT:GL_BACK);
	} else {
		glDisable(GL_CULL_FACE);
	}
	activeCullFace = cf;
}
static inline void setDepthTest(const uint32_t dt) {
	if (dt == activeDepthTest) return;

	if (dt == DEPTHTEST_NONE) glDisable(GL_DEPTH_TEST);
	else {
		if (activeDepthTest == DEPTHTEST_NONE) glEnable(GL_DEPTH_TEST);
		glDepthFunc(dt == DEPTHTEST_LESS ? GL_LEQUAL : GL_GEQUAL);
	}

	activeDepthTest = dt;
}
static inline void enableDepthWrite(bool dw) {
	if (dw == activeDepthWrite) return;

	if (dw) glDepthMask(true);
	else glDepthMask(false);

	activeDepthWrite = dw;
}


//uniform buffer pool
typedef struct {
	uint32_t ptr, size;
} ubp_memory;
typedef struct {
	GLuint ubo;
	uint32_t bufferSize, bufferUsed, alignment;
	list memory;
} uniform_buffer_pool;

uniform_buffer_pool uniform_pool;
uint32_t numActiveUniformBuffers = 0;

//create new uniform buffer pool
void new_uniform_buffer_pool(uniform_buffer_pool *u) {
	new_list(&u->memory, sizeof(ubp_memory));
	u->bufferUsed = 0;

	GLint align;
	glGetIntegerv(GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT, &align);
	u->alignment = (uint32_t)align;
	u->bufferSize = nalign(1024,align);

	glGenBuffers(1, &u->ubo);
	glBindBuffer(GL_UNIFORM_BUFFER, u->ubo);
	glBufferData(GL_UNIFORM_BUFFER, u->bufferSize, 0, GL_DYNAMIC_DRAW);
}
//delete uniform buffer pool freeing memory
static inline void delete_uniform_buffer_pool(uniform_buffer_pool *u) {
	glDeleteBuffers(1, &u->ubo);
	delete_list(&u->memory);
}

//allocate uniform buffer memory of sz
ubp_memory uniform_buffer_pool_allocate(uniform_buffer_pool *u, uint32_t sz) {
	ubp_memory mem;

	//match size with alignment
	sz = nalign(sz,u->alignment);

	//find memory slot
	bool found = false, insert = false;
	uint32_t ptr, index;
	if (u->bufferSize-u->bufferUsed >= sz) {
		ptr = u->bufferUsed;
		found = true;
	} else {
		uint32_t ml = u->memory.length, last = 0;

		ubp_memory *mems = u->memory.buffer;
		for (uint32_t i = 0; i < ml; i++) {
			if (mems->ptr-last >= sz) {
				ptr = last;
				index = i;
				found = true;
				insert = true;
				break;
			}
			last = mems->ptr+mems->size;
			mems++;
		}
	}

	if (!found) {
		//no open slot in whole buffer, expand buffer
		uint32_t obsz = u->bufferSize, rqsz = u->bufferSize+sz;
		while (u->bufferSize < rqsz) u->bufferSize *= 8;

		//copy buffer memory into temp
		GLuint tubo;
		glGenBuffers(1, &tubo);

		glBindBuffer(GL_UNIFORM_BUFFER, tubo);
		glBufferData(GL_UNIFORM_BUFFER, obsz, 0, GL_STREAM_COPY);

		glBindBuffer(GL_COPY_WRITE_BUFFER, tubo);
		glBindBuffer(GL_COPY_READ_BUFFER, u->ubo);
		glCopyBufferSubData(GL_COPY_READ_BUFFER, GL_COPY_WRITE_BUFFER, 0, 0, obsz);

		//resize
		glBindBuffer(GL_COPY_READ_BUFFER, 0);
		glBindBuffer(GL_UNIFORM_BUFFER, u->ubo);
		glBufferData(GL_UNIFORM_BUFFER, u->bufferSize, 0, GL_DYNAMIC_DRAW);

		//copy temp back into buffer
		glBindBuffer(GL_COPY_WRITE_BUFFER, u->ubo);
		glBindBuffer(GL_COPY_READ_BUFFER, tubo);
		glCopyBufferSubData(GL_COPY_READ_BUFFER, GL_COPY_WRITE_BUFFER, 0, 0, obsz);

		//rebind old state
		glBindBuffer(GL_COPY_WRITE_BUFFER, 0);
		glBindBuffer(GL_COPY_READ_BUFFER, 0);
		glBindBuffer(GL_UNIFORM_BUFFER, u->ubo);

		ptr = obsz;
	}

	mem.ptr = ptr;
	mem.size = sz;

	if (insert) list_insert(&u->memory, &mem, index);
	else list_add(&u->memory, &mem);

	if (ptr+sz > u->bufferUsed) u->bufferUsed = ptr+sz;

	return mem;
}
#define ubp_alloc(s) uniform_buffer_pool_allocate(&uniform_pool, s)
//free uniform buffer memory
void uniform_buffer_pool_free(uniform_buffer_pool *u, ubp_memory mem) {
	int32_t mind = list_indexOf(&u->memory, &mem, sizeof(ubp_memory));
	if (mind == -1) return;

	if (u->bufferUsed == mem.ptr+mem.size) {
		if (mind == 0) {
			u->bufferUsed = 0;
		} else {
			ubp_memory last = ((ubp_memory*)u->memory.buffer)[mind-1];
			u->bufferUsed = last.ptr+last.size;
		}
	}
	list_removeAt(&u->memory, mind);
}
#define ubp_free(m) uniform_buffer_pool_free(&uniform_pool, m)

//set uniform memory
static inline void ubp_memory_set(const ubp_memory m, const void *d, const uint32_t sz) {
	glBufferSubData(GL_UNIFORM_BUFFER, m.ptr, sz, d);
}
//set uniform memory array
static inline void ubp_memory_setArray(const ubp_memory m, const uint32_t offset, const void *d, const uint32_t sz) {
	glBufferSubData(GL_UNIFORM_BUFFER, m.ptr+offset, sz, d); 
}
//copy uniform memory
static inline void ubp_memory_copy(const ubp_memory src, const ubp_memory dst) {
	glCopyBufferSubData(GL_UNIFORM_BUFFER, GL_UNIFORM_BUFFER, src.ptr, dst.ptr, src.size);
}
//copy uniform memory array
static inline void ubp_memory_copyArray(const ubp_memory src, const uint32_t srcIndex, const ubp_memory dst, const uint32_t dstIndex, const uint32_t count, const uint32_t stride) {
	glCopyBufferSubData(GL_UNIFORM_BUFFER, GL_UNIFORM_BUFFER, src.ptr+srcIndex*stride, dst.ptr+dstIndex*stride, count*stride);
}

//vertex attributes constants/enums
const uint32_t VERTEX_ATTRIBUTE_POSITION = 1,
		  VERTEX_ATTRIBUTE_UV = 2,
		  VERTEX_ATTRIBUTE_COLOR = 4,
		  VERTEX_ATTRIBUTE_NORMAL = 8,
		  VERTEX_ATTRIBUTE_TANGENT = 16,
		  VERTEX_ATTRIBUTE_BITANGENT = 32,
		  VERTEX_ATTRIBUTE_BONES = 64,
		  VERTEX_ATTRIBUTE_BONEWEIGHTS = 128,

		  VERTEX_INDEXED = 0x80000000;
#define NUM_VERTEX_ATTRIBUTES 8
enum {
		  VERTEX_ATTRIBUTEID_POSITION = 0,
		  VERTEX_ATTRIBUTEID_UV = 1,
		  VERTEX_ATTRIBUTEID_COLOR = 2,
		  VERTEX_ATTRIBUTEID_NORMAL = 3,
		  VERTEX_ATTRIBUTEID_TANGENT = 4,
		  VERTEX_ATTRIBUTEID_BITANGENT = 5,
		  VERTEX_ATTRIBUTEID_BONES = 6,
		  VERTEX_ATTRIBUTEID_BONEWEIGHTS = 7
};

const char *VERTEX_ATTRIBUTE_NAMES[NUM_VERTEX_ATTRIBUTES] = {"in_Position", "in_Uv", "in_Color", "in_Normal", "in_Tangent", "in_Bitangent", "in_Bones", "in_BoneWeights"};
const uint8_t VERTEX_ATTRIBUTE_SIZES[NUM_VERTEX_ATTRIBUTES] = {3,2,4,3,3,3,4,4};


//shader
typedef struct {
	const char *name;
	GLuint textureData;
	ubp_memory blockData;
} shader_global;
#define MAX_SHADER_GLOBALS 16

typedef struct {
	shader_global *global;
	union {
		GLuint blockIndex;
		GLint textureIndex;
	};
} shader_globalbind;

typedef struct {
	GLuint program;
	list globalBinds;
} shader_program;

typedef struct {
	list name;
	bool state;
} shader_option;

typedef struct {
	shader_program current, *multi;
	shader_option *options;
	uint32_t optionCount;
} shader;


uint32_t shaderGlobalsLength = 0, numActiveTextures = 0;
shader_global shaderGlobals[MAX_SHADER_GLOBALS];
list shaders;
shader blitShader, *activeShader = &blitShader;
GLuint blitShaderMaterialIndex;
GLint blitShaderTextureIndex;

//bind uniform pool memory to index
static inline void ubp_memory_bind(ubp_memory m, const GLuint uniformIndex) {
	const uint32_t i = numActiveUniformBuffers++;
	glUniformBlockBinding(activeShader->current.program, uniformIndex, i);
	glBindBufferRange(GL_UNIFORM_BUFFER, i, uniform_pool.ubo, m.ptr, m.size);
}
#define ubp_memory_unbind() numActiveUniformBuffers--

typedef struct {
	vec2 position,scale,sourcePosition,sourceScale,clipPosition,clipScale;
	vec4 tint;
	float aspect,depth,
	rotationSin,rotationCos;
} blit_material;

blit_material fullscreen_blit_material = {{0.0f,0.0f},{1.0f,1.0f},
				{0.5f,0.5f},{1.0f,1.0f},
				{0.0f,0.0f},{1.0f,1.0f},
				{1.0f,1.0f,1.0f,1.0f},
				0.0f,0.0f,0.0f,1.0f};

//compile vertex/fragment shader
GLuint new_vertfrag_shader(const char *code, uint32_t codeLen, bool frag) {
	GLuint vs = glCreateShader(frag?GL_FRAGMENT_SHADER:GL_VERTEX_SHADER), rc;
	glShaderSource(vs, 1, (const GLchar**)&code, &codeLen);
	glCompileShader(vs);
	glGetShaderiv(vs, GL_COMPILE_STATUS, &rc);
	if (rc == GL_FALSE) {
	   GLuint maxLength;
	   glGetShaderiv(vs, GL_INFO_LOG_LENGTH, &maxLength);

       char *log = (char *)malloc(maxLength);
       glGetShaderInfoLog(vs, maxLength, &maxLength, log);
       printf("Error compiling OpenGL %s shader: %s\n", frag?"fragment":"vertex", log);
       free(log);
       return 0;
	}
	return vs;
}
#define new_vertex_shader(a,b) new_vertfrag_shader(a,b,false)
#define new_fragment_shader(a,b) new_vertfrag_shader(a,b,true)


//register global with shader
void shader_registerGlobal(shader_program *s, shader_global *g) {
	shader_globalbind sb;
	sb.global = g;
	bool valid;
	if (g->blockData.size == 0) {
		//texture
		sb.textureIndex = glGetUniformLocation(s->program, g->name);
		valid = sb.textureIndex != -1;
	} else {
		//uniform block
		sb.blockIndex = glGetUniformBlockIndex(s->program, g->name);
		valid = sb.blockIndex != GL_INVALID_INDEX;
	}
	if (valid) list_add(&s->globalBinds, &sb);
}

//link vertex/fragment shaders
bool new_shader_program(shader_program *s, GLuint vs, GLuint fs) {
	GLuint program = glCreateProgram(), rc;
	glAttachShader(program, vs);
	glAttachShader(program, fs);
	//set vertex attributes
	for (int i = 0; i < NUM_VERTEX_ATTRIBUTES; i++) glBindAttribLocation(program, (GLuint)i, VERTEX_ATTRIBUTE_NAMES[i]);
	glLinkProgram(program);
	glDetachShader(program, vs);
	glDetachShader(program, fs);

	glGetProgramiv(program, GL_LINK_STATUS, &rc);
    if (rc == GL_FALSE) {
    	GLuint maxLength;
    	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

       	char *log = (char *)malloc(maxLength);
       	glGetProgramInfoLog(program, maxLength, &maxLength, log);
       	printf("Error linking OpenGL program: %s\n", log);
        free(log);
		glDeleteProgram(program);
        return false;
    }

    s->program = program;

    //get global uniforms
    new_list(&s->globalBinds, sizeof(shader_globalbind));
    for (uint32_t i = 0; i < shaderGlobalsLength; i++) shader_registerGlobal(s, shaderGlobals + i);

    return true;
}


//new shader from single code string
bool new_shader(shader *s, list code) {
	//copy into buffer
	list ocode;
	new_list(&ocode,1);
	list_copy(&ocode,&code);

	//preprocess
	list defines, dstrb;
	new_list(&dstrb, 1);
	new_list(&defines, sizeof(list));

	//enumerate #option
	list optionListStr = cstring_as_list("#option "), defineListStr = cstring_as_list("#define");
	char nline = '\n';
	int32_t ind = 0;
	while ((ind = list_indexOfI(&ocode, optionListStr.buffer, optionListStr.length, ind)) != -1) {
		int32_t sloc = ind;
		ind = list_indexOfI(&ocode, &nline, 1, ind+optionListStr.length);
		if (ind == -1) {
			printf("Error preprocessing shader, '#option' with no ending newline character.\n");
			delete_list(&ocode);
			return false;
		}

		memcpy((char*)ocode.buffer+sloc, defineListStr.buffer, defineListStr.length);

		uint32_t dstri = sloc+optionListStr.length,
				 strl = ind-dstri;
		dstri = list_addm(&dstrb, (char*)ocode.buffer+dstri, strl);

		list dsl;
		dsl.stride = 1;
		list_setBuffer(&dsl, (char*)dstrb.buffer+dstri, strl);
		list_add(&defines, &dsl);
	}

	uint32_t multiCount = 1;
	if (defines.length != 0) {
		s->options = malloc(sizeof(shader_option)*defines.length);
		list *dls = defines.buffer;
		for (uint32_t i = 0; i < defines.length; i++) {
			s->options[i] = (shader_option){dls[i],false};
		}

		multiCount = defines.length*defines.length;
		if (multiCount < 2) multiCount = 2;
		s->multi = malloc(sizeof(shader_program)*multiCount);
	}

	//compile vertex/fragment shaders
	list vertListStr = cstring_as_list("void vert("),
		 fragListStr = cstring_as_list("void frag("),
		 mainListStr = cstring_as_list("main"),
		 inListStr = cstring_as_list("in "),
		 outListStr = cstring_as_list("out "),
		 commentStart = cstring_as_list("/*"),
		 commentEnd = cstring_as_list("*/"),
		 *define = defines.buffer;
	uint32_t *defineInd = alloca(4 * defines.length);

	for (uint32_t i = 0; i < multiCount; i++) {
		//toggle option defines
		uint32_t ds = 1;
		for (uint32_t d = 0; d < defines.length; d++) {
			if ((i/ds)%2 == 0) {
				defineInd[d] = ind = list_indexOf(&ocode, define[d].buffer, define[d].length);
				memset((char*)ocode.buffer+ind-optionListStr.length, ' ', (optionListStr.length+define[d].length));
			}
			ds *= 2;
		}

		int32_t vertind = list_indexOf(&ocode, vertListStr.buffer, vertListStr.length),
				fragind = list_indexOf(&ocode, fragListStr.buffer, fragListStr.length);
		bool nfail = true;
		if (vertind == -1 || fragind == -1) {
			nfail = false;
			printf("Error preprocessing shader, 'vert'/'frag' function missing.\n");
		}

		while (nfail) {
			//vertex shader
			memcpy((char*)ocode.buffer + fragind, commentStart.buffer, 2);
			char tb[2];
			memcpy(tb, (char*)ocode.buffer + ocode.length - 2, 2);
			memcpy((char*)ocode.buffer + ocode.length - 2, commentEnd.buffer, 2);

			ind = vertind;
			memcpy((char*)ocode.buffer + ind + 5, mainListStr.buffer, mainListStr.length);
			GLuint vertexShader = new_vertex_shader(ocode.buffer, ocode.length);
			nfail = vertexShader != 0;
			if (!nfail) break;
			memcpy((char*)ocode.buffer + ind + 5, (char*)vertListStr.buffer + 5, mainListStr.length);

			memcpy((char*)ocode.buffer + fragind, fragListStr.buffer, 2);
			memcpy((char*)ocode.buffer + ocode.length - 2, tb, 2);


			//fragment shader
			memcpy((char*)ocode.buffer + vertind, commentStart.buffer, 2);
			memcpy(tb, (char*)ocode.buffer + fragind - 2, 2);
			memcpy((char*)ocode.buffer + fragind - 2, commentEnd.buffer, 2);

			ind = fragind;
			memcpy((char*)ocode.buffer + ind + 5, mainListStr.buffer, mainListStr.length);
			list_replaceSwapAll(&ocode, outListStr.buffer, outListStr.length, inListStr.buffer, inListStr.length);
			GLuint fragmentShader = new_fragment_shader(ocode.buffer, ocode.length);
			nfail = fragmentShader != 0;
			if (!nfail) {
				glDeleteShader(vertexShader);
				break;
			}
			list_replaceSwapAll(&ocode, inListStr.buffer, inListStr.length, outListStr.buffer, outListStr.length);
			memcpy((char*)ocode.buffer + ind + 5, (char*)fragListStr.buffer + 5, mainListStr.length);

			memcpy((char*)ocode.buffer + vertind, vertListStr.buffer, 2);
			memcpy((char*)ocode.buffer + fragind - 2, tb, 2);

			//program
			if (multiCount > 1) {
				nfail = new_shader_program(s->multi + i, vertexShader, fragmentShader);
				if (nfail) {
					//undo toggled options
					uint32_t ds = 1;
					for (uint32_t d = 0; d < defines.length; d++) {
						if ((i / ds) % 2 == 0) {
							memcpy((char*)ocode.buffer + defineInd[d] - optionListStr.length, defineListStr.buffer, defineListStr.length);
							memcpy((char*)ocode.buffer + defineInd[d], define[d].buffer, define[d].length);
						}
						ds *= 2;
					}
				}
			}
			else {
				nfail = new_shader_program(&s->current, vertexShader, fragmentShader);
			}

			glDeleteShader(vertexShader);
			glDeleteShader(fragmentShader);
			break;
		}
		if (!nfail) {
			for (uint32_t m = 0; m < i; m++) {
				glDeleteProgram(s->multi[i].program);
				delete_list(&s->multi[i].globalBinds);
			}
			delete_list(&ocode);
			delete_list(&dstrb);
			delete_list(&defines);
			return false;
		}
	}

	s->optionCount = defines.length;
	if (multiCount > 1) s->current = *s->multi;

	delete_list(&defines);
	delete_list(&ocode);
	delete_list(&dstrb);

	list_add(&shaders, &s);
	return true;
}

//enable shader option
void shader_enableOption(shader *s, list *dname, bool en) {
	//find define and change state
	uint32_t sid = 0, ds = 1, oc = s->optionCount;
	shader_option *opt = s->options;
	for (uint32_t i = 0; i < oc; i++) {
		if (list_equals(&opt->name, dname)) {
			opt->state = en;
		}
		if (opt->state) sid += ds;
		ds *= 2;
		opt++;
	}

	//update current program from state
	s->current = s->multi[sid];
}
void shader_enableGlobalOption(list *dname, const bool e) {
	shader **sh = shaders.buffer;
	for (uint32_t i = 0; i < shaders.length; i++) shader_enableOption(*sh++,dname,e);
}

//create and register new shader global
int32_t shader_newGlobal(const char *name, const uint32_t bsz) {
	if (shaderGlobalsLength >= MAX_SHADER_GLOBALS) return -1;
	const uint32_t ind = shaderGlobalsLength++;
	shader_global *sg = &shaderGlobals[ind];
	sg->name = name;
	sg->blockData.size = bsz;

	shader **sh = shaders.buffer;
	for (uint32_t i = 0; i < shaders.length; i++) shader_registerGlobal(&(*sh++)->current, sg);
	return ind;
}

//get index of registered shader global from name
int32_t shader_indexOfGlobal(const char *name) {
	for (uint32_t i = 0; i < shaderGlobalsLength; i++) {
		if (strcmp(name,shaderGlobals[i].name) == 0) return (int32_t)i;
	}
	return -1;
}

void shader_setGlobalTexture(const char *name, GLuint tex) {
	int32_t gi = shader_indexOfGlobal(name);
	if (gi == -1) gi = shader_newGlobal(name, 0);
	if (gi == -1) return;
	shaderGlobals[gi].textureData = tex;
}
void shader_setGlobalData(const char *name, ubp_memory mem) {
	int32_t gi = shader_indexOfGlobal(name);
	if (gi == -1) gi = shader_newGlobal(name, mem.size);
	if (gi == -1) return;
	shaderGlobals[gi].blockData = mem;
}

//use shader program and apply global uniforms
void shader_bind(shader *s) {
	if (activeShader == s) return;
	
	uint32_t gl = activeShader->current.globalBinds.length;
	shader_globalbind *binds = activeShader->current.globalBinds.buffer;
	for (uint32_t i = 0; i < gl; i++) {
		shader_global *g = binds[i].global;
		if (g->blockData.size == 0) numActiveTextures--;
		else numActiveUniformBuffers--;
	}
	activeShader = s;
	glUseProgram(s->current.program);

	gl = s->current.globalBinds.length;
	binds = s->current.globalBinds.buffer;
	for (uint32_t i = 0; i < gl; i++) {
		shader_global *g = binds[i].global;
		if (g->blockData.size == 0) {
			//texture
			uint32_t i = numActiveTextures++;
			glActiveTexture(GL_TEXTURE0 + i);
			glBindTexture(GL_TEXTURE_2D, g->textureData);
			glUniform1i(binds[i].textureIndex, i);
		} else {
			//uniform block
			ubp_memory_bind(g->blockData,binds[i].blockIndex);
		}
	}
}


//free shader resources
void delete_shader(shader *s) {
	if (s == activeShader) shader_bind(&blitShader);
	list_removeFast(&shaders, &s);
	if (s->optionCount != 0) {
		uint32_t multiCount = s->optionCount*s->optionCount;
		if (multiCount < 2) multiCount = 2;
		for (uint32_t i = 0; i < multiCount; i++) {
			glDeleteProgram(s->multi[i].program);
			delete_list(&s->multi[i].globalBinds);
		}
		free(s->options[0].name.buffer);
		free(s->multi);
		free(s->options);
	}
	else {
		delete_list(&s->current.globalBinds);
		glDeleteProgram(s->current.program);
	}
}


//texture wrapper for opengl
enum {
	TEXTURE_TYPE_1D = 1,
	TEXTURE_TYPE_2D = 2,
	TEXTURE_TYPE_3D = 3,
	TEXTURE_TYPE_CUBEMAP = 4
};
typedef struct {
	void *data;
	GLuint texture, internalFormat, dataFormat;
	GLenum target;
	uint32_t width, height, depth, bytesPerPixel;
	uint8_t type;
} texture;

GLuint rgbCompressedTextureFormat, rgbaCompressedTextureFormat;
texture whiteTexture;

//create new empty opengl 2d texture
void new_texture(texture *t) {
	t->width = 0;
	t->data = NULL;
	t->type = TEXTURE_TYPE_2D;
	t->target = GL_TEXTURE_2D;

	glGenTextures(1, &t->texture);
	glBindTexture(GL_TEXTURE_2D, t->texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}
void new_texturex(texture *t, const uint8_t type) {
	t->width = 0;
	t->data = NULL;
	t->type = type;

	glGenTextures(1, &t->texture);
	GLenum tt;
	if (type == TEXTURE_TYPE_1D) tt = GL_TEXTURE_1D;
	else if (type == TEXTURE_TYPE_2D) tt = GL_TEXTURE_2D;
	else if (type == TEXTURE_TYPE_3D) tt = GL_TEXTURE_3D;
	else tt = GL_TEXTURE_CUBE_MAP;
	glBindTexture(tt, t->texture);
	t->target = tt;
}
//delete texture
static inline void delete_texture(texture *t) {
	glDeleteTextures(1, &t->texture);
	if (t->data != NULL) free(t->data);
}

void texture_setData(texture *t, void *d, uint32_t *dimensions, const bool alpha, const bool compress) {
	glBindTexture(t->target, t->texture);
	
	GLuint internalFormat, dataFormat;
	uint32_t type = t->type,nDimensions = type,bytesPerPixel;
	if (nDimensions > 3) nDimensions = 3;
	t->bytesPerPixel = bytesPerPixel = dimensions[nDimensions];
	if (bytesPerPixel == 4) {
		if (alpha) internalFormat = compress?rgbaCompressedTextureFormat:GL_RGBA;
		else internalFormat = compress?rgbCompressedTextureFormat:GL_RGB;
		dataFormat = GL_RGBA;
	} else if (bytesPerPixel == 3) {
		internalFormat = compress?rgbCompressedTextureFormat:GL_RGB;
		dataFormat = GL_RGB;
	} else {
		internalFormat = compress?rgbCompressedTextureFormat:GL_RGB;
		dataFormat = GL_LUMINANCE;
	}

	if (type == TEXTURE_TYPE_1D) glTexImage1D(GL_TEXTURE_1D, 0, internalFormat, t->width=dimensions[0], 0, dataFormat, GL_UNSIGNED_BYTE, d);
	else if (type == TEXTURE_TYPE_2D) glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, t->width=dimensions[0], t->height=dimensions[1], 0, dataFormat, GL_UNSIGNED_BYTE, d);
	else if (type == TEXTURE_TYPE_3D) glTexImage3D(GL_TEXTURE_3D, 0, internalFormat, t->width=dimensions[0], t->height=dimensions[1], t->depth=dimensions[2], 0, dataFormat, GL_UNSIGNED_BYTE, d);
	else {
		t->width = dimensions[0];
		t->height = dimensions[1];
		const uint32_t width = dimensions[0], height = dimensions[1], stride = width*height*bytesPerPixel;
		for (uint32_t i = 0; i < 6; i++) glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X+i, 0, internalFormat, width, height, 0, dataFormat, GL_UNSIGNED_BYTE, (uint8_t*)d+i*stride);
	}

	t->data = d;
	t->internalFormat = internalFormat;
	t->dataFormat = dataFormat;
}

static inline void texture_bind(texture *t, const GLint uniformIndex) {
	uint32_t i = numActiveTextures++;
	glActiveTexture(GL_TEXTURE0+i);
	glBindTexture(t->target, t->texture);
	glUniform1i(uniformIndex, i);
}
#define texture_unbind() numActiveTextures--



typedef struct {
	texture color,depth;
	GLuint framebuffer,depthbuffer;
	uint32_t msaa;
	int8_t cubemap;
	bool hasDepthTexture, hasColorTexture;
} rendertexture;

rendertexture screenRenderTexture, *activeRenderTexture = &screenRenderTexture;
uint32_t activeViewportX = UINT_MAX, activeViewportY = UINT_MAX, activeViewportW = UINT_MAX, activeViewportH = UINT_MAX;
int8_t activeRenderTextureCubemap = -1;

void new_rendertexture(rendertexture *t, uint32_t width, uint32_t height, GLuint format, uint32_t depthBits, bool colorTexture, bool depthTexture, uint32_t msaa) {
	glGenFramebuffers(1, &t->framebuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, t->framebuffer);

	t->msaa = msaa;
	t->cubemap = -1;

	if (colorTexture) {
		if (msaa != 0) {
			t->color.data = NULL;
			t->color.target = GL_TEXTURE_2D;
			glGenTextures(1, &t->color.texture);
			glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, t->color.texture);
			glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, msaa, format, width, height, true);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D_MULTISAMPLE, t->color.texture, 0);
			glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, 0);
		}
		else {
			new_texture(&t->color);
			if (format == GL_RGBA32F) {
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			}
			glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
			glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, t->color.texture, 0);
		}

		t->color.width = width;
		t->color.height = height;
		t->color.internalFormat = format;
		t->hasColorTexture = true;
	}
	else {
		t->hasColorTexture = false;
	}

	if (depthTexture) {
		new_texture(&t->depth);
		t->depth.width = width;
		t->depth.height = height;
		t->depth.data = NULL;
		uint32_t dformat = depthBits == 32 ? GL_DEPTH_COMPONENT32 : (depthBits == 24?GL_DEPTH_COMPONENT24 : GL_DEPTH_COMPONENT16);
		t->depth.internalFormat = dformat;
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexImage2D(GL_TEXTURE_2D, 0, dformat, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
		glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, t->depth.texture, 0);
		t->hasDepthTexture = true;
	}
	else {
		if (depthBits != 0) {
			glGenRenderbuffers(1, &t->depthbuffer);
			glBindRenderbuffer(GL_RENDERBUFFER, t->depthbuffer);
			if (msaa != 0) glRenderbufferStorageMultisample(GL_RENDERBUFFER, msaa, GL_DEPTH_COMPONENT, width, height);
			else glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
			glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, t->depthbuffer);
		}
		t->depth.width = depthBits;
		t->hasDepthTexture = false;
	}

	glBindFramebuffer(GL_FRAMEBUFFER, activeRenderTexture->framebuffer);
}

void new_rendertexture_cube(rendertexture *t, uint32_t width, uint32_t height, GLuint format) {
	glGenFramebuffers(1, &t->framebuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, t->framebuffer);

	t->msaa = 0;
	t->cubemap = 0;
	t->hasDepthTexture = false;
	t->hasColorTexture = true;
	
	new_texturex(&t->color,TEXTURE_TYPE_CUBEMAP);
	for (uint32_t i = 0; i < 6; i++) glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X+i,0,format,width,height,0,GL_RGBA,GL_UNSIGNED_BYTE,0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP_POSITIVE_X, t->color.texture, 0);	
	t->color.width = width;
	t->color.height = height;
	t->color.internalFormat = format;
	
	glGenRenderbuffers(1, &t->depthbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, t->depthbuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, t->depthbuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, activeRenderTexture->framebuffer);
}

const char *rendertextureDepthOnlyOption = "RENDERING_DEPTH_ONLY";
void rendertexture_bind(rendertexture *t, float viewX, float viewY, float viewW, float viewH) {
	if (t == &screenRenderTexture) {
		screenRenderTexture.color.width = *screenWidth;
		screenRenderTexture.color.height = *screenHeight;
	}
	
	uint32_t width = t->color.width, height = t->color.height,
		vx = viewX*width, vy = viewY*height, vw = viewW*width, vh = viewH*height;
	if (vx != activeViewportX || vy != activeViewportY || vw != activeViewportW || vh != activeViewportH) {
		glViewport(vx, vy, vw, vh);
		glScissor(vx, vy, vw, vh);
		activeViewportX = vx;
		activeViewportY = vy;
		activeViewportW = vw;
		activeViewportH = vh;
	}

	if (t == activeRenderTexture && t->cubemap == activeRenderTextureCubemap) return;
	bool rdo = t->hasDepthTexture && !t->hasColorTexture,
		 odo = activeRenderTexture->hasDepthTexture&&!activeRenderTexture->hasColorTexture;
	if (rdo != odo) {
		if (rdo) {
			list dcstr = cstring_as_list(rendertextureDepthOnlyOption);
			shader_enableGlobalOption(&dcstr, true);
		}
		else {
			list dcstr = cstring_as_list(rendertextureDepthOnlyOption);
			shader_enableGlobalOption(&dcstr, false);
		}
	}
	if (t->msaa == 0) {
		if (activeRenderTexture->msaa != 0) glDisable(GL_MULTISAMPLE);
	}
	else {
		if (activeRenderTexture->msaa == 0) glEnable(GL_MULTISAMPLE);
	}
	glBindFramebuffer(GL_FRAMEBUFFER, t->framebuffer);
	if (t->cubemap != -1) glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP_POSITIVE_X+t->cubemap, t->color.texture, 0);	
	activeRenderTextureCubemap = t->cubemap;
	activeRenderTexture = t;
}

void delete_rendertexture(rendertexture *t) {
	if (t == activeRenderTexture) rendertexture_bind(&screenRenderTexture, 0.0f, 0.0f, 1.0f, 1.0f);
	glDeleteFramebuffers(1, &t->framebuffer);
	if (t->hasDepthTexture) delete_texture(&t->depth);
	else if (t->depth.width != 0) glDeleteRenderbuffers(1, &t->depthbuffer);
	if (t->hasColorTexture) delete_texture(&t->color);
}

void rendertexture_blit(rendertexture *t, float sx, float sy, float sw, float sh, rendertexture *d, float dx, float dy, float dw, float dh) {
	if (t == &screenRenderTexture || d == &screenRenderTexture) {
		screenRenderTexture.color.width = *screenWidth;
		screenRenderTexture.color.height = *screenHeight;
	}
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, d->framebuffer);
	glBindFramebuffer(GL_READ_FRAMEBUFFER, t->framebuffer);
	glDrawBuffer(GL_COLOR_ATTACHMENT0);
	uint32_t twidth = t->color.width, theight = t->color.height,
		dwidth = d->color.width, dheight = d->color.height;
	glBlitFramebuffer((GLint)(sx*twidth), (GLint)(sy*theight), (GLsizei)(sw*twidth), (GLsizei)(sh*theight),
		(GLint)(dx*dwidth), (GLint)(dy*dheight), (GLsizei)(dw*dwidth), (GLsizei)(dh*dheight), GL_COLOR_BUFFER_BIT, GL_NEAREST);
}

#define rendertexture_setTargetCubemapFace(t,f) (t)->cubemap = f

//mesh, opengl vertex buffer wrapper
typedef struct {
	float *vertices, *uvs, *colors, *normals, *tangents, *bitangents, *bones, *boneWeights;
	uint32_t *indices;
	void *animations;
	uint32_t numVertices, numIndices, numBuffers, flags;
	GLuint buffers[NUM_VERTEX_ATTRIBUTES],arrayObject,indexBuffer;
	uint8_t bufferType[NUM_VERTEX_ATTRIBUTES];
} mesh;

mesh quadMesh, *activeMesh = NULL;

void new_mesh(mesh *b, uint32_t flags) {
	activeMesh = b;
	glGenVertexArrays(1, &b->arrayObject);
	glBindVertexArray(b->arrayObject);

	int nflags = 0;
	for (uint32_t i = 0; i < NUM_VERTEX_ATTRIBUTES; i++) {
		if ((flags>>i)&1) {
			b->bufferType[nflags] = i;
			nflags++;
		}
	}
	glGenBuffers(nflags, &b->buffers[0]);

	int32_t nbufs = nflags;
	for (int32_t i = 0; i < nbufs; i++) {
		GLuint type = b->bufferType[i];
		glBindBuffer(GL_ARRAY_BUFFER, b->buffers[i]);
		glVertexAttribPointer(type, VERTEX_ATTRIBUTE_SIZES[type], GL_FLOAT, false, 0, 0);
		glEnableVertexAttribArray(type);
	}
	if (flags&VERTEX_INDEXED) {
		glGenBuffers(1, &b->indexBuffer);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, b->indexBuffer);
		b->numIndices = 0;
	}

	b->numBuffers = nflags;
	b->flags = flags;
	b->numVertices = 0;
	memset(&b->vertices,0,sizeof(void*)*9);
}
void delete_mesh(mesh *b) {
	glDeleteVertexArrays(1, &b->arrayObject);
	glDeleteBuffers(b->numBuffers, &b->buffers);
	if (b->flags&VERTEX_INDEXED) glDeleteBuffers(1, &b->indexBuffer);
	if (b->vertices != NULL) free(b->vertices);
}

//get index of attribute from attribute id
int32_t mesh_indexOfAttribute(mesh *b, uint32_t attrib) {
	for (int32_t i = 0; i < (int32_t)b->numBuffers; i++) {
		if (b->bufferType[i] == attrib) return i;
	}
	return -1;
}

//bind vertex array
static inline void mesh_bind(mesh *b) {
	if (activeMesh == b) return;
	activeMesh = b;
	glBindVertexArray(b->arrayObject);
}

//set data
void mesh_set(mesh *b, const void *src, uint32_t count, uint32_t attrib, GLenum usage) {
	if (activeMesh != b) {
		activeMesh = b;
		glBindVertexArray(b->arrayObject);
	}
	if (attrib == VERTEX_INDEXED) {
		b->numIndices = count;
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, b->indexBuffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, count*sizeof(GLuint), src, usage);
	} else {
		if (attrib == VERTEX_ATTRIBUTEID_POSITION) b->numVertices = count;
		glBindBuffer(GL_ARRAY_BUFFER, b->buffers[mesh_indexOfAttribute(b,attrib)]);
		glBufferData(GL_ARRAY_BUFFER, count*sizeof(GLfloat)*VERTEX_ATTRIBUTE_SIZES[attrib], src, usage);
	}
}



/*ASCII font loaded from TTF font using SDL_TTF, supports 127 characters defined by GLYPH_ASCII_MIN to GLYPH_ASCII_MAX.
Font glyph texture memory in bytes is maxFontSize*maxFontSize*256.
*/
#define GLYPH_ASCII_MIN 33
#define GLYPH_ASCII_MAX 160
typedef struct {
	vec2 position, size;
	float aspect;
	int32_t advance;
	bool exists;
} glyph;
typedef struct {
	uint32_t maxSize;
	texture glyphs;
	glyph offsets[256];
} font;

static inline void delete_font(font *f) {
	delete_texture(&f->glyphs);
}
void new_font(font *f) {
	new_texture(&f->glyphs);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_G, GL_RED);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_B, GL_RED);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_A, GL_GREEN);
}

bool font_load(font *f, uint32_t maxFontSize, void *d, const uint32_t dsz) {
#ifndef EXCLUDE_LIB_SDL_TTF
	//load TTF_font
	SDL_RWops *rwo = SDL_RWFromMem(d,dsz);
	TTF_Font *ttf = TTF_OpenFontRW(rwo, 0, (maxFontSize*72)/100);
	if (ttf == NULL) {
		SDL_RWclose(rwo);
		return false;
	}

	f->maxSize = maxFontSize;
	uint32_t txwidth = maxFontSize * 16,
		txheight = maxFontSize * 8;
	f->glyphs.width = txwidth;
	f->glyphs.height = txheight;
	float wscale = 1.0f/(float)txwidth, 
		hscale = 1.0f/(float)txheight;

	//move glyphs into texture and get glyph metrics
	glBindTexture(GL_TEXTURE_2D, f->glyphs.texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RG8, txwidth, txheight, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

	void *blankData = malloc(maxFontSize*maxFontSize*4);
	memset(blankData, 0xFF, maxFontSize*maxFontSize*4);
	glTexSubImage2D(GL_TEXTURE_2D,0,0,0,maxFontSize,maxFontSize,GL_RGBA,GL_UNSIGNED_BYTE, blankData);
	memset(blankData, 0, maxFontSize*maxFontSize*4);

	int32_t buf[4];
	if (TTF_GlyphIsProvided(ttf, 32)) TTF_GlyphMetrics(ttf, 32, buf,buf+1,buf+2,buf+3, &f->offsets[32].advance);
	else f->offsets[32].advance = maxFontSize;

	SDL_Color dc = (SDL_Color){0xFF,0xFF,0xFF,0xFF};
	for (uint16_t ac = 0; ac < GLYPH_ASCII_MIN; ac++) f->offsets[ac].exists = false;
	for (uint16_t ac = GLYPH_ASCII_MIN; ac < GLYPH_ASCII_MAX; ac++) {
		uint32_t rac = (ac-GLYPH_ASCII_MIN)+1,
				 tx = (rac%16)*maxFontSize,
				 ty = (rac/16)*maxFontSize;
		glTexSubImage2D(GL_TEXTURE_2D, 0, tx, ty, maxFontSize, maxFontSize, GL_RGBA, GL_UNSIGNED_BYTE, blankData);

		glyph *g = f->offsets+ac;
		if ((g->exists = TTF_GlyphIsProvided(ttf,ac))) {
			TTF_GlyphMetrics(ttf, ac, buf, buf + 1, buf + 2, buf + 3, &g->advance);
			if (buf[0] >= buf[1] || buf[2] >= buf[3]) {
				g->exists = false;
				continue;
			}

			SDL_Surface *s = TTF_RenderGlyph_Blended(ttf, ac, dc),
			*cs = SDL_ConvertSurfaceFormat(s, SDL_PIXELFORMAT_RGBA8888, 0);

			g->size = (vec2){(cs->w-1.0f)*wscale,(cs->h-1.0f)*-hscale};
			g->position = (vec2){(tx + cs->w*0.5f)*wscale, (ty + cs->h*0.5f)*hscale};
			g->aspect = (g->size.x * -2.0f) / g->size.y;

			SDL_LockSurface(cs);

			//swap alpha and color channel
			uint8_t *pxv = cs->pixels;
			uint32_t pxwidth = cs->w, pxheight = cs->h;
			for (uint32_t y = 0; y < pxheight; y++) {
				for (uint32_t x = 0; x < pxwidth; x++) {
					uint8_t tmp = pxv[0];
					pxv[0] = pxv[1];
					pxv[1] = tmp;
					pxv += 4;
				}
			}

			glTexSubImage2D(GL_TEXTURE_2D, 0, tx, ty, cs->w, cs->h, GL_RGBA, GL_UNSIGNED_BYTE, cs->pixels);
			SDL_UnlockSurface(cs);

			SDL_FreeSurface(cs);
			SDL_FreeSurface(s);
		}
	}
	for (uint16_t ac = GLYPH_ASCII_MAX; ac < 256; ac++) f->offsets[ac].exists = false;

	free(blankData);
	TTF_CloseFont(ttf);
	SDL_RWclose(rwo);
#endif
	return true;
}
bool font_loadFromFile(font *f, uint32_t maxFontSize, const char *fpath) {
	uint64_t sz = 0;

	void *mem = file_readall(fpath, &sz);
	if (mem == NULL) return false;
	bool res = font_load(f,maxFontSize,mem,sz);

	free(mem);
	return res;
}

enum {
	ANCHOR_BOTTOM_LEFT = 0, ANCHOR_BOTTOM = 1, ANCHOR_BOTTOM_RIGHT = 2,
	ANCHOR_LEFT = 3, ANCHOR_NONE = 4, ANCHOR_CENTER = 4, ANCHOR_MIDDLE = 4, ANCHOR_RIGHT = 5,
	ANCHOR_TOP_LEFT = 6, ANCHOR_TOP = 7, ANCHOR_TOP_RIGHT = 8
};
#define ANCHOR_X(a) (a%3-1)
#define ANCHOR_Y(a) (a/3-1)

//check if opengl extension supported
bool glExtensionSupported(const char *extName) {
#ifdef glGetStringi
	#define fp glGetStringi
#else
	typedef const GLubyte * (*glGetStringi)(GLenum name, GLuint index);
	glGetStringi fp = (glGetStringi)SDL_GL_GetProcAddress("glGetStringi");
	if (fp == NULL) return false;
#endif

	GLint ne;
	glGetIntegerv(GL_NUM_EXTENSIONS, &ne);
	for (GLint i = 0; i < ne-1; i++) {
		if (strcmp(extName, (char*)fp(GL_EXTENSIONS, i)) == 0) return true;
	}
	return false;
}

//init opengl, built-in shaders and quadmesh
void init_graphics(uint32_t *sw, uint32_t *sh, float *sa, float *fw, float *fh, float *odw, float *odh) {
	screenWidth = sw;
	screenHeight = sh;
	screenAspect = sa;
	screenWidthf = fw;
	screenHeightf = fh;
	oneDivScreenWidth = odw;
	oneDivScreenHeight = odh;

	#ifndef __linux__
	glewExperimental = true;
	glewInit();
	#endif

	//set default opengl settings
	glDisable(GL_BLEND);
	glDisable(GL_CULL_FACE);
	glDisable(GL_MULTISAMPLE);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_SCISSOR_TEST);
	glDepthMask(true);
	glColorMask(true,true,true,true);
	glClearDepth(1.0f);
	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glDisable(GL_POLYGON_OFFSET_FILL);
	glPolygonOffset(0.0f,0.0f);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glPixelStorei(GL_PACK_ALIGNMENT, 1);

	screenRenderTexture.framebuffer = 0;
	screenRenderTexture.hasColorTexture = true;
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	//compressed texture formats
	if (glExtensionSupported("GL_EXT_texture_compression_s3tc")) {
		rgbCompressedTextureFormat = GL_COMPRESSED_RGB_S3TC_DXT1_EXT;
		rgbaCompressedTextureFormat = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
	}
	else {
		rgbCompressedTextureFormat = GL_RGB;
		rgbaCompressedTextureFormat = GL_RGBA;
	}

	new_list(&shaders, sizeof(void*));

#ifndef EXCLUDE_LIB_SDL_TTF
	//init image/font/mesh loading
	TTF_Init();
#endif

	//basic graphics objects
	new_texture(&whiteTexture);
	whiteTexture.width = 1;
	whiteTexture.height = 1;
	uint8_t color[4] = {0xFF,0xFF,0xFF,0xFF};
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,1,1,0,GL_RGBA,GL_UNSIGNED_BYTE,color);

	//fullscreen quad
	const float fsqVertex[4*3] = {-1.0f,-1.0f,0.0f, 1.0f,-1.0f,0.0f, 1.0f,1.0f,0.0f, -1.0f,1.0f,0.0f};

	new_mesh(&quadMesh, VERTEX_ATTRIBUTE_POSITION);
	mesh_set(&quadMesh, fsqVertex, 4, VERTEX_ATTRIBUTEID_POSITION, GL_STATIC_DRAW);

	//blit shader
	new_shader(&blitShader, cstring_as_list(
"#version 150\n"
"precision highp float;"
"precision highp int;"
"in vec3 in_Position;"
"out vec2 uv, screenUv;"
"flat out int iId;\n"
"struct blit_material {"
	"vec2 position, scale, sourcePosition, sourceScale, clipPosition, clipExtents;"
	"vec4 tint;"
	"float aspect, depth, rotationSin, rotationCos;"
"};\n"
"layout(std140) uniform MaterialBlock {"
	"blit_material blit[204];"
"};\n"
"#define IT blit[gl_InstanceID]\n"
"#define FT blit[iId]\n"
"uniform sampler2D texture;"
"void vert() {"
	"iId = gl_InstanceID;"
	"uv = in_Position.xy*vec2(.5,.5)*IT.sourceScale+IT.sourcePosition;"
	"vec2 suv = in_Position.xy*IT.scale*mat2(IT.rotationCos,IT.rotationSin,-IT.rotationSin,IT.rotationCos);"
	"suv.x *= IT.aspect;"
	"suv += IT.position;"
	"screenUv = suv;"
	"gl_Position = vec4(suv,IT.depth+in_Position.z*1e-4,1);"
"}"
"void frag() {"
	"vec4 c = texture2D(texture,uv)*FT.tint;"
	"if (dot(max(abs(screenUv-FT.clipPosition)-FT.clipExtents,0.),vec2(c.w)) > 0.) {"
		"discard;"
		"return;"
	"}"
	"gl_FragColor = c;"
"}"));

	glUseProgram(blitShader.current.program);
	blitShaderTextureIndex = glGetUniformLocation(blitShader.current.program, "texture");
	blitShaderMaterialIndex = glGetUniformBlockIndex(blitShader.current.program, "MaterialBlock");
	
	//create uniform buffer pool
	new_uniform_buffer_pool(&uniform_pool);
}

void free_graphics() {
	delete_texture(&whiteTexture);
	delete_mesh(&quadMesh);
	delete_uniform_buffer_pool(&uniform_pool);

	void *bsb = &blitShader;
	list_removeFast(&shaders,&bsb);
	shader **sp = shaders.buffer;
	for (uint32_t i = 0; i < shaders.length; i++) delete_shader(*sp++);
	delete_shader(&blitShader);
	delete_list(&shaders);
}

#endif
