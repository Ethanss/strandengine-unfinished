﻿/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _SLIST_H_
#define _SLIST_H_

#include "system.h"


enum {
	SLIST_OP_ADD = 0,
	SLIST_OP_REMOVE = 1,
	SLIST_OP_REMOVE_CUSTOM_SIZE = 2,
	SLIST_OP_CLEAR = 3,
	SLIST_OP_INSERT = 4
};
typedef struct {
	list list, lock;
	uint32_t spinning;
	bool threaded,locked;
} slist;

#define construct_slist(stride,threaded) {construct_list(stride),construct_list(stride+8),0,threaded,false}

void new_slist(slist *l, const uint32_t stride, const bool threaded) {
	new_list(&l->list, stride);
	new_list(&l->lock, stride+8);
	l->spinning = 0;
	l->threaded = threaded;
	l->locked = false;
}

void delete_slist(slist *l) {
	delete_list(&l->list);
	delete_list(&l->lock);
}

void slist_clear(slist *l) {
	const bool mthread = l->threaded;
	if (mthread) {
		while (ATOMIC_SWAP(&l->spinning,0,1) == 1);
	}
	if (l->locked) {
		uint32_t offset = list_addEmpty(&l->lock);
		uint8_t *p = l->lock.buffer;
		p += offset*l->lock.stride;
		p[0] = SLIST_OP_CLEAR;
	} else {
		l->list.length = 0;
	}
	if (mthread) l->spinning = 0;
}


//lock synchronized list
static inline void slist_lock(slist *l) {
	const bool mthread = l->threaded;
	if (mthread) {
		while (ATOMIC_SWAP(&l->spinning,0,1) == 1);
	}
	l->locked = true;
	if (mthread) l->spinning = 0;
}

//unlock synchronized list, executing all the add/remove operations in the buffer
void slist_unlock(slist *l) {
	const bool mthread = l->threaded;
	if (mthread) {
		while (ATOMIC_SWAP(&l->spinning,0,1) == 1);
	}
	const uint32_t ll = l->lock.length, stride = l->lock.stride;
	int32_t ind;
	uint8_t *lp = l->lock.buffer;
	for (uint32_t i = 0; i < ll; i++) {
		uint8_t cb = *lp;
		switch (cb) {
		case SLIST_OP_ADD: list_add(&l->list, lp+8); break;
		case SLIST_OP_REMOVE: list_remove(&l->list, lp+8); break;
		case SLIST_OP_REMOVE_CUSTOM_SIZE:
			ind = list_indexOf((list*)l, lp+8, ((uint32_t*)(lp+2))[0]);
			if (ind != -1) list_removeAt((list*)l, ind);
			break;
		case SLIST_OP_CLEAR: l->list.length = 0; break;
		case SLIST_OP_INSERT:
			ind = *(uint32_t*)(lp+4);
			if (ind < l->list.length) list_insert(&l->list, lp+8, ind);
			break;
		}
		lp += stride;
	}
	l->lock.length = 0;
	l->locked = false;
	if (mthread) l->spinning = false;
}

//add object to list, or add operation buffer if locked
void slist_add(slist *l, const void *o) {
	const bool mthread = l->threaded;
	if (mthread) {
		while (ATOMIC_SWAP(&l->spinning,0,1) == 1);
	}
	if (l->locked) {
		uint32_t offset = list_addEmpty(&l->lock);
		uint8_t *p = l->lock.buffer;
		p += offset*l->lock.stride;
		p[0] = SLIST_OP_ADD;
		memcpy(p+8, o, l->list.stride);
	} else {
		list_add(&l->list, o);
	}
	if (mthread) l->spinning = 0;
}

//add multiple objects to list, or add operation buffer if locked
void slist_addm(slist *l, const void *o, const uint32_t count) {
	const bool mthread = l->threaded;
	if (mthread) {
		while (ATOMIC_SWAP(&l->spinning,0,1) == 1);
	}
	if (l->locked) {
		const uint32_t offset = l->lock.length, stride = l->lock.stride, ostride = l->list.stride;
		l->lock.length += count;
		list_expand(&l->lock);
		uint8_t *p = l->lock.buffer, *op = o;
		p += offset*stride;
		for (uint32_t i = 0; i < count; i++) {
			p[0] = SLIST_OP_ADD;
			memcpy(p+8,op,ostride);
			p += stride;
			op += ostride;
		}
	} else {
		list_addm(&l->list, o, count);
	}
	if (mthread) l->spinning = 0;
}

//insert object into list at index
void slist_insert(slist *l, const void *o, uint32_t i) {
	const bool mthread = l->threaded;
	if (mthread) {
		while (ATOMIC_SWAP(&l->spinning,0,1) == 1);
	}
	if (l->locked) {
		uint32_t offset = list_addEmpty(&l->lock);
		uint8_t *p = l->lock.buffer;
		p += offset*l->lock.stride;
		p[0] = SLIST_OP_INSERT;
		((uint32_t*)(p+4))[0] = i;
		memcpy(p+8, o, l->list.stride);
	} else {
		list_insert(&l->list, o, i);
	}
	if (mthread) l->spinning = 0;
}

//remove object from list, or add operation buffer if locked
void slist_remove(slist *l, void *o) {
	const bool mthread = l->threaded;
	if (mthread) {
		while (ATOMIC_SWAP(&l->spinning,0,1) == 1);
	}
	if (l->locked) {
		uint32_t offset = list_addEmpty(&l->lock);
		uint8_t *p = l->lock.buffer;
		p += offset*l->lock.stride;
		p[0] = SLIST_OP_REMOVE;
		memcpy(p+8, o, l->list.stride);
	} else {
		list_remove((list*)l, o);
	}
	if (mthread) l->spinning = 0;
}

void slist_removeCustomSize(slist *l, void *o, const uint32_t sz) {
	const bool mthread = l->threaded;
	if (mthread) {
		while (ATOMIC_SWAP(&l->spinning,0,1) == 1);
	}
	if (l->locked) {
		uint32_t offset = list_addEmpty(&l->lock);
		uint8_t *p = l->lock.buffer;
		p += offset*l->lock.stride;
		p[0] = SLIST_OP_REMOVE_CUSTOM_SIZE;
		((uint32_t*)(p+2))[0] = sz;
		memcpy(p+8, o, l->list.stride);
	} else {
		int32_t ind = list_indexOf((list*)l, o, l->list.stride);
		if (ind != -1) list_removeAt((list*)l, ind);
	}
	if (mthread) l->spinning = 0;
}

#endif
