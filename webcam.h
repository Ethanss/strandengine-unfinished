/*Copyright 2019 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files(the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and / or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions :
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Linux code was derived from 'capture.c' found at https://linuxtv.org/.

Windows code requires Windows Media Foundation which is only present on Windows Vista or later,
Windows Media foundation libraries are: mf.lib, mfplat.lib, mfreadwrite.lib, mfuuid.lib, mf_vista.lib, mfplat_vista.lib
*/

#ifndef _WEBCAM_H_
#define _WEBCAM_H_

#include <stdint.h>
#include <stdbool.h>

#define webcam_capture_cvtyuvrgb() int16_t res = y0 + ar;\
*data++ = res<0 ? 0 : (res>255 ? 255 : res);\
res = y0 + ag;\
*data++ = res<0 ? 0 : (res>255 ? 255 : res);\
res = y0 + ab;\
*data++ = res<0 ? 0 : (res>255 ? 255 : res);\
res = y1 + ar;\
*data++ = res<0 ? 0 : (res>255 ? 255 : res);\
res = y1 + ag;\
*data++ = res<0 ? 0 : (res>255 ? 255 : res);\
res = y1 + ab;\
*data++ = res<0 ? 0 : (res>255 ? 255 : res);

#ifdef __linux__
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <linux/videodev2.h>

typedef struct {
	uint8_t *data, *rawData;
	uint32_t width, height, bufferSize, bufferCount, format, scanLine;
	int handle;
	bool streaming, started;
} webcam;

#ifndef xioctl
static inline int xioctl(int handle, int request, void *arg) {
	int r;
	while ((r = ioctl(handle,request,arg)) == -1 && errno == EINTR);
	return r;
}
#endif

bool new_webcam(webcam *w, const uint32_t deviceId, const uint32_t streamBuffers) {
	char name[32];
	sprintf(name, "/dev/video%d", deviceId);
	
	struct stat st;
    if (stat(name, &st) == -1) return false;
    if (!S_ISCHR(st.st_mode)) return false;

	int handle = w->handle = open(name, O_RDWR | (streamBuffers==0?0:O_NONBLOCK), 0);
	if (handle == -1) return false;
	
	struct v4l2_capability cap;
	struct v4l2_format fmt;
	
	if (xioctl(handle,VIDIOC_QUERYCAP,&cap) == -1) {
		close(handle);
		return false;
	}
	if (!(cap.capabilities&V4L2_CAP_VIDEO_CAPTURE)) {
		close(handle);
		return false;
	}
	
	w->streaming = streamBuffers!=0&&(cap.capabilities&V4L2_CAP_STREAMING)!=0;
	if (!w->streaming) {
		if ((cap.capabilities&V4L2_CAP_READWRITE) == 0) {
			close(handle);
			return false;
		}
	}

	memset(&fmt,0,sizeof(fmt));
	fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	if (xioctl(handle,VIDIOC_G_FMT,&fmt) == -1) {
		close(handle);
		return false;
	}
	
	w->format = fmt.fmt.pix.pixelformat;
	if (!(w->format == V4L2_PIX_FMT_BGR24 || w->format == V4L2_PIX_FMT_RGB24 || w->format == V4L2_PIX_FMT_ARGB32 || w->format == V4L2_PIX_FMT_ABGR32 || w->format == V4L2_PIX_FMT_XRGB32 || w->format == V4L2_PIX_FMT_XBGR32 || w->format == V4L2_PIX_FMT_YUYV || w->format == V4L2_PIX_FMT_UYVY || w->format == V4L2_PIX_FMT_YVYU || w->format == V4L2_PIX_FMT_VYUY)) {
		//native format isnt supported, now try to force one of the supported formats
		#define FMTL 4
		uint32_t fmts[FMTL] = {V4L2_PIX_FMT_YUYV,V4L2_PIX_FMT_UYVY,V4L2_PIX_FMT_RGB24,V4L2_PIX_FMT_ARGB32},fid;
		for (fid = 0; fid < FMTL; fid++) {
			fmt.fmt.pix.field = V4L2_FIELD_ANY;
			fmt.fmt.pix.pixelformat = fmts[fid];
			if (xioctl(handle,VIDIOC_S_FMT,&fmt) == -1) continue;
			w->format = fmts[fid];
		}
		if (fid >= FMTL) {
			close(handle);
			return false;
		}
	}
	
	w->width = fmt.fmt.pix.width;
	w->height = fmt.fmt.pix.height;
	w->scanLine = fmt.fmt.pix.bytesperline;
	w->bufferSize = fmt.fmt.pix.sizeimage;
	if (w->bufferSize < w->scanLine*w->height) w->bufferSize = w->scanLine*w->height;
	
	if (w->streaming) {
		struct v4l2_requestbuffers req;
		memset(&req,0,sizeof(req));
	    req.count = streamBuffers;
	    req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	    req.memory = V4L2_MEMORY_USERPTR;

		if (xioctl(handle,VIDIOC_REQBUFS,&req) == -1) {
			close(handle);
			return false;
		}
		
		w->rawData = malloc(sizeof(void*)*req.count+w->width*w->height*3);
		w->data = w->rawData+sizeof(void*)*req.count;
		w->bufferCount = req.count;
		
		void **wbuf = w->rawData;
		for (uint32_t i = 0; i < req.count; i++) *wbuf++ = malloc(w->bufferSize);
	} else {
		w->rawData = malloc(w->bufferSize+w->width*w->height*3);
		w->data = w->rawData+w->bufferSize;
	}
	w->started = false;
    return true;
}

bool webcam_start(webcam *w) {
	if (w->started) return true;
	if (w->streaming) {
		enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		if (xioctl(w->handle,VIDIOC_STREAMON,&type) == -1) return false;
	
		struct v4l2_buffer buf;
		for (uint32_t i = 0; i < w->bufferCount; i++) {
			memset(&buf,0,sizeof(buf));
			buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
			buf.memory = V4L2_MEMORY_USERPTR;
			buf.index = i;
			buf.m.userptr = (unsigned long)((void**)w->rawData)[i];
			buf.length = w->bufferSize;
			if (xioctl(w->handle,VIDIOC_QBUF,&buf) == -1) return false;
		}
	}
	return w->started=true;
}
void webcam_stop(webcam *w) {
	if (!w->started) return;
	w->started = false;
	if (w->streaming) {
		enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		xioctl(w->handle,VIDIOC_STREAMOFF,&type);
	}
}

static inline void delete_webcam(webcam *w) {
	webcam_stop(w);
	if (w->streaming) {
		for (uint32_t k = 0; k < w->bufferCount; k++) free(((void**)w->rawData)[k]);
	}
	close(w->handle);
	free(w->rawData);
}

bool webcam_capture(webcam *w) {
	if (!w->started) return false;
	
	struct v4l2_buffer buf;
	uint8_t *dp;
	if (w->streaming) {
		memset(&buf,0,sizeof(buf));
		buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buf.memory = V4L2_MEMORY_USERPTR;
		if (xioctl(w->handle,VIDIOC_DQBUF,&buf) == -1) return false;
		dp = (void*)buf.m.userptr;
	} else {
		dp = w->rawData;
		if (read(w->handle,dp,w->bufferSize) == -1) return false;
	}
	
	//convert webcam image format to rgb24
	const uint32_t width = w->width, height = w->height, iscan = w->scanLine, oscan = width*3;
	uint8_t *data = w->data;
	if (w->format == V4L2_PIX_FMT_RGB24) {
		for (uint32_t i = 0; i < height; i++) {
			memcpy(data,dp,oscan);
			dp += iscan;
			data += oscan;
		}
	} else if (w->format == V4L2_PIX_FMT_BGR24) {
		const uint32_t sdiff = iscan-oscan;
		for (uint32_t i = 0; i < height; i++) {
			for (uint32_t x = 0; x < width; x++) {
				const uint8_t b = *dp++, g = *dp++, r = *dp++;
				*data++ = r;
				*data++ = g;
				*data++ = b;
			}
			dp += sdiff;
		}
	} else if (w->format == V4L2_PIX_FMT_ARGB32 || w->format == V4L2_PIX_FMT_XRGB32) {
		const uint32_t sdiff = iscan-width*4;
		for (uint32_t i = 0; i < height; i++) {
			for (uint32_t x = 0; x < width; x++) {
				*data++ = *dp++;
				*data++ = *dp++;
				*data++ = *dp++;
				dp++;
			}
			dp += sdiff;
		}
	} else if (w->format == V4L2_PIX_FMT_ABGR32 || w->format == V4L2_PIX_FMT_XBGR32) {
		const uint32_t sdiff = iscan-width*4;
		for (uint32_t i = 0; i < height; i++) {
			for (uint32_t x = 0; x < width; x++) {
				const uint8_t b = *dp++, g = *dp++, r = *dp++;
				*data++ = r;
				*data++ = g;
				*data++ = b;
				dp++;
			}
			dp += sdiff;
		}
	} else if (w->format == V4L2_PIX_FMT_YUYV) {
		const uint32_t sdiff = iscan-width*2, w2 = width/2;
		for (uint32_t i = 0; i < height; i++) {
			for (uint32_t x = 0; x < w2; x++) {
				const int16_t y0 = *dp++, b = (*dp++)-128, y1 = *dp++, r = (*dp++)-128,
				ar = r+(r>>2)+(r>>3)+(r>>5), ag = -(b>>2)-(b>>4)-(b>>5)-(r>>1)-(r>>3)-(r>>4)-(r>>5), ab = b+(b>>1)+(b>>2)+(b>>6);
				webcam_capture_cvtyuvrgb();
			}
			dp += sdiff;
		}
	} else if (w->format == V4L2_PIX_FMT_UYVY) {
		const uint32_t sdiff = iscan-width*2, w2 = width/2;
		for (uint32_t i = 0; i < height; i++) {
			for (uint32_t x = 0; x < w2; x++) {
				const int16_t b = (*dp++)-128, y0 = *dp++, r = (*dp++)-128, y1 = *dp++,
				ar = r+(r>>2)+(r>>3)+(r>>5), ag = -(b>>2)-(b>>4)-(b>>5)-(r>>1)-(r>>3)-(r>>4)-(r>>5), ab = b+(b>>1)+(b>>2)+(b>>6);
				webcam_capture_cvtyuvrgb();
			}
			dp += sdiff;
		}
	} else if (w->format == V4L2_PIX_FMT_YVYU) {
		const uint32_t sdiff = iscan-width*2, w2 = width/2;
		for (uint32_t i = 0; i < height; i++) {
			for (uint32_t x = 0; x < w2; x++) {
				const int16_t y0 = *dp++, r = (*dp++)-128, y1 = *dp++, b = (*dp++)-128,
				ar = r+(r>>2)+(r>>3)+(r>>5), ag = -(b>>2)-(b>>4)-(b>>5)-(r>>1)-(r>>3)-(r>>4)-(r>>5), ab = b+(b>>1)+(b>>2)+(b>>6);
				webcam_capture_cvtyuvrgb();
			}
			dp += sdiff;
		}
	} else if (w->format == V4L2_PIX_FMT_VYUY) {
		const uint32_t sdiff = iscan-width*2, w2 = width/2;
		for (uint32_t i = 0; i < height; i++) {
			for (uint32_t x = 0; x < w2; x++) {
				const int16_t r = (*dp++)-128, y0 = *dp++, b = (*dp++)-128, y1 = *dp++,
				ar = r+(r>>2)+(r>>3)+(r>>5), ag = -(b>>2)-(b>>4)-(b>>5)-(r>>1)-(r>>3)-(r>>4)-(r>>5), ab = b+(b>>1)+(b>>2)+(b>>6);
				webcam_capture_cvtyuvrgb();
			}
			dp += sdiff;
		}
	}
	
	if (w->streaming) xioctl(w->handle,VIDIOC_QBUF,&buf);
	return true;
}
#else

#include <mfapi.h>
#include <mfidl.h>
#include <mfreadwrite.h>

bool initializedWindowsMediaFoundation = false;

enum {
	WEBCAM_FORMAT_YUYV = 0,
	WEBCAM_FORMAT_UYVY = 1,
	WEBCAM_FORMAT_YVYU = 2,
	WEBCAM_FORMAT_RGB24 = 3,
	WEBCAM_FORMAT_RGB32 = 4
};

typedef struct {
	uint8_t *data;
	uint32_t height, width;
	IMFMediaSource *handle;
	IMFSourceReader *sreader;
	uint8_t format;
	bool started;
} webcam;

bool new_webcam(webcam *w, const uint32_t deviceId, const uint32_t streamBuffers) {
	if (!initializedWindowsMediaFoundation) {
		MFStartup(MF_VERSION,MFSTARTUP_NOSOCKET);
		initializedWindowsMediaFoundation = true;
	}
	UINT32 count = 0;

	//get IMFMediaSource for video capture device
	IMFAttributes *pConfig = NULL;
	IMFActivate **ppDevices = NULL;
	if (FAILED(MFCreateAttributes(&pConfig, 1))) return false;
	if (FAILED(pConfig->lpVtbl->SetGUID(pConfig,&MF_DEVSOURCE_ATTRIBUTE_SOURCE_TYPE,&MF_DEVSOURCE_ATTRIBUTE_SOURCE_TYPE_VIDCAP_GUID))) return false;
	if (FAILED(MFEnumDeviceSources(pConfig,&ppDevices, &count))) return false;
	if (count == 0 || count <= deviceId) return false;
	if (FAILED(ppDevices[deviceId]->lpVtbl->ActivateObject(ppDevices[deviceId], &IID_IMFMediaSource, &w->handle))) return false;
	for (uint32_t i = 0; i < count; i++) ppDevices[i]->lpVtbl->Release(ppDevices[i]);
	CoTaskMemFree(ppDevices);

	//setup IMFSourceReader for IMFMediaSource
	if (FAILED(MFCreateSourceReaderFromMediaSource(w->handle, pConfig, &w->sreader))) return false;
	IMFMediaType *pType = NULL, *nativeType = NULL;
	if (FAILED(w->sreader->lpVtbl->GetNativeMediaType(w->sreader, 0, 0, &nativeType))) return false;
	if (FAILED(nativeType->lpVtbl->GetUINT64(nativeType, &MF_MT_FRAME_SIZE, &w->height))) return false;
	if (FAILED(MFCreateMediaType(&pType))) return false;
	if (FAILED(pType->lpVtbl->SetGUID(pType,&MF_MT_MAJOR_TYPE,&MFMediaType_Video))) return false;
	
	//try and set image format to one of the supported types
	#define FMTL 5
	void *fmts[FMTL] = {&MFVideoFormat_YUY2,&MFVideoFormat_UYVY,&MFVideoFormat_YVYU,&MFVideoFormat_RGB24,&MFVideoFormat_RGB32};
	uint32_t fid;
	for (fid = 0; fid < FMTL; fid++) {
		if (FAILED(pType->lpVtbl->SetGUID(pType,&MF_MT_SUBTYPE,fmts[fid]))) continue;
		if (FAILED(w->sreader->lpVtbl->SetCurrentMediaType(w->sreader,0,NULL,pType))) continue;
		w->format = WEBCAM_FORMAT_YUYV+fid;
		break;
	}
	if (fid >= FMTL) return false;
	#undef FMTL

	pType->lpVtbl->Release(pType);
	nativeType->lpVtbl->Release(nativeType);
	//allocate video data buffer
	w->data = malloc(w->width*w->height*3);
	w->started = false;
	return true;
}

bool webcam_start(webcam *w) {
	if (!w->started) {
		w->started = true;
		IMFSample *sample;
		DWORD streamIndex, flags;
		LONGLONG llTimeStamp;
		if (SUCCEEDED(w->sreader->lpVtbl->ReadSample(w->sreader, MF_SOURCE_READER_FIRST_VIDEO_STREAM, 0, &streamIndex, &flags, &llTimeStamp, &sample))) {
			if (sample != NULL) sample->lpVtbl->Release(sample);
		}
		else return false;
	}
	return true;
}
#define webcam_stop(w) 

static inline void delete_webcam(webcam *w) {
	w->sreader->lpVtbl->Release(w->sreader);
	w->handle->lpVtbl->Shutdown(w->handle);
	w->handle->lpVtbl->Release(w->handle);
	free(w->data);
}

bool webcam_capture(webcam *w) {
	//get webcam frame sample
	IMFSample *sample;
	DWORD streamIndex, flags;
	LONGLONG llTimeStamp;
	if (FAILED(w->sreader->lpVtbl->ReadSample(w->sreader, MF_SOURCE_READER_FIRST_VIDEO_STREAM, 0, &streamIndex, &flags, &llTimeStamp, &sample))) return false;
	if (sample == NULL) return false;
	IMFMediaBuffer *buffer;
	if (FAILED(sample->lpVtbl->GetBufferByIndex(sample, 0, &buffer))) goto failexit;

	const uint32_t width = w->width, height = w->height;
	uint8_t *data = w->data, *raw;
	if (FAILED(buffer->lpVtbl->Lock(buffer, &raw, &streamIndex, &flags))) {
		buffer->lpVtbl->Release(buffer);
		goto failexit;
	}

	uint8_t format = w->format;
	if (format == WEBCAM_FORMAT_RGB24) {
		data += width*height*3-1;
		for (uint32_t y = 0; y < height; y++) {
			for (uint32_t x = 0; x < width; x++) {
				*data-- = *raw++;
				*data-- = *raw++;
				*data-- = *raw++;
			}
		}
	}
	else if (format == WEBCAM_FORMAT_RGB32) {
		data += width*height*3-1;
		for (uint32_t y = 0; y < height; y++) {
			for (uint32_t x = 0; x < width; x++) {
				*data-- = *raw++;
				*data-- = *raw++;
				*data-- = *raw++;
				raw++;
			}
		}
	} else if (w->format == WEBCAM_FORMAT_YUYV) {
		const uint32_t w2 = width / 2;
		for (uint32_t i = 0; i < height; i++) {
			for (uint32_t x = 0; x < w2; x++) {
				const int16_t y0 = *raw++, b = (*raw++) - 128, y1 = *raw++, r = (*raw++) - 128,
					ar = r + (r >> 2) + (r >> 3) + (r >> 5), ag = -(b >> 2) - (b >> 4) - (b >> 5) - (r >> 1) - (r >> 3) - (r >> 4) - (r >> 5), ab = b + (b >> 1) + (b >> 2) + (b >> 6);
				webcam_capture_cvtyuvrgb();
			}
		}
	 }
	 else if (w->format == WEBCAM_FORMAT_UYVY) {
		const uint32_t w2 = width / 2;
		for (uint32_t i = 0; i < height; i++) {
			for (uint32_t x = 0; x < w2; x++) {
				const int16_t b = (*raw++) - 128, y0 = *raw++, r = (*raw++) - 128, y1 = *raw++,
					ar = r + (r >> 2) + (r >> 3) + (r >> 5), ag = -(b >> 2) - (b >> 4) - (b >> 5) - (r >> 1) - (r >> 3) - (r >> 4) - (r >> 5), ab = b + (b >> 1) + (b >> 2) + (b >> 6);
				webcam_capture_cvtyuvrgb();
			}
		}
	 }
	 else if (w->format == WEBCAM_FORMAT_YVYU) {
		const uint32_t w2 = width / 2;
		for (uint32_t i = 0; i < height; i++) {
			for (uint32_t x = 0; x < w2; x++) {
				const int16_t y0 = *raw++, r = (*raw++) - 128, y1 = *raw++, b = (*raw++) - 128,
					ar = r + (r >> 2) + (r >> 3) + (r >> 5), ag = -(b >> 2) - (b >> 4) - (b >> 5) - (r >> 1) - (r >> 3) - (r >> 4) - (r >> 5), ab = b + (b >> 1) + (b >> 2) + (b >> 6);
				webcam_capture_cvtyuvrgb();
			}
		}
	}
	buffer->lpVtbl->Unlock(buffer);
	buffer->lpVtbl->Release(buffer);
	sample->lpVtbl->Release(sample);
	return true;

failexit:
	sample->lpVtbl->Release(sample);
	return false;
}
#endif

#endif
