/*Copyright 2018 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files(the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and / or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions :
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _NORMALIZEUNICODE_H_
#define _NORMALIZEUNICODE_H_

#include <string.h>

char unicodeToAsciiTable0[166] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165 },
unicodeToAsciiTable1[191] = { 65, 65, 65, 65, 65, 65, 67, 69, 69, 69, 69, 73, 73, 73, 73, 78, 79, 79, 79, 79, 79, 85, 85, 85, 85, 89, 97, 97, 97, 97, 97, 97, 99, 101, 101, 101, 101, 105, 105, 105, 105, 110, 111, 111, 111, 111, 111, 117, 117, 117, 117, 121, 121, 65, 97, 65, 97, 65, 97, 67, 99, 67, 99, 67, 99, 67, 99, 68, 100, 69, 101, 69, 101, 69, 101, 69, 101, 69, 101, 71, 103, 71, 103, 71, 103, 71, 103, 72, 104, 73, 105, 73, 105, 73, 105, 73, 105, 73, 74, 106, 75, 107, 76, 108, 76, 108, 76, 108, 78, 110, 78, 110, 78, 110, 79, 111, 79, 111, 79, 111, 82, 114, 82, 114, 82, 114, 83, 115, 83, 115, 83, 115, 83, 115, 84, 116, 84, 116, 85, 117, 85, 117, 85, 117, 85, 117, 85, 117, 85, 117, 87, 119, 89, 121, 89, 90, 122, 90, 122, 90, 122 },
unicodeToAsciiTable2[2] = { 79, 111 },
unicodeToAsciiTable3[2] = { 85, 117 },
unicodeToAsciiTable4[103] = { 65, 97, 73, 105, 79, 111, 85, 117, 85, 117, 85, 117, 85, 117, 85, 117, 65, 97, 65, 97, 71, 103, 75, 107, 79, 111, 79, 111, 106, 71, 103, 78, 110, 65, 97, 65, 97, 65, 97, 69, 101, 69, 101, 73, 105, 73, 105, 79, 111, 79, 111, 82, 114, 82, 114, 85, 117, 85, 117, 83, 115, 84, 116, 72, 104, 65, 97, 69, 101, 79, 111, 79, 111, 79, 111, 79, 111, 89, 121 },
unicodeToAsciiTable5[250] = { 65, 97, 66, 98, 66, 98, 66, 98, 67, 99, 68, 100, 68, 100, 68, 100, 68, 100, 68, 100, 69, 101, 69, 101, 69, 101, 69, 101, 69, 101, 70, 102, 71, 103, 72, 104, 72, 104, 72, 104, 72, 104, 72, 104, 73, 105, 73, 105, 75, 107, 75, 107, 75, 107, 76, 108, 76, 108, 76, 108, 76, 108, 77, 109, 77, 109, 77, 109, 78, 110, 78, 110, 78, 110, 78, 110, 79, 111, 79, 111, 79, 111, 79, 111, 80, 112, 80, 112, 82, 114, 82, 114, 82, 114, 82, 114, 83, 115, 83, 115, 83, 115, 83, 115, 83, 115, 84, 116, 84, 116, 84, 116, 84, 116, 85, 117, 85, 117, 85, 117, 85, 117, 85, 117, 86, 118, 86, 118, 87, 119, 87, 119, 87, 119, 87, 119, 87, 119, 88, 120, 88, 120, 89, 121, 90, 122, 90, 122, 90, 122, 104, 116, 119, 121, 65, 97, 65, 97, 65, 97, 65, 97, 65, 97, 65, 97, 65, 97, 65, 97, 65, 97, 65, 97, 65, 97, 65, 97, 69, 101, 69, 101, 69, 101, 69, 101, 69, 101, 69, 101, 69, 101, 69, 101, 73, 105, 73, 105, 79, 111, 79, 111, 79, 111, 79, 111, 79, 111, 79, 111, 79, 111, 79, 111, 79, 111, 79, 111, 79, 111, 79, 111, 85, 117, 85, 117, 85, 117, 85, 117, 85, 117, 85, 117, 85, 117, 89, 121, 89, 121, 89, 121, 89, 121 },
unicodeToAsciiTable6[2] = { 75, 65 }, unicodeToAsciiTable7[2] = { 60, 62 };

uint32_t normalizeunicode(uint16_t *ustr, const uint32_t ustrLen, char *astr) {
	uint32_t dlen = 0;
	for (uint32_t i = 0; i < ustrLen; i++) {
		uint16_t mc = *ustr++, dc;
		if (mc < 166) dc = unicodeToAsciiTable0[mc];
		else if (mc < 192) continue;
		else if (mc < 383) dc = unicodeToAsciiTable1[mc - 192];
		else if (mc < 416) continue;
		else if (mc < 418) dc = unicodeToAsciiTable2[mc - 416];
		else if (mc < 431) continue;
		else if (mc < 433) dc = unicodeToAsciiTable3[mc - 431];
		else if (mc < 461) continue;
		else if (mc < 564) dc = unicodeToAsciiTable4[mc - 461];
		else if (mc < 894) continue;
		else if (mc < 895) dc = 59;
		else if (mc < 7680) continue;
		else if (mc < 7930) dc = unicodeToAsciiTable5[mc - 7680];
		else if (mc < 8175) continue;
		else if (mc < 8176) dc = 96;
		else if (mc < 8490) continue;
		else if (mc < 8492) dc = unicodeToAsciiTable6[mc - 8490];
		else if (mc < 8800) continue;
		else if (mc < 8801) dc = 61;
		else if (mc < 8814) continue;
		else if (mc < 8816) dc = unicodeToAsciiTable7[mc - 8814];
		else continue;
		*astr++ = dc;
		dlen++;
	}
	return dlen;
}

#endif