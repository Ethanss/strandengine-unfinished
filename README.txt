StrandEngine Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

StrandEngine version UNFINISHED
Library of C(99ish) code for developing real-time applications and games for Linux and Windows. This README contains information, notes and instructions. To get started learning look in the 'Engine/examples' folder for code examples of the classes and functions.

The README notes some issues but also be aware there is a giant number of potentially unknown issues or bugs. Use this code and software at your own risk.


Code Files:
msvcdef.h - Defines for fixing a few inconsistencies between Microsoft Visual Studio and GCC.
endianness.h - Functions for detecting endianness and reversing byte order.
misc.h - Miscellaneous mix of functions, includes hashing function.
system.h - OS/system related functions, includes threadSleep and mutex locks.
list.h - Dynamic array struct and functions.
hashmap.h - Hashmap lookup struct and functions.
pool.h - Pool struct and functions.
slist.h - Synchronized list for multithread safe list operations, including locking/unlocking for iterating.
vecmath.h - Vec2/vec3/vec4 structs and functions.
rng.h - Random number generator with custom sized seed.
timer.h - Timer system and tick/refresh rate helper.
reflection.h - Macros and functions for manually declaring reflection data.
json.h - Struct and functions for loading and saving JSON data.
http.h - Basic HTTP/1.1 GET call function.
stream.h - Flexible stream struct and functions for reading and writing files, console and memory.
threadpool.h - Thread pool struct and functions.
screencapture.h - Screen capture functions.
printftofile.h - Uses a macro to re-route printf to a file, bad for performance but nice for debugging.
bmphelper.h - Helper functions for loading and saving .BMP image files.
pnghelper.h - Helper functions for loading and saving .PNG image files.
compression.h - Binary RLE compression and decompression.
sha1.h - Function for SHA1 hash.
base64.h - Base64 encoding and decoding functions.
database.h - Struct and functions for file based list of objects and hashmap.
nsocket.h - Struct and functions wrapping Linux/Windows sockets.
websocket.h - Functions for opening, reading and writing simple unencrypted websocket connections.
webcam.h - Struct and functions for opening and capturing webcam video.
normalizeunicode.h - Converts unicode text to ASCII.
graphing.h - Struct and functions for drawing 2D graphs and shapes in software.
datapatch.h - Functions for file delta patching.
fouriertransform.h - Fast fourier transform.
cosinetransform.h - Fast cosine transform.
inputcontrol.h - Struct and functions for fake inputs and global keybinds.
processmemory.h - Functions for reading and writing process memory.
processdebug.h - Functions for debugging process threads.
zclass.h - Struct and functions for compiling and executing ZClass runtime scripting language.
window.h - Struct and functions for handling SDL window and OpenGL context setup.
audio.h - Functions for setting up SDL audio output and outputting raw audio stream.
graphics.h - Struct and functions wrapping OpenGL objects like shaders, meshes, textures, uniform buffers and more.
engine.h - Struct and functions for object component system.
engine_serialization.h - Functions for serializing engine objects.
inputmanager.h - Functions for managing input bindings and loading through JSON.
assets.h - Struct and functions for asset loading system.
assets_base.h - Basic text/binary(.txt/.bin) asset loading functions.
audio_assets.h - Audio(.wav) asset loading functions.
graphics_assets.h - Image(.png/.bmp), font(.otf/.ttf), shader(.glsl/.shader) and mesh(.obj/.fbx/.dae/.gltf/.blend/.3ds) loading functions.
engine_launch.h - Functions for normal engine launch.

editor/engine_editor.h - Functions for engine visual editor launch.
editor/editor_style.h - Editor style options like font, font size, font color, foreground and background color.
editor/addons/ - Editor addon code.

components/graphics/rendering.h - Renderer and graphic components for 2D and 3D rendering.

components/gui/gui.h - GUI and GUI components for extendable 2D GUI rendering.
components/gui/guidraggable.h - Draggable GUI component.
components/gui/guidragview.h - Draggable view panel component.
components/gui/guitext.h - Text component.
components/gui/guitextfield.h - Text input component.

components/physics2d/rigidbody2d.h - Rigid body component for 2D physics engine.
components/physics2d/collider2d.h - Collider component for 2D physics engine, supports box, circle and custom colliders.


Dependencies:
SDL(https://www.libsdl.org/index.php) for OpenGL and window/event/os management.
SDL_ttf(https://www.libsdl.org/projects/SDL_ttf/) for pre-rendering fonts.
Freetype(https://www.freetype.org/) for loading fonts.
Zlib(https://zlib.net/) for loading PNG images.
Libpng(http://www.libpng.org/) for loading PNG images.
Open Asset Import Library(http://assimp.sourceforge.net/) for loading 3D models.
GLEW(http://glew.sourceforge.net/) for OpenGL extension functions.

On Ubuntu 'INSTALL64bitLIBS.sh' script installs dependency packages, for other Linux distros look at the packages inside the script. Linux builds require the user to install the required packages.
On Windows the libraries are included in the engine .zip. 'lib32' and 'lib64' folders have the .dlls required for Windows builds, add your platforms lib folder to your PATH environment variable.



General Questions, Issues and Notes


How to build:
Linux builds use GCC and have prepared 'build.sh' scripts.
Windows builds use Microsoft Visual Studio(version Community 2013 and up) and have .vcxproj files.


How to create a new project:
Copy and paste existing project folder. To rename the Visual Studio project open the .vcxproj file in a text editor and search for '<RootNamespace>', replace the name inside with your new project name and save the text file. You also may need to fix any library paths in the project settings if you change the project folders path.


How to compile 32-bit binary on 64-bit on Linux?
Run 'INSTALL32bitLIBS.sh', unfortunatly installing the 32-bit libraries will replace the 64-bit libraries. To switch back to 64-bit libraries run 'INSTALL64bitLIBS.sh'.


How to create your own custom component?
Look at 'components/template.h' it contains code for a template to start from. For thorough examples look at the component systems like 'gui' or 'physics2d'.


How to look around and move camera in editor?
Hold right mouse button to look around and hold both left/middle and right to move.


Could not load editor, failed to load editor font 'abel-regular.ttf'?
Editor mode expects executable files are running 2 directories below the main engine directory(Engine/), so for example 'projects/Testground/exefile' will work but 'projects/exefile' won't. You can override this by changing the font file directory in 'editor/editor_style.h', you will need to do this and provide your own font file if you want to export the editor with your app.


Color system Srgb or linear?
Images are loaded with no gamma changes, shaders apply no gamma changes. This is optimal provided your images are already gamma correct(which they should be). If you are doing 3D rendering that requires different gamma filtering you should apply it in shader code.


Multithreading safety?
First understand the engine runs 3 main threads:
main - Main app logic, updateCalls and syncedCalls run here.
rendering - OpenGL rendering and window event processing, renderCalls and eventCalls run here.
audio - Audio processing, audioCalls and audioEffects run here.

Next understand there is no safety checks, you CAN access any data from any thread BUT it is subject to race conditions and may be unsafe. The best way to understand what is safe is to understand the code itself, even if that may be difficult.
You can safely:
-Call object and component class functions from the main thread.
-Modify audioBuffer from the audio thread.
-Add renderCalls, singleRenderCalls, singleUpdateCalls and eventCalls from any thread.
-Add updateCalls and syncedCalls from the main thread.
-Call OpenGL functions from the rendering thread.

For example if your on the main thread but you want to run a OpenGL function you can use singleRenderCall.


Unicode support?
Unlikely because a lot code in the engine has 1 byte ASCII character codes hard coded. There's other issues in C as well like C-strings and functions like strlen() not working intuitively.


-Shaders compiled with new_shader(any shader asset) use a custom parsing system to combine vertex and fragment shaders and has the following limitations:
vert/frag functions must be the last 2 functions and have no code or comments in between.
No code or comments after the end of the frag function.
No block comments(/* */) inside vert/frag functions.


-List of #define options:
SYMBOL - (optional)DEFAULT VALUE - OPTION DESCRIPTION
ENGINE_EDITMODE - Enables built in editor.
ENGINE_NO_WINDOW - No window or graphics, for console apps.
ENGINE_NO_REFLECTION_NAMES - Strips reflected field names from component meta data, this breaks regular object serialize/deserialize functions and forces serializeFixed. Removing field names can be useful if you want to stop reverse engineering.
ENGINE_ENCRYPT_ASSETS - Encrypt asset packages, this protects against modding and asset ripping but slightly slows down loading.
EXCLUDE_LIB_PNG - Exclude PNG assets and don't use zlib/libpng.
EXCLUDE_LIB_BMP - Exclude BMP assets and don't use bmp loading.
EXCLUDE_LIB_ASSIMP - Exclude 3d mesh assets and don't use assimp lib.
EXCLUDE_LIB_SDL_TTF - Exclude font assets and don't use SDL_TTF or true type libs.
LIST_MAX_STACK_ALLOC - 4096 - Max bytes allocated on stack by list functions.
STREAM_MAX_STACK_ALLOC - 4096 - Max bytes allocated on stack by stream functions.


-Both the 2d and 3d physics engine are a lot different then usual engines, to start each rigid body is a circle/sphere collider. To create a box shaped rigid body you create a jointsystem2d of connected bodies forming the shape of your box or whatever other shape. While this is harder to work with it is actually much more flexible, allowing soft bodies and other effects much more naturally.


-To ensure OpenGL runs on the GPU of some laptops, add these export symbols in one your C code:
__declspec(dllexport) unsigned long NvOptimusEnablement = 1;
__declspec(dllexport) int AmdPowerXpressRequestHighPerformance = 1;


-Libpng has a security vulnerability allowing corrupt PNG files to be used as a potential attack vector, although the odds of a successful attack are very low.


