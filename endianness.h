﻿/*Copyright 2018 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _ENDIANESS_H_
#define _ENDIANESS_H_

#include <stdint.h>

//endianness
uint16_t ENDIAN_TEST_VAL_STRUCT = 1;
#define BIGENDIAN 0
#define LITTLEENDIAN 1
#define ENDIANNESS ((*(char*)&ENDIAN_TEST_VAL_STRUCT)&1)

//reverse bytes
static inline void reverse(const void *b, const uint32_t sz) {
	uint8_t *lo = (uint8_t*)b, *hi = lo+sz-1;
	uint8_t tmp;
	while (lo < hi) {
		tmp = *lo;
		*lo++ = *hi;
		*hi-- = tmp;
	}
}

#endif
