﻿/*Copyright 2018 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Requires linking with Ws2_32.lib on Microsoft Visual Studio.
*/

#ifndef _NSOCKET_H_
#define _NSOCKET_H_

#include "slist.h"
#include "stream.h"


#ifdef __linux__
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/poll.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <netdb.h> 

typedef struct timeval timeval;
#else
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#endif

typedef struct sockaddr sockaddr;
typedef struct sockaddr_in6 sockaddr_in6;
typedef struct pollfd pollfd;
typedef struct addrinfo addrinfo;


typedef struct {
	addrinfo *address;
} inet_address;

typedef struct {
	ifunction receive;
	#ifdef __linux__
	int socket;
	#else
	SOCKET socket;
	#endif
	bool blocking,tcp,server;
} nsocket;


list nsocketNonBlocking = construct_list(sizeof(void*)),
nsocketPollList = construct_list(sizeof(pollfd));

#ifndef __linux__
WSADATA wsaData;
#define init_nsocket_system() WSAStartup(MAKEWORD(2,2),&wsaData)
#define free_nsocket_system() WSACleanup();delete_list(&nsocketNonBlocking);delete_list(&nsocketPollList)
#else
#define init_nsocket_system()
#define free_nsocket_system() delete_list(&nsocketNonBlocking);delete_list(&nsocketPollList)
#endif


//initialize inet_address struct from ip address string, NULL addrNum means any ip address is used and port number
bool new_inet_address(inet_address *a, const char *addrNum, const uint32_t port, bool ipv6, bool tcp) {
	addrinfo hints = {0};
	hints.ai_family = AF_UNSPEC;//ipv6?AF_INET6:AF_INET;
	hints.ai_socktype = tcp?SOCK_STREAM:SOCK_DGRAM;

	char portBuf[8];
	sprintf(portBuf, "%u", port);
	bool res = getaddrinfo(addrNum, portBuf, &hints, &a->address) == 0;
	if (addrNum == NULL && res) {
		if (ipv6) memset(&((struct sockaddr_in6*)a->address->ai_addr)->sin6_addr,0,16);
		else memset(a->address->ai_addr->sa_data+2,0,4);
	}
	return res;
}

//free inet address memory
#define delete_inet_address(a) freeaddrinfo((a)->address)


//create new nsocket and connect socket to inet_address, returns true if connected
bool nsocket_connect(nsocket *s, inet_address *a) {
	s->blocking = true;
	s->server = false;
	bool tcp = a->address->ai_socktype==SOCK_STREAM;
	s->tcp = tcp;
	s->socket = socket(a->address->ai_family,tcp?SOCK_STREAM:SOCK_DGRAM,tcp?IPPROTO_TCP:IPPROTO_UDP);
	return connect(s->socket, a->address->ai_addr, a->address->ai_addrlen) == 0;
}

//remove nsocket from non blocking poll list
void nsocket_removeFromPoll(nsocket *s) {
	list_remove(&nsocketNonBlocking, &s);
	pollfd fd = {0};
	fd.fd = s->socket;
	#ifdef __linux__
	fd.events = POLLIN;
	#else
	fd.events = POLLRDNORM | POLLRDBAND;
	#endif
	fd.revents = 0;
	pollfd *fdp = nsocketPollList.buffer;
	list_remove(&nsocketPollList,&fd);
}

//close socket
void nsocket_close(nsocket *s) {
	if (!s->server && !s->blocking) {
		nsocket_removeFromPoll(s);
		s->blocking = true;
	}
	#ifdef __linux__
	close(s->socket);
	#else
	closesocket(s->socket);
	#endif
}


//set recv timeout
void nsocket_setTimeout(nsocket *s, uint32_t seconds) {
	#ifdef __linux__
	struct timeval tv = (timeval){seconds,0};
	setsockopt(s->socket, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(struct timeval));
	#else
	DWORD tv = seconds * 1000;
	setsockopt(s->socket, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(DWORD));
	#endif
}

//toggle blocking
void nsocket_setBlocking(nsocket *s, bool blocking) {
	if (s->blocking == blocking) return;
	s->blocking = blocking;
	int v = blocking ? 0 : 1;

	#ifdef __linux__
	ioctl(s->socket, FIONBIO, &v);
	#else
	ioctlsocket(s->socket, FIONBIO, &v);
	#endif

	if (!s->server) {
		if (blocking) nsocket_removeFromPoll(s);
		else {
			list_add(&nsocketNonBlocking, &s);
			pollfd fd = {0};
			fd.fd = s->socket;
			#ifdef __linux__
			fd.events = POLLIN;
			#else
			fd.events = POLLRDNORM | POLLRDBAND;
			#endif
			fd.revents = 0;
			list_add(&nsocketPollList,&fd);
		}
	}
}

//toggle nagles algorithm
bool nsocket_setNoDelay(nsocket *s, bool noDelay) {
	int flag = noDelay ? 1 : 0;
	return setsockopt(s->socket, IPPROTO_TCP, TCP_NODELAY, (char*)&flag, sizeof(int)) > 0;
}

//initialize nsocket for hosting server
void nsocket_init(nsocket *s, inet_address *a) {
	s->blocking = true;
	s->server = true;
	bool tcp = a->address->ai_socktype==SOCK_STREAM;
	s->tcp = tcp;
	s->socket = socket(a->address->ai_family,tcp?SOCK_STREAM:SOCK_DGRAM,tcp?IPPROTO_TCP:IPPROTO_UDP);
}

//bind socket to inet_address returns true if successful
static inline bool nsocket_bind(nsocket *s, inet_address *a) {
	return bind(s->socket, a->address[0].ai_addr, a->address[0].ai_addrlen) == 0;
}

//enable socket connection listening with backlog
#define nsocket_listen(s,backlog) listen((s)->socket, backlog)

//accept socket connection, storing connected socket in 'client'. Pass a char buffer to 'clientAddr' at least INET6_ADDRSTRLEN to get the connecting clients ip.
bool nsocket_accept(nsocket *s, nsocket *client, char *clientAddr) {
	client->socket = accept(s->socket,NULL,NULL);
	client->blocking = true;
	client->server = false;
	
	if (clientAddr != NULL) {
		struct sockaddr_storage addr;
		socklen_t len = sizeof(addr);
		getpeername(client->socket,(struct sockaddr*)&addr,&len);
		
		if (addr.ss_family == AF_INET) inet_ntop(AF_INET,&((struct sockaddr_in*)&addr)->sin_addr, clientAddr, INET6_ADDRSTRLEN);
		else inet_ntop(AF_INET6,&((struct sockaddr_in6*)&addr)->sin6_addr, clientAddr, INET6_ADDRSTRLEN);
	}

	#ifdef __linux__
	return client->socket!=-1;
	#else
	return client->socket!=INVALID_SOCKET;
	#endif
}



//read/write, returns number of bytes actually read/write or -1 if error
static inline int32_t nsocket_read(nsocket *s, void *d, const uint32_t sz) {
	#ifdef __linux__
	return read(s->socket, d, sz);
	#else
	return recv(s->socket, d, sz, 0);
	#endif

	return -1;
}
static inline int32_t nsocket_write(nsocket *s, const void *d, const uint32_t sz) {
	#ifdef __linux__
	return write(s->socket, d, sz);
	#else
	return send(s->socket, d, sz, 0);
	#endif

	return -1;
}

//read data until sz is met or until end of stream
bool nsocket_readf(nsocket *s, uint8_t *d, uint32_t sz) {
	while (sz != 0) {
		int32_t rsz = nsocket_read(s,d,sz);
		if (rsz <= 0) return false;
		d += rsz;
		sz -= rsz;
	}
	return true;
}

//poll non-blocking sockets and dispatch receive callbacks, returns false if timed out
bool nsocket_pollNonBlocking(const int32_t timeout) {
	if (nsocketNonBlocking.length == 0) return true;
	
	#ifdef __linux__
	int resVal = poll(nsocketPollList.buffer, nsocketPollList.length, timeout);
	#else
	int resVal = WSAPoll(nsocketPollList.buffer, nsocketPollList.length, timeout);
	#endif
	if (resVal <= 0) return false;
	
	pollfd *fdb = nsocketPollList.buffer;
	nsocket **socks = nsocketNonBlocking.buffer;
	uint32_t plen = nsocketPollList.length;
	for (int32_t i = 0; i < plen; i++) {
		if (fdb->revents) {
			fdb->revents = 0;
			nsocket *sp = socks[i];
			sp->receive.delegate(sp->receive.owner);
			if (plen > nsocketPollList.length) {
				i--;
				fdb--;
				plen = nsocketPollList.length;
			}
		}
		fdb++;
	}
	return true;
}



//nsocket_stream read/write/seek/close functions
typedef struct {
	stream base;
} nsocket_stream;

void nsocket_stream_setPosition(void *p, const uint64_t i) {
	((stream*)p)->position = i;
}
void nsocket_stream_close(void *p) {
	stream *s = p;
	s->position = 0;
	nsocket_close(s->handle);
}
uint32_t nsocket_stream_read(void *p, void *dst, const uint32_t sz) {
	stream *s = p;
	int32_t rsz = nsocket_read(s->handle, dst, sz);
	if (rsz < 0) return 0;
	return (uint32_t)rsz;
}
uint32_t nsocket_stream_write(void *p, void *src, uint32_t sz) {
	stream *s = p;
	int32_t wsz = nsocket_write(s->handle, src, sz);
	if (wsz < 0) return 0;
	return (uint32_t)wsz;
}

//create nsocket_stream backed by nsocket
void new_nsocket_stream(nsocket_stream *s, nsocket *ns) {
	s->base.position = 0;
	s->base.handle = ns;

	s->base.read = &nsocket_stream_read;
	s->base.write = &nsocket_stream_write;
	s->base.setPosition = &nsocket_stream_setPosition;
	s->base.close = &nsocket_stream_close;
}

#endif
