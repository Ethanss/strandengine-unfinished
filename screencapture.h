/*Copyright 2018 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files(the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and / or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions :
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _SCREENCAPTURE_H_
#define _SCREENCAPTURE_H_

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#ifdef __linux__
#include <X11/Xlib.h>
#include <X11/Xutil.h>

void getScreenResolution(uint32_t *width, uint32_t *height) {
	Display *d = XOpenDisplay(NULL);
	Screen *s = DefaultScreenOfDisplay(d);
	width[0] = WidthOfScreen(s);
	height[0] = HeightOfScreen(s);
	XCloseDisplay(d);
}

typedef struct {
	Display *display;
	XImage *image;
	uint8_t *data;
	int screen;
	uint32_t x,y,width,height,depth,bitsPerPixel,dataWidth,dataHeight;
	Window window;
	bool bgr;
} screencapture;

bool new_screencapture(screencapture *s, const char *displayName, const uint32_t x, const uint32_t y, const uint32_t width, const uint32_t height) {
	s->x = x;
	s->y = y;
	s->width = width;
	s->height = height;
	s->dataWidth = width;
	s->dataHeight = height;
	s->display = XOpenDisplay(displayName);
	s->screen = DefaultScreen(s->display);
	s->window = RootWindow(s->display,s->screen);
	s->depth = DefaultDepth(s->display,s->screen);
	s->bitsPerPixel = XBitmapUnit(s->display);
	s->bgr = XBitmapBitOrder(s->display)==LSBFirst;
	uint32_t bytePerPixel = s->bitsPerPixel/8;
	s->image = XCreateImage(s->display,DefaultVisual(s->display,s->screen),s->depth,ZPixmap,0,malloc(width*height*bytePerPixel),width,height,s->bitsPerPixel,bytePerPixel*width);
	if (s->image == NULL) {
		XCloseDisplay(s->display);
		return false;
	}
	s->data = malloc(width*height*3);
	return true;
}
static inline void delete_screencapture(screencapture *s) {
	XDestroyImage(s->image);
	XCloseDisplay(s->display);
	free(s->data);
}

void capturescreen(screencapture *s) {
	const uint32_t w = s->width, h = s->height;
	XImage *img = s->image;
	XGetSubImage(s->display, s->window, s->x, s->y, w, h, AllPlanes, ZPixmap, img, 0, 0);
	
	uint8_t *pp = s->data;
	if (s->bgr) {
		for (uint32_t y = 0; y < h; y++) {
			for (uint32_t x = 0; x < w; x++) {
				const unsigned long v = XGetPixel(img,x,y);
				*pp++ = (v>>16)&0xFF;
				*pp++ = (v>>8)&0xFF;
				*pp++ = v&0xFF;
			}
		}
	} else {
		for (uint32_t y = 0; y < h; y++) {
			for (uint32_t x = 0; x < w; x++) {
				const unsigned long v = XGetPixel(img,x,y);
				*pp++ = v&0xFF;
				*pp++ = (v>>8)&0xFF;
				*pp++ = (v>>16)&0xFF;
			}
		}
	}
}

#else
#include <windows.h>
#include "endianness.h"

void getScreenResolution(uint32_t *width, uint32_t *height) {
	width[0] = GetSystemMetrics(SM_CXSCREEN);
	height[0] = GetSystemMetrics(SM_CYSCREEN);
}

typedef struct {
	HDC src, memory;
	HBITMAP bitmap;
	BITMAPINFOHEADER bitmapInfo;
	uint8_t *data;
	uint32_t x, y, width, height, depth, dataWidth, dataHeight;
} screencapture;

bool new_screencapture(screencapture *s, char *displayName, const uint32_t x, const uint32_t y, const uint32_t width, const uint32_t height) {
	s->x = x;
	s->y = y;
	s->width = width;
	s->height = height;
	s->dataWidth = width;
	s->dataHeight = height;
	s->depth = 3;
	s->src = GetDC(NULL);
	s->memory = CreateCompatibleDC(s->src);
	if (!s->memory) return false;
	s->bitmap = CreateCompatibleBitmap(s->src, width, height);
	s->bitmapInfo.biSize = sizeof(BITMAPINFOHEADER);
	s->bitmapInfo.biPlanes = 1;
	s->bitmapInfo.biBitCount = 32;
	s->bitmapInfo.biCompression = BI_RGB;
	s->bitmapInfo.biSizeImage = 0;
	s->bitmapInfo.biXPelsPerMeter = 0;
	s->bitmapInfo.biYPelsPerMeter = 0;
	s->bitmapInfo.biClrUsed = 0;
	s->bitmapInfo.biClrImportant = 0;
	s->data = malloc(width*height*4);
	return true;
}
static inline void delete_screencapture(screencapture *s) {
	DeleteObject(s->bitmap);
	DeleteDC(s->memory);
	ReleaseDC(NULL,s->src);
	free(s->data);
}

void capturescreen(screencapture *s) {
	const int32_t w = s->width, h = s->height;
	HBITMAP oldbm = SelectObject(s->memory, s->bitmap);
	BitBlt(s->memory, 0, 0, w, h, s->src, s->x, s->y, SRCCOPY);
	SelectObject(s->memory, oldbm);
	s->bitmapInfo.biWidth = w;
	s->bitmapInfo.biHeight = -h;
	GetDIBits(s->memory, s->bitmap, 0, h, s->data, &s->bitmapInfo, DIB_RGB_COLORS);
	
	uint32_t tlen = w*h, v, *rp = s->data;
	uint8_t *wp = rp, *bp = &v;
	for (uint32_t i = 0; i < tlen; i++) {
		v = *rp++;
		*wp++ = bp[2];
		*wp++ = bp[1];
		*wp++ = bp[0];
	}
}
#endif
#endif
