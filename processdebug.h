/*Copyright 2018 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files(the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and / or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions :
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _PROCESSDEBUG_H_
#define _PROCESSDEBUG_H_

#include "processmemory.h"

typedef struct {
	uint32_t control,status,tag,errorOffset,errorSelector,dataOffset,dataSelector;
	float state[20];
} float_registers;

#if M64
typedef struct {
	uint16_t control,status,tag,errorOpcode;
	uint64_t errorOffset,dataOffset;
	uint32_t mxcsr, mxcr_mask;
	float state[32], vector[64], padding[24];
} float_registers64;
#endif


#ifdef __linux__
typedef struct {
	uint32_t ebx,ecx,edx,esi,edi,ebp,eax,xds,xes,xfs,xgs,oeax,eip,xcs,flags,esp,xss;
} general_registers;
typedef struct {
	general_registers registers;
	float_registers floatRegisters;
} thread_context;

#if M64
typedef struct {
	uint64_t r15,r14,r13,r12,rbp,rbx,r11,r10,r9,r8,rax,rcx,rdx,rsi,rdi,orax,rip,cs,eflags,rsp,ss,fsbase,gsbase,xds,xes,xfs,xgs;
} general_registers64;
typedef struct {
	general_registers64 registers;
	float_registers64 floatRegisters;
} thread_context64;
#endif

#else
typedef struct {
	uint32_t padding,ebx,xgs,xfs,xes,xds,edi,esi,ebx,edx,ecx,eax,ebp,eip,xcs,flags,esp,xss;
} general_registers;
typedef struct {
	uint32_t contextFlags,dr0,dr1,dr2,dr3,dr4,dr6,dr7;
	float_registers floatRegisters;
	general_registers registers;
	char padding[512];
} thread_context;

#if M64
typedef struct {
	uint16_t xcs,xds,xes,xfs,xgs,xss;
	uint32_t flags;
	uint64_t dr0,dr1,dr2,dr3,dr6,dr7,rcx,rdx,rbx,rsp,rbp,rsi,rdi,r8,r9,r10,r11,r12,r13,r14,r15,rip;
} general_registers64;
typedef struct {
	uint64_t p1home,p2home,p3home,p4home,p5home,p6home;
	uint32_t contextFlags,mxcsr;
	general_registers64 registers;
	float_registers64 floatRegisters;
	float vector[104];
	uint64_t vectorControl,debugControl,lastBranchToRIP,lastBranchFromRIP,lastExceptionToRIP,lastExceptionFromFIP;
} thread_context64;
#endif

#endif


list *listProcessThreads(const process_id pid, list *l) {
	if (l == NULL) {
		l = malloc(sizeof(list));
		new_list(l,sizeof(thread_id));
	}
	
	#ifdef __linux__
	char tasks[256];
	snprintf(tasks,256,"/proc/%lld/task",pid);
	list pfl = listFiles(tasks);
	
	file_info *fi = pfl.buffer;
	for (uint32_t i = 0; i < pfl.length; i++) {
		if (fi->type == FILE_TYPE_DIRECTORY) {
			thread_id tid = strtoull(fi->name,NULL,10);
			list_add(l,&tid);
		}
		fi++;
	}
	
	delete_list(&pfl);
	#else
	HANDLE threadSnap = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD,0);
	if (threadSnap != INVALID_HANDLE_VALUE) {
		THREADENTRY32 te;
		te.dwSize = sizeof(THREADENTRY32);
		bool running = Thread32First(threadSnap,&te);
		while (running) {
			if (te.th32OwnerProcessID == pid) list_add(l,&te.th32ThreadID);
			running = Thread32Next(threadSnap,&te);
		}
		CloseHandle(threadSnap);
	}
	#endif
	
	return l;
}

static inline bool process_debug_attach(process_memory *m, const thread_id tid) {
	#if __linux__
	if (ptrace(PTRACE_ATTACH,tid,0,0) != -1) {
		waitpid(tid);
		return true;
	}
	return false;
	#else
	return DebugActiveProcess(m->pid);
	#endif
}

static inline bool process_debug_detach(process_memory *m, const thread_id tid) {
	#if __linux__
	return ptrace(PTRACE_DETACH,tid,0,0)!=-1;
	#else
	return DebugActiveProcessStop(m->pid);
	#endif
}

static inline bool process_debug_continue(process_memory *m, const thread_id tid) {
	#if __linux__
	return ptrace(PTRACE_CONT,tid,0,0)!=-1;
	#else
	return ContinueDebugEvent(m->pid,tid,DBG_CONTINUE);
	#endif
}

bool process_debug_is64bit(process_memory *m) {
	#if M64
	#if __linux__
	char buf[64];
	snprintf(buf,64,"/proc/%lld/exe",m->pid);
	FILE *fp = fopen(buf,FILE_MODE_READ);
	int rsz = fread(buf,1,8,fp);
	fclose(fp);
	if (rsz > 4) return buf[4]==2;
	return false;
	#else
	BOOl res;
	if (!IsWow64Process(m->handle,&res)) return false;
	return !res;
	#endif
	#else
	return false;
	#endif
}

bool process_debug_getThreadContext(process_memory *m, const thread_id tid, void *c, bool x64) {
	#if __linux__
	if (ptrace(PTRACE_GETREGS,tid,c,c) == -1) return false;
	void *fpp = x64?(void*)&((thread_context64*)c)->floatRegisters:(void*)&((thread_context*)c)->floatRegisters;
	return ptrace(PTRACE_GETFPREGS,tid,fpp,fpp)!=-1;
	#else
	HANDLE th = OpenThread(THREAD_GET_CONTEXT,false,tid);
	if (th == INVALID_HANDLE_VALUE) return false;
	bool res;
	#if M64
	if (x64) res = GetThreadContext(th,c);
	else res = Wow64GetThreadContext(th,c);
	#else
	res = GetThreadContext(th,c);
	#endif
	CloseHandle(th);
	return res;
	#endif
}
bool process_debug_setThreadContext(process_memory *m, const thread_id tid, void *c, bool x64) {
	#if __linux__
	if (ptrace(PTRACE_SETREGS,tid,c,c) == -1) return false;
	void *fpp = x64?(void*)&((thread_context64*)c)->floatRegisters:(void*)&((thread_context*)c)->floatRegisters;
	return ptrace(PTRACE_SETFPREGS,tid,fpp,fpp)!=-1;
	#else
	HANDLE th = OpenThread(THREAD_SET_CONTEXT,false,tid);
	if (th == INVALID_HANDLE_VALUE) return false;
	#if M64
	if (x64) res = SetThreadContext(th,c);
	else res = Wow64SetThreadContext(th,c);
	#else
	res = SetThreadContext(th,c);
	#endif
	CloseHandle(th);
	return res;
	#endif
}

static inline bool process_debug_setBreakpoint(process_memory *m, const void *addr, uint8_t *old) {
	if (!process_memory_read(m,addr,old,1)) return false;
	const char int3 = 0xCC;
	return process_memory_write(m,addr,&int3,1);
}
static inline bool process_debug_clearBreakpoint(process_memory *m, const void *addr, const uint8_t old) {
	return process_memory_write(m,addr,&old,1);
}

static inline bool process_debug_signal(process_memory *m, const thread_id tid) {
	#if __linux__
	int status;
	waitpid(tid,&status,WNOHANG);
	return WIFSIGNALED(status);
	#else
	DEBUGEVENT evt;
	if (!WaitForDebugEvent(&evt,0)) return false;
	return evt.dwThreadID == tid;
	#endif
}


typedef struct {
	char *name;
	void *address;
	uint32_t nameLength, size;
	bool function;
} module_symbol;

list listModuleSymbols(const char *moduleFile) {
	list ml;
	new_list(&ml,sizeof(module_symbol));
	#if __linux__
	list cres;
	new_list(&cres,1);
	const char *cmdstr = "readelf -s ";
	list_addm(&cres,cmdstr,strlen(cmdstr));
	list_addm(&cres,moduleFile,strlen(moduleFile)+1);
	cres.length = 0;
	if (runSystemCommand(cres.buffer,&cres)) {
		uintptr_t addr = 0;
		uint32_t state = 0, iter = 0, index = 0, tstate, size = 0;
		bool func = false;
		char *p = cres.buffer, *d = p, l = 0;
		for (uint32_t i = 0; i < cres.length; i++) {
			const char c = *p++;
			if (state == 0) {
				if (c == ':' && l >= 0x30 && l <= 0x39) {
					state++;
					i++;
					p++;
					index = i;
				}
			} else if (state == 1) {
				if (c == ' ') {
					d[i] = 0;
					addr = (uintptr_t)strtoull(d+index,NULL,16);
					if (addr != 0) {
						state = 2;
						tstate = 3;
					} else state = 0;
					state = 2;
					tstate = 3;
				}
			} else if (state == 2) {
				if (c != ' ') {
					index = i;
					state = tstate;
				}
			} else if (state == 3) {
				if (c == ' ') {
					d[i] = 0;
					size = strtoul(d+index,NULL,10);
					state = 2;
					tstate = 4;
				}
			} else if (state == 4) {
				func = c == 'F';
				iter = 0;
				state = 6;
				tstate = 5;
			} else if (state == 5) {
				iter++;
				state = iter==4?7:6;
			} else if (state == 6) {
				if (c == ' ') state = 2;
			} else {
				if (c == '\n' && i != index) {
					const uint32_t lindex = list_addEmpty(&ml);
					module_symbol *sym = (module_symbol*)ml.buffer+lindex;
					sym->nameLength = i-index;
					sym->name = malloc(sym->nameLength+1);
					memcpy(sym->name,d+index,sym->nameLength);
					sym->name[sym->nameLength] = 0;
					sym->address = (void*)addr;
					sym->size = size;
					sym->function = func;
				}
			}
			if (c == '\n') state = 0;
			l = c;
		}
	}
	delete_list(&cres);
	#else
	
	#endif
	return ml;
}
void deleteModuleSymbolsList(const list pl) {
	module_symbol *p = pl.buffer;
	for (uint32_t i = 0; i < pl.length; i++) free((p++)->name);
	delete_list(&pl);
}

#endif
