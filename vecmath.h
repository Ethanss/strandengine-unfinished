﻿/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _VECMATH_H_
#define _VECMATH_H_

#ifndef __linux__
#define _USE_MATH_DEFINES
#endif
#include <math.h>
#include <stdbool.h>

typedef struct {
	float x,y;
} vec2;
typedef struct {
	float x,y,z;
} vec3;
typedef struct {
	float x,y,z,w;
} vec4;

static inline bool vec2_equals(vec2 *a, vec2 *b) {
	return a->x == b->x && a->y == b->y;
}
static inline bool vec3_equals(vec3 *a, vec3 *b) {
	return a->x == b->x && a->y == b->y && a->z == b->z;
}
static inline bool vec4_equals(vec4 *a, vec4 *b) {
	return a->x == b->x && a->y == b->y && a->z == b->z && a->w == b->w; 
}

static inline void vec2_add(vec2* a, vec2* b) {
	a->x += b->x;
	a->y += b->y;
}
static inline void vec2_addSingle(vec2* a, float b) {
	a->x += b;
	a->y += b;
}
static inline void vec3_add(vec3* a, vec3* b) {
	a->x += b->x;
	a->y += b->y;
	a->z += b->z;
}
static inline void vec3_addSingle(vec3* a, float b) {
	a->x += b;
	a->y += b;
	a->z += b;
}
static inline void vec4_add(vec4* a, vec4* b) {
	a->x += b->x;
	a->y += b->y;
	a->z += b->z;
	a->w += b->w;
}
static inline void vec4_addSingle(vec4* a, float b) {
	a->x += b;
	a->y += b;
	a->z += b;
	a->w += b;
}
static inline void vec2_sub(vec2* a, vec2* b) {
	a->x -= b->x;
	a->y -= b->y;
}
static inline void vec2_subSingle(vec2* a, float b) {
	a->x -= b;
	a->y -= b;
}
static inline void vec3_sub(vec3* a, vec3* b) {
	a->x -= b->x;
	a->y -= b->y;
	a->z -= b->z;
}
static inline void vec3_subSingle(vec3* a, float b) {
	a->x -= b;
	a->y -= b;
	a->z -= b;
}
static inline void vec4_sub(vec4* a, vec4* b) {
	a->x -= b->x;
	a->y -= b->y;
	a->z -= b->z;
	a->w -= b->w;
}
static inline void vec4_subSingle(vec4* a, float b) {
	a->x -= b;
	a->y -= b;
	a->z -= b;
	a->w -= b;
}
static inline void vec2_mul(vec2* a, vec2* b) {
	a->x *= b->x;
	a->y *= b->y;
}
static inline void vec2_mulSingle(vec2* a, float b) {
	a->x *= b;
	a->y *= b;
}
static inline void vec3_mul(vec3* a, vec3* b) {
	a->x *= b->x;
	a->y *= b->y;
	a->z *= b->z;
}
static inline void vec3_mulSingle(vec3* a, float b) {
	a->x *= b;
	a->y *= b;
	a->z *= b;
}
static inline void vec4_mul(vec4* a, vec4* b) {
	a->x *= b->x;
	a->y *= b->y;
	a->z *= b->z;
	a->w *= b->w;
}
static inline void vec4_mulSingle(vec4* a, float b) {
	a->x *= b;
	a->y *= b;
	a->z *= b;
	a->w *= b;
}
static inline void vec2_div(vec2* a, vec2* b) {
	a->x /= b->x;
	a->y /= b->y;
}
static inline void vec2_divSingle(vec2* a, float b) {
	a->x /= b;
	a->y /= b;
}
static inline void vec3_div(vec3* a, vec3* b) {
	a->x /= b->x;
	a->y /= b->y;
	a->z /= b->z;
}
static inline void vec3_divSingle(vec3* a, float b) {
	a->x /= b;
	a->y /= b;
	a->z /= b;
}
static inline void vec4_div(vec4* a, vec4* b) {
	a->x /= b->x;
	a->y /= b->y;
	a->z /= b->z;
	a->w /= b->w;
}
static inline void vec4_divSingle(vec4* a, float b) {
	a->x /= b;
	a->y /= b;
	a->z /= b;
	a->w /= b;
}

static inline float vec2_length(vec2* a) {
	vec2 t = *a;
	return sqrtf(t.x*t.x+t.y*t.y);
}
static inline float vec3_length(vec3* a) {
	vec3 t = *a;
	return sqrtf(t.x*t.x+t.y*t.y+t.z*t.z);
}
static inline float vec4_length(vec4* a) {
	vec4 t = *a;
	return sqrtf(t.x*t.x+t.y*t.y+t.z*t.z+t.w*t.w);
}

static inline void vec2_normalize(vec2* a) {
	vec2 t = *a;
	float sqlen = t.x*t.x+t.y*t.y;
	if (sqlen != 0.0f) {
		const float ls = 1.0f/sqrtf(sqlen);
		a->x = t.x*ls;
		a->y = t.y*ls;
	}
}
static inline void vec3_normalize(vec3* a) {
	vec3 t = *a;
	float sqlen = t.x*t.x+t.y*t.y+t.z*t.z;
	if (sqlen != 0.0f) {
		const float ls = 1.0f/sqrtf(sqlen);
		a->x = t.x*ls;
		a->y = t.y*ls;
		a->z = t.z*ls;
	}
}
static inline void vec4_normalize(vec4* a) {
	vec4 t = *a;
	float sqlen = vec4_length(a);
	if (sqlen != 0.0f) {
		const float ls = 1.0f/sqrtf(sqlen);
		a->x = t.x*ls;
		a->y = t.y*ls;
		a->z = t.z*ls;
		a->w = t.w*ls;
	}
}

static inline float vec2_dot(vec2* a, vec2* b) {
	return a->x*b->x + a->y*b->y;
}
static inline float vec3_dot(vec3* a, vec3* b) {
	return a->x*b->x + a->y*b->y + a->z*b->z;
}

static inline void vec2_cross(vec2 *ap) {
	const float sn = -sinf(M_PI*0.5f), cn = cosf(M_PI*0.5f);
	vec2 v = *ap;
	ap->x = v.x*cn - v.y*sn;
	ap->y = v.y*cn + v.x*sn;
}
static inline void vec3_cross(vec3* ap, vec3* bp) {
	vec3 a = *ap, b = *bp;
	ap->x = a.y*b.z-a.z*b.y;
	ap->y = a.z*b.x-a.x*b.z;
	ap->z = a.x*b.y-a.y*b.x;
}

static inline void vec2_reflect(vec2* a, vec2* n) {
	vec2 v = *a, d = *n;
	float s = (v.x*d.x+v.y*d.y)*2.0f;
	a->x = v.x-d.x*s;
	a->y = v.y-d.y*s;
}
static inline void vec3_reflect(vec3* a, vec3* n) {
	vec3 v = *a, d = *n;
	float s = (v.x*d.x+v.y*d.y+v.z*d.z)*2.0f;
	a->x = v.x-d.x*s;
	a->y = v.y-d.y*s;
	a->z = v.z-d.z*s;
}

static inline void vec2_tangent(vec2* a, vec2 *t) {
	t->y = a->x;
	t->x = -a->y;
}
void vec3_tangent(vec3* n, vec3* tang, vec3* bitang) {
	vec3 d = *n;
	float a = 1.0f/(1.0f + d.z), b = -d.x*d.y*a;
	tang->x = 1.0f-d.x*d.x*a;
	tang->y = b;
	tang->z = -d.x;
	bitang->x = b;
	bitang->y = 1.0f-d.y*d.y*a;
	bitang->z = -d.y;
}

void vec2_rot(vec2* p, float a) {
	if (a == 0.0f) return;
	const float sn = -sinf(a), cn = cosf(a);
	vec2 v = *p;
	p->x = v.x*cn - v.y*sn;
	p->y = v.y*cn + v.x*sn;
}
void vec2_rotInv(vec2* p, float a) {
	if (a == 0.0f) return;
	const float sn = sinf(a), cn = cosf(a);
	vec2 v = *p;
	p->x = v.x*cn - v.y*sn;
	p->y = v.y*cn + v.x*sn;
}
void vec3_rot(vec3 *p, vec3 *a) {
	//roll
	const float az = a->z;
	if (az != 0.0f) {
		const float sn = -sinf(az), cn = cosf(az), x = p->x, y = p->y;
		p->x = x*cn - y*sn;
		p->y = y*cn + x*sn;
	}
	
	//pitch
	const float ay = a->y;
	if (ay != 0.0f) {
		const float sn = -sinf(ay), cn = cosf(ay), x = p->y, y = p->z;
		p->y = x*cn - y*sn;
		p->z = y*cn + x*sn;
	}
	
	//yaw
	const float ax = a->x;
	if (ax != 0.0f) {
		const float sn = -sinf(ax), cn = cosf(ax), x = p->x, y = p->z;
		p->x = x*cn - y*sn;
		p->z = y*cn + x*sn;
	}
}
void vec3_rotInv(vec3 *p, vec3 *a) {
	//yaw
	const float ax = a->x;
	if (ax != 0.0f) {
		const float sn = sinf(ax), cn = cosf(ax), x = p->x, y = p->z;
		p->x = x*cn - y*sn;
		p->z = y*cn + x*sn;
	}
	
	//pitch
	const float ay = a->y;
	if (ay != 0.0f) {
		const float sn = sinf(ay), cn = cosf(ay), x = p->y, y = p->z;
		p->y = x*cn - y*sn;
		p->z = y*cn + x*sn;
	}
	
	//roll
	const float az = a->z;
	if (az != 0.0f) {
		const float sn = sinf(az), cn = cosf(az), x = p->x, y = p->y;
		p->x = x*cn - y*sn;
		p->y = y*cn + x*sn;
	}
}

static inline void vec3_transform(vec3 *p, vec3 *pos, vec3 *rot, vec3 *scale) {
	vec3_rot(p,rot);
	vec3_mul(p,scale);
	vec3_add(p,pos);
}
static inline void vec3_transformInv(vec3 *p, vec3 *pos, vec3 *rot, vec3 *scale) {
	vec3_sub(p,pos);
	vec3_div(p,scale);
	vec3_rotInv(p,rot);
}
static inline void vec3_pointToScreen(vec3 *sp, vec3 *spos, vec3 *srot, const float aspect, const float fovTang) {
	vec3_sub(sp, spos);
	vec3_rotInv(sp, srot);
	if (sp->z > 1e-4f) {
		vec2_mulSingle((vec2*)sp, fovTang / sp->z);
		sp->x *= aspect;
	}
}

#define todegrees(v) ((v)*57.2957)
#define toradians(v) ((v)*0.017453293)

#define vec2_angle(p) atan2((p)->y,(p)->x)
static inline void vec3_angle(vec3 *p, vec2 *r) {
	float x = p->x, z = p->z;
	r->x = atan2(z,x);
	r->y = atan2(sqrtf(x*x+z*z),p->y);
}

static inline float angleDelta(float a1, float a2) {
	float d = a2-a1;
	if (d > M_PI) d = -((M_PI+a1)+(M_PI-a2));
	else if (d < -M_PI) d = (M_PI-a1)+(M_PI+a2);
	return d;
}

static inline float smoothMax(const float v1, const float v2, const float smooth) {
	return logf(expf(v1*smooth)+expf(v2*smooth))/smooth;
}
static inline float smoothMin(const float v1, const float v2, const float smooth) {
	return logf(expf(v1*-smooth)+expf(v2*-smooth))/-smooth;
}


//modulo that works with negative, repeating consistently when crossing the 0 sign boundary
static inline float modulof(float v, float m) {
	if (v < 0.0f) return fmod(m+v,m);
	return fmod(v,m);
}
static inline int moduloi(int v, int m) {
	if (v < 0) return (m+v)%m;
	return v%m;
}

static inline float lerp(const float src, const float targ, const float t) {
	return (targ-src)*t+src;
}
static inline void vec2_lerp(vec2 *src, vec2 *targ, float t, vec2 *o) {
	o->x = lerp(src->x,targ->x,t); o->y = lerp(src->y,targ->y,t);
}
static inline void vec3_lerp(vec3 *src, vec3 *targ, float t, vec3 *o) {
	o->x = lerp(src->x,targ->x,t); o->y = lerp(src->y,targ->y,t); o->z = lerp(src->z,targ->z,t);
}
static inline void vec4_lerp(vec4 *src, vec4 *targ, float t, vec4 *o) {
	o->x = lerp(src->x,targ->x,t); o->y = lerp(src->y,targ->y,t); o->z = lerp(src->z,targ->z,t); o->w = lerp(src->w,targ->w,t);
}


void vec3_hsvToRGB(vec3 *hsv, vec3 *rgb) {
	//hue, color rotation/circle
	float h = hsv->x*6.0f;
	#define hcr(c,o) rgb->c = fmaxf(0.0f,fminf(1.0f,(fabs(fmod(h o,6.0f) - 3.0f) - 1.0f)))
	hcr(x,); hcr(y,+4.0f); hcr(z,+2.0f);
	#undef hcr

	//saturation, color/white blend
	#define scb(c) rgb->c = lerp(1.0f,rgb->c,hsv->y)
	scb(x); scb(y); scb(z);
	#undef scb

	//value/brightness, multiply
	#define bm(c) rgb->c *= hsv->z
	bm(x); bm(y); bm(z);
	#undef bm
}

void vec3_rgbToHSV(vec3 *rgb, vec3 *hsv) {
	float vmax = fmaxf(rgb->x, fmaxf(rgb->y, rgb->z)),
		  vmin = fminf(rgb->x, fminf(rgb->y, rgb->z));
	//value(max)
	hsv->z = vmax;
	//saturation, max/min difference
	hsv->y = vmax-vmin;

	//hue, invert rotation
	if (rgb->x > rgb->y) {
		if (rgb->x > rgb->z) {
			hsv->x = modulof(rgb->y-rgb->z,6.0f)/6.0f;
		} else {
			hsv->x = (rgb->x-rgb->y+4.0f)/6.0f;
		}
	} else {
		if (rgb->y > rgb->z) {
			hsv->x = (rgb->z-rgb->x+2.0f)/6.0f;
		} else {
			hsv->x = (rgb->x-rgb->y+4.0f)/6.0f;
		}
	}
}

//rectangle vec4 x,y = center, z,w = extents
static inline vec4 vec4_rectangleCombine(const vec4 a, const vec4 b) {
	vec4 ae = (vec4){a.x-a.z,a.x+a.z,a.y-a.w,a.y+a.w},
		be = (vec4){b.x-b.z,b.x+b.z,b.y-b.w,b.y+b.w};
	ae = (vec4){fmaxf(ae.x,be.x), fminf(ae.y,be.y), fmaxf(ae.z,be.z), fminf(ae.w,be.w)};
	return (vec4){(ae.x+ae.y)*0.5f,(ae.z+ae.w)*0.5f,(ae.y-ae.x)*0.5f,(ae.w-ae.z)*0.5f};
}

float fovAngleToTangent(const float fov) {
	return tanf(toradians(90.0f-fov*0.5f));
}
float fovTangentToAngle(const float tang) {
	return todegrees(atanf(1.0f/tang))*2.0f;
}


//complex number operations
void complex_inverse(vec2 *a) {
	vec2 ta = *a;
	float sl = ta.x*ta.x+ta.y*ta.y;
	a->x = ta.x/sl;
	a->y = -ta.y/sl;
}
void complex_mul(vec2 *a, vec2 *b) {
	vec2 ta = *a, tb = *b;
	a->x = ta.x*tb.x-ta.y*tb.y;
	a->y = ta.y*tb.x+ta.x*tb.y;
}
void complex_div(vec2 *a, vec2 *b) {
	vec2 ta = *a, tb = *b;
	float sl = tb.x*tb.x+tb.y*tb.y;
	a->x = (ta.x*tb.x+ta.y*tb.y)/sl;
	a->y = (ta.y*tb.x-ta.x*tb.y)/sl;
}
void complex_exp(vec2 *a) {
	vec2 ta = *a;
	float ev = expf(ta.x);
	a->x = cos(ta.y)*ev;
	a->y = sin(ta.y)*ev;
}
void complex_log(vec2 *a) {
	vec2 ta = *a;
	a->x = logf(ta.x*ta.x+ta.y*ta.y)*0.5f;
	a->y = atan2f(ta.y,ta.x);
}
void complex_pow(vec2 *a, vec2 *ex) {
	complex_log(a);
	complex_mul(ex,a);
	complex_exp(a);
}

//quaternion operations
void quaternion_fromAngleAxis(vec4 *q, vec3 *axis, float angle) {
	angle *= 0.5f;
	q->w = cosf(angle);
	const float sfa = sinf(angle);
	q->x = axis->x*sfa;q->y = axis->y*sfa;q->z = axis->z*sfa;
}
void quaternion_fromEuler(vec4 *q, vec3 *euler) {//euler angle in format yaw(x)-pitch(y)-roll(z)
    float ex = euler->x*0.5f, ey = euler->y*0.5f, ez = euler->z*0.5f,
    sx = sinf(ex), sy = sinf(ey), sz = sinf(ez),
    cx = cosf(ex), cy = cosf(ey), cz = cosf(ez);
    q->x = cx*sy*cz+sx*cy*sz;
	q->y = cx*sy*sz-sx*cy*cz;
	q->z = cx*cy*sz-sx*sy*cz;
	q->w = cx*cy*cz+sx*sy*sz;
}

void quaternion_toAngleAxis(vec4 *q, vec4 *a) {
	a->w = acosf(q->w)*2.0f;
	float sfa = sinf(a->w);
	if (sfa == 0.0f) sfa = 1e10f;
	else sfa = 1.0f/sfa;
	a->x = q->x*sfa;a->y = q->y*sfa;a->z = q->z*sfa;
}
void quaternion_toEuler(vec4 *q, vec3 *euler) {
	vec4 l = (vec4){q->z,-q->x,-q->y,-q->w};
	
	float s1 = 2.0f*(l.w*l.x+l.y*l.z),
		  s2 = 1.0f-2.0f*(l.x*l.x+l.y*l.y),
		  s3 = 2.0f*(l.w*l.y-l.z*l.x);
	euler->z = -atan2f(s1,s2);
	
	if (fabs(s3) >= 0.99999f) euler->y = copysign(M_PI*0.5f,s3);
	else euler->y = asinf(s3);
	
	float c1 = 2.0f*(l.w*l.z+l.x*l.y),
	c2 = 1.0f-2.0f*(l.y*l.y+l.z*l.z);
	euler->x = -atan2f(c1,c2);
}

#define quaternion_normalize(q) vec4_normalize(q)
static inline void quaternion_conjugate(vec4 *q) {
	q->x = -q->x;
	q->y = -q->y;
	q->z = -q->z;
}
#define quaternion_invert(q) quaternion_conjugate(q)
static inline void quaternion_mul(vec4 *q1, vec4 *q2) {
	vec4 a = *q1, b = *q2;
	q1->x = a.y*b.z-a.z*b.y+a.x*b.w+a.w*b.x;
	q1->y = a.z*b.x-a.x*b.z+a.y*b.w+a.w*b.y;
	q1->z = a.x*b.y-a.y*b.x+a.z*b.w+a.w*b.z;
	q1->w = a.w*b.w-a.x*b.x-a.y*b.y-a.z*b.z;
}
void quaternion_rotatePoint(vec4 *q, vec3 *p) {
	vec4 qp = (vec4){p->x,p->y,p->z,0.0f},
	tq = (vec4){-q->x,-q->y,-q->z,q->w};
	quaternion_mul(&qp,q);
	quaternion_mul(&tq,&qp);
	p->x = tq.x;p->y = tq.y;p->z = tq.z;
}
#endif
