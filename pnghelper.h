/*Copyright 2018 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _PNGHELPER_H_
#define _PNGHELPER_H_

#include <png.h>

//load png image into byte array from FILE pointer, only supports 8-bitdepth
void *load_png(FILE *f, uint32_t *dim, bool verticalFlip) {
	uint8_t header[8];
	fread(header, 1, 8, f);
	if (png_sig_cmp(header, 0, 8)) return NULL;

	png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	png_infop info_ptr = png_create_info_struct(png_ptr);
	setjmp(png_jmpbuf(png_ptr));
	png_init_io(png_ptr, f);
	png_set_sig_bytes(png_ptr, 8);

	png_read_info(png_ptr, info_ptr);
	if (info_ptr->bit_depth != 8) {
		png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
		return NULL;
	}
	uint32_t width = info_ptr->width, height = info_ptr->height, rowbytes;
	dim[0] = width;
	dim[1] = height;
	dim[2] = info_ptr->channels;

	rowbytes = width*dim[2];
	void *pixelData = malloc(rowbytes*height), **pixelRows = malloc(sizeof(void*)*height), **prb = pixelRows;
	uint8_t *pdb = pixelData;
	if (verticalFlip) {
		pdb += height*rowbytes;
		for (uint32_t i = 0; i < height; i++) {
			pdb -= rowbytes;
			*prb++ = pdb;
		}
	}
	else {
		for (uint32_t i = 0; i < height; i++) {
			*prb++ = pdb;
			pdb += rowbytes;
		}
	}
	png_read_image(png_ptr, (png_bytep*)pixelRows);

	png_read_end(png_ptr, NULL);
	png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
	free(pixelRows);
	return pixelData;
}

void save_png(FILE *f, uint8_t *dat, uint32_t *dim, void **rp, bool verticalFlip) {
	png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	png_infop info_ptr = png_create_info_struct(png_ptr);	png_init_io(png_ptr, f);

	png_set_IHDR(png_ptr, info_ptr, dim[0], dim[1],
		8, dim[2] == 4 ? PNG_COLOR_TYPE_RGBA : (dim[2] == 3 ? PNG_COLOR_TYPE_RGB : PNG_COLOR_TYPE_GRAY), PNG_INTERLACE_NONE,
		PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);

	png_write_info(png_ptr, info_ptr);

	uint32_t height = dim[1], stride = dim[0] * dim[2];
	bool allocrp = false;
	if (rp == NULL) {
		rp = malloc(height*sizeof(void*));
		allocrp = true;
	}
	void **sp = rp;
	uint8_t* dp = dat;
	if (verticalFlip) {
		dp += height*stride;
		for (uint32_t i = 0; i < height; i++) {
			dp -= stride;
			*sp++ = dp;
		}
	}
	else {
		for (uint32_t i = 0; i < height; i++) {
			*sp++ = dp;
			dp += stride;
		}
	}
	png_write_image(png_ptr, rp);
	png_write_end(png_ptr, NULL);
	if (allocrp) free(rp);
}
#endif
