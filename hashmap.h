﻿/*Copyright 2019 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _HASHMAP_H_
#define _HASHMAP_H_

#include "misc.h"
#include <stdlib.h>

/*
list vs hashmap pros/cons,
-searching(indexOf) for a hash in a hashmap is faster than in a list.
-hashmap has slower enumeration than list.
-hashmap has slower add/removal of objects than lists.
*/

//32-bit unsigned int hash map
typedef struct {
	void *buffer;
	uint32_t bufferSize, valueSize, length, padding;
} hashmap;

#define construct_hashmap(vsz,pad) (hashmap){NULL,0,vsz,0,pad}
static inline void new_hashmap(hashmap *h, const uint32_t vsz, const uint32_t pad) {
	h->valueSize = vsz;
	h->bufferSize = 0;
	h->length = 0;
	h->padding = pad;
}
static inline void delete_hashmap(hashmap *h) {
	if (h->bufferSize != 0) free(h->buffer);
}
#define hashmap_values(h) ((uint8_t*)(h)->buffer+(h)->bufferSize*4)

//get index of hash starting search from index 'bi', returns UINT_MAX if no match found
uint32_t hashmap_indexOfI(hashmap *h, const uint32_t hv, const uint32_t bi) {
	uint32_t bsz = h->bufferSize;
	if (bsz == 0) return UINT_MAX;
	uint32_t *hptr = h->buffer, hi = (hv+bi)%bsz, isz = bsz-hi;
	hptr += hi;
	for (uint32_t k = 0; k < isz; k++) {
		const uint32_t tv = *hptr++;
		if (tv == NULL_HASH) return UINT_MAX;
		if (tv == hv) return hi+k;
	}
	isz = hi;
	hi = 0;
	hptr = h->buffer;
	for (uint32_t k = 0; k < isz; k++) {
		const uint32_t tv = *hptr++;
		if (tv == NULL_HASH) break;
		if (tv == hv) return hi+k;
	}
	return UINT_MAX;
}
#define hashmap_indexOf(h,hv) hashmap_indexOfI(h,hv,0)

//get index of next empty map slot starting search from index 'bi', returns UINT_MAX if no match found
uint32_t hashmap_indexOfEmpty(hashmap *h, const uint32_t bi) {
	uint32_t bsz = h->bufferSize;
	if (bsz == 0) return UINT_MAX;
	uint32_t *hptr = h->buffer, hi = bi % bsz, isz = bsz - hi;
	hptr += hi;
	for (uint32_t k = 0; k < isz; k++) {
		const uint32_t tv = *hptr++;
		if (tv == NULL_HASH) return hi+k;
	}
	isz = hi;
	hi = 0;
	hptr = h->buffer;
	for (uint32_t k = 0; k < isz; k++) {
		const uint32_t tv = *hptr++;
		if (tv == NULL_HASH) return hi+k;
	}
	return UINT_MAX;
}

//put new key/value, expanding buffer if needed
void hashmap_add(hashmap *h, const uint32_t hv, const void *o) {
	//allocate space for new key
	h->length++;
	if (h->length*h->padding >= h->bufferSize) {
		if (h->bufferSize == 0) {
			//alloc new buffer
			h->buffer = malloc(h->valueSize+4*h->padding);
			h->bufferSize = h->padding;
			memset(h->buffer, NULL_HASH, h->bufferSize * 4);
		} else {
			//expand buffer
			void *buf = h->buffer;
			uint32_t obsz = h->bufferSize;
			uint32_t bsz = obsz*8*h->padding, vsz = h->valueSize;
			h->bufferSize = bsz;
			h->buffer = malloc(bsz*(vsz+4));
			uint32_t *oh = buf, *hp = h->buffer;
			memset(hp,NULL_HASH,bsz*4);
			//re-index now that buffer size is different
			uint8_t *ov = (void*)(oh+obsz), *vp = (void*)(hp+bsz);
			for (uint32_t i = 0; i < obsz; i++) {
				uint32_t hv = *oh++;
				if (hv != NULL_HASH) {
					uint32_t ni = hashmap_indexOfEmpty(h,hv);
					hp[ni] = hv;
					memcpy(vp+ni*vsz,ov+i*vsz,vsz);
				}
			}
			free(buf);
		}
	}

	//find new open slot
	uint32_t ind = hashmap_indexOfEmpty(h,hv);
	//set key/value
	uint8_t *bp = h->buffer;
	((uint32_t*)bp)[ind] = hv;
	memcpy(bp+h->bufferSize*4+h->valueSize*ind,o,h->valueSize);
}

//remove key/value at index
void hashmap_removeAt(hashmap *h, const uint32_t index) {
	uint32_t *hptr = (uint32_t*)h->buffer+index;
	*hptr++ = NULL_HASH;
	h->length--;
	//search for hash to reindex
	const uint32_t bsz = h->bufferSize, vsz = h->valueSize;
	uint32_t i, isz = bsz - index - 1;
	for (i = 0; i < isz; i++) {
		const uint32_t hv = *hptr++;
		if (hv == NULL_HASH) return;
		if (hv%bsz == index) break;
	}
	if (i == isz) {
		hptr = h->buffer;
		isz = index;
		for (i = 0; i < isz; i++) {
			const uint32_t hv = *hptr++;
			if (hv == NULL_HASH) return;
			if (hv%bsz == index) break;
		}
	}
	if (i < isz) {
		//found reindex candidate, do reindexing
		uint32_t *bptr = h->buffer;
		uint8_t *vptr = bptr + bsz;
		bptr[index] = hptr[-1];
		hptr[-1] = NULL_HASH;
		memcpy(vptr + vsz*index, vptr + vsz*((i + index) % bsz), vsz);
		if (isz == bsz-index-1) {
			for (uint32_t si = index + i; si < bsz; si++) {
				const uint32_t hv = *hptr++, bi = hv%bsz;
				if (si != bi) {
					uint32_t eind = hashmap_indexOfEmpty(h, bi);
					bptr[eind] = hv;
					hptr[-1] = NULL_HASH;
					memcpy(vptr + vsz*eind, vptr + vsz*si, vsz);
				}
			}
			hptr = h->buffer;
			isz = index;
			i = 0;
		}
		for (uint32_t si = i; si < isz; si++) {
			const uint32_t hv = *hptr++, bi = hv%bsz;
			if (si != bi) {
				uint32_t eind = hashmap_indexOfEmpty(h, bi);
				bptr[eind] = hv;
				hptr[-1] = NULL_HASH;
				memcpy(vptr + vsz*eind, vptr + vsz*si, vsz);
			}
		}
	}
}
//remove key/value
static inline void hashmap_remove(hashmap *h, const uint32_t hv) {
	uint32_t index = hashmap_indexOf(h,hv);
	if (index == UINT_MAX) return;
	hashmap_removeAt(h, index);
}

//get value ptr from key
void *hashmap_get(hashmap *h, const uint32_t hv) {
	uint32_t index = hashmap_indexOf(h,hv);
	if (index == UINT_MAX) return NULL;
	return (uint8_t*)h->buffer+h->bufferSize*4+index*h->valueSize;
}

//enumerate all hashmap entries, outputs next entry after index 'i' to 'hv' and value ptr to 'o'. when 'hv' == NULL_HASH there are no entries left
void hashmap_enumerateEntries(hashmap *h, uint32_t *i, uint32_t *hv, void **o) {
	uint32_t *bp = h->buffer, bsz = h->bufferSize, k = *i;
	for (bp += k; k < bsz; k++) {
		if (*bp != NULL_HASH) {
			*hv = *bp;
			*o = (uint8_t*)h->buffer+bsz*4+k*h->valueSize;
			*i = k+1;
			return;
		}
		bp++;
	}
	*hv = NULL_HASH;
}

#ifdef _STREAM_H_
void hashmap_serialize(hashmap *h, two_arg_function writeObject, stream *s) {
	stream_writeUniversal(s, &h->bufferSize, 4, 16);
	uint32_t *hptr = h->buffer, hlen = h->bufferSize, vsz = h->valueSize;
	uint8_t *vptr = hptr + hlen;
	for (uint32_t i = 0; i < hlen; i++) {
		const uint32_t hv = *hptr++;
		stream_writeUniversal(s, &hv, 4, 4);
		if (hv == NULL_HASH || writeObject == NULL) s->setPosition(s, s->position + vsz);
		else writeObject(s, vptr + i*vsz);
	}
}
void hashmap_deserialize(hashmap *h, two_arg_function readObject, stream *s) {
	stream_readUniversal(s, &h->bufferSize, 4, 16);
	uint32_t hlen = h->bufferSize, vsz = h->valueSize;
	h->buffer = malloc(h->bufferSize*(4 + vsz));
	uint32_t *hptr = h->buffer, hv;
	uint8_t *vptr = hptr + hlen;
	for (uint32_t i = 0; i < hlen; i++) {
		stream_readUniversal(s, &hv, 4, 4);
		*hptr++ = hv;
		if (hv == NULL_HASH || readObject == NULL) s->setPosition(s, s->position + vsz);
		else readObject(s, vptr + i*vsz);
	}
}
#endif

#endif
