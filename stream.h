﻿/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _STREAM_H_
#define _STREAM_H_

#include "misc.h"
#include "system.h"
#include "reflection.h"
#include <stdio.h>

#ifndef __linux__
#include <io.h>
#define fseeko _fseeki64
#define ftello _ftelli64
#else
#include <unistd.h>
#include <fcntl.h>
#endif

#ifndef STREAM_MAX_STACK_ALLOC
#define STREAM_MAX_STACK_ALLOC 4096
#endif

//stream_read/writeUniversal functions will convert elements to this endianness before writing and convert back to host endianness when reading.
#ifndef STREAM_UNIVERSAL_ENDIANNESS
#define STREAM_UNIVERSAL_ENDIANNESS LITTLEENDIAN
#endif

//stream RW function returns number of bytes read/written, arg0 = stream, arg1 = dst/src and arg2 = size in bytes
typedef uint32_t (*streamRWFunction)(void*, void*, const uint32_t);
typedef void (*streamSetPositionFunction)(void*, const uint64_t);
typedef void (*streamCloseFunction)(void*);


//abstract stream
typedef struct {
	void *handle;
	uint64_t position, length;
	streamRWFunction read, write;
	streamSetPositionFunction setPosition;
	streamCloseFunction close;
} stream, memory_stream, file_stream, console_stream;

#define stream_read(s,d,sz) (s)->read(s,d,sz)
#define stream_write(s,d,sz) (s)->write(s,d,sz)
#define stream_setPosition(s,i) (s)->setPosition(s,i)
#define stream_close(s) (s)->close(s)

uint32_t stream_readUniversal(void *p, void *d, const uint32_t elementSz, const uint32_t sz) {
	stream *s = p;
	uint32_t ra = s->read(s, d, sz);
	if (ENDIANNESS == STREAM_UNIVERSAL_ENDIANNESS) {
		return ra;
	} else {
		uint8_t *p = (uint8_t*)d;
		uint32_t tr = 0;
		while (tr < sz) {
			reverse(p,elementSz);
			p += elementSz;
			tr += elementSz;
		}
		return ra;
	}
}

uint32_t stream_writeUniversal(void *p, void *d, const uint32_t elementSz, const uint32_t sz) {
	stream *s = p;
	if (ENDIANNESS == STREAM_UNIVERSAL_ENDIANNESS) {
		return s->write(s,d,sz);
	} else {
		uint32_t segs = sz/elementSz;
		uint8_t *b = d;
		for (uint32_t i = 0; i < segs; i++) {
			reverse(b,4);
			b += 4;
		}
		uint32_t tr = s->write(s,b,sz);
		b = d;
		for (uint32_t i = 0; i < segs; i++) {
			reverse(b,4);
			b += 4;
		}
		return tr;
	}
}

//automatic serialization using reflection
void stream_serialize(void *p, uint8_t *src, reflection_data *rd, const uint16_t fc) {
	stream *s = p;
	if (ENDIANNESS == STREAM_UNIVERSAL_ENDIANNESS) {
		s->write(s,(void*)&fc,2);
		for (uint16_t i = 0; i < fc; i++) {
			uint8_t type = rd->type;
			s->write(s, &type, 1);
			s->write(s, &rd->nameLength, 1);
			s->write(s, rd->name, rd->nameLength);
			if (type < REFLECTION_TYPE_list) {
				s->write(s, src + rd->offset, REFLECTION_TYPE_SIZE[type]);
			}
			else {
				fixed_array *lst = (void*)(src + rd->offset);
				s->write(s, &lst->length, 4);
				s->write(s, &lst->stride, 4);
				s->write(s, lst->buffer, lst->length*lst->stride);
			}
			rd++;
		}
	} else {
		uint8_t tbuf[8];
		*(uint16_t*)tbuf = fc;
		reverse(tbuf,2);
		s->write(s,tbuf,2);
		for (uint16_t i = 0; i < fc; i++) {
			uint8_t type = rd->type;
			s->write(s, &type, 1);
			s->write(s, &rd->nameLength, 1);
			s->write(s, rd->name, rd->nameLength);
			if (type < REFLECTION_TYPE_list) {
				uint8_t typeSz = REFLECTION_TYPE_SIZE[type];
				if (typeSz > 1) {
					memcpy(tbuf, src + rd->offset, typeSz);
					reverse(tbuf, typeSz);
					s->write(s, tbuf, typeSz);
				}
				else {
					s->write(s, src + rd->offset, typeSz);
				}
			}
			else {
				fixed_array *lst = (void*)(src + rd->offset);
				*(uint32_t*)tbuf = lst->length;
				reverse(tbuf, 4);
				s->write(s, tbuf, 4);

				uint32_t stride = lst->stride;
				*(uint32_t*)tbuf = stride;
				reverse(tbuf, 4);
				s->write(s, tbuf, 4);

				if (stride > 1) {
					uint32_t nl = lst->length;
					uint8_t *lb = lst->buffer;
					for (uint32_t k = 0; k < nl; k++) {
						memcpy(tbuf, lb, stride);
						reverse(tbuf, stride);
						s->write(s, tbuf, stride);
						lb += stride;
					}
				}
				else {
					s->write(s, lst->buffer, lst->length*stride);
				}
			}

			rd++;
		}
	}
}
void stream_deserialize(void *p, uint8_t *dst, reflection_data *rd, const uint16_t fc) {
	stream *s = p;
	uint8_t nbuf[256], typeNameLen[2];
	uint16_t sfc;
	s->read(s,&sfc,2);
	if (ENDIANNESS == STREAM_UNIVERSAL_ENDIANNESS) {
		for (uint16_t i = 0; i < sfc; i++) {
			s->read(s,typeNameLen,2);
			s->read(s,nbuf,typeNameLen[1]);
			reflection_data *field = reflection_findField(rd,fc,(void*)nbuf,typeNameLen[1],typeNameLen[0]);
			if (field != NULL) {
				if (typeNameLen[0] < REFLECTION_TYPE_list) {
					s->read(s,dst+field->offset,REFLECTION_TYPE_SIZE[typeNameLen[0]]);
				} else {
					list *lst = (void*)(dst+field->offset);
					s->read(s,&lst->length,4);
					s->read(s,&lst->stride,4);
					if (typeNameLen[0] == REFLECTION_TYPE_list) {
						lst->bufferSize = 0;
						list_expand(lst);
					} else {
						lst->buffer = malloc(lst->length*lst->stride);
					}
					s->read(s,lst->buffer,lst->length*lst->stride);
				}
			}
		}
	} else {
		reverse(&sfc,2);
		for (uint16_t i = 0; i < sfc; i++) {
			s->read(s,typeNameLen,2);
			s->read(s,nbuf,typeNameLen[1]);
			reflection_data *field = reflection_findField(rd,fc,(void*)nbuf,typeNameLen[1],typeNameLen[0]);
			if (field != NULL) {
				if (typeNameLen[0] < REFLECTION_TYPE_list) {
					uint8_t *wd = dst+field->offset;
					uint16_t typeSz = REFLECTION_TYPE_SIZE[typeNameLen[0]];
					s->read(s,wd,typeSz);
					if (typeSz > 1) reverse(wd,typeSz);
				} else {
					list *lst = (void*)(dst+field->offset);
					s->read(s,&lst->length,4);
					reverse(&lst->length,4);
					s->read(s,&lst->stride,4);
					reverse(&lst->stride,4);
					if (typeNameLen[0] == REFLECTION_TYPE_list) {
						lst->bufferSize = 0;
						list_expand(lst);
					} else {
						lst->buffer = malloc(lst->length*lst->stride);
					}

					if (lst->stride > 1) {
						uint32_t nl = lst->length,
						stride = lst->stride;
						uint8_t *lb = lst->buffer;
						s->read(s,lb,nl*stride);

						for (uint32_t k = 0; k < nl; k++) {
							reverse(lb,stride);
							lb += stride;
						}
					} else {
						s->read(s,lst->buffer,lst->length*lst->stride);
					}
				}
			}
		}
	}
}

//serialization on fixed/non-changing structure, faster and more optimal then regular serialization
void stream_serializeFixed(void *p, uint8_t *src, reflection_data *rd, const uint16_t fc) {
	stream *s = p;
	if (ENDIANNESS == STREAM_UNIVERSAL_ENDIANNESS) {
		for (uint16_t i = 0; i < fc; i++) {
			uint8_t type = rd->type;
			if (type < REFLECTION_TYPE_list) {
				s->write(s,src+rd->offset,REFLECTION_TYPE_SIZE[type]);
			} else {
				fixed_array *lst = (void*)(src+rd->offset);
				s->write(s,&lst->length,4);
				s->write(s,&lst->stride,4);
				s->write(s,lst->buffer,lst->length*lst->stride);
			}
			rd++;
		}
	} else {
		uint8_t tbuf[8];
		for (uint16_t i = 0; i < fc; i++) {
			uint8_t type = rd->type;
			if (type < REFLECTION_TYPE_list) {
				uint8_t typeSz = REFLECTION_TYPE_SIZE[type];
				if (typeSz > 1) {
					memcpy(tbuf,src+rd->offset,typeSz);
					reverse(tbuf,typeSz);
					s->write(s,tbuf,typeSz);
				} else {
					s->write(s,src+rd->offset,typeSz);
				}
			} else {
				fixed_array *lst = (void*)(src+rd->offset);
				*(uint32_t*)tbuf = lst->length;
				reverse(tbuf,4);
				s->write(s,tbuf,4);

				uint32_t stride = lst->stride;
				*(uint32_t*)tbuf = stride;
				reverse(tbuf,4);
				s->write(s,tbuf,4);

				if (stride > 1) {
					uint32_t nl = lst->length;
					uint8_t *lb = lst->buffer;
					for (uint32_t k = 0; k < nl; k++) {
						memcpy(tbuf,lb,stride);
						reverse(tbuf,stride);
						s->write(s,tbuf,stride);
						lb += stride;
					}
				} else {
					s->write(s,lst->buffer, lst->length*stride);
				}
			}
			rd++;
		}
	}
}
void stream_deserializeFixed(void *p, uint8_t *dst, reflection_data *rd, const uint16_t fc) {
	stream *s = p;
	if (ENDIANNESS == STREAM_UNIVERSAL_ENDIANNESS) {
		for (uint16_t i = 0; i < fc; i++) {
			uint8_t type = rd->type;
			if (type < REFLECTION_TYPE_list) {
				s->read(s,dst+rd->offset,REFLECTION_TYPE_SIZE[type]);
			} else {
				list *lst = (void*)(dst+rd->offset);
				s->read(s,&lst->length,4);
				s->read(s,&lst->stride,4);
				if (type == REFLECTION_TYPE_list) {
					lst->bufferSize = 0;
					list_expand(lst);
				} else {
					lst->buffer = malloc(lst->length*lst->stride);
				}
				s->read(s,lst->buffer,lst->length*lst->stride);
			}
			rd++;
		}
	} else {
		for (uint16_t i = 0; i < fc; i++) {
			uint8_t type = rd->type;
			if (type < REFLECTION_TYPE_list) {
				uint8_t *wd = dst+rd->offset;
				uint16_t typeSz = REFLECTION_TYPE_SIZE[type];
				s->read(s,wd,typeSz);
				if (typeSz > 1) reverse(wd,typeSz);
			} else {
				list *lst = (void*)(dst+rd->offset);
				s->read(s,&lst->length,4);
				reverse(&lst->length,4);
				s->read(s,&lst->stride,4);
				reverse(&lst->stride,4);
				if (type == REFLECTION_TYPE_list) {
					lst->bufferSize = 0;
					list_expand(lst);
				} else {
					lst->buffer = malloc(lst->length*lst->stride);
				}

				if (lst->stride > 1) {
					uint32_t nl = lst->length,
					stride = lst->stride;
					uint8_t *lb = lst->buffer;
					s->read(s,lb,nl*stride);

					for (uint32_t k = 0; k < nl; k++) {
						reverse(lb,stride);
						lb += stride;
					}
				} else {
					s->read(s,lst->buffer,lst->length*lst->stride);
				}
			}
			rd++;
		}
	}
}

void stream_copy(stream *dst, stream *src, uint64_t sz) {
	uint32_t bufSz = STREAM_MAX_STACK_ALLOC;
	if (sz < bufSz) bufSz = sz;
	uint8_t *buf = alloca(bufSz);

	while (sz != 0) {
		uint32_t rwsz = src->read(src,buf,sz<bufSz?sz:bufSz);
		dst->write(dst,buf,rwsz);
		sz -= rwsz;
	}
}
void stream_move(stream *s, uint64_t srcPos, uint64_t dstPos, uint64_t sz) {
	uint32_t bufSz = STREAM_MAX_STACK_ALLOC;
	if (sz < bufSz) bufSz = sz;
	uint8_t *buf = alloca(bufSz);

	while (sz != 0) {
		s->setPosition(s, srcPos);
		uint32_t rwsz = s->read(s, buf, sz<bufSz ? sz : bufSz);
		srcPos += rwsz;
		s->setPosition(s, dstPos);
		s->write(s, buf, rwsz);
		dstPos += rwsz;
		sz -= rwsz;
	}
}

//memory stream read/write/seek/close functions
void memory_stream_setPosition(void *p, const uint64_t i) {
	((stream*)p)->position = i;
}

void memory_stream_close(void *p) {
	free(((stream*)p)->handle);
}

uint32_t memory_stream_read(void *p, void *dst, const uint32_t sz) {
	stream *s = (stream*)p;

	uint32_t readSz = s->length-s->position;
	if (readSz > sz) readSz = sz;

	memcpy(dst, (uint8_t*)s->handle + s->position, readSz);
	s->position += readSz;
	return readSz;
}

uint32_t memory_stream_write(void *p, void *src, uint32_t sz) {
	stream *s = (stream*)p;

	uint32_t writeSz = s->length-s->position;
	if (writeSz > sz) writeSz = sz;

	memcpy((uint8_t*)s->handle + s->position, src, writeSz);
	s->position += writeSz;
	return writeSz;
}

//create memory stream with new buferr of size 'sz' in bytes
void new_memory_stream(memory_stream *s, const uint64_t sz) {
	s->handle = malloc(sz);
	s->position = 0;
	s->length = sz;

	s->read = &memory_stream_read;
	s->write = &memory_stream_write;
	s->setPosition = &memory_stream_setPosition;
	s->close = &memory_stream_close;
}
//set memory stream to existing buffer
void set_memory_stream(memory_stream *s, void* b, const uint64_t sz) {
	s->handle = b;
	s->position = 0;
	s->length = sz;

	s->read = &memory_stream_read;
	s->write = &memory_stream_write;
	s->setPosition = &memory_stream_setPosition;
	s->close = &memory_stream_close;
}




//file stream read/write/seek/close functions
void file_stream_setPosition(void *p, const uint64_t i) {
	stream *s = (stream*)p;
	fseeko(s->handle, i, SEEK_SET);
	s->position = i;
}

void file_stream_close(void *p) {
	fclose(((stream*)p)->handle);
}

uint32_t file_stream_read(void *p, void *dst, const uint32_t sz) {
	stream *s = (stream*)p;

	uint32_t readSz = s->length-s->position;
	if (readSz > sz) readSz = sz;

	readSz = fread(dst,1,readSz,s->handle);
	s->position += readSz;
	return readSz;
}

uint32_t file_stream_write(void *p, void *src, uint32_t sz) {
	stream *s = (stream*)p;

	uint32_t writeSz = fwrite(src, 1, sz, s->handle);
	s->position += writeSz;
	if (s->position >= s->length) {
		s->length = s->position+1;
	}
	return writeSz;
}

//open file_stream
const char *FILE_MODE_READ = "rb", *FILE_MODE_WRITE = "wb", *FILE_MODE_READWRITE = "w+b", *FILE_MODE_UPDATE_EXISTING = "r+b", *FILE_MODE_APPEND = "ab";
bool new_file_stream(file_stream *s, const char *filepath, const char *mode) {
	//try and open file with mode
	s->handle = fopen(filepath, mode);
	if (s->handle == NULL) return false;

	//get file position
	s->position = 0;
	fseeko(s->handle, 0, SEEK_END);
	int64_t ssz = ftello(s->handle);
	if (ssz < 0) ssz = 0;
	s->length = ssz;
	if (ssz != 0) rewind(s->handle);

	s->read = &file_stream_read;
	s->write = &file_stream_write;
	s->setPosition = &file_stream_setPosition;
	s->close = &file_stream_close;

	return true;
}

//check if file exists and read access
static inline bool file_exists(const char *filepath) {
#ifdef __linux__
	return access(filepath, R_OK) != -1;
#else
	return _access(filepath, 4)!=-1;
#endif
}

//get file size
uint64_t file_size(const char *filepath) {
	FILE *handle = fopen(filepath, "rb");
	if (handle == NULL) return 0;
	fseeko(handle, 0, SEEK_END);
	int64_t sz = ftello(handle);
	if (sz < 0) sz = 0;
	fclose(handle);
	return sz;
}

//set file size
bool file_truncate(const char *filepath, uint64_t sz) {
	#ifdef __linux__
	int fd = open(filepath, O_RDWR|O_TRUNC);
	if (fd < 0) return false;
	ftruncate(fd,sz);
	close(fd);
	return true;
	#else
	HANDLE fh = CreateFile(filepath, GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);
	if (fh == INVALID_HANDLE_VALUE) return false;
	bool res = SetFilePointerEx(fh, (LARGE_INTEGER){ sz }, NULL, FILE_BEGIN) != 0;
	if (res) res &= SetEndOfFile(fh) != 0;
	CloseHandle(fh);
	return res;
	#endif
}

#define file_delete(fp) (remove(fp)==0)

//read/write whole data
char* file_readall(const char *filepath, uint64_t *sz) {
	stream fs;
	if (!new_file_stream(&fs, filepath, FILE_MODE_READ)) return NULL;
	char *buf = malloc(fs.length);
	file_stream_read(&fs, buf, fs.length);
	file_stream_close(&fs);

	if (sz != NULL) sz[0] = fs.length;
	return buf;
}
bool file_writeall(const char *filepath, const void *src, uint64_t sz) {
	stream fs;
	if (!new_file_stream(&fs, filepath, FILE_MODE_WRITE)) return false;
	file_stream_write(&fs, (void*)src, sz);
	file_stream_close(&fs);
	return true;
}

//console stream read/write/seek/close functions
void console_stream_setPosition(void *p, const uint64_t i) {
	((stream*)p)->position = i;
}

void console_stream_close(void *p) {
	((stream*)p)->position = 0;
}

uint32_t console_stream_read(void *p, void *dst, const uint32_t sz) {
	char format[8];
	sprintf(format, "%%%ds", sz);
	uint32_t UNUSED wtf = scanf(format, dst);
	return (uint32_t)strlen((char*)dst);
}

uint32_t console_stream_write(void *p, void *src, uint32_t sz) {
	printf("%.*s", sz, (char*)src);
	return sz;
}

//create console stream
static inline void new_console_stream(console_stream *s) {
	s->position = 0;

	s->read = &console_stream_read;
	s->write = &console_stream_write;
	s->setPosition = &console_stream_setPosition;
	s->close = &console_stream_close;
}

#endif
