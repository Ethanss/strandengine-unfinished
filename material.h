﻿/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _MATERIAL_H_
#define _MATERIAL_H_

#include "graphics.h"

enum {
	GEOMETRY_TRIANGLES = 0,
	GEOMETRY_QUADS = 1,
	GEOMETRY_LINES = 2,
	GEOMETRY_POINTS = 3,
	GEOMETRY_TRIANGLE_FAN = 4,
	GEOMETRY_LINE_LOOP = 5,
	GEOMETRY_TRIANGLE_STRIP = 6,
	GEOMETRY_LINE_STRIP = 7,
	GEOMETRY_QUAD_STRIP = 8
};
const GLenum RENDERER_GEOMETRY_OPENGL[9] = { GL_TRIANGLES, GL_QUADS, GL_LINES, GL_POINTS, GL_TRIANGLE_FAN, GL_LINE_LOOP, GL_TRIANGLE_STRIP, GL_LINE_STRIP, GL_QUAD_STRIP };

typedef struct {
	ubp_memory ubm;
	void *data;
	GLuint ubi;
	uint32_t size;
	bool updated;
} material_data;

typedef struct {
	texture *texture;
	GLint ui;
} material_texture;

typedef struct {
	shader *shader;
	material_texture *textures;
	material_data data;
	list customData;
	GLuint transformUBI;
	uint8_t cullFace, blendMode, depthTest, textureCount, geometryType;
	bool depthWrite;
} material;


static inline void new_material_data(material_data *d, void *dat, const uint32_t dsz, const GLuint ubi) {
	d->data = dat;
	d->size = dsz;
	d->ubi = ubi;
	d->ubm.size = 0;
	d->updated = true;
}

void new_material(material *m, shader *s, void *data, const uint32_t dataSz, char **textures, const uint8_t numTextures, const uint8_t cullFace, const uint8_t blendMode, const uint8_t depthTest, const bool depthWrite, const uint8_t geometry) {
	m->shader = s;
	GLuint csp = s->current.program;
	m->transformUBI = glGetUniformBlockIndex(csp, "TransformBlock");
	m->textureCount = numTextures;
	if (numTextures != 0) {
		m->textures = malloc(sizeof(material_texture)*numTextures);
		material_texture *mt = m->textures;
		for (uint32_t i = 0; i < numTextures; i++) {
			mt->texture = NULL;
			mt->ui = glGetUniformLocation(csp, textures[i]);
			mt++;
		}
	}
	new_material_data(&m->data, data, dataSz, glGetUniformBlockIndex(csp, "MaterialBlock"));
	new_list(&m->customData, sizeof(material_data));
	m->cullFace = cullFace;
	m->blendMode = blendMode;
	m->depthTest = depthTest;
	m->depthWrite = depthWrite;
	m->geometryType = geometry;
}
static inline void delete_material(material *m) {
	delete_list(&m->customData);
	if (m->textureCount != 0) free(m->textures);
}

void material_bind(material *m) {
	setCullFace(m->cullFace);
	setBlendMode(m->blendMode);
	setDepthTest(m->depthTest);
	enableDepthWrite(m->depthWrite);

	shader_bind(m->shader);
	
	uint32_t texc = m->textureCount;
	if (texc != 0) {
		material_texture *mt = m->textures;
		for (uint32_t i = 0; i < texc; i++) {
			if (mt->texture != NULL && mt->ui != -1) texture_bind(mt->texture, mt->ui);
			mt++;
		}
	}

	if (m->data.size != 0) {
		if (m->data.updated) {
			if (m->data.ubm.size == 0) m->data.ubm = ubp_alloc(m->data.size);
			ubp_memory_set(m->data.ubm, m->data.data, m->data.size);
			m->data.updated = false;
		}
		GLuint ubi = m->data.ubi;
		if (ubi != GL_INVALID_INDEX) ubp_memory_bind(m->data.ubm, ubi);
	}

	uint32_t cdc = m->customData.length;
	if (cdc != 0) {
		material_data *cd = m->customData.buffer;
		for (uint32_t i = 0; i < cdc; i++) {
			if (cd->size != 0) {
				if (cd->updated) {
					if (cd->ubm.size == 0) cd->ubm = ubp_alloc(cd->size);
					ubp_memory_set(cd->ubm, cd->data, cd->size);
					cd->updated = false;
				}
				GLuint ubi = cd->ubi;
				if (ubi != GL_INVALID_INDEX) ubp_memory_bind(cd->ubm, ubi);
			}
			cd++;
		}
	}
}
void material_unbind(material *m) {
	uint32_t texc = m->textureCount;
	if (texc != 0) {
		material_texture *mt = m->textures;
		for (uint32_t i = 0; i < texc; i++) {
			if (mt->texture != NULL && mt->ui != -1) texture_unbind();
			mt++;
		}
	}

	if (m->data.size != 0 && m->data.ubi != GL_INVALID_INDEX) ubp_memory_unbind();

	uint32_t cdc = m->customData.length;
	if (cdc != 0) {
		material_data *cd = m->customData.buffer;
		for (uint32_t i = 0; i < cdc; i++) {
			if (cd->size != 0 && cd->ubi != GL_INVALID_INDEX) ubp_memory_unbind();
			cd++;
		}
	}
}


#endif
