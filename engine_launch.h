/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _ENGINE_LAUNCH_H_
#define _ENGINE_LAUNCH_H_

//engine processing thread
void engine_run() {
	//engine thread
	START_SYNC_HERTZ();

	while (running) {
		//timing
		SYNC_HERTZ(updateInterval);
		currentTime = hsyncTime;

#ifndef ENGINE_NO_WINDOW
		//mouse input
		mouseDeltaX = mouseX - lastMouseX;
		mouseDeltaY = mouseY - lastMouseY;
		lastMouseX = mouseX;
		lastMouseY = mouseY;
		mouse[0] = mouse[3] | mouse[6];
		mouse[1] = mouse[4] | mouse[7];
		mouse[2] = mouse[5] | mouse[8];
		mouse[6] = false;
		mouse[7] = false;
		mouse[8] = false;
#endif

		//timers
		timers_execute(&timers, currentTime);

		//update events/callbacks
		call_slist_of_ifunctions(updateCalls);

		//synced calls
		#ifdef ENGINE_NO_WINDOW
		if (true) {
		#else
		if (syncedCallRenderDone) {
		#endif
			uint32_t nextSwapId = (syncedCallSwapId+1)%2;
			call_slist_of_ifunctions_once(syncedCalls[nextSwapId]);
			syncedCallSwapId = nextSwapId;
			syncedCallRenderDone = false;
		}
	}

	stillRunning = false;
}

//rendering thread
#ifndef ENGINE_NO_WINDOW
void engine_render() {
	SDL_Event event;
	START_SYNC_HERTZ();
	while (running) {
		//framerate timing
		SYNC_HERTZ(updateInterval);
		
		//single render callsbacks/events
		call_slist_of_ifunctions_once(singleRenderCalls);
		
		if (engineWindow.active) {
			//render callbacks/events
			call_slist_of_ifunctions(renderCalls);
			window_swapBuffers(&engineWindow);
		}

		//synced calls
		syncedCallRenderDone = true;

		//process events
		while (SDL_PollEvent(&event)) {
			uint32_t bid;
			switch (event.type) {
			case SDL_QUIT:
				running = false;
				break;
			case SDL_WINDOWEVENT:
				bid = event.window.event;
				if (bid == SDL_WINDOWEVENT_MINIMIZED) engineWindow.active = false;
				else if (bid == SDL_WINDOWEVENT_RESTORED) engineWindow.active = true;
				break;
			case SDL_MOUSEMOTION:
				//mouse position state
				mouseX = event.motion.x;
				mouseY = event.motion.y;
				nMouseX = (mouseX / (float)engineWindow.width)*2.0f - 1.0f;
				nMouseY = 1.0f - (mouseY / (float)engineWindow.height)*2.0f;
				break;
				//mouse button state
			case SDL_MOUSEBUTTONDOWN:
				bid = event.button.button;
				if (bid == SDL_BUTTON_LEFT) mouse[6] = mouse[3] = mouse[0] = true;
				else if (bid == SDL_BUTTON_MIDDLE) mouse[7] = mouse[4] = mouse[1] = true;
				else mouse[8] = mouse[5] = mouse[2] = true;
				break;
			case SDL_MOUSEBUTTONUP:
				bid = event.button.button;
				if (bid == SDL_BUTTON_LEFT) mouse[3] = false;
				else if (bid == SDL_BUTTON_MIDDLE) mouse[4] = false;
				else mouse[5] = false;
				break;

				//text input
			case SDL_TEXTINPUT:
				//newly typed text
				if (modifyingTextInput) {
					while (ATOMIC_SWAP(&textInputLock,0,1) == 1);
					if (textInputSelection != 0) {
						uint32_t lrange = textInput.length - textInputCursor;
						list_removeRange(&textInput, textInputCursor, textInputSelection<lrange?textInputSelection:lrange);
						textInputSelection = 0;
					}
					int32_t alen = strlen(event.text.text);
					list_insertm(&textInput, event.text.text, textInputCursor, alen);
					textInputCursor += alen;
					textInputModified = true;
					textInputLock = 0;
				}
				break;

			case SDL_KEYDOWN:
				bid = event.key.keysym.sym;
				if (modifyingTextInput) {
					if (bid == SDLK_BACKSPACE) {//backspace
						while (ATOMIC_SWAP(&textInputLock,0,1) == 1);
						if (textInputSelection == 0) {
							if (textInputCursor != 0) {
								list_removeAt(&textInput, textInputCursor - 1);
								textInputCursor--;
							}
						}
						else {
							uint32_t lrange = textInput.length - textInputCursor;
							list_removeRange(&textInput, textInputCursor, textInputSelection<lrange?textInputSelection:lrange);
							textInputSelection = 0;
						}
						textInputModified = true;
						textInputLock = 0;
					}
					else if (bid == SDLK_DELETE) {//delete
						while (ATOMIC_SWAP(&textInputLock,0,1) == 1);
						if (textInputSelection == 0) {
							if (textInputCursor < textInput.length) list_removeAt(&textInput, textInputCursor);
						}
						else {
							uint32_t lrange = textInput.length - textInputCursor;
							list_removeRange(&textInput, textInputCursor, textInputSelection<lrange?textInputSelection:lrange);
							textInputSelection = 0;
						}
						textInputModified = true;
						textInputLock = 0;
					}
					else if (bid == SDLK_c && SDL_GetModState() & KMOD_CTRL) {//copy
						const char ac = '\0';
						while (ATOMIC_SWAP(&textInputLock,0,1) == 1);
						if (textInputSelection == 0) {
							list_add(&textInput, &ac);
							SDL_SetClipboardText(textInput.buffer);
							list_removeAt(&textInput, textInput.length - 1);
						}
						else {
							uint32_t sci = textInputCursor + textInputSelection;
							bool rep = sci != textInput.length;
							char *ptr = (char*)textInput.buffer + textInputCursor, temp;
							if (rep) temp = ptr[sci];
							list_set(&textInput, &ac, sci);
							SDL_SetClipboardText(ptr);
							if (rep) ptr[sci] = temp;
							else list_removeAt(&textInput, sci);
						}
						textInputLock = 0;
					}
					else if (bid == SDLK_v && SDL_GetModState() & KMOD_CTRL) {//paste
						while (ATOMIC_SWAP(&textInputLock,0,1) == 1);
						char *cpt = SDL_GetClipboardText();
						const uint32_t cl = strlen(cpt);
						list_insertm(&textInput, cpt, textInputCursor, cl);
						SDL_free(cpt);
						textInputCursor += cl;
						textInputSelection = 0;
						textInputModified = true;
						textInputLock = 0;
					}
					else if (textInputNavigation && bid == SDLK_LEFT) {//move text cursor left
						while (ATOMIC_SWAP(&textInputLock,0,1) == 1);
						textInputSelection = 0;
						if (textInputCursor > 0) textInputCursor--;
						textInputModified = true;
						textInputLock = 0;
					}
					else if (textInputNavigation && bid == SDLK_RIGHT) {//right
						while (ATOMIC_SWAP(&textInputLock,0,1) == 1);
						textInputSelection = 0;
						if (textInputCursor < textInput.length) textInputCursor++;
						textInputModified = true;
						textInputLock = 0;
					}
					else if (textInputMultiline && bid == SDLK_RETURN) {//newline
						while (ATOMIC_SWAP(&textInputLock,0,1) == 1);
						char nl = '\n';
						list_insert(&textInput, &nl, textInputCursor);
						textInputCursor++;
						textInputSelection = 0;
						textInputModified = true;
						textInputLock = 0;
					}
				}
				break;
			}

			slist_lock(&eventCalls);
			one_arg_function *eventfs = (one_arg_function*)eventCalls.list.buffer;
			for (uint32_t i = 0; i < eventCalls.list.length; i++) {
				eventfs[i](&event);
			}
			slist_unlock(&eventCalls);
		}
	}

	//wait for update thread to finish
	while (stillRunning) threadSleep(10);
}
#endif


//engine launching/startup
void engine_launch(const engine_launch_options opt) {
	new_engine(opt);
	#ifndef ENGINE_NO_WINDOW
	if (engineWindow.window == NULL) {
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Initialization Error", "Could not initialize graphics.", NULL);
		return;
	}
	#endif
	
	//run update thread and render loop
	running = stillRunning;
	#ifdef ENGINE_NO_WINDOW
	engine_run();
	#else
	createThread(&engine_run, NULL, THREAD_DEFAULT_STACK_SIZE);
	engine_render();
	#endif

	delete_engine(opt);
}

#endif
