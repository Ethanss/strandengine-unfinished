﻿/*Copyright 2018 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _ENGINE_SERIALIZATION_H_
#define _ENGINE_SERIALIZATION_H_

#include "engine.h"

void object_serialize(object *o, void *p) {
	stream *s = p;

	s->write(p, &o->layer, 2);
	stream_writeUniversal(p, &o->position.x, 4, 4 * 9);
	if (o->name.buffer == scene.name.buffer) {
		uint32_t tl = 0xFFFFFFFF;
		stream_writeUniversal(p, &tl, 4, 4);
	}
	else {
		uint32_t nlen = o->name.length;
		stream_writeUniversal(p, &nlen, 4, 4);
		s->write(p, o->name.buffer, nlen);
	}

	const uint32_t nc = o->components.length;
	stream_writeUniversal(p, &nc, 4, 4);
	component **comps = o->components.buffer;
	for (uint32_t i = 0; i < nc; i++) {
		component *cc = *comps++;
		uint8_t cl = min(255,cc->name.length);
		s->write(p,&cl,1);
		s->write(p, cc->name.buffer, cl);
		s->write(p, &cc->active, 1);
		stream_serialize(p, (void*)cc, cc->metadata->fields, cc->metadata->fieldCount);
	}

	stream_writeUniversal(p, &o->children.length, 4, 4);
	for (uint32_t i = 0; i < o->children.length; i++) object_serialize(((object**)o->children.buffer)[i], p);
}
char componentSerializationNameBuffer[255];
void object_deserialize(object *o, void *p) {
	stream *s = p;
	
	s->read(p, &o->layer, 2);
	o->absoluteActive = o->parent->absoluteActive&o->active;

	stream_readUniversal(p, &o->position.x, 4, 4 * 9);
	object_updateAbsoluteTransform(o);
	
	uint32_t nlen;
	stream_readUniversal(p, &nlen, 4, 4);
	if (nlen == 0xFFFFFFFF) {
		o->name = scene.name;
	}
	else {
		o->name.stride = 1;
		o->name.length = nlen;
		list_expand(&o->name);
		s->read(p, o->name.buffer, nlen);
	}

	uint32_t nc;
	stream_readUniversal(p, &nc, 4, 4);
	for (uint32_t i = 0; i < nc; i++) {
		uint8_t nl;
		s->read(p, &nl, 1);
		s->read(p, componentSerializationNameBuffer, nl);

		component_data *compData = LAST_COMPONENT_DATA;
		while (compData != NULL) {
			if (compData->nameLength == nl && memcmp(compData->name, componentSerializationNameBuffer, nl) == 0) break;
			compData = (void*)compData->next;
		}

		if (compData != NULL) {
			component *cc = malloc(compData->size);
			memset(cc,0,sizeof(void*)*7);
			cc->name.stride = 1;
			list_setBuffer(&cc->name,compData->name,compData->nameLength);
			cc->object = o;
			cc->metadata = compData;
			cc->enabled = false;
			s->read(p, &cc->active, 1);
			compData->init(cc);
			stream_deserialize(p, (void*)cc, compData->fields, compData->fieldCount);
			if (o->absoluteActive && cc->active) component_enable(cc);
			list_add(&o->components,&cc);
		}
	}
	
	stream_readUniversal(p, &o->children.length, 4, 4);
	list_expand(&o->children);
	for (uint32_t i = 0; i < o->children.length; i++) {
		object *obj = malloc(sizeof(object));
		new_list(&obj->components, sizeof(void*));
		new_list(&obj->children, sizeof(void*));
		new_slist(&obj->transformUpdateCalls, sizeof(ifunction), false);
		obj->parent = o;
		((object**)o->children.buffer)[i] = obj;
		object_deserialize(obj, p, false);
	}
}

void object_serializeFixed(object *o, void *p, list *cl) {
	stream *s = p;

	s->write(p, &o->layer, 2);
	stream_writeUniversal(p, &o->position.x, 4, 4 * 9);
	if (o->name.buffer == scene.name.buffer) {
		uint32_t tl = 0xFFFFFFFF;
		stream_writeUniversal(p, &tl, 4, 4);
	}
	else {
		uint32_t nlen = o->name.length;
		stream_writeUniversal(p, &nlen, 4, 4);
		s->write(p, o->name.buffer, nlen);
	}

	uint32_t nc = o->components.length;
	component **comps = o->components.buffer;
	stream_writeUniversal(p, &nc, 4, 4);
	for (uint32_t i = 0; i < nc; i++) {
		component *cc = *comps++;
		int32_t cind = list_indexOf(cl, &cc->metadata, sizeof(void*));
		stream_writeUniversal(p, &cind, 4, 4);
		s->write(p, &cc->active, 1);
		stream_serializeFixed(p, (void*)cc, cc->metadata->fields, cc->metadata->fieldCount);
	}

	stream_writeUniversal(p, &o->children.length, 4, 4);
	for (uint32_t i = 0; i < o->children.length; i++) object_serializeFixed(((object**)o->children.buffer)[i], p, cl);
}
void object_deserializeFixed(object *o, void *p, list *cl) {
	stream *s = p;
	s->read(p, &o->layer, 2);
	stream_readUniversal(p, &o->position.x, 4, 4 * 9);
	object_updateTransform(o);
	
	uint32_t nlen;
	stream_readUniversal(p, &nlen, 4, 4);
	if (nlen == 0xFFFFFFFF) {
		o->name = scene.name;
	}
	else {
		o->name.bufferSize = 0;
		o->name.stride = 1;
		o->name.length = nlen;
		list_expand(&o->name);
		s->read(p, o->name.buffer, nlen);
	}

	uint32_t nc;
	stream_readUniversal(p, &nc, 4, 4);
	for (uint32_t i = 0; i < nc; i++) {
		int32_t ci;
		stream_readUniversal(p, &ci, 4, 4);
		if (ci < 0) ci = 0;
		if (ci >= cl->length) ci = cl->length - 1;

		component_data *compData = ((component_data**)cl->buffer)[ci];

		component *cc = malloc(compData->size);
		memset(cc,0,sizeof(void*)*7);
		cc->name.stride = 1;
		list_setBuffer(&cc->name,compData->name,compData->nameLength);
		cc->object = o;
		cc->metadata = compData;
		cc->enabled = false;
		s->read(p, &cc->active, 1);
		compData->init(cc);
		stream_deserialize(p, (void*)cc, compData->fields, compData->fieldCount);
		if (o->absoluteActive && cc->active) component_enable(cc);
		list_add(&o->components,&cc);
	}

	stream_readUniversal(p, &o->children.length, 4, 4);
	list_expand(&o->children);
	for (uint32_t i = 0; i < o->children.length; i++) {
		object *obj = malloc(sizeof(object));
		init_object(obj);
		obj->parent = o;
		((object**)o->children.buffer)[i] = obj;
		object_deserializeFixed(obj, p, cl);
	}
}

list engine_listComponents() {
	list l;
	new_list(&l, sizeof(void*));
	component_data *compData = LAST_COMPONENT_DATA;
	while (compData != NULL) {
		list_add(&l, &compData);
		compData = (void*)compData->next;
	}
	return l;
}

#endif
