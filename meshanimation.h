/*Copyright 2018 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _MESHANIMATION_H_
#define _MESHANIMATION_H_

#include "engine.h"

typedef struct {
	vec4 rotation;
	vec3 position,scale;
	list name;
	void *children, *parent;
	uint32_t numChildren;
	bool bone;
} mesh_animation_node;

typedef struct {
	vec3 value;
	uint32_t time;
} mesh_animation_key3;
typedef struct {
	vec4 value;
	uint32_t time;
} mesh_animation_key4;

typedef struct {
	mesh_animation_key3 *positionKeys, *scaleKeys;
	mesh_animation_key4 *rotationKeys;
	uint32_t numPositionKeys, numRotationKeys, numScaleKeys;
} mesh_animation_channel;

typedef struct {
	mesh_animation_channel *channels;
	list name;
	uint32_t duration, durationInFrames, framerate, numChannels;
} mesh_animation;

typedef struct {
	mesh_animation_node rootNode;
	mesh_animation *animations;
	uint32_t numAnimations;
} mesh_animations;

/*
void delete_mesh_animation_node(mesh_animation_node *n) {
	free(n->name.buffer);
	if (n->numChildren != 0) {
		for (uint32_t i = 0; i < n->numChildren; i++) delete_mesh_animation_node(n->children+i);
		free(n->children);
	}
}
void delete_mesh_animations(mesh_animations *ma) {
	delete_mesh_animation_node(&ma->rootNode);
}

void mesh_animation_node_instantiate(mesh_animation_node *n, object *o, const bool name) {
	object *c = malloc(sizeof(object));
	init_object(c);
	if (name) c->name = n->name;
	c->parent = o;
	list_add(&o->children,&c);
	//printf("%s at %g,%g,%g and scale %g,%g,%g\n",n->name.buffer,n->position.x,n->position.y,n->position.z,n->scale.x,n->scale.y,n->scale.z);
	c->position = n->position;
	c->scale = n->scale;
	c->rotation = (vec3){0.0f,0.0f,0.0f};
	//vec4_quaternionToEuler(&n->rotation,&c->rotation);
	//const float tmp = c->rotation.x;
	//c->rotation.x = c->rotation.z;
	//c->rotation.z = tmp;
	object_updateAbsoluteTransform(c);
	for (uint32_t i = 0; i < n->numChildren; i++) mesh_animation_node_instantiate((mesh_animation_node*)n->children+i,c,name);
}

void mesh_animations_instantiate(mesh_animations *a, object *o, const bool name) {
	for (uint32_t i = 0; i < a->rootNode.numChildren; i++) mesh_animation_node_instantiate((mesh_animation_node*)a->rootNode.children+i,o,name);
}*/

#endif
