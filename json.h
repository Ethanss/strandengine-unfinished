/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _JSON_H_
#define _JSON_H_

#include "list.h"
#include "reflection.h"
#include <ctype.h>

#ifndef JSON_MAX_DEPTH
#define JSON_MAX_DEPTH 16
#endif

enum {
	JSON_TYPE_OBJECT = 0,
	JSON_TYPE_STRING = 1,
	JSON_TYPE_ARRAY = 2,
	JSON_TYPE_INT = 3,
	JSON_TYPE_FLOAT = 4
};
typedef struct {
	list keys, values;
	struct json_object *parent;
} json_object;
typedef struct {
	union {
		void *data;
		json_object *objectv;
		list *listv;
		int32_t intv;
		float floatv;
	};
	uint8_t type;
} json;


static inline void new_json_object(json_object *j) {
	new_list(&j->keys, sizeof(list));
	new_list(&j->values, sizeof(json));
}
void delete_json(json *j);
void delete_json_object(json_object *j) {
	const uint32_t vl = j->values.length;
	json *v = j->values.buffer;
	list *n = j->keys.buffer;
	for (uint32_t i = 0; i < vl; i++) {
		delete_json(v++);
		delete_list(n++);
	}
	delete_list(&j->keys);
	delete_list(&j->values);
}
void delete_json(json *j) {
	uint8_t t = j->type;
	if (t < JSON_TYPE_INT) {
		void *d = j->data;
		if (t == JSON_TYPE_OBJECT) delete_json_object(d);
		else if (t == JSON_TYPE_STRING) delete_list(d);
		else {
			list *l = d;
			json *j = l->buffer;
			const uint32_t al = l->length;
			for (uint32_t i = 0; i < al; i++) delete_json(j++);
			delete_list(d);
		}
		free(d);
	}
}

int32_t json_object_getIndexOf(json_object *j, list *n) {
	list *k = j->keys.buffer;
	uint32_t len = j->keys.length;
	for (uint32_t i = 0; i < len; i++) {
		if (list_equals(k, n)) return i;
		k++;
	}
	return -1;
}
static inline json *json_object_get(json_object *j, list *n) {
	const int32_t i = json_object_getIndexOf(j, n);
	return i == -1 ? NULL : ((json*)j->values.buffer + i);
}
int32_t json_object_getClosestIndexOf(json_object *j, list *n) {
	list *k = j->keys.buffer;
	uint32_t len = j->keys.length;
	for (uint32_t i = 0; i < len; i++) {
		if (list_startsWith(k, n)) return i;
		k++;
	}
	return -1;
}
static inline json *json_object_getClosest(json_object *j, list *n) {
	const int32_t i = json_object_getClosestIndexOf(j, n);
	return i == -1 ? NULL : ((json*)j->values.buffer + i);
}

//WARNING: json_parse/stringify does not support nested arrays(arrays containing other arrays/objects)
json json_parse(list *s) {
	char *p = s->buffer;
	if (*p == '{') p++;
	list buf;
	new_list(&buf, 1);
	json_object *jsp = malloc(sizeof(json_object));
	new_json_object(jsp);
	jsp->parent = NULL;
	list *asp = NULL;
	uint32_t len = s->length, vtype = JSON_TYPE_INT;
	bool keyed = false, inQuotes = false;
	for (uint32_t i = p - (char*)s->buffer; i < len; i++) {
		char c = *p;
		if (inQuotes) {
			//string data
			if (c == '"') inQuotes = false;
			else list_add(&buf, p);
		}
		else {
			//value definitions
			if (c == '"') {
				vtype = JSON_TYPE_STRING;
				inQuotes = true;
			}
			else if (c == '}' || c == ',' || c == ']') {//end value/object/array, jump to last parent if object
				if (buf.length != 0 && vtype != JSON_TYPE_OBJECT) {
					json ojs;
					ojs.type = vtype;
					if (vtype == JSON_TYPE_STRING) {
						list *lb = malloc(sizeof(list));
						*lb = buf;
						ojs.data = lb;
						new_list(&buf, 1);
					}
					else if (vtype == JSON_TYPE_INT || vtype == JSON_TYPE_FLOAT) {
						char zc = '\0';
						list_add(&buf, &zc);
						if (vtype == JSON_TYPE_INT) {
							if (tolower(*(char*)buf.buffer) == 't') ojs.intv = 1;
							else ojs.intv = atoi(buf.buffer);
						} else ojs.floatv = atof(buf.buffer);
					}
					if (asp == NULL) list_add(&jsp->values, &ojs);
					else list_add(asp, &ojs);
				}
				keyed = false;
				buf.length = 0;

				if (c == '}') {
					if (jsp->parent == NULL) break;
					jsp = (json_object*)jsp->parent;
				}
				else if (c == ']') {
					vtype = JSON_TYPE_INT;
					asp = NULL;
				}
				vtype = JSON_TYPE_INT;
			}
			else if (c == '{') {//begin object
				json ojs = (json){ {malloc(sizeof(json_object))}, JSON_TYPE_OBJECT };
				new_json_object(ojs.data);
				ojs.objectv->parent = (struct json_object*)jsp;
				list_add(&jsp->values, &ojs);
				jsp = ojs.data;
			}
			else if (c == '[') {//begin array
				json ojs = (json){ {malloc(sizeof(list))}, JSON_TYPE_ARRAY };
				new_list(ojs.data, sizeof(json));
				list_add(&jsp->values, &ojs);
				asp = ojs.data;
			}
			else if (c == ':') {//begin value entry
				if (asp == NULL) {
					keyed = true;
					vtype = JSON_TYPE_INT;
					list_add(&jsp->keys, &buf);
					new_list(&buf, 1);
				}
			}
			else if (c == ' ' || c == '\t' || c == '\n' || c == '\r') {//seperator characters
			}
			else if (c == '.') {
				vtype = JSON_TYPE_FLOAT;
				list_add(&buf, p);
			}
			else {
				list_add(&buf, p);
			}
		}

		p++;
	}
	if (keyed || (asp != NULL && buf.length != 0)) {
		json ojs;
		ojs.type = vtype;
		if (vtype == JSON_TYPE_STRING) {
			list *lb = malloc(sizeof(list));
			*lb = buf;
			ojs.data = lb;
			new_list(&buf, 1);
		}
		else if (vtype == JSON_TYPE_INT || vtype == JSON_TYPE_FLOAT) {
			char zc = '\0';
			list_add(&buf, &zc);
			if (vtype == JSON_TYPE_INT) ojs.intv = atoi(buf.buffer);
			else ojs.floatv = atof(buf.buffer);
		}
		if (asp == NULL) list_add(&jsp->values, &ojs);
		else list_add(asp, &ojs);
	}
	delete_list(&buf);
	return (json){ {jsp}, JSON_TYPE_OBJECT };
}

void json_stringify(json_object *js, list *s, const bool prettyPrint, const bool properFormat) {
	char ifbuf[32];
	int32_t dbuf[JSON_MAX_DEPTH], depth = 0;

	json_object *jo = js;
	json *vals = jo->values.buffer;
	list *keys = jo->keys.buffer;
	int32_t i = 0, count = jo->values.length;

	const char qc = '"', bc = ':', pc = '{', cpc = '}', ac = '[', cac = ']', cc = ',', nlc = '\n';
	if (properFormat) list_add(s, &pc);

	while (i < count) {
		if (keys != NULL) {
			if (properFormat) list_add(s, &qc);
			list_addm(s, keys->buffer, keys->length);
			if (properFormat) list_add(s, &qc);
			list_add(s, &bc);
		}

		const uint8_t type = vals->type;
		if (type == JSON_TYPE_OBJECT) {
			list_add(s, &pc);
			if (depth < JSON_MAX_DEPTH) {
				jo = vals->data;
				vals = jo->values.buffer;
				keys = jo->keys.buffer;
				dbuf[depth] = i;
				depth++;
				i = -1;
				count = jo->values.length;
			}
			else list_add(s, &cpc);
		}
		else if (type == JSON_TYPE_STRING) {
			list_add(s, &qc);
			list *vd = vals->data;
			list_addm(s, vd->buffer, vd->length);
			list_add(s, &qc);
		}
		else if (type == JSON_TYPE_ARRAY) {
			list_add(s, &ac);
			if (depth < JSON_MAX_DEPTH) {
				count = vals->listv->length;
				vals = vals->listv->buffer;
				keys = NULL;
				dbuf[depth] = i;
				depth++;
				i = -1;
			}
			else list_add(s, &cac);
		}
		else if (type == JSON_TYPE_INT) {
			snprintf(ifbuf, 32, "%d", vals->intv);
			int32_t ilen = strlen(ifbuf);
			list_addm(s, ifbuf, ilen);
		}
		else {
			snprintf(ifbuf, 32, "%f", vals->floatv);
			int32_t ilen = strlen(ifbuf);
			list_addm(s, ifbuf, ilen);
		}

		i++;
		while (i >= count) {
			if (keys == NULL) {
				//end array
				list_add(s, &cac);
			}
			else {
				//end object
				if (jo == js) break;
				list_add(s, &cpc);
				jo = (json_object*)jo->parent;
			}
			if (depth == 0) break;
			vals = jo->values.buffer;
			keys = jo->keys.buffer;
			count = jo->values.length;
			depth--;
			i = dbuf[depth];
			vals += i;
			keys += i;
			i++;
		}
		if (i != 0 && i < count) {
			vals++;
			if (keys != NULL) keys++;
			list_add(s, &cc);
			if (prettyPrint) list_add(s, &nlc);
		}
	}
	if (properFormat) list_add(s, &cpc);
}

#endif
