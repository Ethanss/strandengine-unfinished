/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _BMPHELPER_H_
#define _BMPHELPER_H_

#include "endianness.h"
#include <stdbool.h>
#include <stdint.h>

//load bmp image into byte array from FILE pointer, only supports 8-bit depth
void *load_bmp(FILE *f, uint32_t *dim, bool verticalFlip) {
	uint8_t buf[54];
	if (fread(buf,54,1,f) == 0) return NULL;
	if (buf[0] != 'B' || buf[1] != 'M') return NULL;
	
	dim[0] = *(uint32_t*)(buf+18);
	dim[1] = *(uint32_t*)(buf+22);
	dim[2] = *(uint16_t*)(buf+28);
	if (ENDIANNESS != LITTLEENDIAN) {
		for (uint32_t i = 0; i < 3; i++) reverse(dim+i,4);
	}
	if (dim[2] > 24 || dim[2]%8 != 0) return NULL;
	const uint32_t width = dim[0], height = dim[1], bpp = dim[2]/8;
	
	uint8_t *px = malloc(width*height*3), *wpx = px;
	int32_t ystart = 0,yfin = height,dir = 1;
	if (verticalFlip) {
		ystart = yfin-1;
		dir = yfin = -1;
	}
	const uint32_t row = width*bpp, awidth = ((row-1)/4+1)*4;
	if (bpp == 1) {
		uint32_t lut[256];
		fread(lut,1024,1,f);
		uint8_t *tmp = alloca(row);
		while (ystart != yfin) {
			fseek(f,(1024+54)+ystart*awidth,SEEK_SET);
			if (fread(tmp,row,1,f) != 1) {
				free(px);
				return NULL;
			}
			uint8_t *rpx = tmp, *dpx = wpx;
			for (uint32_t i = 0; i < width; i++) {
				uint32_t lv = lut[*rpx++];
				uint8_t *ba = &lv;
				*dpx++ = ba[2];
				*dpx++ = ba[1];
				*dpx++ = ba[0];
			}
			wpx += row*3;
			ystart += dir;
		}
	} else {
		while (ystart != yfin) {
			fseek(f,54+ystart*awidth,SEEK_SET);
			if (fread(wpx,row,1,f) != 1) {
				free(px);
				return NULL;
			}
			wpx += row;
			ystart += dir;
		}
		if (dim[2] == 3) {
			wpx = px;
			for (uint32_t y = 0; y < height; y++) {
				for (uint32_t x = 0; x < width; x++) {
					const uint8_t tmp = wpx[0];
					wpx[0] = wpx[2];
					wpx[2] = tmp;
					wpx += 3;
				}
			}
		}
	}
	dim[2] = 3;
	return px;
}

//save 24bit bmp image from byte array into FILE pointer
void save_bmp(FILE *f, uint8_t *dat, const uint32_t width, const uint32_t height, const uint32_t bpp) {
	uint8_t hd[54];
	hd[0] = 'B';
	hd[1] = 'M';
	*(uint32_t*)(hd+2) = 54+width*height*3;
	*(uint32_t*)(hd+10) = 54;
	*(uint32_t*)(hd+14) = 40;
	*(int32_t*)(hd+18) = width;
	*(int32_t*)(hd+22) = height;
	*(uint16_t*)(hd+26) = 1;
	*(uint16_t*)(hd+28) = 24;
	*(uint32_t*)(hd+30) = 0;
	*(uint32_t*)(hd+34) = 0;
	*(int32_t*)(hd+38) = 10;
	*(int32_t*)(hd+42) = 10;
	*(uint32_t*)(hd+46) = 0;
	*(uint32_t*)(hd+50) = 0;
	fwrite(hd,54,1,f);
	
	uint32_t w3 = width*3, awidth = ((w3-1)/4+1)*4, adiff = awidth-w3, rowsz = width*bpp;
	uint8_t *aldat = alloca(awidth), *row = dat+(width*(height-1))*bpp;
	
	for (uint32_t y = 0; y < height; y++) {
		uint8_t *ad = aldat, *dp = row;
		if (bpp == 4) {
			for (uint32_t x = 0; x < width; x++) {
				*ad++ = dp[2];
				*ad++ = dp[1];
				*ad++ = dp[0];
				dp += 4;
			}
		} else if (bpp == 3) {
			for (uint32_t x = 0; x < width; x++) {
				*ad++ = dp[2];
				*ad++ = dp[1];
				*ad++ = dp[0];
				dp += 3;
			}
		} else if (bpp == 1) {
			for (uint32_t x = 0; x < width; x++) {
				const uint8_t sv = *dp++;
				*ad++ = sv;
				*ad++ = sv;
				*ad++ = sv;
			}
		}

		fwrite(aldat,awidth,1,f);
		row -= rowsz;
	}
}
#endif
