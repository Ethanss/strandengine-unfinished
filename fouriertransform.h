/*Copyright 2019 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _FOURIERTRANSFORM_H_
#define _FOURIERTRANSFORM_H_

#ifndef __linux__
#define _USE_MATH_DEFINES
#endif
#include <math.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

typedef struct {
	uint32_t size;
	float *buffer,*sinv,*isinv,*cosv;
	bool inverse, dstFlip;
} fouriertransform;

//size must be power of 2
void new_fouriertransform(fouriertransform *ft, const uint32_t sz) {
	ft->size = sz;
	ft->buffer = malloc(sz*5*4);
	ft->sinv = ft->buffer+sz*2;
	ft->isinv = ft->sinv+sz;
	ft->cosv = ft->isinv+sz;
	float *sv = ft->sinv, *isv = ft->isinv, *cv = ft->cosv;
	float ic = M_PI/(float)sz, oc = -ic;
	for (uint32_t i = 0; i < sz; i++) {
		*sv++ = sin(i*ic);
		*isv++ = sin(i*oc);
		*cv++ = cos(i*ic);
	}
	uint32_t bit = 0;
	while (!((sz>>(bit++))&1));
	ft->dstFlip = bit%2==0;
}
#define delete_fouriertransform(ft) free((ft)->buffer)

void _fouriertransformation(fouriertransform *ft, float *d, float *o, uint32_t st) {
	st *= 2;
	if (st < ft->size) {
		_fouriertransformation(ft,o,d,st);
		_fouriertransformation(ft,o+st,d+st,st);
	}

	uint32_t ost = st/2, sz = ft->size;
	float *sv = ft->inverse?ft->isinv:ft->sinv, *cv = ft->cosv;
	for (uint32_t i = 0; i < sz; i += st) {
		float *op = o+(i+ost)*2;
		float ix = *op++, iy = *op,
		wy = sv[i], wx = cv[i],
		ox = ix*wx-iy*wy, oy = iy*wx+ix*wy;
		op = o+i*2;
		ix = *op++;iy = *op; 
		op = d+i;
		*op++ = ix+ox;*op = iy+oy;
		op = d+(i+sz);
		*op++ = ix-ox;*op = iy-oy;
	}
}
void fouriertransformation(fouriertransform *ft, float *src, float *dst, const bool inv) {
	memcpy(ft->buffer,src,ft->size*8);
	ft->inverse = inv;
	_fouriertransformation(ft,ft->buffer,dst,1);
	if (!inv) {
		uint32_t sz = ft->size;
		float dv = 2.0f/(float)sz, *sp = ft->dstFlip?src:dst, *dp = dst;
		sz *= 2;
		for (uint32_t i = 0; i < sz; i++) *dp++ = (*sp++)*dv;
	}
}

#endif

