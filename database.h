/*Copyright 2018 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _DATABASE_H_
#define _DATABASE_H_

#include "stream.h"

//database for fixed size objects and referencing them by an unsigned 32-bit id
typedef struct {
	stream *data;
	uint32_t length, objectSize;
	uint64_t basePosition;
} database;

//database/datamaps starts at whatever the initial stream(dat) position is, so you can fit multiple databases/datamaps in a single file
void new_database(database *db, stream *dat, const uint32_t objSz) {
	db->length = 0;
	db->data = dat;
	db->objectSize = objSz;
	db->basePosition = dat->position;
	stream_writeUniversal(dat, &db->length, 4, 8);
}
void load_database(database *db, stream *dat) {
	db->data = dat;
	db->basePosition = dat->position;
	stream_readUniversal(dat, &db->length, 4, 8);
}
void save_database(database *db) {
	stream *st = db->data;
	st->setPosition(st,db->basePosition);
	stream_writeUniversal(st, &db->length, 4, 4);
}

void database_seek(database *db, const uint32_t id) {
	stream *dat = db->data;
	dat->setPosition(dat,db->basePosition+8+(uint64_t)id*(uint64_t)db->objectSize);
}

//datamap is a stream-based hashmap with a fixed buffer size
typedef struct {
	stream *data;
	uint32_t bufferSize, length, objectSize;
	uint64_t basePosition;
} datamap;

void new_datamap(datamap *dm, stream *dat, const uint32_t objectSize, const uint32_t bufferSize) {
	dm->data = dat;
	dm->bufferSize = bufferSize;
	dm->objectSize = objectSize;
	dm->length = 0;
	dm->basePosition = dat->position;
	stream_writeUniversal(dat, &dm->bufferSize, 4, 12);
	uint32_t zero = NULL_HASH;
	for (uint32_t i = 0; i < bufferSize; i++) stream_writeUniversal(dat,&zero,4,4);
	dat->setPosition(dat,dat->position+(uint64_t)bufferSize*(uint64_t)objectSize-4);
	dat->write(dat,&zero,4);
}
void load_datamap(datamap *dm, stream *dat) {
	dm->data = dat;
	dm->basePosition = dat->position;
	stream_readUniversal(dat, &dm->bufferSize, 4, 12);
}
void save_datamap(datamap *dm) {
	stream *dat = dm->data;
	dat->setPosition(dat,dm->basePosition+4);
	stream_writeUniversal(dat,&dm->length, 4, 4);
}

#ifndef DATAMAP_BATCH_SIZE
#define DATAMAP_BATCH_SIZE 64
#endif

uint32_t datamap_indexOfI(datamap *dm, const uint32_t hv, const uint32_t bi) {
	uint32_t bsz = dm->bufferSize;
	if (bsz == 0) return UINT_MAX;
	stream *dat = dm->data;
	uint32_t hi = (hv+bi)%bsz, isz = bsz-hi;
	uint32_t hbuf[DATAMAP_BATCH_SIZE];
	dat->setPosition(dat,dm->basePosition+12+hi*4);
	
	uint32_t i4 = (max(isz,1)-1)/DATAMAP_BATCH_SIZE+1;
	for (uint32_t i = 0; i < i4; i++) {
		uint32_t b = i*DATAMAP_BATCH_SIZE,
				nsz = isz-b;
		nsz = min(nsz,DATAMAP_BATCH_SIZE);
		stream_readUniversal(dat,hbuf,4,nsz*4);
		uint32_t *hptr = hbuf;
		b += hi;
		for (uint32_t k = 0; k < nsz; k++) {
			const uint32_t tv = *hptr++;
			if (tv == NULL_HASH) return UINT_MAX;
			if (tv == hv) return b+k;
		}
	}
	
	isz = hi;
	i4 = (max(isz,1)-1)/DATAMAP_BATCH_SIZE+1;
	dat->setPosition(dat,dm->basePosition+12);
	for (uint32_t i = 0; i < i4; i++) {
		uint32_t b = i*DATAMAP_BATCH_SIZE,
				nsz = isz-b;
		nsz = min(nsz,DATAMAP_BATCH_SIZE);
		stream_readUniversal(dat,hbuf,4,nsz*4);
		uint32_t *hptr = hbuf;
		for (uint32_t k = 0; k < nsz; k++) {
			const uint32_t tv = *hptr++;
			if (tv == NULL_HASH) return UINT_MAX;
			if (tv == hv) return b+k;
		}
	}
	
	return UINT_MAX;
}
#define datamap_indexOf(dm,hv) datamap_indexOfI(dm,hv,0)

uint32_t datamap_indexOfEmpty(datamap *dm, const uint32_t bi) {
	uint32_t bsz = dm->bufferSize;
	if (bsz == 0) return UINT_MAX;
	stream *dat = dm->data;
	uint32_t hi = bi%bsz, isz = bsz-hi;
	uint32_t hbuf[DATAMAP_BATCH_SIZE];
	dat->setPosition(dat,dm->basePosition+12+hi*4);
	
	uint32_t i4 = (max(isz,1)-1)/DATAMAP_BATCH_SIZE+1;
	for (uint32_t i = 0; i < i4; i++) {
		uint32_t b = i*DATAMAP_BATCH_SIZE,
				nsz = isz-b;
		nsz = min(nsz,DATAMAP_BATCH_SIZE);
		stream_readUniversal(dat,hbuf,4,nsz*4);
		uint32_t *hptr = hbuf;
		b += hi;
		for (uint32_t k = 0; k < nsz; k++) {
			const uint32_t tv = *hptr++;
			if (tv == NULL_HASH) return b+k;
		}
	}
	
	isz = hi;
	i4 = (max(isz,1)-1)/DATAMAP_BATCH_SIZE+1;
	dat->setPosition(dat,dm->basePosition+12);
	for (uint32_t i = 0; i < i4; i++) {
		uint32_t b = i*DATAMAP_BATCH_SIZE,
				nsz = isz-b;
		nsz = min(nsz,DATAMAP_BATCH_SIZE);
		stream_readUniversal(dat,hbuf,4,nsz*4);
		uint32_t *hptr = hbuf;
		for (uint32_t k = 0; k < nsz; k++) {
			const uint32_t tv = *hptr++;
			if (tv == NULL_HASH) return b+k;
		}
	}
	
	return UINT_MAX;
}

//adds hash 'hv' to map and sets stream position to data location so you can serialize the value after adding
void datamap_add(datamap *m, const uint32_t hv) {
	m->length++;

	uint32_t ind = datamap_indexOfEmpty(m,hv);
	stream *dat = m->data;
	uint64_t base = m->basePosition+12;
	dat->setPosition(dat,base+ind*4);
	stream_writeUniversal(dat,&hv,4,4);
	dat->setPosition(dat,base+(uint64_t)m->bufferSize*4+ind*(uint64_t)m->objectSize);
}

void datamap_removeAt(datamap *m, const uint32_t index) {
	stream *dat = m->data;
	dat->setPosition(dat,m->basePosition+12+(uint64_t)index*4);
	uint32_t zero = NULL_HASH;
	stream_writeUniversal(dat,&zero,4,4);
}
void datamap_remove(datamap *m, const uint32_t hv) {
	uint32_t index = datamap_indexOf(m,hv);
	if (index == UINT_MAX) return;
	datamap_removeAt(m, index);
}

void datamap_seek(datamap *m, const uint32_t ind) {
	stream *dat = m->data;
	dat->setPosition(dat,m->basePosition+12+((uint64_t)m->bufferSize*4)+(uint64_t)ind*(uint64_t)m->objectSize);
}


#endif
