﻿/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _DEBUGDRAW_H_
#define _DEBUGDRAW_H_

bool debugDrawInitialized = false;
mesh debugDrawMesh;
ubp_memory debugDrawMaterialUBM;

//draw 3d lines or line strip works in editor or in game, note 'pts' array values are modified by this function
void debugdrawlines(vec3 *pts, const uint32_t npts, const vec4 color, const bool strip) {
	if (!debugDrawInitialized) {
		debugDrawInitialized = true;
		new_mesh(&debugDrawMesh, VERTEX_ATTRIBUTE_POSITION);
		debugDrawMaterialUBM = ubp_alloc(sizeof(blit_material));
	}
	//setup rendering options
	setCullFace(CULLFACE_BACK);
	setDepthTest(DEPTHTEST_NONE);
	setBlendMode(BLENDMODE_TRANSPARENT);
	enableDepthWrite(false);

	fullscreen_blit_material.tint = color;
	ubp_memory_set(debugDrawMaterialUBM, &fullscreen_blit_material,sizeof(blit_material));
	fullscreen_blit_material.tint = (vec4){ 1.0f, 1.0f, 1.0f, 1.0f };

	//project 3d points to screen based on in editor camera or in game
	bool doT = false;
	vec3 cpos = (vec3){ 0.0f, 0.0f, 0.0f }, crot = (vec3){ 0.0f, 0.0f, 0.0f };
#if defined(RENDERING_H)
#if defined(ENGINE_EDITMODE)
	if (editorRendererViewEnabled) {
		cpos = editorRendererViewPosition;
		crot = editorRendererViewRotation;
		doT = true;
	}
	else if (screenRenderer != NULL) {
		object *obj = screenRenderer->base.object;
		cpos = obj->absolutePosition;
		crot = obj->absoluteRotation;
		doT = true;
	}
#else
	if (screenRenderer != NULL) {
		object *obj = screenRenderer->base.object;
		cpos = obj->absolutePosition;
		crot = obj->absoluteRotation;
		doT = true;
	}
#endif
#endif
	//for (uint32_t i = 0; i < npts; i++) vec3_pointToScreen(pts+i, &cpos, &crot, engineWindow.aspect);
	mesh_set(&debugDrawMesh, pts, npts, VERTEX_ATTRIBUTEID_POSITION, GL_STREAM_DRAW);

	//rendering
	mesh_bind(&debugDrawMesh);
	shader_bind(&blitShader);
	texture_bind(&whiteTexture, blitShaderTextureIndex);
	ubp_memory_bind(debugDrawMaterialUBM, blitShaderMaterialIndex);
	glDrawArrays(strip?GL_LINE_STRIP:GL_LINES, 0, npts);
	ubp_memory_unbind();
	texture_unbind();
}

void debugdrawline(const vec3 a, const vec3 b, const vec4 color) {
	vec3 pts[2];
	pts[0] = a;
	pts[1] = b;
	debugdrawlines(pts, 2, color, false);
}

void debugdrawcube(const vec3 pos, const vec3 ext, const vec3 rot, const vec4 color) {
	//generate box corners
	vec3 corners[8], pt[24];
	for (uint32_t i = 0; i < 8; i++) {
		vec3 *lp = corners + i;
		lp->x = ext.x*(i%2==0?1.0f:-1.0f);
		lp->y = ext.y*((i / 2) % 2 == 0 ? 1.0f : -1.0f);
		lp->z = ext.z*((i / 4) % 2 == 0 ? 1.0f : -1.0f);
		vec3_rot(lp, &rot);
		vec3_add(lp,&pos);
	}

	//create box from corner points, forward side
	pt[0] = corners[0]; pt[1] = corners[1];
	pt[2] = corners[1]; pt[3] = corners[3];
	pt[4] = corners[3]; pt[5] = corners[2];
	pt[6] = corners[2]; pt[7] = corners[0];
	//forward to backward lines
	pt[8] = corners[0]; pt[9] = corners[4];
	pt[10] = corners[1]; pt[11] = corners[5];
	pt[12] = corners[2]; pt[13] = corners[6];
	pt[14] = corners[3]; pt[15] = corners[7];
	//backward side
	pt[16] = corners[4]; pt[17] = corners[5];
	pt[18] = corners[5]; pt[19] = corners[7];
	pt[20] = corners[7]; pt[21] = corners[6];
	pt[22] = corners[6]; pt[23] = corners[4];

	debugdrawlines(pt, 24, color, false);
}

void debugdrawsphere(const vec3 pos, const float radius, const vec4 color) {
	vec3 pt[32];
	//circle on z axis
	float a = 0.0f;
	for (uint32_t i = 0; i < 16; i++) {
		pt[i] = (vec3){ pos.x + sinf(a)*radius, pos.y + cosf(a)*radius, pos.z };
		a += 0.418879020f;
	}
	//circle on x axis
	a = 0.0f;
	for (uint32_t i = 15; i < 32; i++) {
		pt[i] = (vec3){ pos.x, pos.y + cosf(a)*radius, pos.z + sinf(a)*radius };
		a += 0.418879020f;
	}
	debugdrawlines(pt, 32, color, true);
}
#endif
