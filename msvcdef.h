#ifdef _MSC_VER
#define inline __inline
#define snprintf _snprintf
#define alloca _alloca
#define atoll(v) _atoi64(v)
#define popen(f,m) _popen(f,m)
#endif
