﻿/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _LIST_H_
#define _LIST_H_

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#ifndef LIST_MAX_STACK_ALLOC
#define LIST_MAX_STACK_ALLOC 4096
#endif

typedef struct {
	void *buffer;
	uint32_t length, stride, bufferSize;
} list;

//initialize list
#define construct_list(s) {NULL,0,s,0}
#define static_list(b,l,s) {b,l,s,7}
static inline void new_list(list *l, const uint32_t stride) {
	l->length = 0;
	l->stride = stride;
	l->bufferSize = 0;
}

//free list buffer
static inline void delete_list(list *l) {
	if (l->bufferSize != 0 && l->bufferSize != 7) {
		free(l->buffer);
		l->bufferSize = 0;
	}
}
#define free_list delete_list


//a list is flagged as writeable when bufferSize != 7
#define list_writeable(l) (l)->bufferSize!=7

//set list buffer, this causes the list to become un-writeable
static inline void list_setBuffer(list *l, void *p, const uint32_t sz) {
	l->buffer = p;
	l->length = sz;
	l->bufferSize = 7;
}

//expand list buffer if needed
void list_expand(list *l) {
	uint32_t lbsz = l->bufferSize;
	if (lbsz == 7) lbsz = 0;
	if (lbsz >= l->length) return;
	
	if (lbsz == 0) l->bufferSize = 1;
	while (l->bufferSize < l->length) l->bufferSize *= 8;

	if (lbsz == 0) l->buffer = malloc(l->stride*l->bufferSize);
	else l->buffer = realloc(l->buffer, l->stride*l->bufferSize);
}

//compress list buffer
void list_compress(list *l) {
	uint32_t ll = l->length, lbsz = l->bufferSize;
	if (lbsz == 0 || lbsz == 7) return;
	if (ll == 0) {
		l->bufferSize = 0;
		free(l->buffer);
		return;
	}

	uint32_t bsz = 1;
	while (bsz < ll) bsz *= 8;

	if (bsz < lbsz) {
		l->bufferSize = bsz;
		l->buffer = realloc(l->buffer, l->stride*bsz);
	}
}

//set value at index 'i', expanding list if 'i' is outside the current length
void list_set(list *l, void *v, const uint32_t i) {
	const uint32_t s = l->stride;
	if (l->length <= i) {
		uint32_t ol = l->length;
		l->length = i + 1;
		list_expand(l);
		if (ol != i) memset((uint8_t*)l->buffer + ol*s, 0, (l->length - ol)*s);
	}
	memcpy((uint8_t*)l->buffer + i*s, v, s);
}

//copy list
void list_copy(list *l, list *b) {
	l->stride = b->stride;
	l->length = b->length;
	list_expand(l);
	memcpy(l->buffer, b->buffer, l->length*l->stride);
}

//add multiple objects to list
uint32_t list_addm(list *l, const void *o, const uint32_t count) {
	const uint32_t sind = l->length;
	l->length += count;
	list_expand(l);

	memcpy((uint8_t*)l->buffer+sind*l->stride, o, l->stride*count);
	return sind;
}
//add object to list
#define list_add(l,o) list_addm(l,o,1)

//add nothing to list creating an empty slot
uint32_t list_addEmpty(list *l) {
	//check if buffer needs to be expanded or created
	const uint32_t sind = l->length;
	l->length++;
	list_expand(l);
	return sind;
}

//insert multiple objects at specific index
void list_insertm(list *l, const void *o, uint32_t index, uint32_t count) {
	uint32_t nAbove = l->length-index;

	l->length += count;
	list_expand(l);

	if (nAbove != 0) memmove((((uint8_t*)l->buffer)+(index+count)*l->stride), (((uint8_t*)l->buffer)+index*l->stride), nAbove*l->stride);
	if (o != NULL) memcpy(((uint8_t*)l->buffer)+index*l->stride, o, l->stride*count);
}
//insert object at specific index
#define list_insert(l,o,i) list_insertm(l,o,i,1)

//remove multiple objects starting from index
void list_removeRange(list *l, uint32_t index, int32_t n) {
	int32_t nAbove = (int32_t)l->length-(int32_t)(index+n);
	if (nAbove > 0) {
		memmove((((uint8_t*)l->buffer) + index*l->stride), (((uint8_t*)l->buffer) + (index + n)*l->stride), nAbove*l->stride);
		l->length -= n;
	} else l->length -= n+nAbove;
}
//remove object from list at index
#define list_removeAt(l,i) list_removeRange(l,i,1)

//remove fast, downside is it changes order of list
void list_removeRangeFast(list *l, uint32_t index, int32_t n) {
	int32_t nAbove = (int32_t)l->length-(int32_t)(index+n);
	if (nAbove > 0) {
		nAbove = nAbove>n?n:nAbove;
		memmove((((uint8_t*)l->buffer) + index*l->stride), (((uint8_t*)l->buffer) + (l->length-nAbove)*l->stride), nAbove*l->stride);
		l->length -= n;
	} else l->length -= n+nAbove;
}
#define list_removeAtFast(l,i) list_removeRangeFast(l,i,1)

//get index of object in buffer equal to o, returns -1 if no object equal to o was found
int32_t memIndexOf(const void *src, const uint32_t srcStride, const uint32_t srcCount, const void *search, const uint32_t searchSz) {
	if (searchSz > srcStride*srcCount) return -1;
	uint8_t* s = src;
	for (uint32_t i = 0; i < srcCount; i++) {
		if (memcmp(search, s, searchSz) == 0) return (int32_t)i;
		s += srcStride;
	}
	return -1;
}
static inline int32_t list_indexOf(const list *l, const void *o, const uint32_t sz) {
	return memIndexOf(l->buffer, l->stride, l->length, o, sz);
}
//same as indexOf but starts at index 'si'
static inline int32_t list_indexOfI(const list *l, const void *o, const uint32_t sz, const uint32_t si) {
	int32_t wsz = (int32_t)l->length - (int32_t)si,
	ind = memIndexOf((uint8_t*)l->buffer + si*l->stride, l->stride, wsz<0?0:wsz, o, sz);
	if (ind == -1) return -1;
	return ind + si;
}

//get index of object in buffer equal to o using the comparison function provided
int32_t list_indexOfCC(const list* l, const void *o, const void *cc) {
	typedef bool (*customComparisonFunction)(const void*, const void*);
	customComparisonFunction ccf = cc;

	const int32_t len = l->length,
			  stride = l->stride;
	const uint8_t* buffer = l->buffer;
	for (int32_t i = 0; i < len; i++) {
		if (ccf(o,buffer+i*stride)) return i;
	}

	return -1;
}

//get last index of object in buffer equal to o, returns -1 if no object equal to o was found
int32_t memLastIndexOf(const void *src, const uint32_t srcStride, const uint32_t srcCount, const void *search, const uint32_t searchSz) {
	const uint32_t ssz = srcStride*srcCount;
	if (searchSz > ssz) return -1;
	const uint8_t* s = src;
	s += ssz;
	uint32_t i = srcCount;
	while (i-- != 0) {
		s -= srcStride;
		if (memcmp(search, s, searchSz) == 0) return (int32_t)i;
	}
	return -1;
}
static inline int32_t list_lastIndexOf(const list *l, const void *o, const uint32_t sz) {
	return memLastIndexOf(l->buffer, l->stride, l->length, o, sz);
}
//same as lastIndexOf but starts at index 'si'
static inline int32_t list_lastIndexOfI(const list *l, const void *o, const uint32_t sz, const uint32_t si) {
	int32_t wsz = (int32_t)l->length - (int32_t)si,
	ind = memLastIndexOf((uint8_t*)l->buffer + si*l->stride, l->stride, wsz<0?0:wsz, o, sz);
	if (ind == -1) return -1;
	return ind + si;
}



//remove object that matches value
static inline void list_remove(list *l, void *v) {
	int32_t index = list_indexOf(l, v, l->stride);
	if (index == -1) return;
	list_removeAt(l, index);
}
static inline void list_removeFast(list *l, void *v) {
	int32_t index = list_indexOf(l, v, l->stride);
	if (index == -1) return;
	list_removeAtFast(l, index);
}

//swap multiple objects in list, if the objects overlap memcpy will fail
void list_swap(list *l, const uint32_t aindex, const uint32_t bindex, const uint32_t count) {
	uint32_t stride = l->stride;
	uint8_t *src = l->buffer,
				*asrc = src+aindex*stride,
				*bsrc = src+bindex*stride;

	uint32_t swapSz = stride*count;
	if (swapSz <= LIST_MAX_STACK_ALLOC) {
		uint8_t *sb = alloca(swapSz);
		memcpy(sb,bsrc,swapSz);
		memcpy(bsrc,asrc,swapSz);
		memcpy(asrc,sb,swapSz);
	} else {
		uint8_t *sb = malloc(swapSz);
		memcpy(sb,bsrc,swapSz);
		memcpy(bsrc,asrc,swapSz);
		memcpy(asrc,sb,swapSz);
		free(sb);
	}
}

//replace data 'src' with 'target'
void list_replace(list *l, void *src, const uint32_t sz, void *dst, const uint32_t dstSz) {
	int32_t ind = list_indexOf(l, src, sz);
	if (ind == -1) return;
	if (sz != dstSz) {
		if (dstSz > sz) {
			list_insertm(l, NULL, ind+sz, (dstSz-sz)/l->stride);
		} else {
			list_removeRange(l, ind+dstSz, (sz-dstSz)/l->stride);
		}
	}
	memcpy((uint8_t*)l->buffer+ind, dst, dstSz);
}

//replace all instances of data 'src' with 'target'
void list_replaceAll(list *l, void *src, const uint32_t sz, void *dst, const uint32_t dstSz) {
	int32_t ind = 0;
	while ((ind = list_indexOfI(l, src, sz, ind)) != -1) {
		if (sz != dstSz) {
			if (dstSz > sz) {
				list_insertm(l, NULL, ind+sz, (dstSz-sz)/l->stride);
			} else {
				list_removeRange(l, ind+dstSz, (sz-dstSz)/l->stride);
			}
		}
		memcpy((uint8_t*)l->buffer+ind*l->stride, dst, dstSz);
		ind++;
	}
}

//replace all instances of 'src' with 'dst' and 'dst' with 'src'
void list_replaceSwapAll(list *l, void *src, const uint32_t sz, void *dst, const uint32_t dstSz) {
	uint32_t stride = l->stride, szs = sz/stride, dstSzs = dstSz/stride, spos = 0;
	for (uint32_t ind = 0; ind < l->length; ind++) {
		uint8_t *bp = (uint8_t*)l->buffer + spos;
		if (memcmp(bp,src,sz) == 0) {
			if (sz != dstSz) {
				if (dstSz > sz) {
					list_insertm(l, NULL, ind+szs, (dstSz-sz)/stride);
					bp = (uint8_t*)l->buffer + spos;
				} else {
					list_removeRange(l, ind+dstSzs, (sz-dstSz)/stride);
				}
			}
			memcpy(bp,dst,dstSz);
		} else if (memcmp(bp,dst,dstSz) == 0) {
			if (sz != dstSz) {
				if (sz > dstSz) {
					list_insertm(l, NULL, ind+dstSzs, (sz-dstSz)/stride);
					bp = (uint8_t*)l->buffer + spos;
				}
				else {
					list_removeRange(l, ind+szs, (dstSz-sz)/stride);
				}
			}
			memcpy(bp,src,sz);
		}
		spos += stride;
	}
}

//check if 2 lists have equal data
static inline bool list_equals(list *l, list *b) {
	if (l->length*l->stride != b->length*b->stride) return false;
	return memcmp(l->buffer, b->buffer, l->length*l->stride)==0;
}

//count number of elements that are different
uint32_t list_difference(list *l, list *b) {
	uint32_t es, minl;
	if (l->length > b->length) {
		minl = b->length;
		es = l->length - minl;
	}
	else {
		minl = l->length;
		es = b->length - minl;
	}
	if (l->stride == 1) {
		uint8_t *lp = l->buffer, *bp = b->buffer;
		for (uint32_t i = 0; i < minl; i++) {
			if (*lp++ != *bp++) es++;
		}
	}
	else if (l->stride == 2) {
		uint16_t *lp = l->buffer, *bp = b->buffer;
		for (uint32_t i = 0; i < minl; i++) {
			if (*lp++ != *bp++) es++;
		}
	}
	else if (l->stride == 4) {
		uint32_t *lp = l->buffer, *bp = b->buffer;
		for (uint32_t i = 0; i < minl; i++) {
			if (*lp++ != *bp++) es++;
		}
	}
	else {
		uint32_t stride = l->stride;
		uint8_t *lp = l->buffer, *bp = b->buffer;
		for (uint32_t i = 0; i < minl; i++) {
			if (memcmp(lp, bp, stride) != 0) es++;
			lp += stride;
			bp += stride;
		}
	}
	return es;
}

//check if list l contains the data of list b at the start
static inline bool memStartsWith(char *a, uint32_t alen, char *b, uint32_t blen) {
	if (blen > alen) return false;
	return memcmp(a, b, blen)==0;
}
static inline bool list_startsWith(list *l, list *b) {
	if (b->length*b->stride > l->length*l->stride) return false;
	return memcmp(l->buffer, b->buffer, b->length*b->stride)==0;
}

//check if list l contains the data of list b at the end
static inline bool memEndsWith(char *a, uint32_t alen, char *b, uint32_t blen) {
	if (blen > alen) return false;
	return memcmp(a+(alen-blen), b, blen)==0;
}
static inline bool list_endsWith(list *l, list *b) {
	if (b->length*b->stride > l->length*l->stride) return false;
	return memcmp((uint8_t*)l->buffer+(l->length*l->stride-b->length*b->stride), b->buffer, b->length*b->stride)==0;
}

typedef bool (*listComparisonFunction)(void*,void*);
//merge sort list 'l', using comparison function 'comp' and buffer 'b'
void list_sort(list *l, list *b, listComparisonFunction comp) {
	b->length = l->length;
	list_expand(b);

	const uint32_t stride = l->stride, stride2 = stride*2, length = l->length;
	uint32_t hsz = length/2;
	uint8_t *lp = l->buffer, *bp = b->buffer;
	for (uint32_t k = 0; k < hsz; k++) {
		uint8_t *l2 = lp+stride;
		if (comp(lp,l2)) memcpy(bp,lp,stride2);
		else {
			memcpy(bp,l2,stride);
			memcpy(bp+stride,lp,stride);
		}
		lp += stride2;
		bp += stride2;
	}
	if (length%2 == 1) memcpy(bp,lp,stride);
	
	list *odl = l;
	l = b;
	b = odl;
	
	hsz = 2;
	while (hsz < length) {
		lp = l->buffer;
		bp = b->buffer;
		
		const uint32_t ssz = (length/hsz+1)/2, ns = stride*hsz, h2 = hsz*2;
		for (uint32_t k = 0; k < ssz; k++) {
			uint8_t *p1 = lp, *p2;
			uint32_t c1 = 0, c2 = 0;
			lp += ns;
			p2 = lp;
			uint32_t sz = length-k*h2;
			if (h2 < sz) sz = h2;
			uint32_t hsz2 = sz-hsz;
			if (hsz >= sz) {
				sz *= stride;
				memcpy(bp,p1,sz);
				bp += sz;
			} else {
				for (uint32_t i = 0; i < sz; i++) {
					uint32_t ii = sz-i;
					if (comp(p1,p2)) {
						memcpy(bp,p1,stride);
						p1 += stride;
						c1++;
					
						if (c1 >= hsz && ii > 1) {
							ii = (ii-1)*stride;
							bp += stride;
							memcpy(bp,p2,ii);
							bp += ii;
							break;
						}
					} else {
						memcpy(bp,p2,stride);
						p2 += stride;
						c2++;
					
						if (c2 >= hsz2 && ii > 1) {
							ii = (ii-1)*stride;
							bp += stride;
							memcpy(bp,p1,ii);
							bp += ii;
							break;
						}
					}
					bp += stride;
				}
			}
			lp += ns;
		}
		
		list *tmp = l;
		l = b;
		b = tmp;
		
		hsz *= 2;
	}
	
	if (l != odl) {
		lp = l->buffer;
		l->buffer = b->buffer;
		b->buffer = lp;
	}
}

#define cstring(name,cstr) const uint32_t sizeof##name = sizeof(cstr)-1;const uint8_t *name = cstr
#define cstring_list(str) {str,sizeof(str),1,7}
#define list_set_cstring(l,str,len) (l)->bufferSize = 7;(l)->length = len;(l)->buffer = str
/*#define list_add_cstring(l,cstr) {const char *str = cstr;int len = strlen(str);list_addm(l,str,len);}
#define list_set_cstring(l,cstr) (l)->length=0;{const char *str = cstr;int len = strlen(str);list_addm(l,str,len);}
#define string(l, cstr) new_list(l,1);list_add_cstring(l,cstr);
*/


static inline list cstring_as_list(const char *str) {
	list l;
	l.stride = 1;
	l.length = strlen(str);
	l.buffer = (void*)str;
	l.bufferSize = 7;
	return l;
}

//temporarily convert char list to cstring by adding a null terminating char
static inline char *list_as_cstring(list *l) {
	if (l->bufferSize == 7) return l->buffer;
	char zc = '\0';
	list_add(l, &zc);
	return l->buffer;
}
static inline void list_clear_cstring(list *l) {
	if (l->bufferSize != 7) l->length--;
}

//splits 'str' using 'seperator', if alloc = false then nothing is allocated and the returned strings are immutable/backed by the original 'str'
list string_split(char *str, const uint32_t slen, const char seperator, const bool alloc) {
	list sp, nl;
	new_list(&sp, sizeof(list));
	new_list(&nl,1);
	if (!alloc) nl.bufferSize = 7;

	uint32_t last = 0;
	char *p = str;
	for (uint32_t k = 0; k < slen; k++) {
		char c = *p++;
		if (c == seperator) {
			uint32_t d = k-last;
			if (alloc) {
				if (d != 0) list_addm(&nl,str+last,d);
				list_add(&sp,&nl);
				new_list(&nl,1);
			} else {
				nl.buffer = str+last;
				nl.length = d;
				list_add(&sp,&nl);
			}
			last = k+1;
		}
	}

	uint32_t dst = slen-last;
	if (dst != 0) {
		if (alloc) {
			list_addm(&nl,str+last,dst);
			list_add(&sp,&nl);
		} else {
			nl.buffer = str+last;
			nl.length = dst;
			list_add(&sp,&nl);
		}
	}

	return sp;
}
static inline list cstring_split(char *str, const char seperator, const bool alloc) {
	return string_split(str, strlen(str), seperator, alloc);
}

typedef int (*stringCharModifierFunction)(int);
static inline void string_applyCharacterModifier(char *str, const uint32_t slen, stringCharModifierFunction f) {
	for (uint32_t i = 0; i < slen; i++) {
		*str = f(*str);
		str++;
	}
}
static inline void cstring_applyCharacterModifier(char *str, stringCharModifierFunction f) {
	char v;
	while ((v = *str) != '\0') {
		*str = f(v);
		str++;
	}
}

#endif
