﻿/*Copyright 2018 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

const char *EDITOR_ADDON_BITFLAGS_CSTR = "Bit Flags";
char editorAddonBitflagStrings[128];
bool editorAddonInitBitflagStrings = false;

void editor_addon_bitflags_refresh(object *opane) {
	vec4 ctv = EDITOR_STYLE_GOOD_BACKGROUND;
	object **oc = opane->children.buffer;
	uint32_t v = 0;
	for (uint32_t b = 0; b < 32; b++) {
		if (vec4_equals(&(*(gui_component**)(*oc++)->components.buffer)->color,&ctv)) v |= 1<<b;
	}
	
	gui_textfield *tf = *(gui_textfield**)oc[0]->components.buffer;
	tf->base.text.length = 64;
	list_expand(&tf->base.text);
	tf->base.text.length = snprintf(tf->base.text.buffer,64,"%d",v);
	SDL_SetClipboardText(tf->base.text.buffer);
	gui_text_updateText(&tf->base);
}

void editor_addon_bitflags_toggle(void *p) {
	gui_component *c = p;
	vec4 ctv = EDITOR_STYLE_FOREGROUND;
	bool active = vec4_equals(&c->color,&ctv);
	c->color = active?EDITOR_STYLE_GOOD_BACKGROUND:EDITOR_STYLE_FOREGROUND;	
	gui_component_transformUpdate(c);
	editor_addon_bitflags_refresh(c->base.object->parent);
}

void editor_addon_bitflags_open() {
	object *wnd = editor_createWindow((char*)EDITOR_ADDON_BITFLAGS_CSTR,(vec2){0.0f,0.0f},editorConstantPixelsToGUI((vec2){300.0f,200.0f}),&editor_onCloseWindowClick);
	
	object *opane = ((object**)wnd->children.buffer)[1];
	gui_dragview *sview = ((gui_dragview**)opane->components.buffer)[0];
	sview->minX = sview->maxX = sview->minY = sview->maxY = 0.0f;
	
	uint32_t i = 0;
	for (uint32_t y = 0; y < 4; y++) {
		for (uint32_t x = 0; x < 8; x++) {
			editor_createButton(opane,editorAddonBitflagStrings+i*4,EDITOR_STYLE_FOREGROUND,EDITOR_STYLE_COLOR,(vec3){(x-3.5f)*0.25f,(2.0f-y)*0.4f,-editorWindowZDepthStep},(vec2){0.1f,0.15f},EDITOR_STYLE_FONT_SIZE, editor_addon_bitflags_toggle);
			i++;
		}
	}

	editor_createTextInput(opane,"",NULL,EDITOR_STYLE_FOREGROUND, EDITOR_STYLE_COLOR, (vec3){0.0f,-0.8f,-editorWindowZDepthStep}, (vec2){0.8f,0.1f}, EDITOR_STYLE_FONT_SIZE);

	editor_addon_bitflags_refresh(opane);
}

void editor_addon_bitflags_init() {
	if (!editorAddonInitBitflagStrings) {
		for (uint32_t i = 0; i < 32; i++) snprintf(editorAddonBitflagStrings+i*4,4,"%d",i);
		editorAddonInitBitflagStrings = true;
	}
	editor_addMenuButton(EDITOR_ADDON_BITFLAGS_CSTR,&editor_addon_bitflags_open);
}

const engine_editor_addon editor_addon_bitflags = {editor_addon_bitflags_init,NULL,LAST_ENGINE_EDITOR_ADDON};

#undef LAST_ENGINE_EDITOR_ADDON
#define LAST_ENGINE_EDITOR_ADDON (void*)&editor_addon_bitflags
