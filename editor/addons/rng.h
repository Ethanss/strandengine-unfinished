﻿/*Copyright 2018 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#include "../../rng.h"

const char *EDITOR_ADDON_RNG_CSTR = "RNG";
object *editorAddonRNGWindow = NULL;
gui_textfield *editorAddonRNGMin,*editorAddonRNGMax,*editorAddonRNGResult;
rng editorAddonRNGSeed;

void onEditorAddonRNGWindowClose(void *c) {
	editor_onCloseWindowClick(c);
	editorAddonRNGWindow = NULL;
}
void editor_addon_rng_genint() {
	list *mtxt = &editorAddonRNGMin->base.text;
	char *cstr = list_as_cstring(mtxt);
	int64_t min = atoll(cstr);
	list_clear_cstring(mtxt);
	mtxt = &editorAddonRNGMax->base.text;
	cstr = list_as_cstring(mtxt);
	int64_t max = atoll(cstr);
	list_clear_cstring(mtxt);

	int64_t res = rng_int64(&editorAddonRNGSeed,min,max);
	mtxt = &editorAddonRNGResult->base.text;
	if (mtxt->length < 64) {
		mtxt->length = 64;
		list_expand(mtxt);
	}
	mtxt->length = snprintf(mtxt->buffer,mtxt->bufferSize,"%"PRId64,res);
	SDL_SetClipboardText(mtxt->buffer);
	gui_text_updateText(&editorAddonRNGResult->base);
}
void editor_addon_rng_genreal() {
	list *mtxt = &editorAddonRNGMin->base.text;
	char *cstr = list_as_cstring(mtxt);
	double min = atof(cstr);
	list_clear_cstring(mtxt);
	mtxt = &editorAddonRNGMax->base.text;
	cstr = list_as_cstring(mtxt);
	double max = atof(cstr);
	list_clear_cstring(mtxt);

	double res = rng_double(&editorAddonRNGSeed,min,max);
	mtxt = &editorAddonRNGResult->base.text;
	if (mtxt->length < 64) {
		mtxt->length = 64;
		list_expand(mtxt);
	}
	mtxt->length = snprintf(mtxt->buffer,mtxt->bufferSize,"%g",res);
	SDL_SetClipboardText(mtxt->buffer);
	gui_text_updateText(&editorAddonRNGResult->base);	
}
void editor_addon_rng_open() {
	if (editorAddonRNGWindow != NULL) return;
	editorAddonRNGWindow = editor_createWindow((char*)EDITOR_ADDON_RNG_CSTR,(vec2){0.0f,0.0f},editorConstantPixelsToGUI((vec2){300.0f,200.0f}),&onEditorAddonRNGWindowClose);
	object *opane = ((object**)editorAddonRNGWindow->children.buffer)[1];
	gui_dragview *sview = ((gui_dragview**)opane->components.buffer)[0];
	sview->minX = sview->maxX = sview->minY = sview->maxY = 0.0f;

	list defMin = cstring_as_list("0"), defMax = cstring_as_list("100");
	editorAddonRNGMin = *(gui_textfield**)editor_createTextInput(opane,"Min",&defMin,EDITOR_STYLE_FOREGROUND, EDITOR_STYLE_COLOR, (vec3){-0.45f,0.5f,-editorWindowZDepthStep}, (vec2){0.3f,0.1f}, EDITOR_STYLE_FONT_SIZE)->components.buffer;
	editorAddonRNGMax = *(gui_textfield**)editor_createTextInput(opane,"Max",&defMax,EDITOR_STYLE_FOREGROUND, EDITOR_STYLE_COLOR, (vec3){0.45f,0.5f,-editorWindowZDepthStep}, (vec2){0.3f,0.1f}, EDITOR_STYLE_FONT_SIZE)->components.buffer;
	editorAddonRNGResult = *(gui_textfield**)editor_createTextInput(opane,"Value",NULL,EDITOR_STYLE_FOREGROUND, EDITOR_STYLE_COLOR, (vec3){0.2f,-0.5f,-editorWindowZDepthStep}, (vec2){0.6f,0.1f}, EDITOR_STYLE_FONT_SIZE)->components.buffer;
	editor_createButton(opane,"Random Int",EDITOR_STYLE_GOOD_BACKGROUND, EDITOR_STYLE_GOOD_FOREGROUND, (vec3){-0.4f,0.0f,-editorWindowZDepthStep}, (vec2){0.3f,0.1f}, EDITOR_STYLE_FONT_SIZE, &editor_addon_rng_genint);
	editor_createButton(opane,"Random Real",EDITOR_STYLE_GOOD_BACKGROUND, EDITOR_STYLE_GOOD_FOREGROUND, (vec3){0.4f,0.0f,-editorWindowZDepthStep}, (vec2){0.3f,0.1f}, EDITOR_STYLE_FONT_SIZE, &editor_addon_rng_genreal);
}
void editor_addon_rng_init() {
	editorAddonRNGSeed = new_rng(8);
	*(uint64_t*)editorAddonRNGSeed.buffer = timeNow();
	editor_addMenuButton((char*)EDITOR_ADDON_RNG_CSTR,&editor_addon_rng_open);
}

const engine_editor_addon editor_addon_rng = {editor_addon_rng_init,NULL,LAST_ENGINE_EDITOR_ADDON};

#undef LAST_ENGINE_EDITOR_ADDON
#define LAST_ENGINE_EDITOR_ADDON (void*)&editor_addon_rng
