﻿/*Copyright 2018 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#include "../../engine_serialization.h"

const char *EDITOR_ADDON_SCENE_SERIALIZATION_CSTR = "Scene Serialization";
object *editorAddonSceneSerializationWindow = NULL;
gui_textfield *editorAddonSceneSerializationPath;
gui_component *editorAddonSceneSerializationFixed;
list editorAddonSceneSerializationLastPath;

void onEditorAddonSceneSerializationSave(void *c) {
	bool fixed = editor_getToggleButtonState(editorAddonSceneSerializationFixed);
	list *str = &editorAddonSceneSerializationPath->base.text;
	object *target = targetEditorObject==NULL?&scene:targetEditorObject;
	file_stream fs;
	bool res = new_file_stream(&fs,list_as_cstring(str),FILE_MODE_WRITE);
	list_clear_cstring(str);
	if (!res) {
		printf("Failed to serialize engine object to file '%.*s'.\n",str->length,str->buffer);
		return;
	}
	if (fixed) {
		list clist = engine_listComponents();
		object_serializeFixed(target,&fs,&clist);
		delete_list(&clist);
	} else {
		object_serialize(target,&fs);
	}
	file_stream_close(&fs);
}
void onEditorAddonSceneSerializationLoad(void *c) {
	bool fixed = editor_getToggleButtonState(editorAddonSceneSerializationFixed);
	list *str = &editorAddonSceneSerializationPath->base.text;
	object *target = targetEditorObject==NULL?&scene:targetEditorObject;
	file_stream fs;
	bool res = new_file_stream(&fs,list_as_cstring(str),FILE_MODE_READ);
	list_clear_cstring(str);
	if (!res) {
		printf("Failed to deserialize engine object from file '%.*s'.\n",str->length,str->buffer);
		return;
	}
	object_deleteChildren(target);
	if (fixed) {
		list clist = engine_listComponents();
		object_deserializeFixed(target,&fs,&clist);
		delete_list(&clist);
	} else {
		char nbuf[256];
		object_deserialize(target,&fs,nbuf,256);
	}
	file_stream_close(&fs);
}

void onEditorAddonSceneSerializationWindowClose(void *c) {
	editorAddonSceneSerializationLastPath.length = 0;
	list *op = &editorAddonSceneSerializationPath->base.text;
	list_addm(&editorAddonSceneSerializationLastPath,op->buffer,op->length);
	editorAddonSceneSerializationWindow = NULL;
	editor_onCloseWindowClick(c);
}
void editor_addon_scene_serialization_open() {
	if (editorAddonSceneSerializationWindow != NULL) return;
	editorAddonSceneSerializationWindow = editor_createWindow(EDITOR_ADDON_SCENE_SERIALIZATION_CSTR,(vec2){0.0f,0.0f},editorConstantPixelsToGUI((vec2){300.0f,200.0f}),&onEditorAddonSceneSerializationWindowClose);
	object *opane = ((object**)editorAddonSceneSerializationWindow->children.buffer)[1];
	gui_dragview *sview = ((gui_dragview**)opane->components.buffer)[0];
	sview->minX = sview->maxX = sview->minY = sview->maxY = 0.0f;
	
	editorAddonSceneSerializationPath = *(void**)editor_createTextInput(opane,"File Path",&editorAddonSceneSerializationLastPath,EDITOR_STYLE_FOREGROUND, EDITOR_STYLE_COLOR, (vec3){0.0f,0.4f,-editorWindowZDepthStep}, (vec2){0.6f,0.1f}, EDITOR_STYLE_FONT_SIZE)->components.buffer;
	editorAddonSceneSerializationFixed = editor_createButton(opane,"Fixed",EDITOR_STYLE_BAD_BACKGROUND, EDITOR_STYLE_BAD_FOREGROUND, (vec3){0.8f,0.4f,-editorWindowZDepthStep}, (vec2){0.17f,0.1f}, EDITOR_STYLE_FONT_SIZE, &editor_toggleButtonCallback)->components.buffer;
	editor_createButton(opane,"Save",EDITOR_STYLE_GOOD_BACKGROUND, EDITOR_STYLE_GOOD_FOREGROUND, (vec3){-0.4f,-0.4f,-editorWindowZDepthStep}, (vec2){0.25f,0.1f}, EDITOR_STYLE_FONT_SIZE, &onEditorAddonSceneSerializationSave);
	editor_createButton(opane,"Load",EDITOR_STYLE_GOOD_BACKGROUND, EDITOR_STYLE_GOOD_FOREGROUND, (vec3){0.4f,-0.4f,-editorWindowZDepthStep}, (vec2){0.25f,0.1f}, EDITOR_STYLE_FONT_SIZE, &onEditorAddonSceneSerializationLoad);
}
void editor_addon_scene_serialization_init() {
	new_list(&editorAddonSceneSerializationLastPath,1);
	const char *defPath = "scene.object";
	list_addm(&editorAddonSceneSerializationLastPath,defPath,strlen(defPath));
	editor_addMenuButton(EDITOR_ADDON_SCENE_SERIALIZATION_CSTR,&editor_addon_scene_serialization_open);
}

const engine_editor_addon editor_addon_scene_serialization = {editor_addon_scene_serialization_init,NULL,LAST_ENGINE_EDITOR_ADDON};

#undef LAST_ENGINE_EDITOR_ADDON
#define LAST_ENGINE_EDITOR_ADDON (void*)&editor_addon_scene_serialization
