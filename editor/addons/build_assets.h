﻿/*Copyright 2018 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#include "../../buildassets.h"

const char *EDITOR_ADDON_BUILD_ASSETS_CSTR = "Build Assets";
object *editorAddonBuildAssetsWindow = NULL;
gui_textfield *editorAddonBuildAssetsPath;
gui_text *editorAddonBuildAssetsState;
list editorAddonBuildAssetsLastPath;

void onEditorAddonBuildAssetsWindowClose(void *c) {
	editorAddonBuildAssetsLastPath.length = 0;
	list *op = &editorAddonBuildAssetsPath->base.text;
	list_addm(&editorAddonBuildAssetsLastPath,op->buffer,op->length);
	editorAddonBuildAssetsWindow = NULL;
	editor_onCloseWindowClick(c);
}
void editor_addon_build_assetsf() {
	char *pcstr = list_as_cstring(&editorAddonBuildAssetsPath->base.text);
	assets_build(pcstr,"./ap");
	list_clear_cstring(&editorAddonBuildAssetsPath->base.text);
}
void editor_addon_build_assets_open() {
	if (editorAddonBuildAssetsWindow != NULL) return;
	editorAddonBuildAssetsWindow = editor_createWindow((char*)EDITOR_ADDON_BUILD_ASSETS_CSTR,(vec2){0.0f,0.0f},editorConstantPixelsToGUI((vec2){300.0f,200.0f}),&onEditorAddonBuildAssetsWindowClose);
	object *opane = ((object**)editorAddonBuildAssetsWindow->children.buffer)[1];
	gui_dragview *sview = ((gui_dragview**)opane->components.buffer)[0];
	sview->minX = sview->maxX = sview->minY = sview->maxY = 0.0f;
	editorAddonBuildAssetsPath = *(gui_textfield**)editor_createTextInput(opane,"File Path",&editorAddonBuildAssetsLastPath,EDITOR_STYLE_FOREGROUND, EDITOR_STYLE_COLOR, (vec3){0.15f,0.33f,-editorWindowZDepthStep}, (vec2){0.8f,0.1f}, EDITOR_STYLE_FONT_SIZE)->components.buffer;
	editorAddonBuildAssetsState = ((gui_text**)editor_createButton(opane,(char*)EDITOR_ADDON_BUILD_ASSETS_CSTR,EDITOR_STYLE_GOOD_BACKGROUND, EDITOR_STYLE_GOOD_FOREGROUND, (vec3){0.0f,-0.33f,-editorWindowZDepthStep}, (vec2){0.3f,0.14f}, EDITOR_STYLE_FONT_SIZE, &editor_addon_build_assetsf)->components.buffer)[1];
}
void editor_addon_build_assets_init() {
	new_list(&editorAddonBuildAssetsLastPath,1);
	const char *defPath = "../assets/";
	list_addm(&editorAddonBuildAssetsLastPath,defPath,strlen(defPath));
	editor_addMenuButton((char*)EDITOR_ADDON_BUILD_ASSETS_CSTR,&editor_addon_build_assets_open);
}

const engine_editor_addon editor_addon_build_assets = {editor_addon_build_assets_init,NULL,LAST_ENGINE_EDITOR_ADDON};

#undef LAST_ENGINE_EDITOR_ADDON
#define LAST_ENGINE_EDITOR_ADDON (void*)&editor_addon_build_assets
