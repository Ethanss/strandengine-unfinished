/*Copyright 2018 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

const char *EDITOR_ADDON_TEXT_HASH_CSTR = "TextToHash";
object *editorAddonTextHashWindow = NULL;
gui_textfield *editorAddonTextHashTxt, *editorAddonTextHashHash;

void onEditorAddonTextHashWindowClose(void *c) {
	editor_onCloseWindowClick(c);
	editorAddonTextHashWindow = NULL;
}
void editor_addon_string_to_hashf() {
	list *ts = &editorAddonTextHashTxt->base.text;
	int32_t hv = hash(ts->buffer,ts->length);
	ts = &editorAddonTextHashHash->base.text;
	if (ts->bufferSize < 32) {
		ts->length = 32;
		list_expand(ts);
	}
	ts->length = snprintf(ts->buffer,ts->bufferSize,"%d",hv);
	SDL_SetClipboardText(ts->buffer);
	gui_text_updateText(&editorAddonTextHashHash->base);
}
void editor_addon_string_to_hash_open() {
	if (editorAddonTextHashWindow != NULL) return;
	editorAddonTextHashWindow = editor_createWindow((char*)EDITOR_ADDON_TEXT_HASH_CSTR,(vec2){0.0f,0.0f},editorConstantPixelsToGUI((vec2){300.0f,200.0f}),&onEditorAddonTextHashWindowClose);
	object *opane = ((object**)editorAddonTextHashWindow->children.buffer)[1];
	gui_dragview *sview = ((gui_dragview**)opane->components.buffer)[0];
	sview->minX = sview->maxX = sview->minY = sview->maxY = 0.0f;

	editor_createButton(opane,"Hash",EDITOR_STYLE_GOOD_BACKGROUND, EDITOR_STYLE_GOOD_FOREGROUND, (vec3){-0.8f,-0.33f,-editorWindowZDepthStep}, (vec2){0.15f,0.1f}, EDITOR_STYLE_FONT_SIZE, &editor_addon_string_to_hashf);
	editorAddonTextHashHash = *(gui_textfield**)editor_createTextInput(opane,"",NULL,EDITOR_STYLE_FOREGROUND, EDITOR_STYLE_COLOR, (vec3){0.15f,-0.33f,-editorWindowZDepthStep}, (vec2){0.8f,0.1f}, EDITOR_STYLE_FONT_SIZE)->components.buffer;
	editorAddonTextHashTxt = *(gui_textfield**)editor_createTextInput(opane,"Text",NULL,EDITOR_STYLE_FOREGROUND, EDITOR_STYLE_COLOR, (vec3){0.15f,0.33f,-editorWindowZDepthStep}, (vec2){0.8f,0.1f}, EDITOR_STYLE_FONT_SIZE)->components.buffer;
}
void editor_addon_string_to_hash_init() {
	editor_addMenuButton((char*)EDITOR_ADDON_TEXT_HASH_CSTR,&editor_addon_string_to_hash_open);
}

const engine_editor_addon editor_addon_string_to_hash = {editor_addon_string_to_hash_init,NULL,LAST_ENGINE_EDITOR_ADDON};

#undef LAST_ENGINE_EDITOR_ADDON
#define LAST_ENGINE_EDITOR_ADDON (void*)&editor_addon_string_to_hash
