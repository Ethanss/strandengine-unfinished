﻿/*Copyright 2018 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#define TRANSFORM_EDIT_H_

#define editor_addon_transform_axis_count 4
uint32_t editor_addon_transform_edit_mode = 0;
float editor_addon_transform_viewAcceleration = 1.0f, editor_addon_transform_objectMovementScale = 1.0f;
bool editor_addon_transform_edit_enabled = false;
object *editor_addon_transform_axis_buttons[editor_addon_transform_axis_count], *editor_addon_transform_currentObject = NULL;
gui_text *editor_addon_transform_edit_text;
const char *editor_addon_transform_edit_strings[7] = {
"Transform screen position",
"Transform screen rotation",
"Transform screen scale",
"Transform world position",
"Transform world rotation",
"Transform world scale",
"Transform off"};

void editor_addon_transform_edit_updateButtonPositions() {
	const vec2 scaleBase = editorConstantPixelsToGUI((vec2){EDITOR_STYLE_MENUBAR_SIZE*4.0f,EDITOR_STYLE_MENUBAR_SIZE*2.0f});
	vec3 buttonPositions[editor_addon_transform_axis_count] = {(vec3){scaleBase.x,0.0f,0.0f},(vec3){0.0f,0.0f,0.0f},(vec3){-scaleBase.x,0.0f,0.0f},(vec3){0.0f,-scaleBase.y,0.0f}};
	bool worldSpace = editor_addon_transform_edit_mode>2;
	for (uint32_t i = 0; i < editor_addon_transform_axis_count; i++) {
		object *obtn = editor_addon_transform_axis_buttons[i];
		obtn->position = buttonPositions[i];
#ifdef _RENDERING_H_
		if (worldSpace) {
			vec3 sp = lastSelectedEditorObject->absolutePosition, rot = editorRendererViewRotation;
			vec3_sub(&sp,&editorRendererViewPosition);
			//vec3_mulSingle(&rot,-1.0f);
			vec3_rotInv(&sp,&rot);
			if (sp.z > 1e-4f) {
				vec2_mulSingle((vec2*)&sp,1.0f/sp.z);
				sp.x *= engineWindow.aspect;
				vec2_add((vec2*)&obtn->position,(vec2*)&sp);
			} else {
				obtn->position.x = 1e4f;
			}
			editor_addon_transform_objectMovementScale = sp.z;
		} else {
			vec2_add((vec2*)&obtn->position,(vec2*)&lastSelectedEditorObject->absolutePosition);
			obtn->position.x *= engineWindow.aspect;
		}
#else
		vec2_add((vec2*)&obtn->position, (vec2*)&lastSelectedEditorObject->absolutePosition);
#endif
		object_updateTransform(obtn);
	}
}

void editor_addon_transform_edit_switch() {
	uint32_t oldMode = editor_addon_transform_edit_mode;
	editor_addon_transform_edit_mode = (editor_addon_transform_edit_mode+1)%7;
	#ifdef _RENDERING_H_
	if (oldMode < 3 && editor_addon_transform_edit_mode > 2) {
		if (screenRenderer == NULL) {
			editorRendererViewPosition = (vec3){0.0f,0.0f,0.0f};
			editorRendererViewRotation = (vec3){0.0f,0.0f,0.0f};
		} else {
			object *sro = screenRenderer->base.object;
			editorRendererViewPosition = sro->absolutePosition;
			editorRendererViewRotation = sro->absoluteRotation;
		}
		editorRendererViewEnabled = true;
	} else if (editor_addon_transform_edit_mode == 6) {
		editorRendererViewEnabled = false;
	}
	#endif
	editor_addon_transform_edit_text->text = cstring_as_list(editor_addon_transform_edit_strings[editor_addon_transform_edit_mode]);
	gui_text_updateText(editor_addon_transform_edit_text);
	
	if (editor_addon_transform_edit_enabled) editor_addon_transform_edit_updateButtonPositions();
}

void editor_addon_transform_edit_update(void UNUSED *unu) {
	bool on;
	if (editor_addon_transform_edit_mode != 6 && lastSelectedEditorObject != NULL) {
		if (editor_addon_transform_edit_mode > 2) {
			#ifdef _RENDERING_H_
			on = screenRenderer!=NULL;
			#else
			on = false;
			#endif
		} else on = true;
	} else on = false;
	if (editor_addon_transform_edit_enabled != on) {
		editor_addon_transform_edit_enabled = on;
		for (uint32_t i = 0; i < editor_addon_transform_axis_count; i++) editor_object_setActive(editor_addon_transform_axis_buttons[i],on);
		//editor_addon_transform_edit_updateButtonPositions();
	}
	
	/*if (on) {
		if (lastSelectedEditorObject != editor_addon_transform_currentObject) {

			editor_addon_transform_currentObject = lastSelectedEditorObject;
		}
	}*/
	
	#ifdef _RENDERING_H_
	if (editor_addon_transform_edit_mode != 6 && editor_addon_transform_edit_mode > 2) {
		if (mouse[2]) {
			editorRendererViewRotation.x += mouseDeltaX*0.01f;
			editorRendererViewRotation.y += mouseDeltaY*-0.01f;
			editorRendererViewRotation.y = clamp(editorRendererViewRotation.y,-1.5f,1.5f);
			
			float move = (mouse[0]?1.0f:0.0f)+(mouse[1]?-1.0f:0.0f);
			if (move != 0.0f) {
				editor_addon_transform_viewAcceleration += deltaTime*editor_addon_transform_viewAcceleration;
				vec3 fwd = (vec3){0.0f,0.0f,move}, rot = editorRendererViewRotation;
				vec3_rot(&fwd,&rot);
				vec3_mulSingle(&fwd,deltaTime*editor_addon_transform_viewAcceleration);
				vec3_add(&editorRendererViewPosition,&fwd);
			} else {
				editor_addon_transform_viewAcceleration = 1.0f;
			}
			
			//if (on) editor_addon_transform_edit_updateButtonPositions();
		}
	}
	#endif
	if (on) editor_addon_transform_edit_updateButtonPositions();
}

void editor_addon_transform_edit_drag(void *p) {
	gui_component *guic = p;
	object *obj = guic->base.object;
	bool worldSpace = editor_addon_transform_edit_mode>2;
	uint32_t axisId,compId = editor_addon_transform_edit_mode%3;
	for (uint32_t i = 0; i < editor_addon_transform_axis_count; i++) {
		if (editor_addon_transform_axis_buttons[i] == obj) {
			axisId = i;
			break;
		}
	}
	
	vec3 *comp = &lastSelectedEditorObject->position+compId;
#ifdef _RENDERING_H_
	if (worldSpace && compId == 0) {
		vec3 md = (vec3){0.0f,0.0f,0.0f},rot = editorRendererViewRotation;
		//vec3_mulSingle(&rot,1.0f);
		if (axisId == 0 || axisId == 3) md.x = mouseDeltaX*engineWindow.oneDivWidth/engineWindow.aspect*2.0f*editor_addon_transform_objectMovementScale;
		else md.x = 0.0f;
		if (axisId == 1 || axisId == 3) md.y = mouseDeltaY*engineWindow.oneDivHeight*-2.0f*editor_addon_transform_objectMovementScale;
		else md.y = 0.0f;
		if (axisId == 2) md.z = mouseDeltaX*engineWindow.oneDivWidth*2.0f;
		else md.z = 0.0f;
		vec3_rot(&md,&rot);
		vec3_add(comp,&md);
	} else {
#else
	{
#endif
		if (compId == 1) {
			comp->z += mouseDeltaX*engineWindow.oneDivWidth*6.28f/engineWindow.aspect;
		} else {
			if (axisId == 0 || axisId == 3) comp->x += mouseDeltaX*engineWindow.oneDivWidth*2.0f/engineWindow.aspect/lastSelectedEditorObject->parent->absoluteScale.x;
			if (axisId == 1 || axisId == 3) comp->y += mouseDeltaY*engineWindow.oneDivHeight*-2.0f/lastSelectedEditorObject->parent->absoluteScale.y;
		}
	}
	object_updateTransform(lastSelectedEditorObject);
	editor_addon_transform_edit_updateButtonPositions();
}

void editor_addon_transform_edit_init() {
	editor_addon_transform_edit_text = editor_addMenuButton(editor_addon_transform_edit_strings[0],&editor_addon_transform_edit_switch);

	const char *axisNames[editor_addon_transform_axis_count] = {"X","Y","Z","O"};
	const vec4 axisColors[editor_addon_transform_axis_count] = {(vec4){1.0f,0.4f,0.1f,0.6f},(vec4){0.1f,0.4f,1.0f,0.6f},(vec4){1.0f,0.0f,1.0f,0.6f},(vec4){0.5f,0.5f,0.5f,0.6f}};
	
	const vec2 scaleBase = editorConstantPixelsToGUI((vec2){EDITOR_STYLE_MENUBAR_SIZE,EDITOR_STYLE_FONT_SIZE});
	vec2 buttonScales[editor_addon_transform_axis_count] = {(vec2){scaleBase.x,scaleBase.y},(vec2){scaleBase.x,scaleBase.y},(vec2){scaleBase.x,scaleBase.y},(vec2){scaleBase.x,scaleBase.y}};
	
	vec4 fntColor = (vec4){0.0f,0.0f,0.0f,1.0f};
	
	for (uint32_t i = 0; i < editor_addon_transform_axis_count; i++) {
		object *no;
		editor_addon_transform_axis_buttons[i] = no = editor_createButton(engineEditorGUI,axisNames[i],axisColors[i],fntColor,(vec3){0.0f,0.0f,0.0f},buttonScales[i],EDITOR_STYLE_FONT_SIZE,NULL);
		gui_component *gc = *((gui_component**)no->components.buffer);
		gc->drag = editor_addon_transform_edit_drag;
		editor_object_setActive(no,false);
	}
	
	cpifunction(editor_addon_transform_edit_update,NULL,slist_add(&engineEditorUpdateCalls,&ifunc));
}

const engine_editor_addon editor_addon_transform_edit = {editor_addon_transform_edit_init,NULL,LAST_ENGINE_EDITOR_ADDON};

#undef LAST_ENGINE_EDITOR_ADDON
#define LAST_ENGINE_EDITOR_ADDON (void*)&editor_addon_transform_edit
