﻿/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _ENGINE_EDITOR_H_
#define _ENGINE_EDITOR_H_

#include "../components/gui/all.h"
#include <inttypes.h>

#define MENUBAR_MAX_WIDTH 10.0f

bool engineEditorPaused = true;
font engineEditorFont;

//custom object/component functions for editor
void editor_component_enable(component *c) {
	if (c->enabled) return;
	c->enabled = true;

	if (c->enable != NULL) c->enable(c);

	ifunction ifunc;
	ifunc.owner = c;
	#define aif(s,d) if (s != NULL) {ifunc.delegate = s; slist_add(&d,&ifunc);}
	aif(c->update,engineEditorUpdateCalls);
	aif(c->render,engineEditorRenderCalls);
	aif(c->transformUpdate, c->object->transformUpdateCalls);
	#undef aif
}
void editor_component_disable(component *c) {
	if (!c->enabled) return;
	c->enabled = false;

	ifunction ifunc;
	ifunc.owner = c;
	#define rif(s,d) if (s != NULL) {ifunc.delegate = s;slist_remove(&d,&ifunc);}
	rif(c->update,engineEditorUpdateCalls);
	rif(c->render,engineEditorRenderCalls);
	rif(c->transformUpdate, c->object->transformUpdateCalls);
	#undef rif

	if (c->disable != NULL) c->disable(c);
}
void editor_component_free(component *c) {
	editor_component_disable(c);
	if (c->free != NULL) syncedCall(*c->free,c);
}

void editor_free_object(object *o) {
	//delete components
	uint32_t nc = o->components.length;
	component** comps = o->components.buffer;
	for (uint32_t i = 0; i < nc; i++) editor_component_free(*comps++);

	//delete children
	nc = o->children.length;
	object** children = o->children.buffer;
	for (uint32_t i = 0; i < nc; i++) editor_free_object(*children++);

	syncedCall(free_object_callback, o);
}

static inline void editor_delete_object(object *o) {
	editor_free_object(o);
	list_remove(&o->parent->children, &o);
	syncedCall(free,o);
}

void editor_object_deleteChildren(object *o) {
	uint32_t nc = o->children.length;
	object** children = o->children.buffer;
	for (uint32_t i = 0; i < nc; i++) {
		object *cc = *children;
		editor_free_object(cc);
		syncedCall(free,cc);
		children++;
	}

	o->children.length = 0;
}

static inline void editor_object_addComponent(object *o, void *p) {
	component *c = p;
	list_add(&o->components, &c);
	c->object = o;

	if (o->absoluteActive && c->active) {
		editor_component_enable(c);
	}
}

void editor_object_setAbsoluteActive(object *o, bool aact) {
	//enable/disable components with new active state
	component **c = o->components.buffer;
	for (uint32_t i = 0; i < o->components.length; i++) {
		if (c[0]->active && aact) editor_component_enable(*c);
		else editor_component_disable(*c);
		c++;
	}

	//update childrens absolute active state
	object **q = o->children.buffer;
	for (uint32_t i = 0; i < o->children.length; i++) {
		editor_object_setAbsoluteActive(*q, q[0]->active & aact);
		q++;
	}

	o->absoluteActive = aact;
}
static inline void editor_object_setActive(object *o, bool act) {
	o->active = act;
	editor_object_setAbsoluteActive(o, act&o->parent->active);
}

void editor_object_setParent(object *o, object *p) {
	if (o->parent != NULL) list_remove(&o->parent->children, &o);
	o->parent = p;
	list_add(&p->children, &o);

	editor_object_setActive(o, o->active);
	object_updateTransform(o);
}

object engineEditorScene, *engineEditorGUI, *engineEditorMenuBar;

void editor_new_object(object *o) {
	init_object(o);
	o->parent = &engineEditorScene;
	list_add(&engineEditorScene.children, &o);
}
static inline object *editor_create_object() {
	object *o = malloc(sizeof(object));
	editor_new_object(o);
	return o;
}




//editor code
object *editorObjectsWindow = NULL, *editorObjectWindow = NULL, *targetEditorObject = NULL, *draggingEditorObject = NULL, *editorObjectDragIcon, *lastSelectedEditorObject = NULL;
gui_component *draggingEditorObjectButton;
gui_textfield *editorObjectTransformTextFields[10];
float editorObjectTransformUpdateCount = 0.0f, editorBottomMenubarSize = 0.0f;
float editorWindowZCounter = 0.0f;
#define editorWindowZDepthStep 2e-4f
#define editorObjectViewComponentSeperator 29

void onEditorRefreshClick(void UNUSED *c);
void onEditorRefreshObjectClick(void UNUSED *c);

object *editorFindObjectFromHierarchy(object *lfor) {
	//get engine object from editor list object via hierarchy trace
	list rbuf;
	new_list(&rbuf,4);
	object *bas = ((object**)editorObjectsWindow->children.buffer)[1], **basch = bas->children.buffer, *cpo = &scene;
	uint32_t nc = bas->children.length, count = 0;
	float tx = lfor->position.x, lx = nc!=0?basch[0]->position.x:0.0f, sepSz = 0.0f;
	bool first = true;
	for (uint32_t i = 0; i < nc; i++) {
		object *co = *basch;
		
		float x = co->position.x;
		if (x == lx) count++;
		else {
			if (x <= tx) {
				if (first) {
					count--;
					first = false;
				}
				if (x > lx) {
					if (sepSz == 0.0f) sepSz = x-lx;
					const uint32_t tc = count+1;
					list_add(&rbuf,&tc);
					cpo = ((object**)cpo->children.buffer)[count];
					count = 0;
				} else {
					const uint32_t stimes = (uint32_t)((lx-x)/sepSz+0.5f);
					for (uint32_t k = 0; k < stimes; k++) cpo = cpo->parent;
					rbuf.length -= stimes;
					count = ((uint32_t*)rbuf.buffer)[rbuf.length];
				}
				lx = x;
			}
		}

		if (co == lfor) {
			if (first) {
				count--;
				first = false;
			}
			delete_list(&rbuf);
			return ((object**)cpo->children.buffer)[count];
		}

		basch++;
	}
	delete_list(&rbuf);
	return NULL;
}
void engineEditorUpdate(void UNUSED *arg) {
	if (targetEditorObject != NULL) {
		editorObjectTransformUpdateCount += deltaTime;
		if (editorObjectTransformUpdateCount > 0.2f) {
			//update target object transform
			float *tv = &targetEditorObject->position.x;
			for (uint32_t i = 0; i < 9; i++) {
				gui_textfield *tf = editorObjectTransformTextFields[i];
				if (tf->base.updatedText && !tf->base.base.culled) {
					if (tf->selected) {
						float ov = *tv;
						*tv = atof(list_as_cstring(&tf->base.text));
						list_clear_cstring(&tf->base.text);
						if (*tv != ov) object_updateTransform(targetEditorObject);
					} else {
						if (tf->base.text.bufferSize < 64) {
							tf->base.text.length = 60;
							list_expand(&tf->base.text);
						}
						tf->base.text.length = snprintf(tf->base.text.buffer, tf->base.text.bufferSize, "%.7g", *tv);
						gui_text_updateText(&tf->base);
					}
				}
				tv++;
			}
			gui_textfield *tf = editorObjectTransformTextFields[9];
			if (tf->base.updatedText && !tf->base.base.culled) {
				if (tf->selected) {
					int32_t lid = atoi(list_as_cstring(&tf->base.text));
					targetEditorObject->layer = clamp(lid,0,31);
					list_clear_cstring(&tf->base.text);
				} else {
					if (tf->base.text.bufferSize < 8) {
						tf->base.text.length = 7;
						list_expand(&tf->base.text);
					}
					uint32_t bc = targetEditorObject->layer;
					tf->base.text.length = snprintf(tf->base.text.buffer,tf->base.text.bufferSize,"%d",bc);
					gui_text_updateText(&tf->base);
				}
			}

			//update component fields
			object *opane = ((object**)editorObjectWindow->children.buffer)[1], **cpane = opane->children.buffer;
			cpane += editorObjectViewComponentSeperator;
			component **comps = targetEditorObject->components.buffer;
			for (uint32_t i = 0; i < targetEditorObject->components.length; i++) {
				object **cpaneo = cpane[0]->children.buffer;
				cpaneo += 2;
				component_data *md = comps[0]->metadata;
				reflection_data *fd = md->fields;
				for (uint32_t f = 0; f < md->fieldCount; f++) {
					gui_textfield *fin = *(gui_textfield**)cpaneo[0]->components.buffer;
					if (fin->base.updatedText && !fin->base.base.culled) {
						list *fit = &fin->base.text;
						uint8_t ftype = fd->type, *fptr = (uint8_t*)comps[0]+fd->offset;
						if (fin->selected) {
							char *cstr;
							if (ftype < REFLECTION_TYPE_list) cstr = list_as_cstring(fit);
							if (ftype < REFLECTION_TYPE_float) {
								int64_t buf = atoll(cstr);
								switch (ftype) {
									case REFLECTION_TYPE_int8_t:*(int8_t*)fptr = buf;break;
									case REFLECTION_TYPE_int16_t:*(int16_t*)fptr = buf;break;
									case REFLECTION_TYPE_int32_t:*(int32_t*)fptr = buf;break;
									case REFLECTION_TYPE_int64_t:*(int64_t*)fptr = buf;break;
								}
							} else if (ftype < REFLECTION_TYPE_double) {
								*(float*)fptr = atof(cstr);
							} else if (ftype < REFLECTION_TYPE_float) {
								*(double*)fptr = atof(cstr);
							} else {
								list_copy((void*)fptr,fit);
							}
							if (ftype < REFLECTION_TYPE_list) list_clear_cstring(fit);
						} else {
							if (ftype < REFLECTION_TYPE_list) {
								if (fit->bufferSize < 64) {
									fit->length = 60;
									list_expand(fit);
								}
							}
							if (ftype < REFLECTION_TYPE_float) {
								int64_t buf = 0;
								switch (ftype) {
									case REFLECTION_TYPE_int8_t:buf = *(int8_t*)fptr;break;
									case REFLECTION_TYPE_int16_t:buf = *(int16_t*)fptr;break;
									case REFLECTION_TYPE_int32_t:buf = *(int32_t*)fptr;break;
									case REFLECTION_TYPE_int64_t:buf = *(int64_t*)fptr;break;
								}
								fit->length = snprintf(fit->buffer,fit->bufferSize,"%"PRId64,buf);	
							} else if (ftype < REFLECTION_TYPE_double) {
								fit->length = snprintf(fit->buffer,fit->bufferSize,"%g",*(float*)fptr);
							} else if (ftype < REFLECTION_TYPE_float) {
								fit->length = snprintf(fit->buffer,fit->bufferSize,"%g",*(double*)fptr);
							} else {
								list_copy(fit,(void*)fptr);
							}
							gui_text_updateText(&fin->base);
						}
					}
					cpaneo += 2;
					fd++;
				}
				comps++;
				cpane++;
			}
			
			editorObjectTransformUpdateCount = 0.0f;
		}
	}
	
	if (draggingEditorObject != NULL && editorObjectsWindow != NULL) {
		//object dragging + re-parenting
		if (mouse[0]) {
			object *po = editorObjectDragIcon->parent;
			editorObjectDragIcon->position.x = (nMouseX/engineWindow.aspect-po->absolutePosition.x)/po->absoluteScale.x;
			editorObjectDragIcon->position.y = (nMouseY-po->absolutePosition.y)/po->absoluteScale.y;
			object_updateTransform(editorObjectDragIcon);
		} else {
			object *dragObj = draggingEditorObject;
			draggingEditorObject = NULL;
			editor_object_setActive(editorObjectDragIcon,false);

			object *opane = ((object**)editorObjectsWindow->children.buffer)[1], *hov = NULL,
			**och = opane->children.buffer;
			const vec2 mc = (vec2){nMouseX/engineWindow.aspect,nMouseY};
			if (!gui_component_intersectsPoint(draggingEditorObjectButton,mc)) {
				const uint32_t onc = opane->children.length;
				for (uint32_t i = 0; i < onc; i++) {
					if (gui_component_intersectsPoint(((gui_component**)och[0]->components.buffer)[0],mc)) {
						hov = och[0];
						break;
					}
					och++;
				}
			}

			if (hov != NULL) {
				object *rpo = editorFindObjectFromHierarchy(hov);
				if (rpo != dragObj) {
					if (rpo == NULL) rpo = &scene;
					if (!object_isChildBelow(dragObj,rpo)) {
						object_setParent(dragObj,rpo);
						syncedCall(onEditorRefreshClick,NULL);
					}
				}
			}
		}
	}
	
	if (engineEditorMenuBar->position.y > -1.0f) {
		//menubar side scrolling
		if (nMouseX > 0.97f && engineEditorMenuBar->position.x > editorBottomMenubarSize) {
			engineEditorMenuBar->position.x -= deltaTime*3.0f;
			object_updateTransform(engineEditorMenuBar);
		}
		else if (nMouseX < -0.97f && engineEditorMenuBar->position.x < 0.0f) {
			engineEditorMenuBar->position.x += deltaTime*3.0f;
			object_updateTransform(engineEditorMenuBar);
		}
	}
}

#define editor_getToggleState(b) vec4_equals(&(b)->color,&)
bool editor_getToggleButtonState(gui_component *b) {
	vec4 cmp = EDITOR_STYLE_GOOD_BACKGROUND;
	return vec4_equals(&b->color,&cmp);
}
void editor_toggleButtonCallback(void *p) {
	gui_component *b = p;
	b->color = editor_getToggleButtonState(b)?EDITOR_STYLE_BAD_BACKGROUND:EDITOR_STYLE_GOOD_BACKGROUND;
	gui_component_transformUpdate(b);
}

vec2 editorConstantPixelsToGUI(const vec2 p) {
	return (vec2){p.x*engineWindow.oneDivWidth,p.y*engineWindow.oneDivHeight};
}

gui_text *editor_addMenuButton(char *name, one_arg_function onClick) {
	list txtBuf = cstring_as_list(name);
	float lineWidth = gui_text_size(EDITOR_STYLE_FONT_SIZE, &engineEditorFont, (vec2){2.0f,2.0f}, &txtBuf, 1.0f, NULL, NULL, false, NULL, (vec2){0.0f,0.0f}, NULL)+EDITOR_STYLE_FONT_SIZE*2.0f*engineWindow.oneDivWidth/engineWindow.aspect;

	object *obj = editor_create_object();
	obj->scale.y = 0.95f;
	obj->scale.x = lineWidth*0.5f/MENUBAR_MAX_WIDTH;
	uint32_t nchild = engineEditorMenuBar->children.length;
	if (nchild > 0) {
		object *last = ((object**)engineEditorMenuBar->children.buffer)[nchild-1];
		obj->position.x = last->position.x+last->scale.x+obj->scale.x+10.0f*engineWindow.oneDivWidth/MENUBAR_MAX_WIDTH;
	} else {
		obj->position.x = -1.0f/MENUBAR_MAX_WIDTH+obj->scale.x+10.0f*engineWindow.oneDivWidth/MENUBAR_MAX_WIDTH;
	}
	editorBottomMenubarSize = obj->position.x*-MENUBAR_MAX_WIDTH+0.75f;
	obj->position.z = -editorWindowZDepthStep;
	editor_object_setParent(obj,engineEditorMenuBar);

	gui_component *bc = create_gui_component();
	bc->color = EDITOR_STYLE_FOREGROUND;
	bc->interactable = true;
	bc->click = onClick;
	editor_object_addComponent(obj,bc);

	gui_text *tc = create_gui_text();
	tc->anchor = ANCHOR_NONE;
	tc->font = &engineEditorFont;
	tc->fontSize = EDITOR_STYLE_FONT_SIZE;
	tc->color = EDITOR_STYLE_COLOR;
	tc->text = txtBuf;
	EDITOR_STYLE_TEXT(tc);
	editor_object_addComponent(obj,tc);
	
	EDITOR_STYLE_BUTTON(obj);
	
	return tc;
}


void editor_onCloseWindowClick(void *p) {
	component *c = p;
	editor_delete_object(c->object->parent);
}
object *editor_createTextInput(object *parent, char *txt, list *defTxt, const vec4 bgColor, const vec4 fontColor, const vec3 pos, const vec2 sz, const float fsize) {
	object *to = editor_create_object();
	to->position = pos;
	*(vec2*)&to->scale = sz;
	editor_object_setParent(to,parent);

	gui_textfield *gtf = create_gui_textfield();
	gtf->base.font = &engineEditorFont;
	gtf->base.fontSize = fsize;
	gtf->base.color = fontColor;
	gtf->base.base.color = bgColor;
	gtf->multiline = true;
	EDITOR_STYLE_TEXT((&gtf->base));
	if (defTxt != NULL) list_copy(&gtf->base.text, defTxt);
	editor_object_addComponent(to, gtf);

	object *lo = editor_create_object();
	lo->position = pos;
	lo->position.x -= 1.0f+sz.x;
	editor_object_setParent(lo,parent);

	gui_text *nt = create_gui_text();
	nt->font = &engineEditorFont;
	nt->fontSize = fsize;
	nt->color = fontColor;
	nt->anchor = ANCHOR_RIGHT;
	nt->text = cstring_as_list(txt);
	EDITOR_STYLE_TEXT(nt);
	editor_object_addComponent(lo,nt);

	EDITOR_STYLE_TEXTINPUT(to);

	return to;
}
object *editor_createButton(object *parent, char *txt, const vec4 bgColor, const vec4 fontColor, const vec3 pos, const vec2 size, const float fsize, one_arg_function onClick) {
	object *bo = editor_create_object();
	bo->position = pos;
	*(vec2*)&bo->scale = size;
	editor_object_setParent(bo,parent);

	gui_component *btn = create_gui_component();
	btn->interactable = true;
	btn->click = onClick;
	btn->color = bgColor;
	editor_object_addComponent(bo,btn);

	gui_text* nt = create_gui_text();
	nt->font = &engineEditorFont;
	nt->fontSize = fsize;
	nt->color = fontColor;
	nt->anchor = ANCHOR_NONE;
	nt->text = cstring_as_list(txt);
	EDITOR_STYLE_TEXT(nt);
	editor_object_addComponent(bo,nt);

	EDITOR_STYLE_BUTTON(bo);

	return bo;
}
object *editor_createWindow(char *name, vec2 pos, vec2 size, one_arg_function onClose) {
	object *no = editor_create_object();
	*((vec2*)&no->position) = pos;
	editorWindowZCounter -= editorWindowZDepthStep*4.0f;
	if (editorWindowZCounter <= -0.95f) editorWindowZCounter = 0.0f;
	no->position.z = editorWindowZCounter;
	*((vec2*)&no->scale) = size;
	editor_object_setParent(no, engineEditorGUI);

	gui_draggable *bg = create_gui_draggable();
	bg->base.color = EDITOR_STYLE_FOREGROUND;
	editor_object_addComponent(no,bg);

	gui_text *nt = create_gui_text();
	nt->font = &engineEditorFont;
	nt->fontSize = EDITOR_STYLE_FONT_SIZE;
	nt->color = EDITOR_STYLE_COLOR;
	EDITOR_STYLE_TEXT(nt);
	list clist = cstring_as_list(name);
	list_copy(&nt->text, &clist);
	editor_object_addComponent(no,nt);

	//float msz = EDITOR_STYLE_MENUBAR_SIZE*engineWindow.oneDivHeight;
	vec2 mscale = (vec2){EDITOR_STYLE_MENUBAR_SIZE*engineWindow.oneDivWidth/size.x,EDITOR_STYLE_MENUBAR_SIZE*engineWindow.oneDivHeight/size.y};
	editor_createButton(no, "X", EDITOR_STYLE_BAD_BACKGROUND, EDITOR_STYLE_BAD_FOREGROUND, (vec3){1.0f-mscale.x,1.0f-mscale.y,-editorWindowZDepthStep}, mscale, EDITOR_STYLE_FONT_SIZE, onClose==NULL?&editor_onCloseWindowClick:onClose);

	object *fp = editor_create_object();
	fp->scale.y = 1.0f-mscale.y;
	fp->position.y = -mscale.y;
	fp->position.z = -editorWindowZDepthStep;
	editor_object_setParent(fp,no);

	gui_dragview *bc = create_gui_dragview();
	bc->base.color = EDITOR_STYLE_BACKGROUND;
	editor_object_addComponent(fp,bc);

	EDITOR_STYLE_WINDOW(no);

	return no;
}
object *editor_confirmPopup(char *txt, vec2 pos, vec2 size, one_arg_function onYes) {
	char zeroStr = 0;
	object *wnd = editor_createWindow(&zeroStr,pos,size,&editor_onCloseWindowClick);
	editor_delete_object(((object**)wnd->children.buffer)[1]);

	object *tb = editor_create_object();
	tb->position.y = EDITOR_STYLE_MENUBAR_SIZE*engineWindow.oneDivHeight*-8.0f;
	editor_object_setParent(tb,wnd);

	float fsz = EDITOR_STYLE_FONT_SIZE;

	gui_text *nt = create_gui_text();
	nt->font = &engineEditorFont;
	nt->fontSize = fsz;
	nt->multiline = true;
	nt->color = EDITOR_STYLE_COLOR;
	nt->text = cstring_as_list(txt);
	EDITOR_STYLE_TEXT(nt);
	editor_object_addComponent(tb,nt);

	float msz = EDITOR_STYLE_MENUBAR_SIZE*engineWindow.oneDivHeight*2.0f,
		wdth = engineWindow.oneDivWidth/engineWindow.aspect;
	editor_createButton(wnd, "Yes", EDITOR_STYLE_GOOD_BACKGROUND, EDITOR_STYLE_GOOD_FOREGROUND, (vec3){-0.5f,-0.8f,-editorWindowZDepthStep}, (vec2){200.0f*wdth,msz}, fsz, onYes);
	editor_createButton(wnd, "No", EDITOR_STYLE_BAD_BACKGROUND, EDITOR_STYLE_BAD_FOREGROUND, (vec3){0.5f,-0.8f,-editorWindowZDepthStep}, (vec2){200.0f*wdth,msz}, fsz, &editor_onCloseWindowClick);

	return wnd;
}
void editor_selectionPopupSearchCallback(void *op) {
	gui_textfield *t = op;
	char *stb = t->base.text.buffer;
	uint32_t stl = t->base.text.length;
	bool noCmp = stl==0;
	object *so = t->base.base.base.object->parent;
	float msz = EDITOR_STYLE_MENUBAR_SIZE*engineWindow.oneDivHeight/so->parent->scale.y,my;
	uint32_t olen = so->children.length-2;
	object **optr = so->children.buffer;
	my = optr[1]->position.y-msz*2.2f;
	optr += 2;
	for (uint32_t i = 0; i < olen; i++) {
		object *eo = *optr++;
		gui_text *et = ((gui_text**)eo->components.buffer)[1];
		bool cres;
		if (noCmp) cres = true;
		else cres = list_indexOf(&et->text,stb,stl)!=-1;
		eo->position.y = my;
		object_updateTransform(eo);
		editor_object_setActive(eo,cres);
		my -= msz*2.2f;
	}
}
object *editor_selectionPopup(char *title, fixed_array opt, vec2 pos, vec2 size, one_arg_function onSelect) {
	object *wnd = editor_createWindow(title, pos,size,&editor_onCloseWindowClick),
		*tb = ((object**)wnd->children.buffer)[1];

	float fsz = EDITOR_STYLE_FONT_SIZE,
		msz = EDITOR_STYLE_MENUBAR_SIZE*engineWindow.oneDivHeight/size.y,
		my = 0.8f;
	char **opts = opt.buffer;

	(*(gui_textfield**)(editor_createTextInput(tb, "Search", NULL, EDITOR_STYLE_FOREGROUND, EDITOR_STYLE_COLOR, (vec3){0.0f,my,-editorWindowZDepthStep}, (vec2){0.7f,msz}, fsz)->components.buffer))->onChange = editor_selectionPopupSearchCallback;
	my -= msz*2.2f;

	for (uint32_t i = 0; i < opt.length; i++) {
		char *txts = *opts++;
		list tlist = cstring_as_list(txts);
		float lineWidth = gui_text_size(EDITOR_STYLE_FONT_SIZE, &engineEditorFont, (vec2){2.0f,1.5f}, &tlist, 1.0f, NULL, NULL, false, NULL, (vec2){0.0f,0.0f}, NULL)+EDITOR_STYLE_FONT_SIZE*engineWindow.oneDivWidth/engineWindow.aspect;
		editor_createButton(tb,txts,EDITOR_STYLE_FOREGROUND,EDITOR_STYLE_COLOR, (vec3){0.0f,my,-editorWindowZDepthStep}, (vec2){lineWidth*0.6f/size.x, msz}, fsz, onSelect);
		my -= msz*2.2f;
	}
	
	gui_dragview *sview = ((gui_dragview**)tb->components.buffer)[0];
	sview->minX = -1.0f;
	sview->maxX = 1.0f;
	sview->minY = my;
	sview->maxY = -my;
	
	return wnd;
}


typedef struct {
	zero_arg_function init, free;
	void *next;
} engine_editor_addon;

#define LAST_ENGINE_EDITOR_ADDON NULL
#include "addons/editor_addons.h"




//engine processing thread
void engine_run() {
	//engine thread
	START_SYNC_HERTZ();
	while (running) {
		//timing
		SYNC_HERTZ(updateInterval);
		currentTime = hsyncTime;

#ifndef ENGINE_NO_WINDOW
		//mouse input
		mouseDeltaX = mouseX - lastMouseX;
		mouseDeltaY = mouseY - lastMouseY;
		lastMouseX = mouseX;
		lastMouseY = mouseY;
		mouse[0] = mouse[3] | mouse[6];
		mouse[1] = mouse[4] | mouse[7];
		mouse[2] = mouse[5] | mouse[8];
		mouse[6] = false;
		mouse[7] = false;
		mouse[8] = false;
#endif

		if (!engineEditorPaused) {
			//timers
			timers_execute(&timers, currentTime);

			//update events/callbacks
			call_slist_of_ifunctions(updateCalls);
		}

		call_slist_of_ifunctions(engineEditorUpdateCalls);

		//synced calls
		#ifdef ENGINE_NO_WINDOW
		if (true) {
		#else
		if (syncedCallRenderDone) {
		#endif
			uint32_t nextSwapId = (syncedCallSwapId+1)%2;
			call_slist_of_ifunctions_once(syncedCalls[nextSwapId]);
			syncedCallSwapId = nextSwapId;
			syncedCallRenderDone = false;
		}
	}

	stillRunning = false;
}
//rendering thread
void engine_render() {
	SDL_Event event;
	START_SYNC_HERTZ();
	while (running) {
		//framerate timing
		SYNC_HERTZ(updateInterval);

		//single render callsbacks/events
		call_slist_of_ifunctions_once(singleRenderCalls);

		if (engineWindow.active) {
			rendertexture_bind(&screenRenderTexture,0.0f,0.0f,1.0f,1.0f);
			glClear(GL_COLOR_BUFFER_BIT);
		
			//render callbacks/events
			call_slist_of_ifunctions(renderCalls);
			call_slist_of_ifunctions(engineEditorRenderCalls);

			if (lastSelectedEditorObject != NULL) {
				component **comps = lastSelectedEditorObject->components.buffer;
				for (uint32_t k = 0; k < lastSelectedEditorObject->components.length; k++) {
					one_arg_function erender = comps[k]->editorRender;
					if (erender != NULL) erender(comps[k]);
				}
			}

			window_swapBuffers(&engineWindow);
		}

		//synced calls
		syncedCallRenderDone = true;

		//process events
		while (SDL_PollEvent(&event)) {
			uint32_t bid;
			switch (event.type) {
			case SDL_QUIT:
				running = false;
				break;
			case SDL_WINDOWEVENT:
				bid = event.window.event;
				if (bid == SDL_WINDOWEVENT_MINIMIZED) engineWindow.active = false;
				else if (bid == SDL_WINDOWEVENT_RESTORED) engineWindow.active = true;
				break;
			case SDL_MOUSEMOTION:
				//mouse position state
				mouseX = event.motion.x;
				mouseY = event.motion.y;
				nMouseX = (mouseX / (float)engineWindow.width)*2.0f - 1.0f;
				nMouseY = 1.0f - (mouseY / (float)engineWindow.height)*2.0f;
				break;
				//mouse button state
			case SDL_MOUSEBUTTONDOWN:
				bid = event.button.button;
				if (bid == SDL_BUTTON_LEFT) mouse[6] = mouse[3] = mouse[0] = true;
				else if (bid == SDL_BUTTON_MIDDLE) mouse[7] = mouse[4] = mouse[1] = true;
				else mouse[8] = mouse[5] = mouse[2] = true;
				break;
			case SDL_MOUSEBUTTONUP:
				bid = event.button.button;
				if (bid == SDL_BUTTON_LEFT) mouse[3] = false;
				else if (bid == SDL_BUTTON_MIDDLE) mouse[4] = false;
				else mouse[5] = false;
				break;

				//text input
			case SDL_TEXTINPUT:
				//newly typed text
				if (modifyingTextInput) {
					SYNCHRONIZE_BEGIN(textInputLock);
					if (textInputSelection != 0) {
						uint32_t lrange = textInput.length - textInputCursor;
						list_removeRange(&textInput, textInputCursor, min(textInputSelection, lrange));
						textInputSelection = 0;
					}
					int32_t alen = strlen(event.text.text);
					list_insertm(&textInput, event.text.text, textInputCursor, alen);
					textInputCursor += alen;
					textInputModified = true;
					SYNCHRONIZE_END(textInputLock);
				}
				break;

			case SDL_KEYDOWN:
				bid = event.key.keysym.sym;
				if (modifyingTextInput) {
					if (bid == SDLK_BACKSPACE) {//backspace
						while (ATOMIC_SWAP(&textInputLock,0,1) == 1);
						if (textInputSelection == 0) {
							if (textInputCursor != 0) {
								list_removeAt(&textInput, textInputCursor - 1);
								textInputCursor--;
							}
						}
						else {
							uint32_t lrange = textInput.length - textInputCursor;
							list_removeRange(&textInput, textInputCursor, textInputSelection<lrange?textInputSelection:lrange);
							textInputSelection = 0;
						}
						textInputModified = true;
						textInputLock = 0;
					}
					else if (bid == SDLK_DELETE) {//delete
						while (ATOMIC_SWAP(&textInputLock,0,1) == 1);
						if (textInputSelection == 0) {
							if (textInputCursor < textInput.length) list_removeAt(&textInput, textInputCursor);
						}
						else {
							uint32_t lrange = textInput.length - textInputCursor;
							list_removeRange(&textInput, textInputCursor, textInputSelection<lrange?textInputSelection:lrange);
							textInputSelection = 0;
						}
						textInputModified = true;
						textInputLock = 0;
					}
					else if (bid == SDLK_c && SDL_GetModState() & KMOD_CTRL) {//copy
						const char ac = '\0';
						while (ATOMIC_SWAP(&textInputLock,0,1) == 1);
						if (textInputSelection == 0) {
							list_add(&textInput, &ac);
							SDL_SetClipboardText(textInput.buffer);
							list_removeAt(&textInput, textInput.length - 1);
						}
						else {
							uint32_t sci = textInputCursor + textInputSelection;
							bool rep = sci != textInput.length;
							char *ptr = (char*)textInput.buffer + textInputCursor, temp;
							if (rep) temp = ptr[sci];
							list_set(&textInput, &ac, sci);
							SDL_SetClipboardText(ptr);
							if (rep) ptr[sci] = temp;
							else list_removeAt(&textInput, sci);
						}
						textInputLock = 0;
					}
					else if (bid == SDLK_v && SDL_GetModState() & KMOD_CTRL) {//paste
						while (ATOMIC_SWAP(&textInputLock,0,1) == 1);
						char *cpt = SDL_GetClipboardText();
						const uint32_t cl = strlen(cpt);
						list_insertm(&textInput, cpt, textInputCursor, cl);
						SDL_free(cpt);
						textInputCursor += cl;
						textInputSelection = 0;
						textInputModified = true;
						textInputLock = 0;
					}
					else if (textInputNavigation && bid == SDLK_LEFT) {//move text cursor left
						while (ATOMIC_SWAP(&textInputLock,0,1) == 1);
						textInputSelection = 0;
						if (textInputCursor > 0) textInputCursor--;
						textInputModified = true;
						textInputLock = 0;
					}
					else if (textInputNavigation && bid == SDLK_RIGHT) {//right
						while (ATOMIC_SWAP(&textInputLock,0,1) == 1);
						textInputSelection = 0;
						if (textInputCursor < textInput.length) textInputCursor++;
						textInputModified = true;
						textInputLock = 0;
					}
					else if (textInputMultiline && bid == SDLK_RETURN) {//newline
						while (ATOMIC_SWAP(&textInputLock,0,1) == 1);
						char nl = '\n';
						list_insert(&textInput, &nl, textInputCursor);
						textInputCursor++;
						textInputSelection = 0;
						textInputModified = true;
						textInputLock = 0;
					}
				}
				else {
					if (bid == SDLK_TAB) {
						bool side = engineEditorMenuBar->position.y > -1.0f;
						engineEditorMenuBar->position.y = side ? -2.0f : -1.0f+engineEditorMenuBar->scale.y;
						object_updateTransform(engineEditorMenuBar);
					}
				}
				break;
			}

			slist_lock(&eventCalls);
			one_arg_function *eventfs = (one_arg_function*)eventCalls.list.buffer;
			uint32_t ell = eventCalls.list.length;
			for (uint32_t i = 0; i < ell; i++) (*eventfs++)(&event);
			slist_unlock(&eventCalls);
		}
	}
	
	//wait for update thread to finish
	while (stillRunning) threadSleep(10);
}




//editor code
char *editorDeleteStr = "Delete", *editorActiveStr = "Active", *editorPauseStr = "Pause", *editorPlayStr = "Play";
void onEditorPauseButtonClick(void *p) {
	engineEditorPaused = !engineEditorPaused;
	lastSelectedEditorObject = NULL;

	gui_component *bc = p;
	gui_text *c = ((gui_text**)bc->base.object->components.buffer)[1];
	c->text = cstring_as_list(engineEditorPaused?editorPlayStr:editorPauseStr);
	gui_text_updateText(c);
}


void onEditorObjectsWindowClose(void *c) {
	editor_onCloseWindowClick(c);
	editorObjectsWindow = NULL;
}

void onEditorObjectMoveOrderUp(void UNUSED *p) {
	list *cl = &targetEditorObject->parent->children;
	uint32_t i = list_indexOf(cl,&targetEditorObject,sizeof(void*));
	if (i > 0) {
		object **clist = cl->buffer;
		clist[i] = clist[i-1];
		clist[i-1] = targetEditorObject;
		onEditorRefreshClick(NULL);
	}
}
void onEditorObjectMoveOrderDown(void UNUSED *p) {
	list *cl = &targetEditorObject->parent->children;
	uint32_t i = list_indexOf(cl,&targetEditorObject,sizeof(void*));
	if (i < cl->length-1) {
		object **clist = cl->buffer;
		clist[i] = clist[i+1];
		clist[i+1] = targetEditorObject;
		onEditorRefreshClick(NULL);
	}
}
void onEditorDeleteObjectCallback(void *p) {
	if (editorObjectWindow == NULL) return;

	component *c = p;
	editor_delete_object(c->object->parent);

	editor_delete_object(editorObjectWindow);
	delete_object(targetEditorObject);
	lastSelectedEditorObject = NULL;
	targetEditorObject = NULL;
	editorObjectWindow = NULL;

	if (editorObjectsWindow != NULL) onEditorRefreshClick(NULL);
}
void editorDeleteConfirmPopup(one_arg_function cb) {
	editor_confirmPopup("Are you completely sure you want to delete this? You cannot undo this operation!", (vec2){0.0f,0.0f}, EDITOR_STYLE_POPUP_SIZE, cb);
}
void onEditorDeleteObjectClick(void UNUSED *c) {
	editorDeleteConfirmPopup(&onEditorDeleteObjectCallback);
}
void onEditorModifyNameClick(void UNUSED *c) {
	object *opane = ((object**)editorObjectWindow->children.buffer)[1];
	gui_textfield *tf = *(gui_textfield**)((object**)opane->children.buffer)[5]->components.buffer;
	list_copy(&targetEditorObject->name, &tf->base.text);
	gui_text *wt = ((gui_text**)editorObjectWindow->components.buffer)[1];
	list_copy(&wt->text, &targetEditorObject->name);
	gui_text_updateText(wt);
	onEditorRefreshClick(NULL);
}
void onEditorActiveSwitchClick(void *p) {
	gui_component *c = p;
	bool act = !targetEditorObject->active;
	object_setActive(targetEditorObject, act);
	c->color = act?EDITOR_STYLE_GOOD_BACKGROUND:EDITOR_STYLE_BAD_BACKGROUND;
	gui_component_transformUpdate(c);
}
void onEditorDuplicateObjectClick(void *p) {
	object_duplicate(targetEditorObject,targetEditorObject->parent,true);
	onEditorRefreshClick(NULL);
}
component *editorGetComponentFromParentIndex(void *p) {
	object *obj = ((component*)p)->object->parent;
	uint32_t ind = list_indexOf(&obj->parent->children, &obj, sizeof(void*));
	ind -= editorObjectViewComponentSeperator;
	return ((component**)targetEditorObject->components.buffer)[ind];
}
void onEditorSwitchActiveComponentClick(void *p) {
	component *targ = editorGetComponentFromParentIndex(p);
	bool act = !targ->active;
	component_setActive(targ,act);
	
	gui_component *comp = p;
	comp->color = act?EDITOR_STYLE_GOOD_BACKGROUND:EDITOR_STYLE_BAD_BACKGROUND;
	gui_component_transformUpdate(comp);
}
component *editorDeleteComponentCallbackBuffer;
void onEditorDeleteComponentCallback(void *p) {
	component *c = p;
	editor_delete_object(c->object->parent);

	delete_component(editorDeleteComponentCallbackBuffer);
	onEditorRefreshObjectClick(NULL);
}
void onEditorDeleteComponentClick(void *p) {
	editorDeleteComponentCallbackBuffer = editorGetComponentFromParentIndex(p);
	editorDeleteConfirmPopup(&onEditorDeleteComponentCallback);
}
void onEditorAddComponentSelect(void *cb) {
	if (targetEditorObject == NULL) return;

	component *selBtn = cb;
	object *selObj = selBtn->object;
	uint32_t selId = list_indexOf(&selObj->parent->children, &selObj, sizeof(void*))-2,
	count = 0;
	component_data *cdat = (void*)LAST_COMPONENT_DATA;
	while (cdat != NULL) {
		if (count == selId) break;
		count++;
		cdat = (void*)cdat->next;
	}

	component *nc = malloc(cdat->size);
	*nc = (component){NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, cstring_as_list(cdat->name), cdat, true, false};
	cdat->init(nc);
	object_addComponent(targetEditorObject,nc);

	editor_delete_object(selObj->parent->parent);
	onEditorRefreshObjectClick(NULL);
}
void onEditorAddComponentClick(void UNUSED *sdhf) {
	uint32_t ncomps = 0;
	component_data *c = (void*)LAST_COMPONENT_DATA;
	while (c != NULL) {
		ncomps++;
		c = (void*)c->next;
	}

	fixed_array fa;
	fa.length = ncomps;
	fa.buffer = malloc(sizeof(void*)*ncomps);

	char **compns = fa.buffer;
	c = (void*)LAST_COMPONENT_DATA;
	while (c != NULL) {
		*compns++ = c->name;
		c = (void*)c->next;
	}

	editor_selectionPopup("Add Component", fa, (vec2){0.0f,0.0f}, EDITOR_STYLE_WINDOW_SIZE, &onEditorAddComponentSelect);
	free(fa.buffer);
}
void onEditorRefreshObjectClick(void UNUSED *c) {
	editorObjectTransformUpdateCount = 0.0f;
	
	object *opane = ((object**)editorObjectWindow->children.buffer)[1];
	editor_object_deleteChildren(opane);

	gui_dragview *sview = ((gui_dragview**)opane->components.buffer)[0];
	sview->minX = -4.0f;
	sview->maxX = 4.0f;

	float msz = (EDITOR_STYLE_MENUBAR_SIZE-4)*engineWindow.oneDivHeight/opane->absoluteScale.y,
		msz2 = msz*2.4f,
		wdth = engineWindow.oneDivWidth/engineWindow.aspect/opane->scale.x,
		wfsz = EDITOR_STYLE_FONT_SIZE,
		mpos = 1.0f;

	mpos -= msz*1.5f;
	editor_createButton(opane, editorActiveStr, targetEditorObject->active?EDITOR_STYLE_GOOD_BACKGROUND:EDITOR_STYLE_BAD_BACKGROUND, EDITOR_STYLE_GOOD_FOREGROUND, (vec3){0.4f,mpos,-editorWindowZDepthStep}, (vec2){110.0f*wdth,msz}, wfsz, &onEditorActiveSwitchClick);
	editor_createButton(opane, editorDeleteStr, EDITOR_STYLE_BAD_BACKGROUND, EDITOR_STYLE_BAD_FOREGROUND, (vec3){0.8f,mpos,-editorWindowZDepthStep}, (vec2){100.0f*wdth,msz}, wfsz, &onEditorDeleteObjectClick);
	editor_createButton(opane, "Duplicate", EDITOR_STYLE_FOREGROUND, EDITOR_STYLE_COLOR, (vec3){-0.1f,mpos,-editorWindowZDepthStep}, (vec2){140.0f*wdth,msz}, wfsz, &onEditorDuplicateObjectClick);
	editor_createButton(opane, "Up", EDITOR_STYLE_FOREGROUND, EDITOR_STYLE_COLOR, (vec3){-0.9f,mpos,-editorWindowZDepthStep}, (vec2){50.0f*wdth,msz}, wfsz, &onEditorObjectMoveOrderUp);
	editor_createButton(opane, "Down", EDITOR_STYLE_FOREGROUND, EDITOR_STYLE_COLOR, (vec3){-0.65f,mpos,-editorWindowZDepthStep}, (vec2){80.0f*wdth,msz}, wfsz, &onEditorObjectMoveOrderDown);

	editor_createTextInput(opane, "Name", &targetEditorObject->name, EDITOR_STYLE_FOREGROUND, EDITOR_STYLE_COLOR, (vec3){0.55f,mpos-=msz2,-editorWindowZDepthStep}, (vec2){0.8f,msz}, wfsz);
	editor_createButton(opane, "Change", EDITOR_STYLE_GOOD_BACKGROUND, EDITOR_STYLE_GOOD_FOREGROUND, (vec3){-0.8f,mpos,-editorWindowZDepthStep}, (vec2){120.0f*wdth,msz}, wfsz, &onEditorModifyNameClick);

	editorObjectTransformTextFields[9] = *(void**)editor_createTextInput(opane, "Layer", NULL, EDITOR_STYLE_FOREGROUND, EDITOR_STYLE_COLOR, (vec3){0.55f,mpos-=msz2,-editorWindowZDepthStep}, (vec2){0.8f,msz}, wfsz)->components.buffer;

	char *yzstr[2] = {"Y","Z"}, *psrstr[3] = {"Position X", "Rotation X", "Scale X"};
	for (uint32_t i = 0; i < 9; i++) {
		uint32_t a = i%3;
		editorObjectTransformTextFields[i] = *(void**)editor_createTextInput(opane, a==0?psrstr[i/3]:yzstr[a-1], NULL, EDITOR_STYLE_FOREGROUND, EDITOR_STYLE_COLOR, (vec3){0.55f,mpos-=msz2,-editorWindowZDepthStep}, (vec2){0.8f,msz}, wfsz)->components.buffer;	
	}
	
	editor_createButton(opane, "Add Component", EDITOR_STYLE_GOOD_BACKGROUND, EDITOR_STYLE_GOOD_FOREGROUND, (vec3){0.0f,mpos-=msz2,-editorWindowZDepthStep}, (vec2){300.0f*wdth,msz}, wfsz, &onEditorAddComponentClick);
	mpos -= msz2;

	//printf("editorObjectViewComponentSeperator %d\n",opane->children.length);

	uint32_t ncomps = targetEditorObject->components.length;
	component **cptr = targetEditorObject->components.buffer;
	for (uint32_t i = 0; i < ncomps; i++) {
		component *cc = *cptr++;
		component_data *cd = cc->metadata;

		object *compPane = editor_create_object();
		float cysz = 1.0f/compPane->parent->absoluteScale.y, sz = msz*(1+cd->fieldCount)*cysz*2.0f;//msz2*(1+cd->fieldCount)*cysz;
		mpos -= sz;
		compPane->position = (vec3){0.0f,mpos,-editorWindowZDepthStep};
		compPane->scale.y = sz/cysz;
		editor_object_setParent(compPane, opane);

		gui_text *nm = create_gui_text();
		nm->base.base.name = cstring_as_list(component_data_gui_component.name);
		nm->fontSize = EDITOR_STYLE_FONT_SIZE;
		nm->font = &engineEditorFont;
		nm->color = EDITOR_STYLE_COLOR;
		nm->text = cc->name;
		EDITOR_STYLE_TEXT(nm);
		editor_object_addComponent(compPane, nm);

		float mszs = msz/sz, btnsz = 1.0f-mszs;
		editor_createButton(compPane, editorActiveStr, cc->active?EDITOR_STYLE_GOOD_BACKGROUND:EDITOR_STYLE_BAD_BACKGROUND, EDITOR_STYLE_GOOD_FOREGROUND, (vec3){0.3f,btnsz,-editorWindowZDepthStep}, (vec2){110.0f*wdth,mszs}, wfsz, &onEditorSwitchActiveComponentClick);
		editor_createButton(compPane, editorDeleteStr, EDITOR_STYLE_BAD_BACKGROUND, EDITOR_STYLE_BAD_FOREGROUND, (vec3){0.75f,btnsz,-editorWindowZDepthStep}, (vec2){110.0f*wdth,mszs}, wfsz, &onEditorDeleteComponentClick);

		reflection_data *fields = cd->fields;
		float fpos = btnsz;
		for (uint32_t k = 0; k < cd->fieldCount; k++) {
			editor_createTextInput(compPane, fields->name, NULL, EDITOR_STYLE_FOREGROUND, EDITOR_STYLE_COLOR, (vec3){0.55f,fpos-=mszs*2.2f,-editorWindowZDepthStep}, (vec2){0.8f,mszs}, wfsz);
			fields++;
		}

		mpos -= sz*0.2f;
	}

	mpos = min(mpos,-1.0f);
	sview->minY = mpos;
	sview->maxY = -mpos;

	editorObjectTransformUpdateCount = 1.01f;
}


void onEditorObjectWindowClose(void *c) {
	targetEditorObject = NULL;
	editorObjectWindow = NULL;
	editor_onCloseWindowClick(c);
}

uint64_t lastEditorObjectClickTime = 0;
void onEditorObjectClick(void *p) {
	gui_component *gc = p;

	object *fobj = editorFindObjectFromHierarchy(gc->base.object);
	if (fobj == NULL) return;

	lastSelectedEditorObject = fobj;
	if (currentTime-lastEditorObjectClickTime < 300) {
		targetEditorObject = fobj;
		if (editorObjectWindow == NULL) {
			editorObjectWindow = editor_createWindow(list_as_cstring(&targetEditorObject->name), (vec2){0.0f,0.0f}, EDITOR_STYLE_WINDOW_SIZE, &onEditorObjectWindowClose);
			list_clear_cstring(&targetEditorObject->name);
		} else {
			gui_text *wt = ((gui_text**)editorObjectWindow->components.buffer)[1];
			list_copy(&wt->text, &targetEditorObject->name);
			gui_text_updateText(wt);
		}
		onEditorRefreshObjectClick(NULL);
	} else {
		draggingEditorObject = fobj;
		draggingEditorObjectButton = p;
		editor_object_setActive(editorObjectDragIcon,true);
	}

	lastEditorObjectClickTime = currentTime;
}


uint32_t onEditorBuildObjectHierarchy(object *o, uint32_t oc, uint32_t pc) {
	object *opane = ((object**)editorObjectsWindow->children.buffer)[1];
	float msz = (EDITOR_STYLE_MENUBAR_SIZE-4)*engineWindow.oneDivHeight / opane->absoluteScale.y,
		wdth = engineWindow.oneDivWidth/engineWindow.aspect,
		wfsz = EDITOR_STYLE_FONT_SIZE;

	uint32_t nc = o->children.length;
	object **child = o->children.buffer;
	for (uint32_t i = 0; i < nc; i++) {
		object *cc = *child++;
		((gui_text**)editor_createButton(opane, list_as_cstring(&cc->name), EDITOR_STYLE_FOREGROUND, EDITOR_STYLE_COLOR, (vec3){pc*20.0f*engineWindow.oneDivWidth/engineWindow.aspect,oc*-msz*2.2f,-editorWindowZDepthStep}, (vec2){400.0f*wdth,msz}, wfsz, &onEditorObjectClick)->components.buffer)[1]->anchor = ANCHOR_LEFT;
		list_clear_cstring(&cc->name);
		oc++;
		if (cc->children.length != 0) oc = onEditorBuildObjectHierarchy(cc,oc,pc+1);
	}

	return oc;
}
void onEditorRefreshClick(void UNUSED *c) {
	object *opane = ((object**)editorObjectsWindow->children.buffer)[1];
	editor_object_deleteChildren(opane);
	gui_dragview *sview = ((gui_dragview**)opane->components.buffer)[0];
	sview->minX = -4.0f;
	sview->maxX = 4.0f;
	uint32_t tc = onEditorBuildObjectHierarchy(&scene,0,0);

	float ly = tc*EDITOR_STYLE_MENUBAR_SIZE*2.2f*engineWindow.oneDivHeight/opane->absoluteScale.y;
	sview->minY = -ly;
	sview->maxY = ly;
	gui_dragview_move(sview,(vec2){ 0.0f, 0.8f });
}


void onEditorNewObjectClick(void UNUSED *c) {
	create_object();
	onEditorRefreshClick(NULL);
}


void onEditorViewObjectsClick(void UNUSED *c) {
	if (editorObjectsWindow != NULL) return;
	editorObjectsWindow = editor_createWindow("Objects", (vec2){-0.6f,0.0f}, EDITOR_STYLE_WINDOW_SIZE, &onEditorObjectsWindowClose);

	float msz = EDITOR_STYLE_MENUBAR_SIZE*engineWindow.oneDivHeight/editorObjectsWindow->absoluteScale.y,
	wdth = engineWindow.oneDivWidth/engineWindow.aspect,
	opy = 1.0f-msz;
	editor_createButton(editorObjectsWindow, "New", EDITOR_STYLE_GOOD_BACKGROUND, EDITOR_STYLE_GOOD_FOREGROUND, (vec3){-0.2f,opy,-editorWindowZDepthStep}, (vec2){70.0f*wdth,msz}, EDITOR_STYLE_FONT_SIZE, &onEditorNewObjectClick);
	editor_createButton(editorObjectsWindow, "Refresh", EDITOR_STYLE_GOOD_BACKGROUND, EDITOR_STYLE_GOOD_FOREGROUND, (vec3){-0.2f+260.0f*wdth,opy,-editorWindowZDepthStep}, (vec2){130.0f*wdth,msz}, EDITOR_STYLE_FONT_SIZE, &onEditorRefreshClick);

	object *dragIcon = editor_create_object();
	editor_object_setActive(dragIcon,false);
	dragIcon->position.z = -editorWindowZDepthStep*3.0f;
	dragIcon->scale = (vec3){400.0f*wdth,msz,1.0f};
	editor_object_setParent(dragIcon,editorObjectsWindow);
	gui_component *icon = create_gui_component();
	icon->color = EDITOR_STYLE_GOOD_BACKGROUND;
	editor_object_addComponent(dragIcon,icon);
	editorObjectDragIcon = dragIcon;

	onEditorRefreshClick(NULL);
}


void init_engine_editor() {
	//setup engine editor
	new_font(&engineEditorFont);
	if (!font_loadFromFile(&engineEditorFont, (uint32_t)(EDITOR_STYLE_FONT_SIZE*EDITOR_STYLE_FONT_RESOLUTION), EDITOR_STYLE_FONT_FILE)) {
		printf("Could not load editor, failed to load editor font '%s'.\n", EDITOR_STYLE_FONT_FILE);
		return;
	}
	init_object(&engineEditorScene);

	engineEditorScene.parent = &engineEditorScene;
	engineEditorGUI = editor_create_object();
	engineEditorGUI->scale.x = 1.0f/engineWindow.aspect;
	engineEditorGUI->position.z = 1.0f;
	object_updateTransform(engineEditorGUI);
	gui *eegui = create_gui();
	editor_object_addComponent(engineEditorGUI, eegui);

	engineEditorMenuBar = editor_create_object();
	engineEditorMenuBar->scale.x = MENUBAR_MAX_WIDTH;
	engineEditorMenuBar->scale.y = EDITOR_STYLE_MENUBAR_SIZE*engineWindow.oneDivHeight;
	engineEditorMenuBar->position.y = -1.0f+engineEditorMenuBar->scale.y;
	editor_object_setParent(engineEditorMenuBar, engineEditorGUI);
	gui_component *barBackground = create_gui_component();
	barBackground->color = EDITOR_STYLE_BACKGROUND;
	editor_object_addComponent(engineEditorMenuBar, barBackground);

	editor_addMenuButton(editorPlayStr, &onEditorPauseButtonClick);
	editor_addMenuButton("Objects Hierarchy", &onEditorViewObjectsClick);

	EDITOR_STYLE_INIT();
	
	cpifunction(&engineEditorUpdate, NULL, slist_add(&engineEditorUpdateCalls, &ifunc));

	//load editor addons
	engine_editor_addon *caddon = LAST_ENGINE_EDITOR_ADDON;
	while (caddon != NULL) {
		caddon->init();
		caddon = caddon->next;
	}
}

void free_engine_editor() {
	delete_font(&engineEditorFont);
	editor_free_object(&engineEditorScene);
	delete_slist(&engineEditorUpdateCalls);
	delete_slist(&engineEditorRenderCalls);

	//free editor addons
	engine_editor_addon *caddon = LAST_ENGINE_EDITOR_ADDON;
	while (caddon != NULL) {
		if (caddon->free != NULL) caddon->free();
		caddon = caddon->next;
	}
}


//engine editor launching/startup
void engine_launch(const engine_launch_options opt) {
	new_slist(&engineEditorUpdateCalls, sizeof(ifunction), false);
	new_slist(&engineEditorRenderCalls, sizeof(ifunction), true);
	new_engine(opt);
	if (engineWindow.window == NULL) return;
	init_engine_editor();

	//run engine thread and render loop
	running = stillRunning;
	#ifdef ENGINE_NO_WINDOW
	engine_run();
	#else
	createThread(&engine_run, NULL, THREAD_DEFAULT_STACK_SIZE);
	engine_render();
	#endif

	//cleanup editor
	free_engine_editor();

	//cleanup engine
	delete_engine(opt);
}

#endif
