﻿/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _EDITOR_STYLE_H_
#define EDITOR_STYLE_FONT_FILE "../../editor/abel-regular.ttf"

#define EDITOR_STYLE_MENUBAR_SIZE 32
#define EDITOR_STYLE_FONT_SIZE ((EDITOR_STYLE_MENUBAR_SIZE*10)/13)
#define EDITOR_STYLE_FONT_RESOLUTION 0.9f
#define EDITOR_STYLE_WINDOW_SIZE editorConstantPixelsToGUI((vec2){300.0f,450.0f})
#define EDITOR_STYLE_POPUP_SIZE editorConstantPixelsToGUI((vec2){200.0f,200.0f})

const vec4 EDITOR_STYLE_BACKGROUND = {0.04f,0.04f,0.04f,1.0f},
EDITOR_STYLE_FOREGROUND = {0.1f,0.1f,0.1f,1.0f},
EDITOR_STYLE_COLOR = {0.88f,0.85f,0.95f,1.0f},
EDITOR_STYLE_BAD_BACKGROUND = {0.4f,0.06f,0.06f,1.0f},
EDITOR_STYLE_BAD_FOREGROUND = {1.0f,1.0f,1.0f,1.0f},
EDITOR_STYLE_GOOD_BACKGROUND = {0.09f,0.14f,0.3f,1.0f},
EDITOR_STYLE_GOOD_FOREGROUND = {0.94f,0.8f,0.8f,1.0f};

#define EDITOR_STYLE_INIT() 
#define EDITOR_STYLE_TEXT(txt)
#define EDITOR_STYLE_BUTTON(btn)
#define EDITOR_STYLE_WINDOW(wnd)
#define EDITOR_STYLE_TEXTINPUT(txt)

#endif
