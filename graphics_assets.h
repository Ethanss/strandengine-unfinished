﻿/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _GRAPHICS_ASSETS_H_
#define _GRAPHICS_ASSETS_H_

#include "assets_base.h"
#include "graphics.h"
#include "material.h"

#ifndef EXCLUDE_LIB_SDL_TTF
//font asset
void asset_type_font_loadMain(asset *a) {
	font *t = malloc(sizeof(font));
	new_font(t);
	font_load(t, a->instances, a->asset, a->fileSize);
	free(a->asset);
	a->asset = t;
	a->instances = 1;
	a->fileSize = 0xFFFFFFFF;
}

void asset_type_font_load(asset *a) {
	//load font data
	void *d = malloc(a->fileSize);
	fread(d, 1, a->fileSize, a->fileHandle);

	//load max font size from options
	uint32_t mfsz = 32;
	json_object *js = a->options;
	if (js != NULL) {
		if (js->values.length != 0) {
			json *jr = (json*)js->values.buffer;
			if (jr->type == JSON_TYPE_INT) mfsz = jr->intv;
		}
		delete_json_object(js);
		free(js);
	}

	//run load font on main thread
	a->asset = d;
	a->instances = mfsz;
	a->options = NULL;
	singleRenderCall(asset_type_font_loadMain, a);
	while (a->fileSize != 0xFFFFFFFF) threadSleep(1);
}

void asset_type_font_unloadMain(asset *a) {
	delete_font(a->asset);
	a->instances = 1;
}
void asset_type_font_unload(asset *a) {
	singleRenderCall(asset_type_font_unloadMain, a);
	while (a->instances == 0) threadSleep(1);
	asset_type_unload(a);
}

const fixed_array asset_type_font_exts[1] = { { ".ttf", 4, 1 } };
const asset_type ASSET_TYPE_FONT = { (void*)&asset_type_font_load, (void*)&asset_type_font_unload, { (void*)asset_type_font_exts, 1, 0 }, (void*)LAST_ASSET_TYPE };
#undef LAST_ASSET_TYPE
#define LAST_ASSET_TYPE &ASSET_TYPE_FONT
#endif

//texture asset
void asset_type_texture_loadMain(asset *a) {
	texture *t = malloc(sizeof(texture));
	new_texture(t);
	void *d = a->asset;
	if (d == NULL) {
		a->asset = t;
		free(a->options);
		a->options = NULL;
		a->fileSize = 0xFFFFFFFF;	
		return;
	}
	uint32_t opt = a->fileSize,
	*dim = (void*)a->options;
	texture_setData(t,d,dim,(opt&1)!=0,((opt>>1)&1)!=0);
	bool nearest = (opt >> 4) & 1;
	if (((opt>>2)&1) != 0) {
		if (nearest) {
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		}
		else {
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		}
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else {
		if (nearest) {
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		}
	}
	if (((opt >> 6) & 1) != 0) {
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	}
	a->asset = t;
	free(dim);
	if (((opt>>3)&1) == 0) {
		t->data = NULL;
		free(d);
	}
	a->options = NULL;
	a->fileSize = 0xFFFFFFFF;
}

#ifndef EXCLUDE_LIB_PNG
#include "pnghelper.h"
#else
#define load_png(a,d,o) malloc(d[0]*d[1]*d[2])
#endif

#ifndef EXCLUDE_LIB_BMP
#include "bmphelper.h"
#else
#define load_bmp(a,d,f) malloc(d[0]*d[1]*d[2])
#endif

void asset_type_texture_load(asset *a) {
	//load options
	uint32_t opt = 1 | (1<<2) | (1<<5);
	json_object *js = a->options;
	if (js != NULL) {
		const char *tos[7] = {"a",//alpha
							 "c",//compress
							 "m",//mipmap
							 "d",//data, keep data in memory
							 "n",//nearest
							 "v",//vertical flip
							 "r"//repeat texture
							 };
		const bool tod[7] = {true,false,true,false,false,true,false};
		opt = 0;
		for (int i = 0; i < 7; i++) {
			list tl = static_list(tos[i], 1, 1);
			json *jr = json_object_getClosest(js,&tl);
			if (jr != NULL) opt |= jr->intv<<i;
			else opt |= (tod[i]?1:0)<<i;
		}
		delete_json_object(js);
		free(js);
	}
	
	//load texture data
	uint32_t *dim = malloc(12);
	void *d;
	d = load_png(a->fileHandle, dim, ((opt >> 5) & 1) != 0);

	//run load texture on main thread
	a->asset = d;
	a->fileSize = opt;
	a->options = (void*)dim;
	singleRenderCall(asset_type_texture_loadMain,a);
	while (a->fileSize == opt) threadSleep(1);
}

void asset_type_texture_unloadMain(asset *a) {
	delete_texture(a->asset);
	a->instances = 1;
}
void asset_type_texture_unload(asset *a) {
	singleRenderCall(asset_type_texture_unloadMain,a);
	while (a->instances == 0) threadSleep(1);
	asset_type_unload(a);
}

const fixed_array asset_type_texture_exts[2] = {{".png",4,1},{".bmp",4,1}};
const asset_type ASSET_TYPE_TEXTURE = {(void*)&asset_type_texture_load, (void*)&asset_type_texture_unload, {(void*)asset_type_texture_exts,1,0}, (void*)LAST_ASSET_TYPE};
#undef LAST_ASSET_TYPE
#define LAST_ASSET_TYPE &ASSET_TYPE_TEXTURE


//mesh asset
#ifndef EXCLUDE_LIB_ASSIMP
#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include "meshanimation.h"

void asset_type_mesh_swapYZ(struct aiVector3D *aiv, float *va, const uint32_t count) {
	for (uint32_t i = 0; i < count; i ++) {
		*va++ = aiv->x;
		*va++ = aiv->z;
		*va++ = aiv->y;
		aiv++;
	}
}
void asset_type_mesh_animationNodeSwapYZ(mesh_animation_node *n) {
	float tmp = n->position.y;
	n->position.y = n->position.z;
	n->position.z = tmp;
	tmp = n->scale.y;
	n->scale.y = n->scale.z;
	n->scale.z = tmp;
	for (uint32_t i = 0; i < n->numChildren; i++) asset_type_mesh_animationNodeSwapYZ((mesh_animation_node*)n->children+i);
}
/*
void assimp_decompose4x4Matrix(struct aiMatrix4x4 mat, mesh_animation_node *n, const float sfactor) {
	n->position.x = mat.a4*sfactor;
	n->position.y = mat.b4*sfactor;
	n->position.z = mat.c4*sfactor;
	
	vec3 buf = (vec3){mat.a1,mat.b1,mat.c1};
	n->scale.x = vec3_length(&buf);
	if (n->scale.x != 0.0f) {
		vec3_mulSingle(&buf,1.0f/n->scale.x);
		mat.a1 = buf.x;mat.b1 = buf.y;mat.c1 = buf.z;
	}
	n->scale.x *= sfactor;
	
	//if (n->scale.x != 1.0f) printf("%s scale is %g\n", n->name.buffer, n->scale.x);
	
	buf = (vec3){mat.a2,mat.b2,mat.c2};
	n->scale.y = vec3_length(&buf);
	if (n->scale.y != 0.0f) {
		vec3_mulSingle(&buf,1.0f/n->scale.y);
		mat.a2 = buf.x;mat.b2 = buf.y;mat.c2 = buf.z;
	}
	n->scale.y *= sfactor;
	
	buf = (vec3){mat.a3,mat.b3,mat.c3};
	n->scale.z = vec3_length(&buf);
	if (n->scale.z != 0.0f) {
		vec3_mulSingle(&buf,1.0f/n->scale.z);
		mat.a3 = buf.x;mat.b3 = buf.y;mat.c3 = buf.z;
	}
	n->scale.z *= sfactor;
	
	vec4_quaternionFromMatrix(&n->rotation,(vec3){mat.a1,mat.a2,mat.a3},(vec3){mat.b1,mat.b2,mat.b3},(vec3){mat.c1,mat.c2,mat.c3});	
	//matrix rotation(forward,up,right vectors) to quaternion, credits euclideanspace.com
	//buf = *(vec3*)&mat.b1;
	//*(vec3*)&mat.b1 = *(vec3*)&mat.c1;
	//*(vec3*)&mat.c1 = buf;
	/*
	float tmp = mat.a2;
	mat.a2 = mat.a3;
	mat.a3 = tmp;
	
	tmp = mat.b2;
	mat.b2 = mat.b3;
	mat.b3 = tmp;
	
	tmp = mat.c2;
	mat.c2 = mat.c3;
	mat.c3 = tmp;
	
	const float rtrace = mat.a1+mat.b2+mat.c3;
	if (rtrace > 0.0f) {//s=trace
		const float s = sqrtf(rtrace+1.0f), s1 = 0.5f/s;
		*rot = (vec4){(mat.c2-mat.b3)*s1,(mat.a3-mat.c1)*s1,(mat.b1-mat.a2)*s1,s*0.5f};
	} else if (mat.a1 > mat.b2 && mat.a1 > mat.c3) {//s=qx
		const float s = sqrtf(1.0f+mat.a1-mat.b2-mat.c3), s1 = 0.5f/s;
		*rot = (vec4){s*0.5f,(mat.a2-mat.b1)*s1,(mat.a3-mat.c1)*s1,(mat.c2-mat.b3)*s1};
	} else if (mat.b2 > mat.c3) {//s=qy
		const float s = sqrtf(1.0f+mat.b2-mat.a1-mat.c3), s1 = 0.5f/s;
		*rot = (vec4){(mat.a2-mat.b1)*s1,s*0.5f,(mat.b3+mat.c2)*s1,(mat.a3-mat.c1)*s1};
	} else {//s=qz
		const float s = sqrtf(1.0f+mat.c3-mat.a1-mat.b2), s1 = 0.5f/s;
		*rot = (vec4){(mat.a3-mat.c1)*s1,(mat.b3+mat.c2)*s1,s*0.5f,(mat.b1-mat.a2)*s1};
	}*/
//}
/*
bool asset_type_mesh_traverseNodeHierarchy(struct aiNode *an, struct aiMesh *mesh, mesh_animation_node *dn) {
	const uint32_t nlen = strlen(an->mName.data);
	char *nnam = malloc(nlen+1);
	memcpy(nnam,an->mName.data,nlen+1);
	dn->name.stride = 1;
	list_setBuffer(&dn->name,nnam,nlen);
	
	assimp_decompose4x4Matrix(an->mTransformation,dn,1.0f);//dn->parent==NULL?1.0f:((mesh_animation_node*)dn->parent)->scale.x);
	
	dn->numChildren = 0;
	if (an->mNumChildren == 0) return false;
	
	bool hbone = false;
	mesh_animation_node *children = malloc(sizeof(mesh_animation_node)*an->mNumChildren);
	for (uint32_t i = 0; i < an->mNumChildren; i++) {
		uint32_t k;
		char *cname = an->mChildren[i]->mName.data;
		for (k = 0; k < mesh->mNumBones; k++) {
			if (strcmp(mesh->mBones[k]->mName.data,cname) == 0) break;
		}
		const uint32_t nci = dn->numChildren;
		bool ibone = k<mesh->mNumBones;
		hbone |= ibone;
		children[nci].bone = ibone;
		children[nci].parent = dn;
		if (asset_type_mesh_traverseNodeHierarchy(an->mChildren[i],mesh,children+nci) || ibone) {
			//if (children[nci].scale.x != ) {
			//	printf("%s scale used to be %g now its 1\n",cname,children[nci].scale.x);
				//children[nci].scale = (vec3){1.0f,1.0f,1.0f};
			//}
			dn->numChildren++;
		}
	}
	
	if (dn->numChildren == 0) free(children);
	else if (dn->numChildren != an->mNumChildren) dn->children = realloc(children,sizeof(mesh_animation_node)*dn->numChildren);
	else dn->children = children;
	
	return hbone;
}
void traverseNodeHierarchy(struct aiNode *an, uint32_t pad) {
	for (uint32_t i = 0; i < pad; i++) putchar(' ');
	printf("%s has %d children and %d meshes {\n", an->mName.data, an->mNumChildren, an->mNumMeshes);
	for (uint32_t i = 0; i < an->mNumChildren; i++) traverseNodeHierarchy(an->mChildren[i],pad+1);
}*/
void asset_type_mesh_loadMain(asset *a) {
	mesh *m = malloc(sizeof(mesh));
	const uint32_t opt = a->fileSize, smind = (opt>>24)&255;
	struct aiScene *ascene = a->asset;
	if (ascene != NULL) {
		/*printf("%d meshes, %d animations\n", ascene->mNumMeshes, ascene->mNumAnimations);
		if (ascene->mNumAnimations != 0 && (opt>>2)&1) {
			for (uint32_t i = 0; i < ascene->mNumAnimations; i++) {
				printf("animation named %s\n",ascene->mAnimations[i]->mName.data);
			}
			struct aiMesh *amesh = ascene->mMeshes[0];
			printf("\n%d bones\n", amesh->mNumBones);
			for (uint32_t i = 0; i < amesh->mNumBones; i++) {
				struct aiBone *ab = amesh->mBones[i];
				printf("bone %s has %d weights\n", ab->mName.data, ab->mNumWeights);
			}
			if (ascene->mRootNode == NULL) printf("root null wtf?!\n");
			else {
				printf("\ntraversing node hierarchy:\n");
				traverseNodeHierarchy(ascene->mRootNode,0);
			}
		}*/
		if (ascene->mNumMeshes > smind) {
			struct aiMesh *amesh = ascene->mMeshes[smind];
			uint32_t doff[8];
			bool indexed = (opt >> 3) & 1, zIsY = (opt>>5)&1;
			uint32_t meshFlags = VERTEX_ATTRIBUTE_POSITION, vcount = amesh->mNumVertices, tsz = vcount*3;
			if (indexed) {
				meshFlags |= VERTEX_INDEXED;
				doff[7] = tsz;
				tsz += m->numIndices;
			}
			if (amesh->mTextureCoords[0] != NULL) {
				meshFlags |= VERTEX_ATTRIBUTE_UV;
				doff[0] = tsz;
				tsz += vcount * 2;
			}
			if (amesh->mColors[0] != NULL) {
				meshFlags |= VERTEX_ATTRIBUTE_COLOR;
				doff[1] = tsz;
				tsz += vcount * 4;
			}
			if (amesh->mNormals != NULL) {
				meshFlags |= VERTEX_ATTRIBUTE_NORMAL;
				doff[2] = tsz;
				tsz += vcount * 3;
			}
			if ((opt >> 1) & 1) {
				if (amesh->mTangents != NULL) {
					meshFlags |= VERTEX_ATTRIBUTE_TANGENT;
					doff[3] = tsz;
					tsz += vcount * 3;
				}
				if (amesh->mBitangents != NULL) {
					meshFlags |= VERTEX_ATTRIBUTEID_BITANGENT;
					doff[4] = tsz;
					tsz += vcount * 3;
				}
			}
			
			new_mesh(m, meshFlags);

			if ((opt>>2)&1 && ascene->mNumAnimations != 0 && amesh->mNumBones != 0) {
				//mesh_animations *manim = m->animations = malloc(sizeof(mesh_animations));
				//manim->rootNode.parent = NULL;
				//asset_type_mesh_traverseNodeHierarchy(ascene->mRootNode,amesh,&manim->rootNode);
				//if (zIsY) asset_type_mesh_animationNodeSwapYZ(&manim->rootNode);
				//manim->numAnimations = ascene->mNumAnimations;
				//if (zIsY) printf("z is y\n");

			}
			
			m->numVertices = vcount;
			if (indexed) m->numIndices = amesh->mNumFaces*3;			
			bool rw = opt & 1;
			float *verts = malloc(tsz * 4);
			if (zIsY) asset_type_mesh_swapYZ(amesh->mVertices,verts,vcount);
			else memcpy(verts, amesh->mVertices, vcount * 12);
			mesh_set(m, verts, vcount, VERTEX_ATTRIBUTEID_POSITION, GL_STATIC_DRAW);
			if (rw) m->vertices = verts;
			if (indexed) {
				uint32_t *ind = verts + doff[7], *ib = ind;
				uint32_t nFace = amesh->mNumFaces;
				for (uint32_t i = 0; i < nFace; i++) {
					uint32_t *vid = amesh->mFaces[i].mIndices;
					*ib++ = *vid++; *ib++ = *vid++; *ib++ = *vid++;
				}
				mesh_set(m, ind, nFace * 3, VERTEX_INDEXED, GL_STATIC_DRAW);
				if (rw) m->indices = ind;
			}
			if (amesh->mTextureCoords[0] != NULL) {
				float *tuv = verts + doff[0], *puv = tuv,
					*uv = (float*)amesh->mTextureCoords[0];
				for (uint32_t i = 0; i < vcount; i++) {
					*puv++ = *uv++; *puv++ = *uv++;
					uv++;
				}
				mesh_set(m, tuv, vcount, VERTEX_ATTRIBUTEID_UV, GL_STATIC_DRAW);
				if (rw) m->uvs = tuv;
			}
			if (amesh->mColors[0] != NULL) {
				float *tc = verts + doff[1];
				memcpy(tc, amesh->mColors[0], vcount * 16);
				mesh_set(m, tc, vcount, VERTEX_ATTRIBUTEID_COLOR, GL_STATIC_DRAW);
				if (rw) m->colors = tc;
			}
			if (amesh->mNormals != NULL) {
				float *tn = verts + doff[2];
				if (zIsY) asset_type_mesh_swapYZ(amesh->mNormals,tn,vcount);
				else memcpy(tn, amesh->mNormals, vcount * 12);
				mesh_set(m, tn, vcount, VERTEX_ATTRIBUTEID_NORMAL, GL_STATIC_DRAW);
				if (rw) m->normals = tn;
			}
			if ((opt >> 1) & 1) {
				if (amesh->mTangents != NULL) {
					float *tc = verts + doff[3];
					if (zIsY) asset_type_mesh_swapYZ(amesh->mTangents,tc,vcount);
					else memcpy(tc, amesh->mTangents, vcount * 12);
					mesh_set(m, tc, vcount, VERTEX_ATTRIBUTEID_TANGENT, GL_STATIC_DRAW);
					if (rw) m->tangents = tc;
				}
				if (amesh->mBitangents != NULL) {
					float *tc = verts + doff[4];
					if (zIsY) asset_type_mesh_swapYZ(amesh->mBitangents,tc,vcount);
					else memcpy(tc, amesh->mBitangents, vcount * 12);
					mesh_set(m, tc, vcount, VERTEX_ATTRIBUTEID_BITANGENT, GL_STATIC_DRAW);
					if (rw) m->bitangents = tc;
				}
			}
			if (!rw) {
				m->vertices = NULL;
				free(verts);
			}
		}
		aiReleaseImport(ascene);
	}
	if (ascene == NULL || ascene->mNumMeshes <= smind) {
		m->numVertices = m->numIndices = 0;
		m->vertices = NULL;
		new_mesh(m, VERTEX_ATTRIBUTE_POSITION);
	}

	a->asset = m;
	a->fileSize = 0xFFFFFFFF;
}

void asset_type_mesh_load(asset *a) {
	//load options
	uint32_t opt = (1<<2) | (1<<3) | (1<<5);
	json_object *js = a->options;
	if (js != NULL) {
		const char *tos[6] = { "r",//readwrite
								"t",//tangents
								"a",//animation
								"i",//indexed
								"z",//z is y
								"s"//submesh
								};
		const bool tod[6] = { false, false, false, true, false, false };
		opt = 0;
		for (int i = 0; i < 6; i++) {
			list tl = cstring_as_list(tos[i]);
			json *jr = json_object_getClosest(js, &tl);
			if (jr != NULL) {
				if (i == 4) opt |= (jr->intv&255) << 24;
				else opt |= (jr->intv&1) << i;
			} else opt |= tod[i] << i;
		}
		delete_json(js);
	}

	//load assimp mesh
	void *d = malloc(a->fileSize);
	fread(d, 1, a->fileSize, a->fileHandle);

	const struct aiScene *ascene = aiImportFileFromMemory(d,a->fileSize,(((opt>>1)&1)?aiProcess_CalcTangentSpace:0) | aiProcess_Triangulate | (((opt>>3)&1)?0:aiProcess_JoinIdenticalVertices) | aiProcess_SortByPType,a->name.buffer);
	free(d);
	if (ascene == NULL) printf(aiGetErrorString());

	//run load mesh on main thread
	a->asset = ascene;
	a->fileSize = opt;
	singleRenderCall(asset_type_mesh_loadMain, a);
	while (a->fileSize == opt) threadSleep(1);
}

void asset_type_mesh_unloadMain(asset *a) {
	mesh *ma = a->asset;
	//if (ma->animations != NULL) delete_mesh_animations(ma->animations);
	delete_mesh(ma);
	a->instances = 1;
}
void asset_type_mesh_unload(asset *a) {
	singleRenderCall(asset_type_mesh_unloadMain, a);
	while (a->instances == 0) threadSleep(1);
	asset_type_unload(a);
}

const fixed_array asset_type_mesh_exts[6] = {{ ".fbx", 4, 1 }, {".obj",4,1}, {".dae", 4, 1}, { ".gltf", 5, 1 }, { ".blend", 6, 1 }, { ".3ds", 4, 1 }};
const asset_type ASSET_TYPE_MESH = { (void*)&asset_type_mesh_load, (void*)&asset_type_mesh_unload, {(void*)asset_type_mesh_exts, 6, 0}, (void*)LAST_ASSET_TYPE };
#undef LAST_ASSET_TYPE
#define LAST_ASSET_TYPE &ASSET_TYPE_MESH

#endif


//shader asset
void asset_type_shader_loadMain(asset *a) {
	shader *s = malloc(sizeof(shader));
	if (!new_shader(s, (list){a->asset,a->fileSize,1,7})) s = &blitShader;
	free(a->asset);
	a->asset = s;
	a->fileSize = a->instances;
	a->includes = a->options;
	a->options = NULL;
	a->instances = UINT_MAX;
}

uint32_t asset_shader_loadRenderingOptionsJSON(json_object *js) {
	const char *tos[4] = { "c",//cull
		"b",//blend
		"d",//depthtest
		"w" };//writedepth
	const uint8_t tod[4] = { CULLFACE_BACK, BLENDMODE_NONE, DEPTHTEST_LESS, true };
	uint32_t opt = 0;
	for (int i = 0; i < 4; i++) {
		list tl = cstring_as_list(tos[i]);
		json *jr = json_object_getClosest(js, &tl);
		int ioff = i * 8;
		uint16_t bv = 0;
		if (jr != NULL) {
			if (jr->type == JSON_TYPE_STRING) {
				list *str = jr->data;
				if (str->length != 0) {
					char fc = tolower(*(char*)str->buffer);
					if (i == 0) {
						if (fc == 'b') bv = CULLFACE_BACK;
						else if (fc == 'f') bv = CULLFACE_FRONT;
						else bv = CULLFACE_NONE;
					}
					else if (i == 1) {
						if (fc == 's') bv = BLENDMODE_SUBTRACT;
						else if (fc == 'r') bv = BLENDMODE_REVERSE_SUBTRACT;
						else if (fc == 'a') bv = BLENDMODE_ADDITIVE;
						else if (fc == 't') bv = BLENDMODE_TRANSPARENT;
						else if (fc == 'm') bv = BLENDMODE_MULTIPLY;
						else if (fc == 'p') bv = BLENDMODE_PREMULTIPLIED_TRANSPARENT;
					}
					else if (i == 2) {
						if (fc == 'l') bv = DEPTHTEST_LESS;
						else if (fc == 'g') bv = DEPTHTEST_GREATER;
					}
					else {
						if (fc == 'o' || fc == 't') bv = 1;
					}
				}
			}
			else bv = (uint32_t)jr->intv;
		}
		else bv = tod[i];
		opt |= bv << ioff;
	}
	return opt;
}

void asset_type_shader_load(asset *a) {
	//load shader text
	uint32_t fl = a->fileSize;
	uint8_t *d = malloc(fl);
	fread(d, 1, fl, a->fileHandle);

	//add includes
	asset **inca = NULL;

	const char *incStr = "#include \"";
	int32_t incInd = memIndexOf(d, 1, fl, incStr, 10);
	if (incInd != -1) {
		list incs,ctxt;
		new_list(&incs, sizeof(void*));
		new_list(&ctxt, 1);
		list_addm(&ctxt, d, fl);
		while (incInd != -1) {
			char qstring = '"';
			int32_t oInd = incInd + 10,
				eInd = memIndexOf((uint8_t*)ctxt.buffer + oInd, 1, fl - oInd, &qstring, 1);
			if (eInd == -1) break;

			asset *la = asset_database_get(&assets, hash((uint8_t*)ctxt.buffer + oInd, eInd));
			if (la != NULL && la->type == &ASSET_TYPE_RAW) {
				int32_t lchange = eInd + 11;
				list_removeRange(&ctxt, incInd, lchange);
				list_insertm(&ctxt, la->asset, incInd, la->fileSize);
				list_add(&incs, &la);
				eInd = incInd+la->fileSize;
			}
			else {
				eInd += oInd + 1;
			}
			incInd = memIndexOf((uint8_t*)ctxt.buffer + eInd, 1, fl - eInd, incStr, 10) + eInd;
		}
		if (incs.length != 0) {
			inca = malloc((incs.length + 1)*sizeof(void*));
			memcpy(inca, incs.buffer, incs.length*sizeof(void*));
			inca[incs.length] = NULL;
			delete_list(&incs);
			free(d);
			d = malloc(ctxt.length);
			memcpy(d, ctxt.buffer, ctxt.length);
			a->fileSize = ctxt.length;
		}
		delete_list(&ctxt);
	}

	//render options
	uint32_t opt = CULLFACE_BACK | (DEPTHTEST_LESS<<16) | (1<<24);
	json_object *js = a->options;
	if (js != NULL) {
		opt = asset_shader_loadRenderingOptionsJSON(js);
		delete_json_object(js);
		free(js);
	}

	//run new shader on main thread
	a->asset = d;
	a->instances = opt;
	a->options = inca;
	singleRenderCall(asset_type_shader_loadMain, a);
	while (a->instances != UINT_MAX) threadSleep(1);
	a->instances = 1;
}

void asset_type_shader_unloadMain(asset *a) {
	delete_shader(a->asset);
	a->instances = 1;
}
void asset_type_shader_unload(asset *a) {
	singleRenderCall(asset_type_shader_unloadMain, a);
	while (a->instances == 0) threadSleep(1);
	asset_type_unload(a);
}

const fixed_array asset_type_shader_exts[2] = { { ".glsl", 5, 1 }, { ".shader", 7, 1 } };
const asset_type ASSET_TYPE_SHADER = { (void*)&asset_type_shader_load, (void*)&asset_type_shader_unload, { (void*)asset_type_shader_exts, 2, 0 }, (void*)LAST_ASSET_TYPE};
#undef LAST_ASSET_TYPE
#define LAST_ASSET_TYPE &ASSET_TYPE_SHADER


//material asset
void asset_type_material_loadMain(asset *a) {
	material *m = a->asset;
	json_object *js = m->customData.buffer;
	uint8_t *tex = m->textures;
	uint32_t ropt = a->instances, texc = m->textureCount;
	char **texn = NULL;
	if (texc != 0) {
		texn = alloca(sizeof(void*)*texc);
		for (uint32_t i = 0; i < texc; i++) texn[i] = list_as_cstring((void*)(tex+(sizeof(list)+sizeof(void*))*i));
	}
	new_material(m,m->shader,m->data.data,m->data.size,texn,texc,ropt&255,(ropt>>8)&255,(ropt>>16)&255,(ropt>>24)&255,m->geometryType);
	if (texc != 0) {
		for (uint32_t i = 0; i < texc; i++) m->textures[i].texture = (void*)(tex+(sizeof(list)+sizeof(void*))*i+sizeof(list));
		free(tex);
	}
	delete_json_object(js);
	free(js);
	a->instances = 1;
	a->fileSize = UINT_MAX;
}

void asset_type_material_load(asset *a) {
	//load material text
	uint32_t fl = a->fileSize;
	uint8_t *d = malloc(fl);
	fread(d, 1, fl, a->fileHandle);

	material *mat = malloc(sizeof(material));
	mat->geometryType = GEOMETRY_TRIANGLE_FAN;
	mat->shader = &blitShader;

	list incs;
	new_list(&incs, sizeof(void*));

	//load material json
	list lstr;
	list_setBuffer(&lstr, d, fl);
	json_object *js = json_parse(&lstr).objectv;
	free(d);
	list bl = cstring_as_list("s");//shader
	json *jr = json_object_getClosest(js, &bl);
	uint32_t ropt = CULLFACE_BACK | (DEPTHTEST_LESS << 16) | (1 << 24);
	if (jr != NULL && jr->type == JSON_TYPE_STRING) {
		list *jstr = jr->data;
		asset *sas = asset_database_get(&assets, hash(jstr->buffer, jstr->length));
		if (sas->type == &ASSET_TYPE_SHADER) {
			mat->shader = sas->asset;
			ropt = sas->fileSize;
			list_add(&incs, &sas);
		}
	}
	bl = cstring_as_list("o");//options
	jr = json_object_getClosest(js, &bl);
	if (jr != NULL && jr->type == JSON_TYPE_OBJECT) ropt = asset_shader_loadRenderingOptionsJSON(jr->data);
	bl = cstring_as_list("d");//data
	jr = json_object_getClosest(js, &bl);
	if (jr != NULL && jr->type == JSON_TYPE_OBJECT) {
		json_object *jo = jr->data;
		json *jv = jo->values.buffer;
		const uint32_t klen = jo->values.length;
		uint32_t fcount = 0;
		for (uint32_t i = 0; i < klen; i++) {
			if (jv->type >= JSON_TYPE_INT) fcount++;
			else if (jv->type == JSON_TYPE_ARRAY) fcount += ((list*)jv->data)->length;
			jv++;
		}

		fcount *= 4;
		uint32_t *dat;
		mat->data.size = fcount;
		mat->data.data = dat = malloc(fcount);

		jv = jo->values.buffer;
		for (uint32_t i = 0; i < klen; i++) {
			if (jv->type >= JSON_TYPE_INT) *dat++ = jv->intv;
			else if (jv->type == JSON_TYPE_ARRAY) {
				list *a = jv->data;
				json *av = a->buffer;
				for (uint32_t k = 0; k < a->length; k++) {
					if (av->type >= JSON_TYPE_INT) *dat++ = av->intv;
					else *dat++ = 0;
					av++;
				}
			}
			jv++;
		}
	}
	else {
		mat->data.size = 0;
	}

	bl = cstring_as_list("t");//textures
	jr = json_object_getClosest(js, &bl);
	if (jr != NULL && jr->type == JSON_TYPE_OBJECT) {
		json_object *jo = jr->data;
		list *names = jo->keys.buffer;
		json *values = jo->values.buffer;
		const uint32_t texc = jo->keys.length;
		uint8_t *tb;
		mat->textureCount = texc;
		mat->textures = tb = malloc(texc*(sizeof(list) + sizeof(void*)));
		for (uint32_t i = 0; i < texc; i++) {
			memcpy(tb, names, sizeof(list));
			tb += sizeof(list);
			texture *tex;
			uint32_t ahv = 1;
			if (values->type == JSON_TYPE_INT) ahv = values->intv;
			else if (values->type == JSON_TYPE_STRING) {
				list *hstr = values->data;
				ahv = hash(hstr->buffer, hstr->length);
			}
			asset *texa = asset_database_get(&assets, ahv);
			if (texa == NULL || texa->type != &ASSET_TYPE_TEXTURE) {
				tex = &whiteTexture;
			}
			else {
				tex = texa->asset;
				list_add(&incs, &texa);
			}
			*(void**)tb = tex;
			tb += sizeof(void*);
			names++;
			values++;
		}	
	} else {
		mat->textureCount = 0;
	}
	
	bl = cstring_as_list("g");//geometry type
	jr = json_object_getClosest(js, &bl);
	if (jr != NULL) {
		if (jr->type == JSON_TYPE_STRING) {
			uint8_t geoType = GEOMETRY_TRIANGLES;
			list *gstr = jr->data;
			string_applyCharacterModifier(gstr->buffer, gstr->length, tolower);
			const char *optCStr[7] = {"lines",//lines
				"poi",//points
				"fan",//triangle fan
				"loo",//line loop
				"glstr",//triangle strip
				"nestr",//line strip
				"adstr"};//quad strip
			for (uint32_t i = 0; i < 7; i++) {
				char *cstr = optCStr[i];
				if (list_indexOf(gstr, cstr, strlen(cstr)) != -1) {
					geoType = i + 1;
					break;
				}
			}
			mat->geometryType = geoType;
		}
		else if (jr->type == JSON_TYPE_INT) {
			mat->geometryType = jr->intv;
		}
	}

	if (incs.length != 0) {
		a->includes = malloc(sizeof(void*)*(incs.length + 1));
		memcpy(a->includes, incs.buffer, incs.length*sizeof(void*));
		a->includes[incs.length] = NULL;
		delete_list(&incs);
	}
	else {
		a->includes = NULL;
	}

	//run new material on main thread
	mat->customData.buffer = js;
	a->asset = mat;
	a->instances = ropt;
	singleRenderCall(asset_type_material_loadMain, a);
	while (a->fileSize != UINT_MAX) threadSleep(1);
}

void asset_type_material_unloadMain(asset *a) {
	delete_material(a->asset);
	a->instances = 1;
}
void asset_type_material_unload(asset *a) {
	singleRenderCall(asset_type_material_unloadMain, a);
	while (a->instances == 0) threadSleep(1);
	asset_type_unload(a);
}

const fixed_array asset_type_material_exts[2] = { { ".mat", 4, 1 }, { ".material", 9, 1 } };
const asset_type ASSET_TYPE_MATERIAL = { (void*)&asset_type_material_load, (void*)&asset_type_material_unload, { (void*)asset_type_material_exts, 2, 0 }, (void*)LAST_ASSET_TYPE };
#undef LAST_ASSET_TYPE
#define LAST_ASSET_TYPE &ASSET_TYPE_MATERIAL

#endif
