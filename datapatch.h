﻿/*Copyright 2018 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _DATAPATCH_H_
#define _DATAPATCH_H_

#include "stream.h"

#define DATAPATCH_STACK_ALLOC (STREAM_MAX_STACK_ALLOC/2)

//record changes between old and new data and store in patch data
void new_datapatch(stream *patchData, stream *oldData, stream *newData) {
	uint8_t ob[DATAPATCH_STACK_ALLOC], nb[DATAPATCH_STACK_ALLOC];
	uint32_t orsz = 1, nrsz = 1;
	uint64_t sectionPos, headerPos;
	bool inSection = false;
	while (true) {
		if (orsz != 0) orsz = oldData->read(oldData,ob,DATAPATCH_STACK_ALLOC);
		if (nrsz != 0) nrsz = newData->read(newData,nb,DATAPATCH_STACK_ALLOC);
		
		if (nrsz == 0) {
			if (orsz == 0) break;
		} else {
			//compare data finding sections
			uint32_t msz = orsz<=nrsz?orsz:nrsz, i = 0;
			uint8_t *op = ob, *np = nb;
			while (i < msz) {
				uint32_t si = i;
				if (inSection) {
					while (i < msz) {
						if ((*op++) == (*np++)) break;
						i++;
					}
					if (si != i) patchData->write(patchData,nb+si,si-i);//write section data
					if (i < msz) {
						//go back to section header and write size
						sectionPos = (oldData->position-orsz+i)-sectionPos;
						uint64_t opos = patchData->position;
						patchData->setPosition(patchData,headerPos);
						stream_writeUniversal(patchData,&sectionPos,8,8);
						patchData->setPosition(patchData,opos);
						inSection = false;
						i++;
					}
				} else {
					//look for changes in data to start patching section
					while (i < msz) {
						if ((*op++) != (*np++)) break;
						i++;
					}
					if (i < msz) {
						//patch section header, position+size
						sectionPos = oldData->position-orsz+i;
						stream_writeUniversal(patchData,&sectionPos,8,8);
						headerPos = patchData->position;
						stream_writeUniversal(patchData,&sectionPos,8,8);
						patchData->write(patchData,nb+i,1);
						inSection = true;
						i++;
					}
				}
			}
			
			if (nrsz > orsz) {
				if (!inSection) {
					sectionPos = oldData->position-orsz+i;
					stream_writeUniversal(patchData,&sectionPos,8,8);
					headerPos = patchData->position;
					stream_writeUniversal(patchData,&sectionPos,8,8);
					inSection = true;
				}
				patchData->write(patchData,nb+i,nrsz-i);	
			}
		}
	}
	if (inSection) {
		sectionPos = (newData->position-nrsz+i)-sectionPos;
		uint64_t opos = patchData->position;
		patchData->setPosition(patchData,headerPos);
		stream_writeUniversal(patchData,&sectionPos,8,8);
		patchData->setPosition(patchData,opos);
	}
	//signal end using empty section
	sectionPos = 0;
	stream_writeUniversal(patchData,&sectionPos,8,8);
	stream_writeUniversal(patchData,&sectionPos,8,8);
	sectionPos = newData->length;
	stream_writeUniversal(patchData,&sectionPos,8,8);
}


//apply changes stored in patchData to data, returns the new length of data in bytes
uint64_t datapatch_apply(stream *patchData, stream *data) {
	uint64_t section[2];
	while (true) {
		patchData->readUniversal(patchData,section,8,16);
		if (section[1] == 0) break;
		data->setPosition(data,section[0]);
		stream_copy(data,patchData,section[1]);
	}
	patchData->readUniversal(patchData,section,8,8);
	return section[0];
}

#endif
