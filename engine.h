/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _ENGINE_H_
#define _ENGINE_H_

#include "slist.h"
#include "vecmath.h"
#ifndef ENGINE_NO_WINDOW
#include "window.h"
#include "graphics.h"
#endif
#include "system.h"
#include "timer.h"
#include "stream.h"


//object component system
typedef struct object {
	vec3 position, rotation, scale, absolutePosition, absoluteRotation, absoluteScale;
	list children, components;
	slist transformUpdateCalls;
	struct object *parent;
	list name;
	uint8_t layer;
	bool active, absoluteActive;
} object;

typedef struct {
	char *name;
	one_arg_function init;
	two_arg_function duplicate;
	struct component_data *next;
	reflection_data *fields;
	uint16_t size,fieldCount,nameLength;
} component_data;
#define LAST_COMPONENT_DATA NULL

typedef struct {
	one_arg_function free, enable, disable, update, render, transformUpdate;
#ifdef ENGINE_EDITMODE
	one_arg_function editorRender;
#define COMPONENT_EDITOR_RENDER_FILLER NULL,
#else
#define COMPONENT_EDITOR_RENDER_FILLER 
#endif
	object *object;
	list name;
	component_data *metadata;
	bool active, enabled;
} component;


//global engine variables
uint32_t updateInterval = 10;
uint64_t currentTime;
float timeScale = 1.0f, deltaTime = 0.01f;
bool running = false, stillRunning = true, syncedCallRenderDone = false;
uint8_t syncedCallSwapId = 0;
object scene;
#define csif(mt) construct_slist(sizeof(ifunction),mt)
slist updateCalls = csif(false),
singleUpdateCalls = csif(true),
renderCalls = csif(true),
singleRenderCalls = csif(true),
eventCalls = csif(true),
syncedCalls[2] = {csif(false),csif(false)};
#undef csif
timers_handling timers;

//proxy variables when there is no window
#ifndef ENGINE_NO_WINDOW
window engineWindow;

#define MOUSEBUTTON_LEFT 0
#define MOUSEBUTTON_MIDDLE 1
#define MOUSEBUTTON_RIGHT 2
bool mouse[9] = { false, false, false, false, false, false, false, false, false };
int32_t mouseX = -1, mouseY = -1, lastMouseX = -1, lastMouseY = -1, mouseDeltaX = 0, mouseDeltaY = 0;
float nMouseX = -1.0f, nMouseY = -1.0f;

list textInput = construct_list(1);
uint32_t textInputCursor = 0, textInputSelection = 0, textInputLock = 0;
bool modifyingTextInput = false, textInputMultiline = false, textInputModified = false, textInputNavigation = false;
#endif

//singleRenderCall will call function 'f' on the main/render thread 1 time passing the argument 'o'
#ifdef ENGINE_NO_WINDOW
#define singleRenderCall(f,o)
#else
#define singleRenderCall(f,o) if (!running) {f(o);} else {cpifunction((void*)f,o,slist_add(&singleRenderCalls,&ifunc));}
#endif

/*syncedCall will call function 'f' on the main thread 1 time passing the argument 'o',
'f' will only be called after 1 update tick and 1 render tick.*/
#define syncedCall(f,o) cpifunction((void*)f,o,slist_add(&syncedCalls[syncedCallSwapId],&ifunc))

#define call_slist_of_ifunctions_base(sl) slist_lock(&sl);\
{ifunction *ifuncs = (ifunction*)sl.list.buffer;\
for (uint32_t i = 0; i < sl.list.length; i++) {\
	ifuncs->delegate(ifuncs->owner);\
	ifuncs++;\
}}
#define call_slist_of_ifunctions(sl) call_slist_of_ifunctions_base(sl);slist_unlock(&sl)
#define call_slist_of_ifunctions_once(sl) call_slist_of_ifunctions_base(sl);sl.list.length=0;slist_unlock(&sl)


//set update rate and time scale
void setUpdateRate(const uint32_t ticksPerSecond, const float timeScal) {
	updateInterval = 1000/ticksPerSecond;
	timeScale = timeScal;
	deltaTime = updateInterval*timeScal/1000.0f;
}

//component abstract functions
void component_enable(component *c) {
	if (c->enabled) return;
	c->enabled = true;

	one_arg_function f = c->enable;
	if (f != NULL) f(c);

	ifunction ifunc;
	ifunc.owner = c;
	#define aif(s,d) f = c->s;if (f != NULL) {ifunc.delegate = f; slist_add(&d,&ifunc);}
	aif(update,updateCalls);
	aif(render,renderCalls);
	aif(transformUpdate, c->object->transformUpdateCalls);
	#undef aif
}
void component_disable(component *c) {
	if (!c->enabled) return;
	c->enabled = false;

	one_arg_function f;

	ifunction ifunc;
	ifunc.owner = c;
	#define rif(s,d) f = c->s;if (f != NULL) {ifunc.delegate = f;slist_remove(&d,&ifunc);}
	rif(update,updateCalls);
	rif(render,renderCalls);
	rif(transformUpdate, c->object->transformUpdateCalls);
	#undef rif

	f = c->disable;
	if (f != NULL) f(c);
}

static inline void free_component(component *c) {
	component_disable(c);
	if (c->free != NULL) syncedCall(*c->free,c);
}
static inline void delete_component(component *c) {
	if (c->object != NULL) list_remove(&c->object->components, &c);
	free_component(c);
	syncedCall(free,c);
}

//toggle component active
static inline void component_setActive(component *c, bool act) {
	if (c->object != NULL) {
		if (act && c->object->absoluteActive) component_enable(c);
		else component_disable(c);
	}
	
	c->active = act;
}
//move component to a different object
static inline void component_setObject(component *c, object *o) {
	if (c->object != NULL) list_remove(&c->object->components, &c);
	list_add(&o->components, &c);
	c->object = o;
}

//creates a duplicate of component c
component *component_duplicate(component *c) {
	component *n = malloc(c->metadata->size);
	component_data *md = c->metadata;
	*n = (component){NULL, NULL, NULL, NULL, NULL, NULL, COMPONENT_EDITOR_RENDER_FILLER NULL, c->name, md, c->active, false};
	md->init(n);
	reflection_copy(md->fields,md->fieldCount,c,n,true);
	if (md->duplicate != NULL) md->duplicate(n,c);
	return n;
}

//auto function generation
//create(allocate) new object/component type
#define deffunc_create_type(typeName) static inline typeName *create_##typeName() {\
typeName *o = malloc(sizeof(typeName));\
new_##typeName(o);\
return o;}

//init component functions
#if defined(ENGINE_NO_REFLECTION_NAMES) && !defined(ENGINE_EDITMODE)
#define RCF REFLECTF
#else
#define RCF REFLECT
#endif
//sizeof(compName) might not need -1 on windows
#define define_componento(compName,fs,fc,dup) component_data component_data_##compName = {#compName, (void*)&init_##compName, (void*)dup, (void*)LAST_COMPONENT_DATA,(void*)fs,sizeof(compName),fc,sizeof(#compName)-1};\
static inline void new_##compName(compName *o) {\
*((component*)o) = (component){NULL, NULL, NULL, NULL, NULL, NULL, COMPONENT_EDITOR_RENDER_FILLER NULL, cstring_as_list(component_data_##compName.name), (void*)&component_data_##compName, true, false};\
init_##compName(o);\
}\
deffunc_create_type(compName);
#define define_component(compName,fs,fc) define_componento(compName,fs,fc,NULL)
#define define_componentd(compName,fs,fc,dup) define_componento(compName,fs,fc,dup)

void free_object_callback(object *o) {
	//delete components
	uint32_t nc = o->components.length;
	component** comps = o->components.buffer;
	for (uint32_t i = 0; i < nc; i++) free(*comps++);

	//delete children
	nc = o->children.length;
	object** children = o->children.buffer;
	for (uint32_t i = 0; i < nc; i++) free(*children++);

	delete_list(&o->components);
	delete_list(&o->children);
	delete_slist(&o->transformUpdateCalls);
	delete_list(&o->name);
}

void free_object(object *o) {
	//delete components
	uint32_t nc = o->components.length;
	component** comps = o->components.buffer;
	for (uint32_t i = 0; i < nc; i++) free_component(*comps++);

	//delete children
	nc = o->children.length;
	object** children = o->children.buffer;
	for (uint32_t i = 0; i < nc; i++) free_object(*children++);

	syncedCall(free_object_callback, o);
}


//delete all objects children
void object_deleteChildren(object *o) {
	uint32_t nc = o->children.length;
	object** children = o->children.buffer;
	for (uint32_t i = 0; i < nc; i++) {
		object *cc = *children++;
		free_object(cc);
		syncedCall(free,cc);
	}
	o->children.length = 0;
}

//delete object and all its children also removing object entry from parent
static inline void delete_object(object *o) {
	free_object(o);
	list_remove(&o->parent->children, &o);
	syncedCall(free,o);
}

//add component to object
static inline void object_addComponent(object *o, void *p) {
	component *c = p;
	list_add(&o->components, &c);
	c->object = o;

	if (o->absoluteActive && c->active) component_enable(c);
}

//update transform absolute position, rotation, scale
void object_updateAbsoluteTransform(object *o) {
	object *p = o->parent;
	
	o->absolutePosition = o->position;
	vec3_transform(&o->absolutePosition,&p->absolutePosition,&p->absoluteRotation,&p->absoluteScale);

	o->absoluteRotation = o->rotation;
	vec3_add(&o->absoluteRotation,&p->absoluteRotation);
	o->absoluteScale = o->scale;
	vec3_mul(&o->absoluteScale,&p->absoluteScale);
}
void object_updateTransform(object *o) {
	object_updateAbsoluteTransform(o);

	//call transform update events
	const uint32_t uc = o->transformUpdateCalls.list.length;
	if (uc != 0) {
		slist_lock(&o->transformUpdateCalls);
		ifunction *df = (ifunction*)o->transformUpdateCalls.list.buffer;
		for (uint32_t i = 0; i < uc; i++) df[i].delegate(df[i].owner);
		slist_unlock(&o->transformUpdateCalls);
	}

	//update children transforms
	uint32_t nc = o->children.length;
	object **c = (object**)o->children.buffer;
	for (uint32_t i = 0; i < nc; i++) object_updateTransform(*c++);
}

//set object active state
void object_setAbsoluteActive(object *o, bool aact) {
	//enable/disable components with new active state
	component **c = o->components.buffer;
	for (uint32_t i = 0; i < o->components.length; i++) {
		if (c[0]->active && aact) component_enable(*c);
		else component_disable(*c);
		c++;
	}

	//update childrens absolute active state
	object **q = o->children.buffer;
	for (uint32_t i = 0; i < o->children.length; i++) {
		object_setAbsoluteActive(*q, q[0]->active & aact);
		q++;
	}

	o->absoluteActive = aact;
}
static inline void object_setActive(object *o, bool act) {
	o->active = act;
	object_setAbsoluteActive(o, act&o->parent->active);
}

//setup object
void init_object(object *o) {
	o->name = cstring_as_list("object");
	o->active = true;
	o->absoluteActive = true;
	o->layer = 0;
	o->position = (vec3){0.0f,0.0f,0.0f};
	o->rotation = (vec3){0.0f,0.0f,0.0f};
	o->scale = (vec3){1.0f,1.0f,1.0f};
	o->absolutePosition = (vec3){0.0f,0.0f,0.0f};
	o->absoluteRotation = (vec3){0.0f,0.0f,0.0f};
	o->absoluteScale = (vec3){1.0f,1.0f,1.0f};
	new_list(&o->components, sizeof(void*));
	new_list(&o->children, sizeof(void*));
	new_slist(&o->transformUpdateCalls, sizeof(ifunction), false);
}
//setup object and add to scene
void new_object(object *o) {
	init_object(o);
	o->parent = &scene;
	list_add(&scene.children, &o);
}
deffunc_create_type(object);



//set parent of object o to p
void object_setParent(object *o, object *p) {
	if (o->parent != NULL) list_remove(&o->parent->children, &o);
	o->parent = p;
	list_add(&p->children, &o);

	object_setActive(o, o->active);
	object_updateTransform(o);
}

//get object in children of o
object *object_findChild(object *o, list *childName) {
	uint32_t nc = o->children.length;
	object **c = o->children.buffer;
	for (uint32_t i = 0; i < nc; i++) {
		if (list_equals(&c[i][0].name, childName)) return c[i];
	}

	return NULL;
}

//get object in transform heirarchy of o by name string
object *object_find(object *o, list *objName) {
	uint32_t nc = o->children.length;
	object **c = o->children.buffer;
	for (uint32_t i = 0; i < nc; i++) {
		if (list_equals(&c[i][0].name, objName)) return c[i];

		object *res = object_find(c[i], objName);
		if (res != NULL) return res;
	}

	return NULL;
}

//check that object 's' isnt in a child below 'o'
bool object_isChildBelow(object *o, object *s) {
	uint32_t nc = o->children.length;
	object **c = o->children.buffer;
	for (uint32_t i = 0; i < nc; i++) {
		object *cc = *c++;
		if (cc == s) return true;
		if (cc->children.length > 0) {
			if (object_isChildBelow(cc,s)) return true;
		}
	}
	return false;
}

//get component in object by name string
component *object_getComponent(object *o, list *componentName) {
	component **comp = o->components.buffer;
	for (uint32_t i = 0; i < o->components.length; i++) {
		if (list_equals(&comp[i][0].name, componentName)) return comp[i];
	}
	return NULL;
}

//get component in object whos name starts with 'componentName'
component *object_getComponentClosest(object *o, list *componentName) {
	component **comp = o->components.buffer;
	for (uint32_t i = 0; i < o->components.length; i++) {
		if (list_startsWith(&comp[i][0].name, componentName)) return comp[i];
	}
	return NULL;
}

//get component in object or parent by name string
component *object_get(object *o, list *componentName) {
	component *c;
	while ((c = object_getComponent(o,componentName)) == NULL) {
		void *lp = o;
		o = o->parent;
		if (o == lp) return NULL;
	}
	return c;
}

component *object_getClosest(object *o, list *componentName) {
	component *c;
	while ((c = object_getComponentClosest(o,componentName)) == NULL) {
		void *lp = o;
		o = o->parent;
		if (o == lp) return NULL;
	}
	return c;
}

//returns the next component named 'n' after index 't' and outputs the next index to 't'. returns NULL if no more components named 'n'
component* object_enumerateComponents(object *o, list *n, uint32_t *t) {
	component **comp = o->components.buffer;
	for (uint32_t i = *t+1; i < o->components.length; i++) {
		if (list_equals(&comp[i][0].name, n)) {
			*t = i;
			return comp[i];
		}
	}

	return NULL;
}

//creates a duplicate of object o under parent 'p'
object *object_duplicate(object *o, object *p, const bool children) {
	object *n = malloc(sizeof(object));
	memcpy(n,o,sizeof(object));
	
	if (n->name.bufferSize != 7)  {
		new_list(&n->name,1);
		list_copy(&n->name,&o->name);
	}

	new_list(&n->components, sizeof(void*));
	new_list(&n->children, sizeof(void*));
	new_slist(&n->transformUpdateCalls, sizeof(ifunction), false);
	
	n->absoluteActive = n->active&p->absoluteActive;
	n->parent = p;
	list_add(&p->children,&n);

	for (uint32_t i = 0; i < o->components.length; i++) {
		object_addComponent(n, component_duplicate(((component**)o->components.buffer)[i]));
	}
	
	if (children) {
		for (uint32_t i = 0; i < o->children.length; i++) object_duplicate(((object**)o->children.buffer)[i],n,true);
	}

	return n; 
}


//engine launching/startup
typedef struct {
	zero_arg_function startup, shutdown;
	char *windowTitle;
	uint32_t windowWidth, windowHeight;
	bool windowBordered, windowFullscreen, windowVSync;
} engine_launch_options;

void new_engine(const engine_launch_options opt) {
	currentTime = timeNow();
	
	#ifndef ENGINE_NO_WINDOW
	//init SDL, window and opengl context
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		printf("SDL error at SDL_Init(SDL_INIT_VIDEO): '%s'.\n",SDL_GetError());
		engineWindow.window = NULL;
		return;
	}
	if (!new_window(&engineWindow, opt.windowTitle, opt.windowWidth, opt.windowHeight, opt.windowBordered, opt.windowFullscreen, opt.windowVSync)) {
		engineWindow.window = NULL;
		return;
	}
	window_init_graphics(engineWindow);
	#endif

	//init scene node
	init_object(&scene);
	scene.parent = &scene;

	//init timer system
	new_timers(&timers);
	
	//startup
	opt.startup();
}
void delete_engine(const engine_launch_options opt) {
	//shutdown
	if (opt.shutdown != NULL) opt.shutdown();

	//destroy scene/all objects left 
	free_object(&scene);

	delete_slist(&updateCalls);
	delete_slist(&singleUpdateCalls);
	delete_slist(&renderCalls);
	delete_slist(&singleRenderCalls);
	delete_slist(&syncedCalls[0]);
	delete_slist(&syncedCalls[1]);
	delete_slist(&eventCalls);
	delete_timers(&timers);

	#ifndef ENGINE_NO_WINDOW
	delete_list(&textInput);
	//free graphics
	free_graphics();
	//delete window/opengl context
	delete_window(&engineWindow);
	//quit SDL
	SDL_Quit();
	#endif
}

#endif
