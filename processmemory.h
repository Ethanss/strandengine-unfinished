/*Copyright 2018 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files(the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and / or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions :
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _PROCESSMEMORY_H_
#define _PROCESSMEMORY_H_

#ifdef __linux__
#include "stream.h"
#include <sys/uio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ptrace.h>
#define process_id pid_t
#else
#include "system.h"
#include <TlHelp32.h>
#include <Psapi.h>
#include <tchar.h>
#define process_id DWORD
#endif

#define INVALID_PROCESS_ID 0

typedef struct {
	char *filepath;
	uint32_t filepathLength;
	process_id id;
} process_info;

//return list of processes running on the system
list listProcesses() {
	list pl;
	new_list(&pl,sizeof(process_info));
#ifdef __linux__
	char buf[LIST_MAX_STACK_ALLOC];
	const char *procStr = "/proc/", *exeStr = "/exe";
	const uint32_t exeLen = strlen(exeStr);
	list nl, pfl = listFiles(procStr);
	new_list(&nl,1);
	list_addm(&nl,procStr,strlen(procStr));
	file_info *fi = pfl.buffer;
	for (uint32_t i = 0; i < pfl.length; i++) {
		if (fi->type == FILE_TYPE_DIRECTORY) {
			const uint32_t fnl = strlen(fi->name);
			list_addm(&nl,fi->name,fnl);
			list_addm(&nl,exeStr,exeLen);
			
			int32_t fsz = readlink(list_as_cstring(&nl),buf,LIST_MAX_STACK_ALLOC);
			if (fsz > 0) {
				const uint32_t index = list_addEmpty(&pl);
				process_info *dp = (process_info*)pl.buffer+index;
				dp->id = atoi(fi->name);
				if (fsz == LIST_MAX_STACK_ALLOC) fsz--;
				dp->filepath = malloc(fsz+1);
				memcpy(dp->filepath,buf,fsz);
				dp->filepath[fsz] = 0;
				dp->filepathLength = fsz;
			}
			nl.length -= fnl+exeLen+1;
		}
		fi++;
	}
	delete_list(&pfl);
	delete_list(&nl);
#else
	HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
	if (snapshot != INVALID_HANDLE_VALUE) {
		PROCESSENTRY32 entry;
		entry.dwSize = sizeof(PROCESSENTRY32);
		MODULEENTRY32 ModuleEntry32;
		ModuleEntry32.dwSize = sizeof(MODULEENTRY32);
		
		bool running = Process32First(snapshot,&entry)==TRUE;
		while (running) {			
			HANDLE modSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, pid);
			if (modSnapshot != INVALID_HANDLE_VALUE) {
				if (Module32First(modSnapshot,&ModuleEntry32)) {
					const uint32_t index = list_addEmpty(&pl);
					process_info *dp = (process_info*)pl.buffer+index;
					dp->id = entry.th32ProcessID;
					dp->filepathLength = strlen(ModuleEntry32.szExePath);
					const uint32_t esz = dp->filepathLength+1;
					dp->filepath = malloc(esz);
					memcpy(dp->filepath,ModuleEntry32.szExePath,esz);
				}
				CloseHandle(modSnapshot);
			}
			running = Process32Next(snapshot, &entry);
		}
		CloseHandle(snapshot);
	}
#endif
	return pl;
}
void deleteProcessesList(const list pl) {
	process_info *p = pl.buffer;
	for (uint32_t i = 0; i < pl.length; i++) free((p++)->filepath);
	delete_list(&pl);
}

//find process in list from name
process_info *findProcess(const char *procName, list *processes) {
	const uint32_t pnl = strlen(procName);
	process_info *proc = processes->buffer;
	for (uint32_t i = 0; i < processes->length; i++) {
		if (memEndsWith(proc->filepath,proc->filepathLength,procName,pnl)) return proc;
		proc++;
	}
	return NULL;
}


typedef struct {
	char *filepath;
	uint32_t filepathLength;
	void *baseAddress;
} module_info;

//return list of modules in process
list listProcessModules(const process_id pid) {
	list ml;
	new_list(&ml,sizeof(module_info));
#ifdef __linux__
	uint32_t lastLen = 0;
	char b[LIST_MAX_STACK_ALLOC*2],last[32];
	snprintf(b,LIST_MAX_STACK_ALLOC,"/proc/%lld/maps",pid);
	FILE *fp = fopen(b,FILE_MODE_READ);
	if (fp != NULL) {
		uint32_t rsz, ll = 0;
		while ((rsz = fread(b+ll,1,LIST_MAX_STACK_ALLOC,fp)) != 0) {
			rsz += ll;
			for (uint32_t i = ll; i < rsz; i++) {
				if (b[i] == '\n') {
					const uint32_t llen = i-ll;
					if (llen != 0) {
						const char sep = '/', div = '-';
						char *cp = b+ll;
						const int32_t pind = memIndexOf(cp,1,llen,&sep,1), dind = memIndexOf(cp,1,llen,&div,1);
						if (pind != -1 && dind != -1) {
							bool isOriginal;
							if (lastLen != 0 && llen > lastLen) isOriginal = memcmp(last,cp+llen-lastLen,lastLen)!=0;
							else isOriginal = true;
							
							if (isOriginal) {
								const uint32_t index = list_addEmpty(&ml);
								module_info *mi = (module_info*)ml.buffer+index;
								cp[dind] = 0;
								mi->baseAddress = (void*)(uintptr_t)strtoull(cp,NULL,16);
								lastLen = mi->filepathLength = llen-pind;
								mi->filepath = malloc(lastLen+1);
								memcpy(mi->filepath,cp+pind,lastLen);
								mi->filepath[lastLen] = 0;
								if (lastLen > 32) lastLen = 32;
								memcpy(last,cp+llen-lastLen,lastLen);
							}
						} else {
							lastLen = 0;
						}
					}
					ll = i+1;
				}
			}
			if (ll < rsz) {
				const uint32_t nll = rsz-ll;
				memmove(b,b+ll,nll);
				ll = nll;
			} else ll = 0;
		}
		fclose(fp);
	}
#else
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, pid);
	if (hSnapshot != INVALID_HANDLE_VALUE) {
		MODULEENTRY32 ModuleEntry32;
		ModuleEntry32.dwSize = sizeof(MODULEENTRY32);
		bool running = Module32First(hSnapshot,&ModuleEntry32);
		while (running) {
			const uint32_t index = list_addEmpty(&ml);
			module_info *mi = (module_info*)ml.buffer+index;
			mi->baseAddress = ModuleEntry32.modBaseAddr;
			mi->filepathLength = strlen(ModuleEntry32.szExePath);
			const uint32_t slen = mi->filepathLength+1;
			mi->filepath = malloc(slen);
			memcpy(mi->filepath,ModuleEntry32.szExePath,slen);

			running = Module32Next(hSnapshot, &ModuleEntry32);
		}
		CloseHandle(hSnapshot);
	}
#endif
	return ml;
}
void deleteProcessModulesList(const list pl) {
	module_info *p = pl.buffer;
	for (uint32_t i = 0; i < pl.length; i++) free((p++)->filepath);
	delete_list(&pl);
}

//get module from name and list of modules
module_info *findProcessModule(const char *moduleName, list *modules) {
	const uint32_t pnl = strlen(moduleName);
	module_info *mod = modules->buffer;
	for (uint32_t i = 0; i < modules->length; i++) {
		if (memEndsWith(mod->filepath,mod->filepathLength,moduleName,pnl)) return mod;
		mod++;
	}
	return NULL;
}


typedef struct {
	process_id pid;
	#ifdef __linux__
	char mempath[64];
	int32_t lastWriteState;
	bool write;
	#else
	HANDLE handle;
	#endif
} process_memory;

#ifdef __linux__
const char *YAMA_PTRACE_SCOPE_FILE_STR = "/proc/sys/kernel/yama/ptrace_scope";
#endif

bool new_process_memory(process_memory *m, const process_id id, bool write, bool debug) {
	m->pid = id;
	#ifdef __linux__
	FILE *fp;
	if (write && (fp=fopen(YAMA_PTRACE_SCOPE_FILE_STR,FILE_MODE_READ)) != NULL) {
		char pwstr[32];
		uint32_t owsz = fread(pwstr,1,31,fp);
		fclose(fp);
		pwstr[owsz] = 0;
		m->lastWriteState = atoi(pwstr);
		
		char state = '0';
		m->write = file_writeall(YAMA_PTRACE_SCOPE_FILE_STR,&state,1);
	} else m->write = false;
	if (m->write) snprintf(m->mempath, 64, "/proc/%lld/mem", id);
	return true;
	#else
	if (debug) {
		HANDLE hToken;
		LUID luid;

		TOKEN_PRIVILEGES tkp;
		OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken);
		LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &luid);

		tkp.PrivilegeCount = 1;
		tkp.Privileges[0].Luid = luid;
		tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
	
		bool res = AdjustTokenPrivileges(hToken, false, &tkp, sizeof(tkp), NULL, NULL);
		CloseHandle(hToken);
	}
	m->handle = OpenProcess(debug?PROCESS_ALL_ACCESS:(write?(PROCESS_VM_OPERATION | PROCESS_VM_READ | PROCESS_VM_WRITE):PROCESS_VM_READ),false,id);
	return m->handle!=NULL;
	#endif
}
void delete_process_memory(process_memory *m) {
	#ifdef __linux__
	if (m->write) {
		char pwstr[32];
		int32_t len = snprintf(pwstr,32,"%d",m->lastWriteState);
		file_writeall(YAMA_PTRACE_SCOPE_FILE_STR,pwstr,len);
	}
	#else
	CloseHandle(m->handle);
	#endif
}

static inline bool process_memory_read(process_memory *m, void *src, void *dst, const uint32_t sz) {
	#ifdef __linux__
	struct iovec rv = {src,sz}, lv = {dst,sz};
	return process_vm_readv(m->pid,&lv,1,&rv,1,0)==sz;
	#else
	return ReadProcessMemory(m->handle,src,dst,sz,NULL);
	#endif
}
bool process_memory_write(process_memory *m, void *dst, void *src, const uint32_t sz) {
	#ifdef __linux__
	struct iovec rv = {dst,sz}, lv = {src,sz};
	if (process_vm_writev(m->pid,&lv,1,&rv,1,0) != sz) {
		if (ptrace(PTRACE_ATTACH,m->pid,0,0) == -1) return false;
		waitpid(m->pid,NULL,__WALL);
		uint32_t rsz = process_vm_writev(m->pid,&lv,1,&rv,1,0);
		ptrace(PTRACE_DETACH,m->pid,0,0);
		return rsz!=sz;
	}
	#else
	if (!WriteProcessMemory(m->handle,dst,src,sz,NULL)) {
		DWORD last;
		VirtualProtectEx(m->handle,dst,sz,PAGE_EXECUTE_READWRITE,&last);
		bool res = WriteProcessMemory(m->handle,dst,src,sz,NULL);
		VirtualProtectEx(m->handle,dst,sz,last,NULL);
		return res;
	}
	#endif
	return true;
}

typedef struct {
	void *src, *dst;
	uint32_t size;
} process_readwrite;

bool process_memory_readBatch(process_memory *m, process_readwrite *r, const uint32_t count) {
	#ifdef __linux__
	struct iovec *io,*lv,*rv;
	const uint32_t asz = count*sizeof(iovec)*2;
	if (count > LIST_MAX_STACK_ALLOC) io = malloc(asz);
	else io = alloca(asz);
	lv = io;
	rv = io+count;
	uint32_t tsz = 0;
	for (uint32_t i = 0; i < count; i++) {
		const uint32_t ssz = r->size;
		tsz += ssz;
		rv->iov_base = r->src;
		rv->iov_len = ssz;
		rv++;
		lv->iov_base = r->dst;
		lv->iov_len = ssz;
		lv++;
	}
	bool res = process_vm_readv(m->pid,io,count,io+count,count,0)!=tsz;
	if (count > LIST_MAX_STACK_ALLOC) free(io);
	return res;
	#else
	HANDLE h = m->handle;
	for (uint32_t i = 0; i < count; i++) {
		if (!ReadProcessMemory(h,r->src,r->dst,r->size,NULL)) return false;
		r++;
	}
	#endif
	return true;
}
bool process_memory_writeBatch(process_memory *m, process_readwrite *r, const uint32_t count) {
	#ifdef __linux__
	struct iovec *io,*lv,*rv;
	const uint32_t asz = count*sizeof(iovec)*2;
	if (count > LIST_MAX_STACK_ALLOC) io = malloc(asz);
	else io = alloca(asz);
	lv = io;
	rv = io+count;
	uint32_t tsz = 0;
	for (uint32_t i = 0; i < count; i++) {
		const uint32_t ssz = r->size;
		tsz += ssz;
		rv->iov_base = r->dst;
		rv->iov_len = ssz;
		rv++;
		lv->iov_base = r->src;
		lv->iov_len = ssz;
		lv++;
	}
	bool res = process_vm_writev(m->pid,io,count,io+count,count,0) != tsz;
	if (!res) {
		res = ptrace(PTRACE_ATTACH,m->pid,0,0) == -1;
		if (res) {
			waitpid(m->pid,NULL,__WALL);
			res = process_vm_writev(m->pid,io,count,io+count,count,0)!=tsz;
			ptrace(PTRACE_DETACH,m->pid,0,0);
		}
	}
	if (count > LIST_MAX_STACK_ALLOC) free(io);
	return res;
	#else
	HANDLE h = m->handle;
	for (uint32_t i = 0; i < count; i++) {
		if (!WriteProcessMemory(h,r->dst,r->src,sz,NULL)) {
			DWORD last;
			VirtualProtectEx(h,r->dst,sz,PAGE_EXECUTE_READWRITE,&last);
			bool res = WriteProcessMemory(h,r->dst,r->src,sz,NULL);
			VirtualProtectEx(h,r->dst,sz,last,NULL);
			if (!res) return false;
		}
		r++;
	}
	#endif
	return true;
}


list *process_memory_search(process_memory *m, const char *target, const int32_t wildcard, const uint32_t sz, const void *start, const void *end, list *results, const int32_t resultLimit) {
	if (results == NULL) {
		results = malloc(sizeof(list));
		new_list(results,sizeof(void*));
	}
	
	const uint32_t SEARCH_STRIDE = 4096;
	if (sz > SEARCH_STRIDE) return results;
	
	uint32_t advance = 0;
	uint8_t b[SEARCH_STRIDE*2], *p = start;
	while ((uintptr_t)p < (uintptr_t)end) {
		if (process_memory_read(m,p,b+advance,SEARCH_STRIDE)) {
			const uint32_t ssz = SEARCH_STRIDE+advance, diff = ssz-sz+1;
			uint8_t *c = b;
			if (wildcard < 0) {
				for (uint32_t i = 0; i < diff; i++) {
					if (memcmp(c,target,sz) == 0) {
						void *rp = p+i;
						list_add(results,&rp);
						if (resultLimit > 0 && results->length >= resultLimit) return results;
					}
					c++;
				}
			} else {
				for (uint32_t i = 0; i < diff; i++) {
					uint8_t *a = c, *b = target;
					uint32_t szc = sz;
					while (szc != 0) {
						const char v = *a++, t = *b++;
						if (t != wildcard && v != t) break;
						szc--;
					}
					if (szc == 0) {
						void *rp = p+i;
						list_add(results,&rp);
						if (resultLimit > 0 && results->length >= resultLimit) return results;
					}
					c++;
				}
			}
			if (sz > 1) {
				advance = sz-1;
				memmove(b,c,advance);
			}
		}
		p += SEARCH_STRIDE;
	}
	
	return results;
}

#endif
