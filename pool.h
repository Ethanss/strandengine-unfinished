﻿/*Copyright 2019 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _POOL_H_
#define _POOL_H_

#include "misc.h"
#include <stdlib.h>

typedef struct {
	void *buffer;
	uint32_t bufferSize, stride, nextOpenIndex;
	one_arg_function newStruct, freeStruct;
} pool;


//initialize pool
static inline void new_pool(pool *p, const uint32_t vsz, one_arg_function newf, one_arg_function freef) {
	p->bufferSize = 0;
	p->stride = vsz+1;
	p->nextOpenIndex = 0;
	p->newStruct = newf;
	p->freeStruct = freef;
}

//clear/return all objects to pool
void pool_clear(pool *p) {
	if (p->bufferSize == 0) return;

	uint8_t *d = p->buffer;
	uint32_t stride = p->stride;
	for (uint32_t i = 0; i < p->bufferSize; i++) {
		if (*d) p->freeStruct(d+1);
		d += stride;
	}
}

//free pool memory
static inline void delete_pool(pool *p) {
	if (p->bufferSize != 0) {
		pool_clear(p);
		free(p->buffer);
	}
}

//get object from pool returns id, use pool_object to get object pointer from id
uint32_t pool_get(pool *p) {
	if (p->bufferSize == 0) {
		p->buffer = malloc(p->stride);
		p->bufferSize = 1;
		p->newStruct((uint8_t*)p->buffer+1);
	}

	uint32_t iind = p->nextOpenIndex;
	uint8_t *pb = (uint8_t*)p->buffer + iind*p->stride;
	pb[0] = 1;//set pool slot to full

	//find next open index
	pb = p->buffer;
	uint32_t obsz = p->bufferSize,
			 stride = p->stride,
			 count = 0,
			 oind = (iind+1)%obsz;
	while (count < obsz && (pb+oind*stride)[0]) {
		oind = (oind+1)%obsz;
		count++;
	}

	if (count >= obsz) {
		//no new open slot, expand buffer
		p->bufferSize *= 8;
		p->buffer = realloc(p->buffer, p->bufferSize*p->stride);

		count = p->bufferSize;
		pb = p->buffer;
		for (uint32_t i = obsz; i < count; i++) {
			(pb+i*stride)[0] = 0;//set pool slot to empty
			p->newStruct(pb+i*stride+1);//init struct
		}
		p->nextOpenIndex = obsz;
	} else {
		p->nextOpenIndex = oind;
	}

	return iind;
}

//access pool object
#define pool_object(p,i) ((uint8_t*)p->buffer+p->stride*i+1)

//return object to pool
#define pool_return(p,i) ((uint8_t*)p->buffer)[p->stride*i] = 0


#endif
