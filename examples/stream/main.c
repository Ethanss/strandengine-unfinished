/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#include "../../msvcdef.h"
#include "../../stream.h"

int main(int argc, char **argv) {
	//open text file for reading
	file_stream fs;
	if (!new_file_stream(&fs, "text.txt", FILE_MODE_READ)) {
		printf("Failed to open 'text.txt'!\n");
		return 0;
	}
	
	char *txt = malloc(fs.length);//allocate char array
	stream_read(&fs, txt, fs.length);//read text from file into char array
	printf("%.*s\n", fs.length, txt);//print text

	//close stream and free allocated char array
	file_stream_close(&fs);
	free(txt);
	return 0;
}
