outn=${PWD##*/}
if [ -n "$1" ]
then
outn="$outn""$1"" -m""$1"
fi
gcc main.c -o $outn -mmmx -msse -msse2 -msse4.1 -msse4.2 -fdce -fno-strict-aliasing -D_FILE_OFFSET_BITS=64 -Wl,--gc-sections -Wall -Wextra -Wno-format -Wno-incompatible-pointer-types -Wno-discarded-qualifiers -Wno-implicit -Wno-unused-result -Wno-unused-parameter -Wno-missing-braces -Wno-sign-compare -Wl,--no-undefined
