/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#include "../../msvcdef.h"
#include "../../rng.h"
#include "../../system.h"

int main(int argc, char **argv) {
	//new random number generator with 8-byte/64-bit seed
	rng r = new_rng(8);

	//initialize seed to current time in milliseconds
	*(uint64_t*)r.buffer = timeNow();
	
	//guess random numbers
	char nstr[32];
	while (true) {
		printf("Guess the next number(0-100), type 'quit' to stop: \n");
		
		//get input string from console
		fgets(nstr, 32, stdin);
		printf("You said %s", nstr);
		
		//check if input starts with 'q', if so then quit
		if (tolower(nstr[0]) == 'q') break;
		
		int32_t guess = atoi(nstr),//convert input to integer
			  nnum = rng_int32(&r,0,101);//generate random integer between 0-100
			  
		//compare guess to random number
		printf("The next number is %d, your guess was %d away.\n\n", nnum, abs(nnum - guess));
	}

	//free rng memory
	delete_rng(&r);
	return 0;
}
