/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#include "../../msvcdef.h"
#include "../../database.h"

int main(int argc, char **argv) {
	//example data is an array of cstrings
	char *text[4] = {"Data ", "storage ", "and ", "lookup.\n"};

	//store database in memory
	memory_stream dbs;
	new_memory_stream(&dbs, 4096);
	
	//create database for storing cstrings
	database db;
	new_database(&db, &dbs, 64);//max length of c-strings in database is 63, 64 minus NULL terminator

	//store datamap in memory
	memory_stream dms;
	new_memory_stream(&dms, 4096);
	
	//create datamap of database ids
	datamap dm;
	new_datamap(&dm, &dms, 4, 4);

	for (uint32_t i = 0; i < 4; i++) {
		//store cstrings in database
		database_seek(&db, i);
		db.data->write(db.data, text[i], strlen(text[i])+1);
		
		//store cstring database ids in hashmap
		datamap_add(&dm, cstring_hash(text[i]));
		stream_writeUniversal(dm.data, &i, 4, 4);
	}

	for (uint32_t i = 0; i < 4; i++) {
		//look up cstring database id in datamap by hash value
		uint32_t hv = cstring_hash(text[i]),
			ind = datamap_indexOf(&dm, hv),
			id;
			
		//read database id
		datamap_seek(&dm,ind);
		stream_readUniversal(dm.data,&id,4,4);
		
		//read cstring from database
		database_seek(&db,id);
		char sbuf[64];
		db.data->read(db.data,sbuf,64);
		printf(sbuf);
	}

	return 0;
}
