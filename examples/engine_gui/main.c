﻿/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

//exclude graphics assets we arent using
#define EXCLUDE_LIB_PNG
#define EXCLUDE_LIB_BMP
#define EXCLUDE_LIB_ASSIMP

//include headers, this is in a specific order
#include "../../msvcdef.h"
#include "../../engine.h"

//asset system and graphics asset types
#include "../../assets_base.h"
#include "../../graphics_assets.h"
#include "../../assets.h"

//all gui components
#include "../../components/gui/all.h"

//final engine header
#include "../../engine_launch.h"


//button event callbacks
void hoverEnter(gui_component *btn) {
	//color button black on hover
	btn->color = (vec4){0.0f,0.0f,0.0f,1.0f};
	gui_component_transformUpdate(btn);//triggers component color to update
}
void hoverLeave(gui_component *btn) {
	//color button white on exit hover
	btn->color = (vec4){1.0f,1.0f,1.0f,1.0f};
	gui_component_transformUpdate(btn);
}
void click(gui_component *btn) {
	//semi randomly move button when clicked
	object *bo = btn->base.object;
	float tf = (currentTime%100000)*0.001f;
	bo->position.x = sinf(tf*29.89723f)*0.7f;
	bo->position.y = cosf(tf*33.5834f)*0.7f;
	object_updateTransform(bo);
}

void startup() {
	stillRunning = false;
	
	//load the font used for the editor abel-regular
	asset fontAsset;
	if (!asset_loadManaged(&fontAsset,"../../editor/abel-regular.ttf",NULL)) {
		printf("Failed to load font.\n");
		return;
	}
	font *font = fontAsset.asset;
	
	//set frame/tick rate to 50hz with a time scale of 1
	setUpdateRate(50,1.0f);
	
	//create gui base
	object *gui_base_obj = create_object();
	gui *gui_base = create_gui();
	object_addComponent(gui_base_obj,gui_base);
	
	//dragview + grey background
	gui_dragview *background = create_gui_dragview();
	background->base.color = (vec4){0.2f,0.2f,0.2f,1.0f};
	object_addComponent(gui_base_obj,background);

	//add text label to background
	object *draglbl_obj = create_object();
	draglbl_obj->position.z = -0.1f;
	object_setParent(draglbl_obj,gui_base_obj);
	
	gui_text *draglbl = create_gui_text();
	draglbl->text = cstring_as_list("Click back here to drag/scroll.");
	draglbl->fontSize = 22;
	draglbl->font = font;
	draglbl->anchor = ANCHOR_CENTER;
	object_addComponent(draglbl_obj,draglbl);
	
	//create button example using interactive gui_component
	object *btn_obj = create_object();
	btn_obj->position = (vec3){0.0f,0.5f,-0.1f};
	btn_obj->scale = (vec3){0.3f,0.1f,1.0f};
	object_setParent(btn_obj,gui_base_obj);
	
	gui_component *btn = create_gui_component();
	btn->hoverEnter = hoverEnter;
	btn->hoverLeave = hoverLeave;
	btn->click = click;
	btn->interactable = true;
	object_addComponent(btn_obj,btn);
	
	//add text to button
	gui_text *btnlbl = create_gui_text();
	btnlbl->text = cstring_as_list("Click me!");
	btnlbl->fontSize = 32;
	btnlbl->font = font;
	btnlbl->anchor = ANCHOR_CENTER;
	btnlbl->color = (vec4){0.0f,0.0f,0.0f,1.0f};
	object_addComponent(btn_obj,btnlbl);
	
	//example text field
	object *field_obj = create_object();
	field_obj->position = (vec3){0.0f,-0.5f,-0.2f};
	field_obj->scale = (vec3){0.5f,0.07f,1.0f};
	object_setParent(field_obj,gui_base_obj);
	
	gui_textfield *field = create_gui_textfield();
	field->base.text = cstring_as_list("Enter text here!");
	field->base.fontSize = 32;
	field->base.font = font;
	field->base.color = (vec4){0.0f,0.0f,0.0f,1.0f};
	object_addComponent(field_obj,field);
	
	//if stillRunning isn't set to true then the engine will not start, for example if your startup/initialization fails
	stillRunning = true;
}
	
#ifdef __linux__
int main(int argc, char **argv) {//linux entry point
	parseCommandLineArguments(argc, argv);
#else
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nShowCmd) {//windows entry point
	setupWindowsConsole(false);
	parseCommandLineArguments();
#endif
	//engine launch
	engine_launch((engine_launch_options){&startup, NULL,//startup and cleanup callbacks
	//window options
	"GUI Example",//title
	500, 500,//size=500x500
	true,//border=true
	false,//fullscreen=false
	false//vsync=false
	});
	return 0;
}

