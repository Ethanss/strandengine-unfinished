/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#include "../../msvcdef.h"
#include "../../compression.h"

int main(int argc, char **argv) {
	//Compression base, higher is better for more random data. For best results try compressing with different bases and using best result.
	#define CBASE 1

	//sequence of repeating numbers
	#define SZ 1024
	uint32_t data[SZ];
	for (uint32_t i = 0; i < SZ; i++) data[i] = i%8;
	
	//compress using binary run length encoding
	uint8_t compressed[SZ*4];
	uint32_t csz = compressbinary(data,compressed,SZ*4,CBASE);
	printf("Compressed %d bytes into %d bytes, 100:%d compression ratio.\n",SZ*4,csz,csz*100/(SZ*4));
	
	//decompress
	uint32_t decompressed[SZ];
	decompressbinary(compressed,decompressed,SZ*4,CBASE);

	//compare to original
	printf(memcmp(data,decompressed,SZ*4)==0?"Lossless compression.\n":"Error decompressed data does not equal original.\n");
	
	return 0;
}
