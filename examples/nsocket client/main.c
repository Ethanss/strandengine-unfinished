/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#include "../../msvcdef.h"
#include "../../nsocket.h"

int main(int argc, char **argv) {
	//initialize nsocket system
	init_nsocket_system(
	false//multithreaded=false
	);
	
	//initialize server address to connect to
	#define PORT 9999
	inet_address serverAddr;
	new_inet_address(&serverAddr,"localhost",PORT,true,true);
	
	//connect to server
	nsocket server;
	if (!nsocket_connect(&server,&serverAddr)) {
		printf("Could not connect to local server on port %d.\n",PORT);
		return 0;
	}
	delete_inet_address(&serverAddr);//dont need inet_address anymore, so free the memory
	
	printf("Connected to server.\n");
	
	//send string msg over to server
	char *msg = "i am but a humble client";
	uint32_t msgLen = strlen(msg);
	//first we send the length of the string as a uint32(4 bytes)
	nsocket_write(&server,&msgLen,4);
	//then we send the message itself
	nsocket_write(&server,msg,msgLen);
	
	//read message length back from the server
	nsocket_read(&server,&msgLen,4);
	//now that we know how big the message is we can allocate memory for it and read it
	char *msgBuf = malloc(msgLen);
	nsocket_read(&server,msgBuf,msgLen);
	
	//display message from server
	printf("Server message: '%.*s'\n",msgLen,msgBuf);
	
	//we are done, disconnect from server
	nsocket_close(&server);
	
	//cleanup
	free_nsocket_system();
}
