/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#include "../../msvcdef.h"
#include "../../nsocket.h"

int main(int argc, char **argv) {
	//initialize nsocket system
	init_nsocket_system(
	false//multithreaded=false
	);
	
	//initialize inet_address for hosting
	#define PORT 9999
	inet_address hostAddr;
	new_inet_address(&hostAddr,
	NULL,//NULL address for host
	PORT,true,true);
	
	//create server socket
	nsocket sock;
	nsocket_init(&sock,&hostAddr);
	//bind server socket to host port
	if (!nsocket_bind(&sock,&hostAddr)) {
		printf("Could not open server socket on port %d.\n",PORT);
		return 0;
	}
	delete_inet_address(&hostAddr);//dont need inet_address anymore, so free the memory
	
	//listen for up to 1 incoming connection
	nsocket_listen(&sock,1);
	printf("Server is waiting for a connection...\n");

	//accept incoming connection and print out connecting ip address
	char caddr[INET6_ADDRSTRLEN];
	nsocket client;
	if (!nsocket_accept(&sock,&client,caddr)) {
		printf("Failed to accept connection.\n");
		return 0;
	}
	printf("Received connection from '%s'.\n", caddr);
	
	//send string msg over to client
	char *msg = "HELLO I AM A MIGHTY SERVER";
	uint32_t msgLen = strlen(msg);
	//first we send the length of the string as a uint32(4 bytes)
	nsocket_write(&client,&msgLen,4);
	//then we send the message itself
	nsocket_write(&client,msg,msgLen);
	
	//read message length back from the client
	nsocket_read(&client,&msgLen,4);
	//now that we know how big the message is we can allocate memory for it and read it
	char *msgBuf = malloc(msgLen);
	nsocket_read(&client,msgBuf,msgLen);
	
	//display message from client
	printf("Client message: '%.*s'\n",msgLen,msgBuf);
	
	//we are done, disconnect from client
	nsocket_close(&client);
	
	//cleanup
	nsocket_close(&sock);
	free_nsocket_system();
	return 0;
}
