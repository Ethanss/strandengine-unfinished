/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#include "../../msvcdef.h"
#include "../../reflection.h"
#include "../../stream.h"

//define person struct
typedef struct {
	list name;
	int32_t age;
	float height;
} person;

//define reflected fields data for person
#define REFLECT_TARGET person
#define person_fieldCount 3
const reflection_data person_fields[person_fieldCount] = {REFLECT(list,name),REFLECT(int32_t,age),REFLECT(float,height)};
#undef REFLECT_TARGET

int main(int argc, char **argv) {
	//set person data
	person p;
	p.name = cstring_as_list("John Paul");
	p.age = 50;
	p.height = 5.8f;

	//automatically serialize person using reflection data, serialization functions also handle endianness conversion
	person o;
	memory_stream ms;
	new_memory_stream(&ms, 128);
	stream_serializeFixed(&ms, &p, person_fields, person_fieldCount);
	stream_setPosition(&ms, 0);
	stream_deserializeFixed(&ms, &o, person_fields, person_fieldCount);
	memory_stream_close(&ms);

	//print out person data using reflection
	for (uint32_t i = 0; i < person_fieldCount; i++) {
		printf("%s: ", person_fields[i].name);//field name
		//field value
		uint8_t *fptr = &o;
		fptr += person_fields[i].offset;
		const uint8_t type = person_fields[i].type;
		if (type == REFLECTION_TYPE_int32_t) printf("%d\n", *(int32_t*)fptr);
		else if (type == REFLECTION_TYPE_float) printf("%g\n", *(float*)fptr);
		else printf("%.*s\n", ((list*)fptr)->length, ((list*)fptr)->buffer);
	}
	
	return 0;
}
