/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#include "../../msvcdef.h"
#include "../../hashmap.h"

int main(int argc, char **argv) {
	//example cstrings
	char *examples[4] = { "Testing ", "hashmap ", "lookup ", "example.\n" };
	//calculate string hashes
	uint32_t hashes[4];
	for (uint32_t i = 0; i < 4; i++) hashes[i] = cstring_hash(examples[i]);//look at 'hash' function in misc.h for more info

	//create hashmap 'm' of cstring pointers
	hashmap m = construct_hashmap(sizeof(void*), 2);

	//fill hashmap with cstrings and their hashes
	for (uint32_t i = 0; i < 4; i++) hashmap_add(&m, hashes[i], &examples[i]);

	//lookup cstrings using hashes and print
	for (uint32_t i = 0; i < 4; i++) {
		char *str = *(char**)hashmap_get(&m, hashes[i]);
		printf(str);
	}

	//free hashmap memory
	delete_hashmap(&m);
	return 0;
}
