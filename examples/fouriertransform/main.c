/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#include "../../msvcdef.h"
#include "../../fouriertransform.h"
#include "../../cosinetransform.h"

void printFloats(float *f, uint32_t n) {
	for (uint32_t i = 0; i < n; i++) printf(i==0?"%.2g":", %.2g",*f++);
}

int main(int argc, char **argv) {
	//fill float data with sin wave
	#define N 8
	float data[N];
	for (uint32_t i = 0; i < N; i++) data[i] = sinf(i*M_PI/(float)(N-1));
	
	//print data
	printf("Sine wave data: ");
	printFloats(data,N);
	printf("\n\n");
	
	
	//fast cosine transform
	float sdat[N],rdat[N];
	cosinetransform(data,sdat,N,false);
	printf("Fast cosine transform forward result: ");
	printFloats(sdat,N);
	printf("\n");
	
	cosinetransform(sdat,rdat,N,true);
	printf("Fast cosine transform inverse result: ");
	printFloats(rdat,N);
	printf("\n\n");
	
	
	//create new fast fouriertransform class
	fouriertransform fft;
	new_fouriertransform(&fft,N);

	//convert data to complex numbers for fourier transform
	float cdat[N*2], *fp = cdat;
	for (uint32_t i = 0; i < N; i++) {
		*fp++ = data[i];
		*fp++ = 0.0f;
	}
	
	//run fouriertransform on data and print complex frequency spectrum
	fouriertransformation(&fft,cdat,cdat,false);
	printf("Fourier transform forward result: ");
	printFloats(cdat,N*2);
	printf("\n");
	
	//run inverse and convert back to real space data
	fouriertransformation(&fft,cdat,cdat,true);
	printf("Fourier transform inverse result: ");
	printFloats(cdat,N*2);
	printf("\n");
	
	return 0;
}
