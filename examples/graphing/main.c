/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#include "../../msvcdef.h"
#include "../../stream.h"
#include "../../graphing.h"
#include "../../bmphelper.h"

int main(int argc, char **argv) {
	//create new 500x500 graphimage, our canvas for rendering our graph
	graphimage gi;
	new_graphimage(&gi,500,500);
	
	//clear white background
	graphimage_clear(&gi,(vec4){1.0f,1.0f,1.0f,1.0f});
	
	//draw black grid lines
	graphimage_grid(&gi, (vec2){0.0f,0.0f}, (vec2){gi.width,gi.height}, 2.0f, (vec4){0.0f,0.0f,0.0f,1.0f}, 11, 11);
	
	//draw bar graph of sine wave
	const float BAR_SIZE = 25.0f, BAR_SIZE2 = BAR_SIZE*0.5f;
	for (float x = 0.0f; x < gi.width; x += BAR_SIZE) {
		float v = (sinf(x*0.02f)*0.5f+0.5f)*gi.height*0.5f;
		graphimage_box(&gi, (vec2){x+BAR_SIZE2,v}, (vec2){BAR_SIZE2,v}, 0.0f, (vec4){1.0f,0.1f,0.1f,0.9f});
	}
	
	//draw label
	const list lbl = cstring_as_list("Label");
	graphimage_text(&gi, &lbl, (vec2){20.0f,20.0f}, 15.0f, 0.05f, 1.4f, (vec4){0.0f,0.0f,0.0f,1.0f});
	
	//save graph to bitmap image file
	FILE *fp = fopen("graph.bmp",FILE_MODE_WRITE);
	save_bmp(fp,gi.data,gi.width,gi.height,3);
	fclose(fp);
	
	printf("Saved to 'graph.bmp'.\n");
	return 0;
}
