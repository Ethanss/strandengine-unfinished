/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#include "../../msvcdef.h"
#include "../../misc.h"

int main(int argc, char **argv) {
	//prints out which cpu features are supported by checking cpu_id bit flags
	printf("CPU Feature Support:\n");
	#define ps(f) printf("%s %s\n",#f,CPU_SUPPORTS_##f()?"available":"not supported")
	ps(MMX);
	ps(SSE);
	ps(SSE2);
	ps(SSE3);
	ps(FMA);
	ps(DCA);
	ps(SSE4_1);
	ps(SSE4_2);
	ps(MOVBE);
	ps(AES);
	ps(OSXSAVE);
	ps(AVX);
	ps(RDRAND);
	ps(AVX2);
	ps(AVX512_F);
	ps(AVX512_DQ);
	ps(RDSEED);
	ps(AVX512_IFMA);
	ps(AVX512_PF);
	ps(AVX512_ER);
	ps(AVX512_CD);
	ps(SHA);
	ps(AVX512_BW);
	ps(AVX512_VL);
	ps(AVX512_VBMI);
	ps(AVX512_VBMI2);
	ps(AVX512_VNNI);
	ps(AVX512_BITALG);
	ps(AVX512_VPOPCNTDQ);
	ps(AVX512_4VNNIW);
	ps(AVX512_4FMAPS);
	return 0;
}
