/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#include "../../msvcdef.h"
#include "../../list.h"

int main(int argc, char **argv) {
	//create list of characters 'l'
	list l;
	new_list(&l, 1);//1 = sizeof ASCII char 

	//immutable list backed by cstring 'hws'
	list hws = cstring_as_list("Hello world!");//for more info look at the function code of 'cstring_as_list' and 'list_setBuffer' in list.h
	list_addm(&l, hws.buffer, hws.length);//add 'hws' to 'l'

	//print cstring, convert list using list_as_cstring
	printf("%s\n", list_as_cstring(&l));//add null-terminating char to 'l'
	list_clear_cstring(&l);//remove null char

	//replace 'hs' with 'bs' in 'l'
	char *hs = "Hello", *bs = "Bye";
	list_replace(&l, hs, strlen(hs), bs, strlen(bs));

	//print string using length instead of converting to cstring
	printf("%.*s\n", l.length, (char*)l.buffer);

	//free list memory
	delete_list(&l);
	return 0;
}
