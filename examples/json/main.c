/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#include "../../msvcdef.h"
#include "../../json.h"
#include "../../stream.h"

int main(int argc, char **argv) {	
	//load example.json text from file
	uint64_t fsz;
	char *fdata = file_readall("example.json", &fsz);
	list txt;
	list_setBuffer(&txt, fdata, fsz);
	
	//parse json
	json_object *jso = json_parse(&txt).objectv;
	free(fdata);

	//print json object data
	char *typeNames[5] = {"object","string","array","int","float"};
	list *keys = jso->keys.buffer;
	json *vals = jso->values.buffer;
	const uint32_t len = jso->keys.length;
	for (uint32_t i = 0; i < len; i++) {
		//field name and type
		printf("Name: %.*s, Type: %s, Value: ", keys->length, (char*)keys->buffer, typeNames[vals->type]);
		
		//print int, float or string value
		if (vals->type == JSON_TYPE_INT) printf("%d\n", vals->intv);
		else if (vals->type == JSON_TYPE_FLOAT) printf("%g\n", vals->floatv);
		else if (vals->type == JSON_TYPE_STRING) {
			list *sd = vals->data;
			printf("%.*s\n", sd->length, (char*)sd->buffer);
		}
		else printf("\n");
		
		keys++;
		vals++;
	}

	delete_json_object(jso);
	free(jso);
	return 0;
}
