/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#include "../../msvcdef.h"
#include "../../http.h"

int main(int argc, char **argv) {
	//http requires nsocket system
	init_nsocket_system(false);

	//allocate memory needed
	char url[512];
	list data = construct_list(1);
	
	//main loop
	while (true) {
		//get URL from console input
		printf("Enter file URL to HTTP GET:\n");
		fgets(url, 512, stdin);
		
		//http 1.1 request, storing result in data list
		data.length = 0;
		int16_t rc = http_request_url(url,NULL,NULL,&data);
		printf("Response code: %d.\n",rc);
		
		//print data to console as string if it exists
		if (data.length != 0) {
			printf("Data: '%.*s'\n\n",data.length,data.buffer);
		}
	}
	
	return 0;
}
