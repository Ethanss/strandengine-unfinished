﻿/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _WEBSOCKET_H_
#define _WEBSOCKET_H_

#include "nsocket.h"
#include "sha1.h"
#include "base64.h"

//process websocket http handshake, returns true if successful
bool websocket_open(nsocket *s) {
	uint8_t header[1024];
	int32_t rsz = nsocket_read(s,header,1024);
	if (rsz < 20) return false;
	
	cstring(keyName,"Sec-WebSocket-Key");
	int32_t keyInd = memIndexOf(header,1,rsz,keyName,sizeofkeyName);
	if (keyInd == -1) return false;
	const char *nl = "\r\n";
	keyInd += sizeofkeyName+2;
	uint8_t *keyp = header+keyInd;
	int32_t endKeyInd = memIndexOf(keyp,1,rsz-keyInd,nl,2);
	if (endKeyInd == -1 || endKeyInd > 64) return false;
	
	uint8_t key[128];
	memcpy(key,keyp,endKeyInd);
	cstring(secret,"258EAFA5-E914-47DA-95CA-C5AB0DC85B11");
	memcpy(key+endKeyInd,secret,sizeofsecret);
	endKeyInd += sizeofsecret;
	
	uint8_t hash[20];
	SHA1(hash,key,endKeyInd);
	
	cstring(resBase,"HTTP/1.1 101 Switching Protocols\r\nUpgrade: websocket\r\nConnection: Upgrade\r\nSec-WebSocket-Accept: ");
	uint32_t resSz = sizeofresBase;
	memcpy(header,resBase,resSz);
	resSz += Base64encode(header+resSz,hash,20);
	memcpy(header+resSz,nl,2);
	resSz += 2;
	memcpy(header+resSz,nl,2);
	resSz += 2;
	if (nsocket_write(s,header,resSz) <= 0) return false;
	
	return true;
}

//send websocket binary data frame, max sz of 65535, returns true on success
bool websocket_write(nsocket *s, uint8_t *dat, const uint16_t sz) {
	uint8_t frame[16];
	uint32_t fcount;
	frame[0] = 128|2;
	if (sz < 126) {
		frame[1] = sz;
		fcount = 2;
	} else {
		frame[1] = 126;
		frame[2] = (sz>>8)&255;
		frame[3] = sz&255;
		fcount = 4;
	}
	if (nsocket_write(s,frame,fcount) <= 0) return false;
	return nsocket_write(s,dat,sz) > 0;
}

//receive websocket data frame, returns size or -1 if failure, buf must be at least 128 for processing ping/pong frames
int32_t websocket_read(nsocket *s, uint8_t *buf, const uint16_t maxSz) {
	uint32_t tsz = 0;
	while (true) {
		if (nsocket_read(s,buf,4) < 4) return -1;
		bool ping = false, continuation = false;
		if (buf[0]&8) {
			if (buf[0]&9) {
				buf[0] = (buf[0]&0xF0)|0xA;
				if (nsocket_write(s,buf,4) <= 0) return -1;
				ping = true;
			} else {
				if (nsocket_write(s,buf,4) <= 0) return -1;
				nsocket_close(s);
				return -1;
			}
		} else continuation = (buf[0]&128)?false:true;
		
		int32_t rsz;
		uint8_t mask[4];
		buf[1] &= 0x7F;
		if (buf[1] > 125) {
			rsz = (buf[2]<<8)|buf[3];
			if (nsocket_read(s,mask,4) < 4) return -1;
			if (ping) {
				if (nsocket_write(s,mask,4) <= 0) return -1;
			}
		} else {
			rsz = buf[1];
			mask[0] = buf[2];
			mask[1] = buf[3];
			if (nsocket_read(s,mask+2,2) < 2) return -1;
			if (ping) {
				if (nsocket_write(s,mask+2,2) <= 0) return -1;
			}
		}
		if (!ping && tsz+rsz > maxSz) return -1;
		if (!nsocket_readf(s,buf,rsz)) return -1;
		if (ping) nsocket_write(s,buf,rsz);
		else {
			for (uint32_t i = 0; i < rsz; i++) buf[i] ^= mask[i%4];
			tsz += rsz;
			buf += rsz;
			if (!continuation) break;
		}
	}
	return tsz;
}

#endif
