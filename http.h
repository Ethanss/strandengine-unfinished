﻿/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef _HTTP_H_
#define _HTTP_H_

#include "nsocket.h"

//http get/post, returns status code and outputs response to res
int16_t http_request(char *host, char *file, char *userAgent, list *postData, list *res) {
	inet_address haddr;
	if (!new_inet_address(&haddr,host,80,true,true)) return 404;

	nsocket sock;
	const bool sockCreateRes = nsocket_connect(&sock,&haddr);
	delete_inet_address(&haddr);
	if (!sockCreateRes) return 404;

	char *getHttp1 = "GET /", *postHttp1 = "POST /", *getHttp2 = " HTTP/1.1\r\nConnection: close\r\nHost: ", *getHttp3 = "\r\n\r\n", *getHttp4 = "\r\nUser-Agent: ",
	*postHttp2 = "\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: ";

	if (postData == NULL) nsocket_write(&sock,getHttp1,5);
	else nsocket_write(&sock,postHttp1,6);
	nsocket_write(&sock,file,strlen(file));
	nsocket_write(&sock,getHttp2, 36);
	nsocket_write(&sock,host,strlen(host));
	if (userAgent != NULL) {
		nsocket_write(&sock,getHttp4,14);
		nsocket_write(&sock,userAgent,strlen(userAgent));
	}
	if (postData != NULL) {
		nsocket_write(&sock,postHttp2,67);
		uint8_t pdlb[32];
		uint32_t pdl = snprintf((void*)pdlb,32,"%d",postData->length);
		nsocket_write(&sock,pdlb,pdl);
		nsocket_write(&sock,getHttp3,4);
		nsocket_write(&sock,postData->buffer,postData->length);
	} else {
		nsocket_write(&sock,getHttp3,4);
	}

	res->length = 64;
	list_expand(res);

	#define hter(c) if (c) {nsocket_close(&sock);return 400;}

	int32_t rl = 0;
	while (rl < 12) {
		int32_t nr = nsocket_read(&sock,(char*)res->buffer+rl,64-rl);
		hter(nr<1);
		rl += nr;
	}
	res->length = rl;

	char spaceChar = ' ';
	int32_t spaceIndex = list_indexOf(res,&spaceChar,1);
	hter(spaceIndex==-1);
	uint16_t statusCode = strtol((char*)res->buffer+spaceIndex,NULL,10);
	if (statusCode != 200) return statusCode;

	res->length = 4000;
	list_expand(res);
	res->length = rl;
	char *contentStr = "Content-Length", *transferStr = "Transfer-Encoding";
	uint32_t contentStrLen = strlen(contentStr), transferStrLen = strlen(transferStr);
	int32_t transferIndex;
	while ((spaceIndex = list_indexOf(res,contentStr,contentStrLen)) == -1 && (transferIndex = list_indexOf(res,transferStr,transferStrLen)) == -1) {
		int32_t nr = nsocket_read(&sock,(char*)res->buffer+rl,4096-rl);
		if (nr < 1) break;
		rl += nr;
		res->length = rl;
	}

	if (spaceIndex != -1) {
		//content length
		uint32_t contentLength = strtol((char*)res->buffer+spaceIndex+15,NULL,10);

		int32_t nextSpaceIndex = list_indexOfI(res,getHttp3,4,spaceIndex+15);
		hter(nextSpaceIndex<1);

		nextSpaceIndex += 4;
		rl -= nextSpaceIndex;
		memcpy(res->buffer,(char*)res->buffer+nextSpaceIndex,rl);

		res->length = contentLength;
		list_expand(res);

		while ((uint32_t)rl < contentLength) {
			int32_t nr = nsocket_read(&sock,(char*)res->buffer+rl,contentLength-rl);
			hter(nr<1);
			rl += nr;
		}
	} else if (transferIndex != -1) {
		//transfer in chunks
		int32_t nextSpaceIndex = list_indexOfI(res,getHttp3,4,transferIndex+17);
		hter(nextSpaceIndex<1);

		res->length = 0;
		uint32_t rl = 0;

		char bbuf[32];
		while (true) {
			uint32_t ic = 0;
			while (true) {
				int32_t rc = nsocket_read(&sock,&bbuf[ic],32-ic);
				hter(rc<1);
				ic += rc;
				char lbc = bbuf[ic-1];
				if (lbc == ';' || lbc == '\r') break;
			}
			bbuf[ic-1] = 0;
			uint32_t csz = strtol(bbuf,NULL,16);
			if (csz == 0) break;
			res->length += csz;
			list_expand(res);

			ic = 0;
			while (ic < csz) {
				int32_t rc = nsocket_read(&sock,(char*)res->buffer+rl,csz-ic);
				hter(rc<1);
				rl += rc;
				ic += rc;
			}
		}
	} else {
		//unknown content length
		int32_t nextSpaceIndex = list_indexOf(res,getHttp3,4);
		hter(nextSpaceIndex<1);

		nextSpaceIndex += 4;
		rl -= nextSpaceIndex;
		memcpy(res->buffer,(char*)res->buffer+nextSpaceIndex,rl);

		while (true) {
			res->length = rl+4096;
			list_expand(res);

			int32_t nr = nsocket_read(&sock,(char*)res->buffer+rl,4096);
			if (nr < 1) break;

			rl += nr;
		}

		res->length = rl;
	}
	#undef hter

	nsocket_close(&sock);
	return statusCode;
}

//same as above except parses host/file from url automatically, url buffer must be writeable
int16_t http_request_url(char *url, char *userAgent, list *postData, list *res) {
	char slash = '/';
	const uint32_t ulen = strlen(url);
	if (ulen == 0) return 404;
	if (url[ulen-1] == '\n') url[ulen-1] = '\0';
	int32_t ind = memIndexOf(url,1,ulen,&slash,1);
	if (ind == -1) return 404;
	url[ind] = '\0';
	return http_request(url,url+ind+1,userAgent,postData,res);
}

#endif
